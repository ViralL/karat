var mailOwl;
var mobileMode;

function GetSelector($this) {
    var selector = $this
            .parents()
            .map(function() { return this.tagName; })
            .get()
            .reverse()
            .concat([this.nodeName])
            .join(">");

    var id = $this.attr("id");
    if (id) { 
        selector += "#"+ id;
    }

    var classNames = $this.attr("class");
    if (classNames) {
        selector += "." + $.trim(classNames).replace(/\s/gi, ".");
    }

    return selector;
}

function collection() {
    $(".collect-wrap-more").click(function() {
        $(".tog-hid").fadeIn().removeClass("tog-hid")
    }),
    $(".coll .wrap").css({
        height: $(".coll .wrap:first").width() + "px"
    }),
    $(".coll .w-m-40 .wrap").css({
        height: $(".coll .w-m-40 .wrap:first").width() + "px"
    })
}
if (function(e, t) {
    function i(e) {
        var t = e.length
          , i = ue.type(e);
        return !ue.isWindow(e) && (!(1 !== e.nodeType || !t) || ("array" === i || "function" !== i && (0 === t || "number" == typeof t && t > 0 && t - 1 in e)))
    }
    function n(e) {
        var t = we[e] = {};
        return ue.each(e.match(pe) || [], function(e, i) {
            t[i] = !0
        }),
        t
    }
    function o(e, i, n, o) {
        if (ue.acceptData(e)) {
            var s, r, a = ue.expando, l = e.nodeType, c = l ? ue.cache : e, u = l ? e[a] : e[a] && a;
            if (u && c[u] && (o || c[u].data) || n !== t || "string" != typeof i)
                return u || (u = l ? e[a] = te.pop() || ue.guid++ : a),
                c[u] || (c[u] = l ? {} : {
                    toJSON: ue.noop
                }),
                ("object" == typeof i || "function" == typeof i) && (o ? c[u] = ue.extend(c[u], i) : c[u].data = ue.extend(c[u].data, i)),
                r = c[u],
                o || (r.data || (r.data = {}),
                r = r.data),
                n !== t && (r[ue.camelCase(i)] = n),
                "string" == typeof i ? null == (s = r[i]) && (s = r[ue.camelCase(i)]) : s = r,
                s
        }
    }
    function s(e, t, i) {
        if (ue.acceptData(e)) {
            var n, o, s = e.nodeType, r = s ? ue.cache : e, l = s ? e[ue.expando] : ue.expando;
            if (r[l]) {
                if (t && (n = i ? r[l] : r[l].data)) {
                    ue.isArray(t) ? t = t.concat(ue.map(t, ue.camelCase)) : t in n ? t = [t] : (t = ue.camelCase(t),
                    t = t in n ? [t] : t.split(" ")),
                    o = t.length;
                    for (; o--; )
                        delete n[t[o]];
                    if (i ? !a(n) : !ue.isEmptyObject(n))
                        return
                }
                (i || (delete r[l].data,
                a(r[l]))) && (s ? ue.cleanData([e], !0) : ue.support.deleteExpando || r != r.window ? delete r[l] : r[l] = null)
            }
        }
    }
    function r(e, i, n) {
        if (n === t && 1 === e.nodeType) {
            var o = "data-" + i.replace(xe, "-$1").toLowerCase();
            if ("string" == typeof (n = e.getAttribute(o))) {
                try {
                    n = "true" === n || "false" !== n && ("null" === n ? null : +n + "" === n ? +n : be.test(n) ? ue.parseJSON(n) : n)
                } catch (e) {}
                ue.data(e, i, n)
            } else
                n = t
        }
        return n
    }
    function a(e) {
        var t;
        for (t in e)
            if (("data" !== t || !ue.isEmptyObject(e[t])) && "toJSON" !== t)
                return !1;
        return !0
    }
    function l() {
        return !0
    }
    function c() {
        return !1
    }
    function u() {
        try {
            return G.activeElement
        } catch (e) {}
    }
    function d(e, t) {
        do {
            e = e[t]
        } while (e && 1 !== e.nodeType);return e
    }
    function p(e, t, i) {
        if (ue.isFunction(t))
            return ue.grep(e, function(e, n) {
                return !!t.call(e, n, e) !== i
            });
        if (t.nodeType)
            return ue.grep(e, function(e) {
                return e === t !== i
            });
        if ("string" == typeof t) {
            if (He.test(t))
                return ue.filter(t, e, i);
            t = ue.filter(t, e)
        }
        return ue.grep(e, function(e) {
            return ue.inArray(e, t) >= 0 !== i
        })
    }
    function h(e) {
        var t = Ie.split("|")
          , i = e.createDocumentFragment();
        if (i.createElement)
            for (; t.length; )
                i.createElement(t.pop());
        return i
    }
    function f(e, t) {
        return ue.nodeName(e, "table") && ue.nodeName(1 === t.nodeType ? t : t.firstChild, "tr") ? e.getElementsByTagName("tbody")[0] || e.appendChild(e.ownerDocument.createElement("tbody")) : e
    }
    function m(e) {
        return e.type = (null !== ue.find.attr(e, "type")) + "/" + e.type,
        e
    }
    function g(e) {
        var t = Ge.exec(e.type);
        return t ? e.type = t[1] : e.removeAttribute("type"),
        e
    }
    function v(e, t) {
        for (var i, n = 0; null != (i = e[n]); n++)
            ue._data(i, "globalEval", !t || ue._data(t[n], "globalEval"))
    }
    function y(e, t) {
        if (1 === t.nodeType && ue.hasData(e)) {
            var i, n, o, s = ue._data(e), r = ue._data(t, s), a = s.events;
            if (a) {
                delete r.handle,
                r.events = {};
                for (i in a)
                    for (n = 0,
                    o = a[i].length; o > n; n++)
                        ue.event.add(t, i, a[i][n])
            }
            r.data && (r.data = ue.extend({}, r.data))
        }
    }
    function w(e, t) {
        var i, n, o;
        if (1 === t.nodeType) {
            if (i = t.nodeName.toLowerCase(),
            !ue.support.noCloneEvent && t[ue.expando]) {
                o = ue._data(t);
                for (n in o.events)
                    ue.removeEvent(t, n, o.handle);
                t.removeAttribute(ue.expando)
            }
            "script" === i && t.text !== e.text ? (m(t).text = e.text,
            g(t)) : "object" === i ? (t.parentNode && (t.outerHTML = e.outerHTML),
            ue.support.html5Clone && e.innerHTML && !ue.trim(t.innerHTML) && (t.innerHTML = e.innerHTML)) : "input" === i && Ye.test(e.type) ? (t.defaultChecked = t.checked = e.checked,
            t.value !== e.value && (t.value = e.value)) : "option" === i ? t.defaultSelected = t.selected = e.defaultSelected : ("input" === i || "textarea" === i) && (t.defaultValue = e.defaultValue)
        }
    }
    function b(e, i) {
        var n, o, s = 0, r = typeof e.getElementsByTagName !== V ? e.getElementsByTagName(i || "*") : typeof e.querySelectorAll !== V ? e.querySelectorAll(i || "*") : t;
        if (!r)
            for (r = [],
            n = e.childNodes || e; null != (o = n[s]); s++)
                !i || ue.nodeName(o, i) ? r.push(o) : ue.merge(r, b(o, i));
        return i === t || i && ue.nodeName(e, i) ? ue.merge([e], r) : r
    }
    function x(e) {
        Ye.test(e.type) && (e.defaultChecked = e.checked)
    }
    function S(e, t) {
        if (t in e)
            return t;
        for (var i = t.charAt(0).toUpperCase() + t.slice(1), n = t, o = mt.length; o--; )
            if ((t = mt[o] + i)in e)
                return t;
        return n
    }
    function C(e, t) {
        return e = t || e,
        "none" === ue.css(e, "display") || !ue.contains(e.ownerDocument, e)
    }
    function k(e, t) {
        for (var i, n, o, s = [], r = 0, a = e.length; a > r; r++)
            n = e[r],
            n.style && (s[r] = ue._data(n, "olddisplay"),
            i = n.style.display,
            t ? (s[r] || "none" !== i || (n.style.display = ""),
            "" === n.style.display && C(n) && (s[r] = ue._data(n, "olddisplay", E(n.nodeName)))) : s[r] || (o = C(n),
            (i && "none" !== i || !o) && ue._data(n, "olddisplay", o ? i : ue.css(n, "display"))));
        for (r = 0; a > r; r++)
            n = e[r],
            n.style && (t && "none" !== n.style.display && "" !== n.style.display || (n.style.display = t ? s[r] || "" : "none"));
        return e
    }
    function T(e, t, i) {
        var n = lt.exec(t);
        return n ? Math.max(0, n[1] - (i || 0)) + (n[2] || "px") : t
    }
    function _(e, t, i, n, o) {
        for (var s = i === (n ? "border" : "content") ? 4 : "width" === t ? 1 : 0, r = 0; 4 > s; s += 2)
            "margin" === i && (r += ue.css(e, i + ft[s], !0, o)),
            n ? ("content" === i && (r -= ue.css(e, "padding" + ft[s], !0, o)),
            "margin" !== i && (r -= ue.css(e, "border" + ft[s] + "Width", !0, o))) : (r += ue.css(e, "padding" + ft[s], !0, o),
            "padding" !== i && (r += ue.css(e, "border" + ft[s] + "Width", !0, o)));
        return r
    }
    function $(e, t, i) {
        var n = !0
          , o = "width" === t ? e.offsetWidth : e.offsetHeight
          , s = tt(e)
          , r = ue.support.boxSizing && "border-box" === ue.css(e, "boxSizing", !1, s);
        if (0 >= o || null == o) {
            if (o = it(e, t, s),
            (0 > o || null == o) && (o = e.style[t]),
            ct.test(o))
                return o;
            n = r && (ue.support.boxSizingReliable || o === e.style[t]),
            o = parseFloat(o) || 0
        }
        return o + _(e, t, i || (r ? "border" : "content"), n, s) + "px"
    }
    function E(e) {
        var t = G
          , i = dt[e];
        return i || (i = D(e, t),
        "none" !== i && i || (et = (et || ue("<iframe frameborder='0' width='0' height='0'/>").css("cssText", "display:block !important")).appendTo(t.documentElement),
        t = (et[0].contentWindow || et[0].contentDocument).document,
        t.write("<!doctype html><html><body>"),
        t.close(),
        i = D(e, t),
        et.detach()),
        dt[e] = i),
        i
    }
    function D(e, t) {
        var i = ue(t.createElement(e)).appendTo(t.body)
          , n = ue.css(i[0], "display");
        return i.remove(),
        n
    }
    function A(e, t, i, n) {
        var o;
        if (ue.isArray(t))
            ue.each(t, function(t, o) {
                i || gt.test(e) ? n(e, o) : A(e + "[" + ("object" == typeof o ? t : "") + "]", o, i, n)
            });
        else if (i || "object" !== ue.type(t))
            n(e, t);
        else
            for (o in t)
                A(e + "[" + o + "]", t[o], i, n)
    }
    function M(e) {
        return function(t, i) {
            "string" != typeof t && (i = t,
            t = "*");
            var n, o = 0, s = t.toLowerCase().match(pe) || [];
            if (ue.isFunction(i))
                for (; n = s[o++]; )
                    "+" === n[0] ? (n = n.slice(1) || "*",
                    (e[n] = e[n] || []).unshift(i)) : (e[n] = e[n] || []).push(i)
        }
    }
    function N(e, i, n, o) {
        function s(l) {
            var c;
            return r[l] = !0,
            ue.each(e[l] || [], function(e, l) {
                var u = l(i, n, o);
                return "string" != typeof u || a || r[u] ? a ? !(c = u) : t : (i.dataTypes.unshift(u),
                s(u),
                !1)
            }),
            c
        }
        var r = {}
          , a = e === At;
        return s(i.dataTypes[0]) || !r["*"] && s("*")
    }
    function O(e, i) {
        var n, o, s = ue.ajaxSettings.flatOptions || {};
        for (o in i)
            i[o] !== t && ((s[o] ? e : n || (n = {}))[o] = i[o]);
        return n && ue.extend(!0, e, n),
        e
    }
    function L(e, i, n) {
        for (var o, s, r, a, l = e.contents, c = e.dataTypes; "*" === c[0]; )
            c.shift(),
            s === t && (s = e.mimeType || i.getResponseHeader("Content-Type"));
        if (s)
            for (a in l)
                if (l[a] && l[a].test(s)) {
                    c.unshift(a);
                    break
                }
        if (c[0]in n)
            r = c[0];
        else {
            for (a in n) {
                if (!c[0] || e.converters[a + " " + c[0]]) {
                    r = a;
                    break
                }
                o || (o = a)
            }
            r = r || o
        }
        return r ? (r !== c[0] && c.unshift(r),
        n[r]) : t
    }
    function H(e, t, i, n) {
        var o, s, r, a, l, c = {}, u = e.dataTypes.slice();
        if (u[1])
            for (r in e.converters)
                c[r.toLowerCase()] = e.converters[r];
        for (s = u.shift(); s; )
            if (e.responseFields[s] && (i[e.responseFields[s]] = t),
            !l && n && e.dataFilter && (t = e.dataFilter(t, e.dataType)),
            l = s,
            s = u.shift())
                if ("*" === s)
                    s = l;
                else if ("*" !== l && l !== s) {
                    if (!(r = c[l + " " + s] || c["* " + s]))
                        for (o in c)
                            if (a = o.split(" "),
                            a[1] === s && (r = c[l + " " + a[0]] || c["* " + a[0]])) {
                                !0 === r ? r = c[o] : !0 !== c[o] && (s = a[0],
                                u.unshift(a[1]));
                                break
                            }
                    if (!0 !== r)
                        if (r && e.throws)
                            t = r(t);
                        else
                            try {
                                t = r(t)
                            } catch (e) {
                                return {
                                    state: "parsererror",
                                    error: r ? e : "No conversion from " + l + " to " + s
                                }
                            }
                }
        return {
            state: "success",
            data: t
        }
    }
    function j() {
        try {
            return new e.XMLHttpRequest
        } catch (e) {}
    }
    function B() {
        try {
            return new e.ActiveXObject("Microsoft.XMLHTTP")
        } catch (e) {}
    }
    function P() {
        return setTimeout(function() {
            Pt = t
        }),
        Pt = ue.now()
    }
    function I(e, t, i) {
        for (var n, o = (Ft[t] || []).concat(Ft["*"]), s = 0, r = o.length; r > s; s++)
            if (n = o[s].call(i, t, e))
                return n
    }
    function z(e, t, i) {
        var n, o, s = 0, r = qt.length, a = ue.Deferred().always(function() {
            delete l.elem
        }), l = function() {
            if (o)
                return !1;
            for (var t = Pt || P(), i = Math.max(0, c.startTime + c.duration - t), n = i / c.duration || 0, s = 1 - n, r = 0, l = c.tweens.length; l > r; r++)
                c.tweens[r].run(s);
            return a.notifyWith(e, [c, s, i]),
            1 > s && l ? i : (a.resolveWith(e, [c]),
            !1)
        }, c = a.promise({
            elem: e,
            props: ue.extend({}, t),
            opts: ue.extend(!0, {
                specialEasing: {}
            }, i),
            originalProperties: t,
            originalOptions: i,
            startTime: Pt || P(),
            duration: i.duration,
            tweens: [],
            createTween: function(t, i) {
                var n = ue.Tween(e, c.opts, t, i, c.opts.specialEasing[t] || c.opts.easing);
                return c.tweens.push(n),
                n
            },
            stop: function(t) {
                var i = 0
                  , n = t ? c.tweens.length : 0;
                if (o)
                    return this;
                for (o = !0; n > i; i++)
                    c.tweens[i].run(1);
                return t ? a.resolveWith(e, [c, t]) : a.rejectWith(e, [c, t]),
                this
            }
        }), u = c.props;
        for (W(u, c.opts.specialEasing); r > s; s++)
            if (n = qt[s].call(c, e, u, c.opts))
                return n;
        return ue.map(u, I, c),
        ue.isFunction(c.opts.start) && c.opts.start.call(e, c),
        ue.fx.timer(ue.extend(l, {
            elem: e,
            anim: c,
            queue: c.opts.queue
        })),
        c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always)
    }
    function W(e, t) {
        var i, n, o, s, r;
        for (i in e)
            if (n = ue.camelCase(i),
            o = t[n],
            s = e[i],
            ue.isArray(s) && (o = s[1],
            s = e[i] = s[0]),
            i !== n && (e[n] = s,
            delete e[i]),
            (r = ue.cssHooks[n]) && "expand"in r) {
                s = r.expand(s),
                delete e[n];
                for (i in s)
                    i in e || (e[i] = s[i],
                    t[i] = o)
            } else
                t[n] = o
    }
    function R(e, t, i) {
        var n, o, s, r, a, l, c = this, u = {}, d = e.style, p = e.nodeType && C(e), h = ue._data(e, "fxshow");
        i.queue || (a = ue._queueHooks(e, "fx"),
        null == a.unqueued && (a.unqueued = 0,
        l = a.empty.fire,
        a.empty.fire = function() {
            a.unqueued || l()
        }
        ),
        a.unqueued++,
        c.always(function() {
            c.always(function() {
                a.unqueued--,
                ue.queue(e, "fx").length || a.empty.fire()
            })
        })),
        1 === e.nodeType && ("height"in t || "width"in t) && (i.overflow = [d.overflow, d.overflowX, d.overflowY],
        "inline" === ue.css(e, "display") && "none" === ue.css(e, "float") && (ue.support.inlineBlockNeedsLayout && "inline" !== E(e.nodeName) ? d.zoom = 1 : d.display = "inline-block")),
        i.overflow && (d.overflow = "hidden",
        ue.support.shrinkWrapBlocks || c.always(function() {
            d.overflow = i.overflow[0],
            d.overflowX = i.overflow[1],
            d.overflowY = i.overflow[2]
        }));
        for (n in t)
            if (o = t[n],
            zt.exec(o)) {
                if (delete t[n],
                s = s || "toggle" === o,
                o === (p ? "hide" : "show"))
                    continue;
                u[n] = h && h[n] || ue.style(e, n)
            }
        if (!ue.isEmptyObject(u)) {
            h ? "hidden"in h && (p = h.hidden) : h = ue._data(e, "fxshow", {}),
            s && (h.hidden = !p),
            p ? ue(e).show() : c.done(function() {
                ue(e).hide()
            }),
            c.done(function() {
                var t;
                ue._removeData(e, "fxshow");
                for (t in u)
                    ue.style(e, t, u[t])
            });
            for (n in u)
                r = I(p ? h[n] : 0, n, c),
                n in h || (h[n] = r.start,
                p && (r.end = r.start,
                r.start = "width" === n || "height" === n ? 1 : 0))
        }
    }
    function q(e, t, i, n, o) {
        return new q.prototype.init(e,t,i,n,o)
    }
    function F(e, t) {
        var i, n = {
            height: e
        }, o = 0;
        for (t = t ? 1 : 0; 4 > o; o += 2 - t)
            i = ft[o],
            n["margin" + i] = n["padding" + i] = e;
        return t && (n.opacity = n.width = e),
        n
    }
    function X(e) {
        return ue.isWindow(e) ? e : 9 === e.nodeType && (e.defaultView || e.parentWindow)
    }
    var U, Y, V = typeof t, Q = e.location, G = e.document, J = G.documentElement, K = e.jQuery, Z = e.$, ee = {}, te = [], ie = "1.10.2", ne = te.concat, oe = te.push, se = te.slice, re = te.indexOf, ae = ee.toString, le = ee.hasOwnProperty, ce = ie.trim, ue = function(e, t) {
        return new ue.fn.init(e,t,Y)
    }, de = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, pe = /\S+/g, he = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/, fe = /^<(\w+)\s*\/?>(?:<\/\1>|)$/, me = /^[\],:{}\s]*$/, ge = function(e, t) {
        return t.toUpperCase()
    }, ve = function(e) {
        (G.addEventListener || "load" === e.type || "complete" === G.readyState) && (ye(),
        ue.ready())
    }, ye = function() {
        G.addEventListener ? (G.removeEventListener("DOMContentLoaded", ve, !1),
        e.removeEventListener("load", ve, !1)) : (G.detachEvent("onreadystatechange", ve),
        e.detachEvent("onload", ve))
    };
    ue.fn = ue.prototype = {
        jquery: ie,
        constructor: ue,
        init: function(e, i, n) {
            var o, s;
            if (!e)
                return this;
            if ("string" == typeof e) {
                if (!(o = "<" === e.charAt(0) && ">" === e.charAt(e.length - 1) && e.length >= 3 ? [null, e, null] : he.exec(e)) || !o[1] && i)
                    return !i || i.jquery ? (i || n).find(e) : this.constructor(i).find(e);
                if (o[1]) {
                    if (i = i instanceof ue ? i[0] : i,
                    ue.merge(this, ue.parseHTML(o[1], i && i.nodeType ? i.ownerDocument || i : G, !0)),
                    fe.test(o[1]) && ue.isPlainObject(i))
                        for (o in i)
                            ue.isFunction(this[o]) ? this[o](i[o]) : this.attr(o, i[o]);
                    return this
                }
                if ((s = G.getElementById(o[2])) && s.parentNode) {
                    if (s.id !== o[2])
                        return n.find(e);
                    this.length = 1,
                    this[0] = s
                }
                return this.context = G,
                this.selector = e,
                this
            }
            return e.nodeType ? (this.context = this[0] = e,
            this.length = 1,
            this) : ue.isFunction(e) ? n.ready(e) : (e.selector !== t && (this.selector = e.selector,
            this.context = e.context),
            ue.makeArray(e, this))
        },
        selector: "",
        length: 0,
        toArray: function() {
            return se.call(this)
        },
        get: function(e) {
            return null == e ? this.toArray() : 0 > e ? this[this.length + e] : this[e]
        },
        pushStack: function(e) {
            var t = ue.merge(this.constructor(), e);
            return t.prevObject = this,
            t.context = this.context,
            t
        },
        each: function(e, t) {
            return ue.each(this, e, t)
        },
        ready: function(e) {
            return ue.ready.promise().done(e),
            this
        },
        slice: function() {
            return this.pushStack(se.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(e) {
            var t = this.length
              , i = +e + (0 > e ? t : 0);
            return this.pushStack(i >= 0 && t > i ? [this[i]] : [])
        },
        map: function(e) {
            return this.pushStack(ue.map(this, function(t, i) {
                return e.call(t, i, t)
            }))
        },
        end: function() {
            return this.prevObject || this.constructor(null)
        },
        push: oe,
        sort: [].sort,
        splice: [].splice
    },
    ue.fn.init.prototype = ue.fn,
    ue.extend = ue.fn.extend = function() {
        var e, i, n, o, s, r, a = arguments[0] || {}, l = 1, c = arguments.length, u = !1;
        for ("boolean" == typeof a && (u = a,
        a = arguments[1] || {},
        l = 2),
        "object" == typeof a || ue.isFunction(a) || (a = {}),
        c === l && (a = this,
        --l); c > l; l++)
            if (null != (s = arguments[l]))
                for (o in s)
                    e = a[o],
                    n = s[o],
                    a !== n && (u && n && (ue.isPlainObject(n) || (i = ue.isArray(n))) ? (i ? (i = !1,
                    r = e && ue.isArray(e) ? e : []) : r = e && ue.isPlainObject(e) ? e : {},
                    a[o] = ue.extend(u, r, n)) : n !== t && (a[o] = n));
        return a
    }
    ,
    ue.extend({
        expando: "jQuery" + (ie + Math.random()).replace(/\D/g, ""),
        noConflict: function(t) {
            return e.$ === ue && (e.$ = Z),
            t && e.jQuery === ue && (e.jQuery = K),
            ue
        },
        isReady: !1,
        readyWait: 1,
        holdReady: function(e) {
            e ? ue.readyWait++ : ue.ready(!0)
        },
        ready: function(e) {
            if (!0 === e ? !--ue.readyWait : !ue.isReady) {
                if (!G.body)
                    return setTimeout(ue.ready);
                ue.isReady = !0,
                !0 !== e && --ue.readyWait > 0 || (U.resolveWith(G, [ue]),
                ue.fn.trigger && ue(G).trigger("ready").off("ready"))
            }
        },
        isFunction: function(e) {
            return "function" === ue.type(e)
        },
        isArray: Array.isArray || function(e) {
            return "array" === ue.type(e)
        }
        ,
        isWindow: function(e) {
            return null != e && e == e.window
        },
        isNumeric: function(e) {
            return !isNaN(parseFloat(e)) && isFinite(e)
        },
        type: function(e) {
            return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? ee[ae.call(e)] || "object" : typeof e
        },
        isPlainObject: function(e) {
            var i;
            if (!e || "object" !== ue.type(e) || e.nodeType || ue.isWindow(e))
                return !1;
            try {
                if (e.constructor && !le.call(e, "constructor") && !le.call(e.constructor.prototype, "isPrototypeOf"))
                    return !1
            } catch (e) {
                return !1
            }
            if (ue.support.ownLast)
                for (i in e)
                    return le.call(e, i);
            for (i in e)
                ;
            return i === t || le.call(e, i)
        },
        isEmptyObject: function(e) {
            var t;
            for (t in e)
                return !1;
            return !0
        },
        error: function(e) {
            throw Error(e)
        },
        parseHTML: function(e, t, i) {
            if (!e || "string" != typeof e)
                return null;
            "boolean" == typeof t && (i = t,
            t = !1),
            t = t || G;
            var n = fe.exec(e)
              , o = !i && [];
            return n ? [t.createElement(n[1])] : (n = ue.buildFragment([e], t, o),
            o && ue(o).remove(),
            ue.merge([], n.childNodes))
        },
        parseJSON: function(i) {
            return e.JSON && e.JSON.parse ? e.JSON.parse(i) : null === i ? i : "string" == typeof i && (i = ue.trim(i)) && me.test(i.replace(/\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g, "@").replace(/"[^"\\\r\n]*"|true|false|null|-?(?:\d+\.|)\d+(?:[eE][+-]?\d+|)/g, "]").replace(/(?:^|:|,)(?:\s*\[)+/g, "")) ? Function("return " + i)() : (ue.error("Invalid JSON: " + i),
            t)
        },
        parseXML: function(i) {
            var n, o;
            if (!i || "string" != typeof i)
                return null;
            try {
                e.DOMParser ? (o = new DOMParser,
                n = o.parseFromString(i, "text/xml")) : (n = new ActiveXObject("Microsoft.XMLDOM"),
                n.async = "false",
                n.loadXML(i))
            } catch (e) {
                n = t
            }
            return n && n.documentElement && !n.getElementsByTagName("parsererror").length || ue.error("Invalid XML: " + i),
            n
        },
        noop: function() {},
        globalEval: function(t) {
            t && ue.trim(t) && (e.execScript || function(t) {
                e.eval.call(e, t)
            }
            )(t)
        },
        camelCase: function(e) {
            return e.replace(/^-ms-/, "ms-").replace(/-([\da-z])/gi, ge)
        },
        nodeName: function(e, t) {
            return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
        },
        each: function(e, t, n) {
            var o = 0
              , s = e.length
              , r = i(e);
            if (n) {
                if (r)
                    for (; s > o && !1 !== t.apply(e[o], n); o++)
                        ;
                else
                    for (o in e)
                        if (!1 === t.apply(e[o], n))
                            break
            } else if (r)
                for (; s > o && !1 !== t.call(e[o], o, e[o]); o++)
                    ;
            else
                for (o in e)
                    if (!1 === t.call(e[o], o, e[o]))
                        break;
            return e
        },
        trim: ce && !ce.call("\ufeff ") ? function(e) {
            return null == e ? "" : ce.call(e)
        }
        : function(e) {
            return null == e ? "" : (e + "").replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "")
        }
        ,
        makeArray: function(e, t) {
            var n = t || [];
            return null != e && (i(Object(e)) ? ue.merge(n, "string" == typeof e ? [e] : e) : oe.call(n, e)),
            n
        },
        inArray: function(e, t, i) {
            var n;
            if (t) {
                if (re)
                    return re.call(t, e, i);
                for (n = t.length,
                i = i ? 0 > i ? Math.max(0, n + i) : i : 0; n > i; i++)
                    if (i in t && t[i] === e)
                        return i
            }
            return -1
        },
        merge: function(e, i) {
            var n = i.length
              , o = e.length
              , s = 0;
            if ("number" == typeof n)
                for (; n > s; s++)
                    e[o++] = i[s];
            else
                for (; i[s] !== t; )
                    e[o++] = i[s++];
            return e.length = o,
            e
        },
        grep: function(e, t, i) {
            var n, o = [], s = 0, r = e.length;
            for (i = !!i; r > s; s++)
                n = !!t(e[s], s),
                i !== n && o.push(e[s]);
            return o
        },
        map: function(e, t, n) {
            var o, s = 0, r = e.length, a = i(e), l = [];
            if (a)
                for (; r > s; s++)
                    null != (o = t(e[s], s, n)) && (l[l.length] = o);
            else
                for (s in e)
                    null != (o = t(e[s], s, n)) && (l[l.length] = o);
            return ne.apply([], l)
        },
        guid: 1,
        proxy: function(e, i) {
            var n, o, s;
            return "string" == typeof i && (s = e[i],
            i = e,
            e = s),
            ue.isFunction(e) ? (n = se.call(arguments, 2),
            o = function() {
                return e.apply(i || this, n.concat(se.call(arguments)))
            }
            ,
            o.guid = e.guid = e.guid || ue.guid++,
            o) : t
        },
        access: function(e, i, n, o, s, r, a) {
            var l = 0
              , c = e.length
              , u = null == n;
            if ("object" === ue.type(n)) {
                s = !0;
                for (l in n)
                    ue.access(e, i, l, n[l], !0, r, a)
            } else if (o !== t && (s = !0,
            ue.isFunction(o) || (a = !0),
            u && (a ? (i.call(e, o),
            i = null) : (u = i,
            i = function(e, t, i) {
                return u.call(ue(e), i)
            }
            )),
            i))
                for (; c > l; l++)
                    i(e[l], n, a ? o : o.call(e[l], l, i(e[l], n)));
            return s ? e : u ? i.call(e) : c ? i(e[0], n) : r
        },
        now: function() {
            return (new Date).getTime()
        },
        swap: function(e, t, i, n) {
            var o, s, r = {};
            for (s in t)
                r[s] = e.style[s],
                e.style[s] = t[s];
            o = i.apply(e, n || []);
            for (s in t)
                e.style[s] = r[s];
            return o
        }
    }),
    ue.ready.promise = function(t) {
        if (!U)
            if (U = ue.Deferred(),
            "complete" === G.readyState)
                setTimeout(ue.ready);
            else if (G.addEventListener)
                G.addEventListener("DOMContentLoaded", ve, !1),
                e.addEventListener("load", ve, !1);
            else {
                G.attachEvent("onreadystatechange", ve),
                e.attachEvent("onload", ve);
                var i = !1;
                try {
                    i = null == e.frameElement && G.documentElement
                } catch (e) {}
                i && i.doScroll && function e() {
                    if (!ue.isReady) {
                        try {
                            i.doScroll("left")
                        } catch (t) {
                            return setTimeout(e, 50)
                        }
                        ye(),
                        ue.ready()
                    }
                }()
            }
        return U.promise(t)
    }
    ,
    ue.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(e, t) {
        ee["[object " + t + "]"] = t.toLowerCase()
    }),
    Y = ue(G),
    function(e, t) {
        function i(e, t, i, n) {
            var o, s, r, a, l, c, p, h, f, m;
            if ((t ? t.ownerDocument || t : P) !== A && D(t),
            t = t || A,
            i = i || [],
            !e || "string" != typeof e)
                return i;
            if (1 !== (a = t.nodeType) && 9 !== a)
                return [];
            if (N && !n) {
                if (o = ve.exec(e))
                    if (r = o[1]) {
                        if (9 === a) {
                            if (!(s = t.getElementById(r)) || !s.parentNode)
                                return i;
                            if (s.id === r)
                                return i.push(s),
                                i
                        } else if (t.ownerDocument && (s = t.ownerDocument.getElementById(r)) && j(t, s) && s.id === r)
                            return i.push(s),
                            i
                    } else {
                        if (o[2])
                            return K.apply(i, t.getElementsByTagName(e)),
                            i;
                        if ((r = o[3]) && x.getElementsByClassName && t.getElementsByClassName)
                            return K.apply(i, t.getElementsByClassName(r)),
                            i
                    }
                if (x.qsa && (!O || !O.test(e))) {
                    if (h = p = B,
                    f = t,
                    m = 9 === a && e,
                    1 === a && "object" !== t.nodeName.toLowerCase()) {
                        for (c = u(e),
                        (p = t.getAttribute("id")) ? h = p.replace(be, "\\$&") : t.setAttribute("id", h),
                        h = "[id='" + h + "'] ",
                        l = c.length; l--; )
                            c[l] = h + d(c[l]);
                        f = de.test(e) && t.parentNode || t,
                        m = c.join(",")
                    }
                    if (m)
                        try {
                            return K.apply(i, f.querySelectorAll(m)),
                            i
                        } catch (e) {} finally {
                            p || t.removeAttribute("id")
                        }
                }
            }
            return w(e.replace(ae, "$1"), t, i, n)
        }
        function n() {
            function e(i, n) {
                return t.push(i += " ") > C.cacheLength && delete e[t.shift()],
                e[i] = n
            }
            var t = [];
            return e
        }
        function o(e) {
            return e[B] = !0,
            e
        }
        function s(e) {
            var t = A.createElement("div");
            try {
                return !!e(t)
            } catch (e) {
                return !1
            } finally {
                t.parentNode && t.parentNode.removeChild(t),
                t = null
            }
        }
        function r(e, t) {
            for (var i = e.split("|"), n = e.length; n--; )
                C.attrHandle[i[n]] = t
        }
        function a(e, t) {
            var i = t && e
              , n = i && 1 === e.nodeType && 1 === t.nodeType && (~t.sourceIndex || Y) - (~e.sourceIndex || Y);
            if (n)
                return n;
            if (i)
                for (; i = i.nextSibling; )
                    if (i === t)
                        return -1;
            return e ? 1 : -1
        }
        function l(e) {
            return o(function(t) {
                return t = +t,
                o(function(i, n) {
                    for (var o, s = e([], i.length, t), r = s.length; r--; )
                        i[o = s[r]] && (i[o] = !(n[o] = i[o]))
                })
            })
        }
        function c() {}
        function u(e, t) {
            var n, o, s, r, a, l, c, u = R[e + " "];
            if (u)
                return t ? 0 : u.slice(0);
            for (a = e,
            l = [],
            c = C.preFilter; a; ) {
                (!n || (o = le.exec(a))) && (o && (a = a.slice(o[0].length) || a),
                l.push(s = [])),
                n = !1,
                (o = ce.exec(a)) && (n = o.shift(),
                s.push({
                    value: n,
                    type: o[0].replace(ae, " ")
                }),
                a = a.slice(n.length));
                for (r in C.filter)
                    !(o = me[r].exec(a)) || c[r] && !(o = c[r](o)) || (n = o.shift(),
                    s.push({
                        value: n,
                        type: r,
                        matches: o
                    }),
                    a = a.slice(n.length));
                if (!n)
                    break
            }
            return t ? a.length : a ? i.error(e) : R(e, l).slice(0)
        }
        function d(e) {
            for (var t = 0, i = e.length, n = ""; i > t; t++)
                n += e[t].value;
            return n
        }
        function p(e, t, i) {
            var n = t.dir
              , o = i && "parentNode" === n
              , s = z++;
            return t.first ? function(t, i, s) {
                for (; t = t[n]; )
                    if (1 === t.nodeType || o)
                        return e(t, i, s)
            }
            : function(t, i, r) {
                var a, l, c, u = I + " " + s;
                if (r) {
                    for (; t = t[n]; )
                        if ((1 === t.nodeType || o) && e(t, i, r))
                            return !0
                } else
                    for (; t = t[n]; )
                        if (1 === t.nodeType || o)
                            if (c = t[B] || (t[B] = {}),
                            (l = c[n]) && l[0] === u) {
                                if (!0 === (a = l[1]) || a === S)
                                    return !0 === a
                            } else if (l = c[n] = [u],
                            l[1] = e(t, i, r) || S,
                            !0 === l[1])
                                return !0
            }
        }
        function h(e) {
            return e.length > 1 ? function(t, i, n) {
                for (var o = e.length; o--; )
                    if (!e[o](t, i, n))
                        return !1;
                return !0
            }
            : e[0]
        }
        function f(e, t, i, n, o) {
            for (var s, r = [], a = 0, l = e.length, c = null != t; l > a; a++)
                (s = e[a]) && (!i || i(s, n, o)) && (r.push(s),
                c && t.push(a));
            return r
        }
        function m(e, t, i, n, s, r) {
            return n && !n[B] && (n = m(n)),
            s && !s[B] && (s = m(s, r)),
            o(function(o, r, a, l) {
                var c, u, d, p = [], h = [], m = r.length, g = o || y(t || "*", a.nodeType ? [a] : a, []), v = !e || !o && t ? g : f(g, p, e, a, l), w = i ? s || (o ? e : m || n) ? [] : r : v;
                if (i && i(v, w, a, l),
                n)
                    for (c = f(w, h),
                    n(c, [], a, l),
                    u = c.length; u--; )
                        (d = c[u]) && (w[h[u]] = !(v[h[u]] = d));
                if (o) {
                    if (s || e) {
                        if (s) {
                            for (c = [],
                            u = w.length; u--; )
                                (d = w[u]) && c.push(v[u] = d);
                            s(null, w = [], c, l)
                        }
                        for (u = w.length; u--; )
                            (d = w[u]) && (c = s ? ee.call(o, d) : p[u]) > -1 && (o[c] = !(r[c] = d))
                    }
                } else
                    w = f(w === r ? w.splice(m, w.length) : w),
                    s ? s(null, r, w, l) : K.apply(r, w)
            })
        }
        function g(e) {
            for (var t, i, n, o = e.length, s = C.relative[e[0].type], r = s || C.relative[" "], a = s ? 1 : 0, l = p(function(e) {
                return e === t
            }, r, !0), c = p(function(e) {
                return ee.call(t, e) > -1
            }, r, !0), u = [function(e, i, n) {
                return !s && (n || i !== $) || ((t = i).nodeType ? l(e, i, n) : c(e, i, n))
            }
            ]; o > a; a++)
                if (i = C.relative[e[a].type])
                    u = [p(h(u), i)];
                else {
                    if (i = C.filter[e[a].type].apply(null, e[a].matches),
                    i[B]) {
                        for (n = ++a; o > n && !C.relative[e[n].type]; n++)
                            ;
                        return m(a > 1 && h(u), a > 1 && d(e.slice(0, a - 1).concat({
                            value: " " === e[a - 2].type ? "*" : ""
                        })).replace(ae, "$1"), i, n > a && g(e.slice(a, n)), o > n && g(e = e.slice(n)), o > n && d(e))
                    }
                    u.push(i)
                }
            return h(u)
        }
        function v(e, t) {
            var n = 0
              , s = t.length > 0
              , r = e.length > 0
              , a = function(o, a, l, c, u) {
                var d, p, h, m = [], g = 0, v = "0", y = o && [], w = null != u, b = $, x = o || r && C.find.TAG("*", u && a.parentNode || a), k = I += null == b ? 1 : Math.random() || .1;
                for (w && ($ = a !== A && a,
                S = n); null != (d = x[v]); v++) {
                    if (r && d) {
                        for (p = 0; h = e[p++]; )
                            if (h(d, a, l)) {
                                c.push(d);
                                break
                            }
                        w && (I = k,
                        S = ++n)
                    }
                    s && ((d = !h && d) && g--,
                    o && y.push(d))
                }
                if (g += v,
                s && v !== g) {
                    for (p = 0; h = t[p++]; )
                        h(y, m, a, l);
                    if (o) {
                        if (g > 0)
                            for (; v--; )
                                y[v] || m[v] || (m[v] = G.call(c));
                        m = f(m)
                    }
                    K.apply(c, m),
                    w && !o && m.length > 0 && g + t.length > 1 && i.uniqueSort(c)
                }
                return w && (I = k,
                $ = b),
                y
            };
            return s ? o(a) : a
        }
        function y(e, t, n) {
            for (var o = 0, s = t.length; s > o; o++)
                i(e, t[o], n);
            return n
        }
        function w(e, t, i, n) {
            var o, s, r, a, l, c = u(e);
            if (!n && 1 === c.length) {
                if (s = c[0] = c[0].slice(0),
                s.length > 2 && "ID" === (r = s[0]).type && x.getById && 9 === t.nodeType && N && C.relative[s[1].type]) {
                    if (!(t = (C.find.ID(r.matches[0].replace(xe, Se), t) || [])[0]))
                        return i;
                    e = e.slice(s.shift().value.length)
                }
                for (o = me.needsContext.test(e) ? 0 : s.length; o-- && (r = s[o],
                !C.relative[a = r.type]); )
                    if ((l = C.find[a]) && (n = l(r.matches[0].replace(xe, Se), de.test(s[0].type) && t.parentNode || t))) {
                        if (s.splice(o, 1),
                        !(e = n.length && d(s)))
                            return K.apply(i, n),
                            i;
                        break
                    }
            }
            return _(e, c)(n, t, !N, i, de.test(e)),
            i
        }
        var b, x, S, C, k, T, _, $, E, D, A, M, N, O, L, H, j, B = "sizzle" + -new Date, P = e.document, I = 0, z = 0, W = n(), R = n(), q = n(), F = !1, X = function(e, t) {
            return e === t ? (F = !0,
            0) : 0
        }, U = typeof t, Y = 1 << 31, V = {}.hasOwnProperty, Q = [], G = Q.pop, J = Q.push, K = Q.push, Z = Q.slice, ee = Q.indexOf || function(e) {
            for (var t = 0, i = this.length; i > t; t++)
                if (this[t] === e)
                    return t;
            return -1
        }
        , te = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", ie = "[\\x20\\t\\r\\n\\f]", ne = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", oe = ne.replace("w", "w#"), se = "\\[" + ie + "*(" + ne + ")" + ie + "*(?:([*^$|!~]?=)" + ie + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + oe + ")|)|)" + ie + "*\\]", re = ":(" + ne + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + se.replace(3, 8) + ")*)|.*)\\)|)", ae = RegExp("^" + ie + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ie + "+$", "g"), le = RegExp("^" + ie + "*," + ie + "*"), ce = RegExp("^" + ie + "*([>+~]|" + ie + ")" + ie + "*"), de = RegExp(ie + "*[+~]"), pe = RegExp("=" + ie + "*([^\\]'\"]*)" + ie + "*\\]", "g"), he = RegExp(re), fe = RegExp("^" + oe + "$"), me = {
            ID: RegExp("^#(" + ne + ")"),
            CLASS: RegExp("^\\.(" + ne + ")"),
            TAG: RegExp("^(" + ne.replace("w", "w*") + ")"),
            ATTR: RegExp("^" + se),
            PSEUDO: RegExp("^" + re),
            CHILD: RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + ie + "*(even|odd|(([+-]|)(\\d*)n|)" + ie + "*(?:([+-]|)" + ie + "*(\\d+)|))" + ie + "*\\)|)", "i"),
            bool: RegExp("^(?:" + te + ")$", "i"),
            needsContext: RegExp("^" + ie + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + ie + "*((?:-\\d)?\\d*)" + ie + "*\\)|)(?=[^-]|$)", "i")
        }, ge = /^[^{]+\{\s*\[native \w/, ve = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, ye = /^(?:input|select|textarea|button)$/i, we = /^h\d$/i, be = /'|\\/g, xe = RegExp("\\\\([\\da-f]{1,6}" + ie + "?|(" + ie + ")|.)", "ig"), Se = function(e, t, i) {
            var n = "0x" + t - 65536;
            return n !== n || i ? t : 0 > n ? String.fromCharCode(n + 65536) : String.fromCharCode(55296 | n >> 10, 56320 | 1023 & n)
        };
        try {
            K.apply(Q = Z.call(P.childNodes), P.childNodes),
            Q[P.childNodes.length].nodeType
        } catch (e) {
            K = {
                apply: Q.length ? function(e, t) {
                    J.apply(e, Z.call(t))
                }
                : function(e, t) {
                    for (var i = e.length, n = 0; e[i++] = t[n++]; )
                        ;
                    e.length = i - 1
                }
            }
        }
        T = i.isXML = function(e) {
            var t = e && (e.ownerDocument || e).documentElement;
            return !!t && "HTML" !== t.nodeName
        }
        ,
        x = i.support = {},
        D = i.setDocument = function(e) {
            var i = e ? e.ownerDocument || e : P
              , n = i.defaultView;
            return i !== A && 9 === i.nodeType && i.documentElement ? (A = i,
            M = i.documentElement,
            N = !T(i),
            n && n.attachEvent && n !== n.top && n.attachEvent("onbeforeunload", function() {
                D()
            }),
            x.attributes = s(function(e) {
                return e.className = "i",
                !e.getAttribute("className")
            }),
            x.getElementsByTagName = s(function(e) {
                return e.appendChild(i.createComment("")),
                !e.getElementsByTagName("*").length
            }),
            x.getElementsByClassName = s(function(e) {
                return e.innerHTML = "<div class='a'></div><div class='a i'></div>",
                e.firstChild.className = "i",
                2 === e.getElementsByClassName("i").length
            }),
            x.getById = s(function(e) {
                return M.appendChild(e).id = B,
                !i.getElementsByName || !i.getElementsByName(B).length
            }),
            x.getById ? (C.find.ID = function(e, t) {
                if (typeof t.getElementById !== U && N) {
                    var i = t.getElementById(e);
                    return i && i.parentNode ? [i] : []
                }
            }
            ,
            C.filter.ID = function(e) {
                var t = e.replace(xe, Se);
                return function(e) {
                    return e.getAttribute("id") === t
                }
            }
            ) : (delete C.find.ID,
            C.filter.ID = function(e) {
                var t = e.replace(xe, Se);
                return function(e) {
                    var i = typeof e.getAttributeNode !== U && e.getAttributeNode("id");
                    return i && i.value === t
                }
            }
            ),
            C.find.TAG = x.getElementsByTagName ? function(e, i) {
                return typeof i.getElementsByTagName !== U ? i.getElementsByTagName(e) : t
            }
            : function(e, t) {
                var i, n = [], o = 0, s = t.getElementsByTagName(e);
                if ("*" === e) {
                    for (; i = s[o++]; )
                        1 === i.nodeType && n.push(i);
                    return n
                }
                return s
            }
            ,
            C.find.CLASS = x.getElementsByClassName && function(e, i) {
                return typeof i.getElementsByClassName !== U && N ? i.getElementsByClassName(e) : t
            }
            ,
            L = [],
            O = [],
            (x.qsa = ge.test(i.querySelectorAll)) && (s(function(e) {
                e.innerHTML = "<select><option selected=''></option></select>",
                e.querySelectorAll("[selected]").length || O.push("\\[" + ie + "*(?:value|" + te + ")"),
                e.querySelectorAll(":checked").length || O.push(":checked")
            }),
            s(function(e) {
                var t = i.createElement("input");
                t.setAttribute("type", "hidden"),
                e.appendChild(t).setAttribute("t", ""),
                e.querySelectorAll("[t^='']").length && O.push("[*^$]=" + ie + "*(?:''|\"\")"),
                e.querySelectorAll(":enabled").length || O.push(":enabled", ":disabled"),
                e.querySelectorAll("*,:x"),
                O.push(",.*:")
            })),
            (x.matchesSelector = ge.test(H = M.webkitMatchesSelector || M.mozMatchesSelector || M.oMatchesSelector || M.msMatchesSelector)) && s(function(e) {
                x.disconnectedMatch = H.call(e, "div"),
                H.call(e, "[s!='']:x"),
                L.push("!=", re)
            }),
            O = O.length && RegExp(O.join("|")),
            L = L.length && RegExp(L.join("|")),
            j = ge.test(M.contains) || M.compareDocumentPosition ? function(e, t) {
                var i = 9 === e.nodeType ? e.documentElement : e
                  , n = t && t.parentNode;
                return e === n || !(!n || 1 !== n.nodeType || !(i.contains ? i.contains(n) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(n)))
            }
            : function(e, t) {
                if (t)
                    for (; t = t.parentNode; )
                        if (t === e)
                            return !0;
                return !1
            }
            ,
            X = M.compareDocumentPosition ? function(e, t) {
                if (e === t)
                    return F = !0,
                    0;
                var n = t.compareDocumentPosition && e.compareDocumentPosition && e.compareDocumentPosition(t);
                return n ? 1 & n || !x.sortDetached && t.compareDocumentPosition(e) === n ? e === i || j(P, e) ? -1 : t === i || j(P, t) ? 1 : E ? ee.call(E, e) - ee.call(E, t) : 0 : 4 & n ? -1 : 1 : e.compareDocumentPosition ? -1 : 1
            }
            : function(e, t) {
                var n, o = 0, s = e.parentNode, r = t.parentNode, l = [e], c = [t];
                if (e === t)
                    return F = !0,
                    0;
                if (!s || !r)
                    return e === i ? -1 : t === i ? 1 : s ? -1 : r ? 1 : E ? ee.call(E, e) - ee.call(E, t) : 0;
                if (s === r)
                    return a(e, t);
                for (n = e; n = n.parentNode; )
                    l.unshift(n);
                for (n = t; n = n.parentNode; )
                    c.unshift(n);
                for (; l[o] === c[o]; )
                    o++;
                return o ? a(l[o], c[o]) : l[o] === P ? -1 : c[o] === P ? 1 : 0
            }
            ,
            i) : A
        }
        ,
        i.matches = function(e, t) {
            return i(e, null, null, t)
        }
        ,
        i.matchesSelector = function(e, t) {
            if ((e.ownerDocument || e) !== A && D(e),
            t = t.replace(pe, "='$1']"),
            !(!x.matchesSelector || !N || L && L.test(t) || O && O.test(t)))
                try {
                    var n = H.call(e, t);
                    if (n || x.disconnectedMatch || e.document && 11 !== e.document.nodeType)
                        return n
                } catch (e) {}
            return i(t, A, null, [e]).length > 0
        }
        ,
        i.contains = function(e, t) {
            return (e.ownerDocument || e) !== A && D(e),
            j(e, t)
        }
        ,
        i.attr = function(e, i) {
            (e.ownerDocument || e) !== A && D(e);
            var n = C.attrHandle[i.toLowerCase()]
              , o = n && V.call(C.attrHandle, i.toLowerCase()) ? n(e, i, !N) : t;
            return o === t ? x.attributes || !N ? e.getAttribute(i) : (o = e.getAttributeNode(i)) && o.specified ? o.value : null : o
        }
        ,
        i.error = function(e) {
            throw Error("Syntax error, unrecognized expression: " + e)
        }
        ,
        i.uniqueSort = function(e) {
            var t, i = [], n = 0, o = 0;
            if (F = !x.detectDuplicates,
            E = !x.sortStable && e.slice(0),
            e.sort(X),
            F) {
                for (; t = e[o++]; )
                    t === e[o] && (n = i.push(o));
                for (; n--; )
                    e.splice(i[n], 1)
            }
            return e
        }
        ,
        k = i.getText = function(e) {
            var t, i = "", n = 0, o = e.nodeType;
            if (o) {
                if (1 === o || 9 === o || 11 === o) {
                    if ("string" == typeof e.textContent)
                        return e.textContent;
                    for (e = e.firstChild; e; e = e.nextSibling)
                        i += k(e)
                } else if (3 === o || 4 === o)
                    return e.nodeValue
            } else
                for (; t = e[n]; n++)
                    i += k(t);
            return i
        }
        ,
        C = i.selectors = {
            cacheLength: 50,
            createPseudo: o,
            match: me,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(e) {
                    return e[1] = e[1].replace(xe, Se),
                    e[3] = (e[4] || e[5] || "").replace(xe, Se),
                    "~=" === e[2] && (e[3] = " " + e[3] + " "),
                    e.slice(0, 4)
                },
                CHILD: function(e) {
                    return e[1] = e[1].toLowerCase(),
                    "nth" === e[1].slice(0, 3) ? (e[3] || i.error(e[0]),
                    e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])),
                    e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && i.error(e[0]),
                    e
                },
                PSEUDO: function(e) {
                    var i, n = !e[5] && e[2];
                    return me.CHILD.test(e[0]) ? null : (e[3] && e[4] !== t ? e[2] = e[4] : n && he.test(n) && (i = u(n, !0)) && (i = n.indexOf(")", n.length - i) - n.length) && (e[0] = e[0].slice(0, i),
                    e[2] = n.slice(0, i)),
                    e.slice(0, 3))
                }
            },
            filter: {
                TAG: function(e) {
                    var t = e.replace(xe, Se).toLowerCase();
                    return "*" === e ? function() {
                        return !0
                    }
                    : function(e) {
                        return e.nodeName && e.nodeName.toLowerCase() === t
                    }
                },
                CLASS: function(e) {
                    var t = W[e + " "];
                    return t || (t = RegExp("(^|" + ie + ")" + e + "(" + ie + "|$)")) && W(e, function(e) {
                        return t.test("string" == typeof e.className && e.className || typeof e.getAttribute !== U && e.getAttribute("class") || "")
                    })
                },
                ATTR: function(e, t, n) {
                    return function(o) {
                        var s = i.attr(o, e);
                        return null == s ? "!=" === t : !t || (s += "",
                        "=" === t ? s === n : "!=" === t ? s !== n : "^=" === t ? n && 0 === s.indexOf(n) : "*=" === t ? n && s.indexOf(n) > -1 : "$=" === t ? n && s.slice(-n.length) === n : "~=" === t ? (" " + s + " ").indexOf(n) > -1 : "|=" === t && (s === n || s.slice(0, n.length + 1) === n + "-"))
                    }
                },
                CHILD: function(e, t, i, n, o) {
                    var s = "nth" !== e.slice(0, 3)
                      , r = "last" !== e.slice(-4)
                      , a = "of-type" === t;
                    return 1 === n && 0 === o ? function(e) {
                        return !!e.parentNode
                    }
                    : function(t, i, l) {
                        var c, u, d, p, h, f, m = s !== r ? "nextSibling" : "previousSibling", g = t.parentNode, v = a && t.nodeName.toLowerCase(), y = !l && !a;
                        if (g) {
                            if (s) {
                                for (; m; ) {
                                    for (d = t; d = d[m]; )
                                        if (a ? d.nodeName.toLowerCase() === v : 1 === d.nodeType)
                                            return !1;
                                    f = m = "only" === e && !f && "nextSibling"
                                }
                                return !0
                            }
                            if (f = [r ? g.firstChild : g.lastChild],
                            r && y) {
                                for (u = g[B] || (g[B] = {}),
                                c = u[e] || [],
                                h = c[0] === I && c[1],
                                p = c[0] === I && c[2],
                                d = h && g.childNodes[h]; d = ++h && d && d[m] || (p = h = 0) || f.pop(); )
                                    if (1 === d.nodeType && ++p && d === t) {
                                        u[e] = [I, h, p];
                                        break
                                    }
                            } else if (y && (c = (t[B] || (t[B] = {}))[e]) && c[0] === I)
                                p = c[1];
                            else
                                for (; (d = ++h && d && d[m] || (p = h = 0) || f.pop()) && ((a ? d.nodeName.toLowerCase() !== v : 1 !== d.nodeType) || !++p || (y && ((d[B] || (d[B] = {}))[e] = [I, p]),
                                d !== t)); )
                                    ;
                            return (p -= o) === n || 0 == p % n && p / n >= 0
                        }
                    }
                },
                PSEUDO: function(e, t) {
                    var n, s = C.pseudos[e] || C.setFilters[e.toLowerCase()] || i.error("unsupported pseudo: " + e);
                    return s[B] ? s(t) : s.length > 1 ? (n = [e, e, "", t],
                    C.setFilters.hasOwnProperty(e.toLowerCase()) ? o(function(e, i) {
                        for (var n, o = s(e, t), r = o.length; r--; )
                            n = ee.call(e, o[r]),
                            e[n] = !(i[n] = o[r])
                    }) : function(e) {
                        return s(e, 0, n)
                    }
                    ) : s
                }
            },
            pseudos: {
                not: o(function(e) {
                    var t = []
                      , i = []
                      , n = _(e.replace(ae, "$1"));
                    return n[B] ? o(function(e, t, i, o) {
                        for (var s, r = n(e, null, o, []), a = e.length; a--; )
                            (s = r[a]) && (e[a] = !(t[a] = s))
                    }) : function(e, o, s) {
                        return t[0] = e,
                        n(t, null, s, i),
                        !i.pop()
                    }
                }),
                has: o(function(e) {
                    return function(t) {
                        return i(e, t).length > 0
                    }
                }),
                contains: o(function(e) {
                    return function(t) {
                        return (t.textContent || t.innerText || k(t)).indexOf(e) > -1
                    }
                }),
                lang: o(function(e) {
                    return fe.test(e || "") || i.error("unsupported lang: " + e),
                    e = e.replace(xe, Se).toLowerCase(),
                    function(t) {
                        var i;
                        do {
                            if (i = N ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang"))
                                return (i = i.toLowerCase()) === e || 0 === i.indexOf(e + "-")
                        } while ((t = t.parentNode) && 1 === t.nodeType);return !1
                    }
                }),
                target: function(t) {
                    var i = e.location && e.location.hash;
                    return i && i.slice(1) === t.id
                },
                root: function(e) {
                    return e === M
                },
                focus: function(e) {
                    return e === A.activeElement && (!A.hasFocus || A.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                },
                enabled: function(e) {
                    return !1 === e.disabled
                },
                disabled: function(e) {
                    return !0 === e.disabled
                },
                checked: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && !!e.checked || "option" === t && !!e.selected
                },
                selected: function(e) {
                    return e.parentNode && e.parentNode.selectedIndex,
                    !0 === e.selected
                },
                empty: function(e) {
                    for (e = e.firstChild; e; e = e.nextSibling)
                        if (e.nodeName > "@" || 3 === e.nodeType || 4 === e.nodeType)
                            return !1;
                    return !0
                },
                parent: function(e) {
                    return !C.pseudos.empty(e)
                },
                header: function(e) {
                    return we.test(e.nodeName)
                },
                input: function(e) {
                    return ye.test(e.nodeName)
                },
                button: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && "button" === e.type || "button" === t
                },
                text: function(e) {
                    var t;
                    return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || t.toLowerCase() === e.type)
                },
                first: l(function() {
                    return [0]
                }),
                last: l(function(e, t) {
                    return [t - 1]
                }),
                eq: l(function(e, t, i) {
                    return [0 > i ? i + t : i]
                }),
                even: l(function(e, t) {
                    for (var i = 0; t > i; i += 2)
                        e.push(i);
                    return e
                }),
                odd: l(function(e, t) {
                    for (var i = 1; t > i; i += 2)
                        e.push(i);
                    return e
                }),
                lt: l(function(e, t, i) {
                    for (var n = 0 > i ? i + t : i; --n >= 0; )
                        e.push(n);
                    return e
                }),
                gt: l(function(e, t, i) {
                    for (var n = 0 > i ? i + t : i; t > ++n; )
                        e.push(n);
                    return e
                })
            }
        },
        C.pseudos.nth = C.pseudos.eq;
        for (b in {
            radio: !0,
            checkbox: !0,
            file: !0,
            password: !0,
            image: !0
        })
            C.pseudos[b] = function(e) {
                return function(t) {
                    return "input" === t.nodeName.toLowerCase() && t.type === e
                }
            }(b);
        for (b in {
            submit: !0,
            reset: !0
        })
            C.pseudos[b] = function(e) {
                return function(t) {
                    var i = t.nodeName.toLowerCase();
                    return ("input" === i || "button" === i) && t.type === e
                }
            }(b);
        c.prototype = C.filters = C.pseudos,
        C.setFilters = new c,
        _ = i.compile = function(e, t) {
            var i, n = [], o = [], s = q[e + " "];
            if (!s) {
                for (t || (t = u(e)),
                i = t.length; i--; )
                    s = g(t[i]),
                    s[B] ? n.push(s) : o.push(s);
                s = q(e, v(o, n))
            }
            return s
        }
        ,
        x.sortStable = B.split("").sort(X).join("") === B,
        x.detectDuplicates = F,
        D(),
        x.sortDetached = s(function(e) {
            return 1 & e.compareDocumentPosition(A.createElement("div"))
        }),
        s(function(e) {
            return e.innerHTML = "<a href='#'></a>",
            "#" === e.firstChild.getAttribute("href")
        }) || r("type|href|height|width", function(e, i, n) {
            return n ? t : e.getAttribute(i, "type" === i.toLowerCase() ? 1 : 2)
        }),
        x.attributes && s(function(e) {
            return e.innerHTML = "<input/>",
            e.firstChild.setAttribute("value", ""),
            "" === e.firstChild.getAttribute("value")
        }) || r("value", function(e, i, n) {
            return n || "input" !== e.nodeName.toLowerCase() ? t : e.defaultValue
        }),
        s(function(e) {
            return null == e.getAttribute("disabled")
        }) || r(te, function(e, i, n) {
            var o;
            return n ? t : (o = e.getAttributeNode(i)) && o.specified ? o.value : !0 === e[i] ? i.toLowerCase() : null
        }),
        ue.find = i,
        ue.expr = i.selectors,
        ue.expr[":"] = ue.expr.pseudos,
        ue.unique = i.uniqueSort,
        ue.text = i.getText,
        ue.isXMLDoc = i.isXML,
        ue.contains = i.contains
    }(e);
    var we = {};
    ue.Callbacks = function(e) {
        e = "string" == typeof e ? we[e] || n(e) : ue.extend({}, e);
        var i, o, s, r, a, l, c = [], u = !e.once && [], d = function(t) {
            for (o = e.memory && t,
            s = !0,
            a = l || 0,
            l = 0,
            r = c.length,
            i = !0; c && r > a; a++)
                if (!1 === c[a].apply(t[0], t[1]) && e.stopOnFalse) {
                    o = !1;
                    break
                }
            i = !1,
            c && (u ? u.length && d(u.shift()) : o ? c = [] : p.disable())
        }, p = {
            add: function() {
                if (c) {
                    var t = c.length;
                    (function t(i) {
                        ue.each(i, function(i, n) {
                            var o = ue.type(n);
                            "function" === o ? e.unique && p.has(n) || c.push(n) : n && n.length && "string" !== o && t(n)
                        })
                    })(arguments),
                    i ? r = c.length : o && (l = t,
                    d(o))
                }
                return this
            },
            remove: function() {
                return c && ue.each(arguments, function(e, t) {
                    for (var n; (n = ue.inArray(t, c, n)) > -1; )
                        c.splice(n, 1),
                        i && (r >= n && r--,
                        a >= n && a--)
                }),
                this
            },
            has: function(e) {
                return e ? ue.inArray(e, c) > -1 : !(!c || !c.length)
            },
            empty: function() {
                return c = [],
                r = 0,
                this
            },
            disable: function() {
                return c = u = o = t,
                this
            },
            disabled: function() {
                return !c
            },
            lock: function() {
                return u = t,
                o || p.disable(),
                this
            },
            locked: function() {
                return !u
            },
            fireWith: function(e, t) {
                return !c || s && !u || (t = t || [],
                t = [e, t.slice ? t.slice() : t],
                i ? u.push(t) : d(t)),
                this
            },
            fire: function() {
                return p.fireWith(this, arguments),
                this
            },
            fired: function() {
                return !!s
            }
        };
        return p
    }
    ,
    ue.extend({
        Deferred: function(e) {
            var t = [["resolve", "done", ue.Callbacks("once memory"), "resolved"], ["reject", "fail", ue.Callbacks("once memory"), "rejected"], ["notify", "progress", ue.Callbacks("memory")]]
              , i = "pending"
              , n = {
                state: function() {
                    return i
                },
                always: function() {
                    return o.done(arguments).fail(arguments),
                    this
                },
                then: function() {
                    var e = arguments;
                    return ue.Deferred(function(i) {
                        ue.each(t, function(t, s) {
                            var r = s[0]
                              , a = ue.isFunction(e[t]) && e[t];
                            o[s[1]](function() {
                                var e = a && a.apply(this, arguments);
                                e && ue.isFunction(e.promise) ? e.promise().done(i.resolve).fail(i.reject).progress(i.notify) : i[r + "With"](this === n ? i.promise() : this, a ? [e] : arguments)
                            })
                        }),
                        e = null
                    }).promise()
                },
                promise: function(e) {
                    return null != e ? ue.extend(e, n) : n
                }
            }
              , o = {};
            return n.pipe = n.then,
            ue.each(t, function(e, s) {
                var r = s[2]
                  , a = s[3];
                n[s[1]] = r.add,
                a && r.add(function() {
                    i = a
                }, t[1 ^ e][2].disable, t[2][2].lock),
                o[s[0]] = function() {
                    return o[s[0] + "With"](this === o ? n : this, arguments),
                    this
                }
                ,
                o[s[0] + "With"] = r.fireWith
            }),
            n.promise(o),
            e && e.call(o, o),
            o
        },
        when: function(e) {
            var t, i, n, o = 0, s = se.call(arguments), r = s.length, a = 1 !== r || e && ue.isFunction(e.promise) ? r : 0, l = 1 === a ? e : ue.Deferred(), c = function(e, i, n) {
                return function(o) {
                    i[e] = this,
                    n[e] = arguments.length > 1 ? se.call(arguments) : o,
                    n === t ? l.notifyWith(i, n) : --a || l.resolveWith(i, n)
                }
            };
            if (r > 1)
                for (t = Array(r),
                i = Array(r),
                n = Array(r); r > o; o++)
                    s[o] && ue.isFunction(s[o].promise) ? s[o].promise().done(c(o, n, s)).fail(l.reject).progress(c(o, i, t)) : --a;
            return a || l.resolveWith(n, s),
            l.promise()
        }
    }),
    ue.support = function(t) {
        var i, n, o, s, r, a, l, c, u, d = G.createElement("div");
        if (d.setAttribute("className", "t"),
        d.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",
        i = d.getElementsByTagName("*") || [],
        !(n = d.getElementsByTagName("a")[0]) || !n.style || !i.length)
            return t;
        s = G.createElement("select"),
        a = s.appendChild(G.createElement("option")),
        o = d.getElementsByTagName("input")[0],
        n.style.cssText = "top:1px;float:left;opacity:.5",
        t.getSetAttribute = "t" !== d.className,
        t.leadingWhitespace = 3 === d.firstChild.nodeType,
        t.tbody = !d.getElementsByTagName("tbody").length,
        t.htmlSerialize = !!d.getElementsByTagName("link").length,
        t.style = /top/.test(n.getAttribute("style")),
        t.hrefNormalized = "/a" === n.getAttribute("href"),
        t.opacity = /^0.5/.test(n.style.opacity),
        t.cssFloat = !!n.style.cssFloat,
        t.checkOn = !!o.value,
        t.optSelected = a.selected,
        t.enctype = !!G.createElement("form").enctype,
        t.html5Clone = "<:nav></:nav>" !== G.createElement("nav").cloneNode(!0).outerHTML,
        t.inlineBlockNeedsLayout = !1,
        t.shrinkWrapBlocks = !1,
        t.pixelPosition = !1,
        t.deleteExpando = !0,
        t.noCloneEvent = !0,
        t.reliableMarginRight = !0,
        t.boxSizingReliable = !0,
        o.checked = !0,
        t.noCloneChecked = o.cloneNode(!0).checked,
        s.disabled = !0,
        t.optDisabled = !a.disabled;
        try {
            delete d.test
        } catch (e) {
            t.deleteExpando = !1
        }
        o = G.createElement("input"),
        o.setAttribute("value", ""),
        t.input = "" === o.getAttribute("value"),
        o.value = "t",
        o.setAttribute("type", "radio"),
        t.radioValue = "t" === o.value,
        o.setAttribute("checked", "t"),
        o.setAttribute("name", "t"),
        r = G.createDocumentFragment(),
        r.appendChild(o),
        t.appendChecked = o.checked,
        t.checkClone = r.cloneNode(!0).cloneNode(!0).lastChild.checked,
        d.attachEvent && (d.attachEvent("onclick", function() {
            t.noCloneEvent = !1
        }),
        d.cloneNode(!0).click());
        for (u in {
            submit: !0,
            change: !0,
            focusin: !0
        })
            d.setAttribute(l = "on" + u, "t"),
            t[u + "Bubbles"] = l in e || !1 === d.attributes[l].expando;
        d.style.backgroundClip = "content-box",
        d.cloneNode(!0).style.backgroundClip = "",
        t.clearCloneStyle = "content-box" === d.style.backgroundClip;
        for (u in ue(t))
            break;
        return t.ownLast = "0" !== u,
        ue(function() {
            var i, n, o, s = "padding:0;margin:0;border:0;display:block;box-sizing:content-box;-moz-box-sizing:content-box;-webkit-box-sizing:content-box;", r = G.getElementsByTagName("body")[0];
            r && (i = G.createElement("div"),
            i.style.cssText = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px",
            r.appendChild(i).appendChild(d),
            d.innerHTML = "<table><tr><td></td><td>t</td></tr></table>",
            o = d.getElementsByTagName("td"),
            o[0].style.cssText = "padding:0;margin:0;border:0;display:none",
            c = 0 === o[0].offsetHeight,
            o[0].style.display = "",
            o[1].style.display = "none",
            t.reliableHiddenOffsets = c && 0 === o[0].offsetHeight,
            d.innerHTML = "",
            d.style.cssText = "box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%;",
            ue.swap(r, null != r.style.zoom ? {
                zoom: 1
            } : {}, function() {
                t.boxSizing = 4 === d.offsetWidth
            }),
            e.getComputedStyle && (t.pixelPosition = "1%" !== (e.getComputedStyle(d, null) || {}).top,
            t.boxSizingReliable = "4px" === (e.getComputedStyle(d, null) || {
                width: "4px"
            }).width,
            n = d.appendChild(G.createElement("div")),
            n.style.cssText = d.style.cssText = s,
            n.style.marginRight = n.style.width = "0",
            d.style.width = "1px",
            t.reliableMarginRight = !parseFloat((e.getComputedStyle(n, null) || {}).marginRight)),
            typeof d.style.zoom !== V && (d.innerHTML = "",
            d.style.cssText = s + "width:1px;padding:1px;display:inline;zoom:1",
            t.inlineBlockNeedsLayout = 3 === d.offsetWidth,
            d.style.display = "block",
            d.innerHTML = "<div></div>",
            d.firstChild.style.width = "5px",
            t.shrinkWrapBlocks = 3 !== d.offsetWidth,
            t.inlineBlockNeedsLayout && (r.style.zoom = 1)),
            r.removeChild(i),
            i = d = o = n = null)
        }),
        i = s = r = a = n = o = null,
        t
    }({});
    var be = /(?:\{[\s\S]*\}|\[[\s\S]*\])$/
      , xe = /([A-Z])/g;
    ue.extend({
        cache: {},
        noData: {
            applet: !0,
            embed: !0,
            object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
        },
        hasData: function(e) {
            return !!(e = e.nodeType ? ue.cache[e[ue.expando]] : e[ue.expando]) && !a(e)
        },
        data: function(e, t, i) {
            return o(e, t, i)
        },
        removeData: function(e, t) {
            return s(e, t)
        },
        _data: function(e, t, i) {
            return o(e, t, i, !0)
        },
        _removeData: function(e, t) {
            return s(e, t, !0)
        },
        acceptData: function(e) {
            if (e.nodeType && 1 !== e.nodeType && 9 !== e.nodeType)
                return !1;
            var t = e.nodeName && ue.noData[e.nodeName.toLowerCase()];
            return !t || !0 !== t && e.getAttribute("classid") === t
        }
    }),
    ue.fn.extend({
        data: function(e, i) {
            var n, o, s = null, a = 0, l = this[0];
            if (e === t) {
                if (this.length && (s = ue.data(l),
                1 === l.nodeType && !ue._data(l, "parsedAttrs"))) {
                    for (n = l.attributes; n.length > a; a++)
                        o = n[a].name,
                        0 === o.indexOf("data-") && (o = ue.camelCase(o.slice(5)),
                        r(l, o, s[o]));
                    ue._data(l, "parsedAttrs", !0)
                }
                return s
            }
            return "object" == typeof e ? this.each(function() {
                ue.data(this, e)
            }) : arguments.length > 1 ? this.each(function() {
                ue.data(this, e, i)
            }) : l ? r(l, e, ue.data(l, e)) : null
        },
        removeData: function(e) {
            return this.each(function() {
                ue.removeData(this, e)
            })
        }
    }),
    ue.extend({
        queue: function(e, i, n) {
            var o;
            return e ? (i = (i || "fx") + "queue",
            o = ue._data(e, i),
            n && (!o || ue.isArray(n) ? o = ue._data(e, i, ue.makeArray(n)) : o.push(n)),
            o || []) : t
        },
        dequeue: function(e, t) {
            t = t || "fx";
            var i = ue.queue(e, t)
              , n = i.length
              , o = i.shift()
              , s = ue._queueHooks(e, t)
              , r = function() {
                ue.dequeue(e, t)
            };
            "inprogress" === o && (o = i.shift(),
            n--),
            o && ("fx" === t && i.unshift("inprogress"),
            delete s.stop,
            o.call(e, r, s)),
            !n && s && s.empty.fire()
        },
        _queueHooks: function(e, t) {
            var i = t + "queueHooks";
            return ue._data(e, i) || ue._data(e, i, {
                empty: ue.Callbacks("once memory").add(function() {
                    ue._removeData(e, t + "queue"),
                    ue._removeData(e, i)
                })
            })
        }
    }),
    ue.fn.extend({
        queue: function(e, i) {
            var n = 2;
            return "string" != typeof e && (i = e,
            e = "fx",
            n--),
            n > arguments.length ? ue.queue(this[0], e) : i === t ? this : this.each(function() {
                var t = ue.queue(this, e, i);
                ue._queueHooks(this, e),
                "fx" === e && "inprogress" !== t[0] && ue.dequeue(this, e)
            })
        },
        dequeue: function(e) {
            return this.each(function() {
                ue.dequeue(this, e)
            })
        },
        delay: function(e, t) {
            return e = ue.fx ? ue.fx.speeds[e] || e : e,
            t = t || "fx",
            this.queue(t, function(t, i) {
                var n = setTimeout(t, e);
                i.stop = function() {
                    clearTimeout(n)
                }
            })
        },
        clearQueue: function(e) {
            return this.queue(e || "fx", [])
        },
        promise: function(e, i) {
            var n, o = 1, s = ue.Deferred(), r = this, a = this.length, l = function() {
                --o || s.resolveWith(r, [r])
            };
            for ("string" != typeof e && (i = e,
            e = t),
            e = e || "fx"; a--; )
                (n = ue._data(r[a], e + "queueHooks")) && n.empty && (o++,
                n.empty.add(l));
            return l(),
            s.promise(i)
        }
    });
    var Se, Ce, ke = /[\t\r\n\f]/g, Te = /^(?:input|select|textarea|button|object)$/i, _e = /^(?:a|area)$/i, $e = /^(?:checked|selected)$/i, Ee = ue.support.getSetAttribute, De = ue.support.input;
    ue.fn.extend({
        attr: function(e, t) {
            return ue.access(this, ue.attr, e, t, arguments.length > 1)
        },
        removeAttr: function(e) {
            return this.each(function() {
                ue.removeAttr(this, e)
            })
        },
        prop: function(e, t) {
            return ue.access(this, ue.prop, e, t, arguments.length > 1)
        },
        removeProp: function(e) {
            return e = ue.propFix[e] || e,
            this.each(function() {
                try {
                    this[e] = t,
                    delete this[e]
                } catch (e) {}
            })
        },
        addClass: function(e) {
            var t, i, n, o, s, r = 0, a = this.length, l = "string" == typeof e && e;
            if (ue.isFunction(e))
                return this.each(function(t) {
                    ue(this).addClass(e.call(this, t, this.className))
                });
            if (l)
                for (t = (e || "").match(pe) || []; a > r; r++)
                    if (i = this[r],
                    n = 1 === i.nodeType && (i.className ? (" " + i.className + " ").replace(ke, " ") : " ")) {
                        for (s = 0; o = t[s++]; )
                            0 > n.indexOf(" " + o + " ") && (n += o + " ");
                        i.className = ue.trim(n)
                    }
            return this
        },
        removeClass: function(e) {
            var t, i, n, o, s, r = 0, a = this.length, l = 0 === arguments.length || "string" == typeof e && e;
            if (ue.isFunction(e))
                return this.each(function(t) {
                    ue(this).removeClass(e.call(this, t, this.className))
                });
            if (l)
                for (t = (e || "").match(pe) || []; a > r; r++)
                    if (i = this[r],
                    n = 1 === i.nodeType && (i.className ? (" " + i.className + " ").replace(ke, " ") : "")) {
                        for (s = 0; o = t[s++]; )
                            for (; n.indexOf(" " + o + " ") >= 0; )
                                n = n.replace(" " + o + " ", " ");
                        i.className = e ? ue.trim(n) : ""
                    }
            return this
        },
        toggleClass: function(e, t) {
            var i = typeof e;
            return "boolean" == typeof t && "string" === i ? t ? this.addClass(e) : this.removeClass(e) : ue.isFunction(e) ? this.each(function(i) {
                ue(this).toggleClass(e.call(this, i, this.className, t), t)
            }) : this.each(function() {
                if ("string" === i)
                    for (var t, n = 0, o = ue(this), s = e.match(pe) || []; t = s[n++]; )
                        o.hasClass(t) ? o.removeClass(t) : o.addClass(t);
                else
                    (i === V || "boolean" === i) && (this.className && ue._data(this, "__className__", this.className),
                    this.className = this.className || !1 === e ? "" : ue._data(this, "__className__") || "")
            })
        },
        hasClass: function(e) {
            for (var t = " " + e + " ", i = 0, n = this.length; n > i; i++)
                if (1 === this[i].nodeType && (" " + this[i].className + " ").replace(ke, " ").indexOf(t) >= 0)
                    return !0;
            return !1
        },
        val: function(e) {
            var i, n, o, s = this[0];
            return arguments.length ? (o = ue.isFunction(e),
            this.each(function(i) {
                var s;
                1 === this.nodeType && (s = o ? e.call(this, i, ue(this).val()) : e,
                null == s ? s = "" : "number" == typeof s ? s += "" : ue.isArray(s) && (s = ue.map(s, function(e) {
                    return null == e ? "" : e + ""
                })),
                (n = ue.valHooks[this.type] || ue.valHooks[this.nodeName.toLowerCase()]) && "set"in n && n.set(this, s, "value") !== t || (this.value = s))
            })) : s ? (n = ue.valHooks[s.type] || ue.valHooks[s.nodeName.toLowerCase()],
            n && "get"in n && (i = n.get(s, "value")) !== t ? i : (i = s.value,
            "string" == typeof i ? i.replace(/\r/g, "") : null == i ? "" : i)) : void 0
        }
    }),
    ue.extend({
        valHooks: {
            option: {
                get: function(e) {
                    var t = ue.find.attr(e, "value");
                    return null != t ? t : e.text
                }
            },
            select: {
                get: function(e) {
                    for (var t, i, n = e.options, o = e.selectedIndex, s = "select-one" === e.type || 0 > o, r = s ? null : [], a = s ? o + 1 : n.length, l = 0 > o ? a : s ? o : 0; a > l; l++)
                        if (i = n[l],
                        !(!i.selected && l !== o || (ue.support.optDisabled ? i.disabled : null !== i.getAttribute("disabled")) || i.parentNode.disabled && ue.nodeName(i.parentNode, "optgroup"))) {
                            if (t = ue(i).val(),
                            s)
                                return t;
                            r.push(t)
                        }
                    return r
                },
                set: function(e, t) {
                    for (var i, n, o = e.options, s = ue.makeArray(t), r = o.length; r--; )
                        n = o[r],
                        (n.selected = ue.inArray(ue(n).val(), s) >= 0) && (i = !0);
                    return i || (e.selectedIndex = -1),
                    s
                }
            }
        },
        attr: function(e, i, n) {
            var o, s, r = e.nodeType;
            if (e && 3 !== r && 8 !== r && 2 !== r)
                return typeof e.getAttribute === V ? ue.prop(e, i, n) : (1 === r && ue.isXMLDoc(e) || (i = i.toLowerCase(),
                o = ue.attrHooks[i] || (ue.expr.match.bool.test(i) ? Ce : Se)),
                n === t ? o && "get"in o && null !== (s = o.get(e, i)) ? s : (s = ue.find.attr(e, i),
                null == s ? t : s) : null !== n ? o && "set"in o && (s = o.set(e, n, i)) !== t ? s : (e.setAttribute(i, n + ""),
                n) : (ue.removeAttr(e, i),
                t))
        },
        removeAttr: function(e, t) {
            var i, n, o = 0, s = t && t.match(pe);
            if (s && 1 === e.nodeType)
                for (; i = s[o++]; )
                    n = ue.propFix[i] || i,
                    ue.expr.match.bool.test(i) ? De && Ee || !$e.test(i) ? e[n] = !1 : e[ue.camelCase("default-" + i)] = e[n] = !1 : ue.attr(e, i, ""),
                    e.removeAttribute(Ee ? i : n)
        },
        attrHooks: {
            type: {
                set: function(e, t) {
                    if (!ue.support.radioValue && "radio" === t && ue.nodeName(e, "input")) {
                        var i = e.value;
                        return e.setAttribute("type", t),
                        i && (e.value = i),
                        t
                    }
                }
            }
        },
        propFix: {
            for: "htmlFor",
            class: "className"
        },
        prop: function(e, i, n) {
            var o, s, r, a = e.nodeType;
            if (e && 3 !== a && 8 !== a && 2 !== a)
                return r = 1 !== a || !ue.isXMLDoc(e),
                r && (i = ue.propFix[i] || i,
                s = ue.propHooks[i]),
                n !== t ? s && "set"in s && (o = s.set(e, n, i)) !== t ? o : e[i] = n : s && "get"in s && null !== (o = s.get(e, i)) ? o : e[i]
        },
        propHooks: {
            tabIndex: {
                get: function(e) {
                    var t = ue.find.attr(e, "tabindex");
                    return t ? parseInt(t, 10) : Te.test(e.nodeName) || _e.test(e.nodeName) && e.href ? 0 : -1
                }
            }
        }
    }),
    Ce = {
        set: function(e, t, i) {
            return !1 === t ? ue.removeAttr(e, i) : De && Ee || !$e.test(i) ? e.setAttribute(!Ee && ue.propFix[i] || i, i) : e[ue.camelCase("default-" + i)] = e[i] = !0,
            i
        }
    },
    ue.each(ue.expr.match.bool.source.match(/\w+/g), function(e, i) {
        var n = ue.expr.attrHandle[i] || ue.find.attr;
        ue.expr.attrHandle[i] = De && Ee || !$e.test(i) ? function(e, i, o) {
            var s = ue.expr.attrHandle[i]
              , r = o ? t : (ue.expr.attrHandle[i] = t) != n(e, i, o) ? i.toLowerCase() : null;
            return ue.expr.attrHandle[i] = s,
            r
        }
        : function(e, i, n) {
            return n ? t : e[ue.camelCase("default-" + i)] ? i.toLowerCase() : null
        }
    }),
    De && Ee || (ue.attrHooks.value = {
        set: function(e, i, n) {
            return ue.nodeName(e, "input") ? (e.defaultValue = i,
            t) : Se && Se.set(e, i, n)
        }
    }),
    Ee || (Se = {
        set: function(e, i, n) {
            var o = e.getAttributeNode(n);
            return o || e.setAttributeNode(o = e.ownerDocument.createAttribute(n)),
            o.value = i += "",
            "value" === n || i === e.getAttribute(n) ? i : t
        }
    },
    ue.expr.attrHandle.id = ue.expr.attrHandle.name = ue.expr.attrHandle.coords = function(e, i, n) {
        var o;
        return n ? t : (o = e.getAttributeNode(i)) && "" !== o.value ? o.value : null
    }
    ,
    ue.valHooks.button = {
        get: function(e, i) {
            var n = e.getAttributeNode(i);
            return n && n.specified ? n.value : t
        },
        set: Se.set
    },
    ue.attrHooks.contenteditable = {
        set: function(e, t, i) {
            Se.set(e, "" !== t && t, i)
        }
    },
    ue.each(["width", "height"], function(e, i) {
        ue.attrHooks[i] = {
            set: function(e, n) {
                return "" === n ? (e.setAttribute(i, "auto"),
                n) : t
            }
        }
    })),
    ue.support.hrefNormalized || ue.each(["href", "src"], function(e, t) {
        ue.propHooks[t] = {
            get: function(e) {
                return e.getAttribute(t, 4)
            }
        }
    }),
    ue.support.style || (ue.attrHooks.style = {
        get: function(e) {
            return e.style.cssText || t
        },
        set: function(e, t) {
            return e.style.cssText = t + ""
        }
    }),
    ue.support.optSelected || (ue.propHooks.selected = {
        get: function(e) {
            var t = e.parentNode;
            return t && (t.selectedIndex,
            t.parentNode && t.parentNode.selectedIndex),
            null
        }
    }),
    ue.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        ue.propFix[this.toLowerCase()] = this
    }),
    ue.support.enctype || (ue.propFix.enctype = "encoding"),
    ue.each(["radio", "checkbox"], function() {
        ue.valHooks[this] = {
            set: function(e, i) {
                return ue.isArray(i) ? e.checked = ue.inArray(ue(e).val(), i) >= 0 : t
            }
        },
        ue.support.checkOn || (ue.valHooks[this].get = function(e) {
            return null === e.getAttribute("value") ? "on" : e.value
        }
        )
    });
    var Ae = /^(?:input|select|textarea)$/i
      , Me = /^key/
      , Ne = /^(?:mouse|contextmenu)|click/
      , Oe = /^(?:focusinfocus|focusoutblur)$/
      , Le = /^([^.]*)(?:\.(.+)|)$/;
    ue.event = {
        global: {},
        add: function(e, i, n, o, s) {
            var r, a, l, c, u, d, p, h, f, m, g, v = ue._data(e);
            if (v) {
                for (n.handler && (c = n,
                n = c.handler,
                s = c.selector),
                n.guid || (n.guid = ue.guid++),
                (a = v.events) || (a = v.events = {}),
                (d = v.handle) || (d = v.handle = function(e) {
                    return typeof ue === V || e && ue.event.triggered === e.type ? t : ue.event.dispatch.apply(d.elem, arguments)
                }
                ,
                d.elem = e),
                i = (i || "").match(pe) || [""],
                l = i.length; l--; )
                    r = Le.exec(i[l]) || [],
                    f = g = r[1],
                    m = (r[2] || "").split(".").sort(),
                    f && (u = ue.event.special[f] || {},
                    f = (s ? u.delegateType : u.bindType) || f,
                    u = ue.event.special[f] || {},
                    p = ue.extend({
                        type: f,
                        origType: g,
                        data: o,
                        handler: n,
                        guid: n.guid,
                        selector: s,
                        needsContext: s && ue.expr.match.needsContext.test(s),
                        namespace: m.join(".")
                    }, c),
                    (h = a[f]) || (h = a[f] = [],
                    h.delegateCount = 0,
                    u.setup && !1 !== u.setup.call(e, o, m, d) || (e.addEventListener ? e.addEventListener(f, d, !1) : e.attachEvent && e.attachEvent("on" + f, d))),
                    u.add && (u.add.call(e, p),
                    p.handler.guid || (p.handler.guid = n.guid)),
                    s ? h.splice(h.delegateCount++, 0, p) : h.push(p),
                    ue.event.global[f] = !0);
                e = null
            }
        },
        remove: function(e, t, i, n, o) {
            var s, r, a, l, c, u, d, p, h, f, m, g = ue.hasData(e) && ue._data(e);
            if (g && (u = g.events)) {
                for (t = (t || "").match(pe) || [""],
                c = t.length; c--; )
                    if (a = Le.exec(t[c]) || [],
                    h = m = a[1],
                    f = (a[2] || "").split(".").sort(),
                    h) {
                        for (d = ue.event.special[h] || {},
                        h = (n ? d.delegateType : d.bindType) || h,
                        p = u[h] || [],
                        a = a[2] && RegExp("(^|\\.)" + f.join("\\.(?:.*\\.|)") + "(\\.|$)"),
                        l = s = p.length; s--; )
                            r = p[s],
                            !o && m !== r.origType || i && i.guid !== r.guid || a && !a.test(r.namespace) || n && n !== r.selector && ("**" !== n || !r.selector) || (p.splice(s, 1),
                            r.selector && p.delegateCount--,
                            d.remove && d.remove.call(e, r));
                        l && !p.length && (d.teardown && !1 !== d.teardown.call(e, f, g.handle) || ue.removeEvent(e, h, g.handle),
                        delete u[h])
                    } else
                        for (h in u)
                            ue.event.remove(e, h + t[c], i, n, !0);
                ue.isEmptyObject(u) && (delete g.handle,
                ue._removeData(e, "events"))
            }
        },
        trigger: function(i, n, o, s) {
            var r, a, l, c, u, d, p, h = [o || G], f = le.call(i, "type") ? i.type : i, m = le.call(i, "namespace") ? i.namespace.split(".") : [];
            if (l = d = o = o || G,
            3 !== o.nodeType && 8 !== o.nodeType && !Oe.test(f + ue.event.triggered) && (f.indexOf(".") >= 0 && (m = f.split("."),
            f = m.shift(),
            m.sort()),
            a = 0 > f.indexOf(":") && "on" + f,
            i = i[ue.expando] ? i : new ue.Event(f,"object" == typeof i && i),
            i.isTrigger = s ? 2 : 3,
            i.namespace = m.join("."),
            i.namespace_re = i.namespace ? RegExp("(^|\\.)" + m.join("\\.(?:.*\\.|)") + "(\\.|$)") : null,
            i.result = t,
            i.target || (i.target = o),
            n = null == n ? [i] : ue.makeArray(n, [i]),
            u = ue.event.special[f] || {},
            s || !u.trigger || !1 !== u.trigger.apply(o, n))) {
                if (!s && !u.noBubble && !ue.isWindow(o)) {
                    for (c = u.delegateType || f,
                    Oe.test(c + f) || (l = l.parentNode); l; l = l.parentNode)
                        h.push(l),
                        d = l;
                    d === (o.ownerDocument || G) && h.push(d.defaultView || d.parentWindow || e)
                }
                for (p = 0; (l = h[p++]) && !i.isPropagationStopped(); )
                    i.type = p > 1 ? c : u.bindType || f,
                    r = (ue._data(l, "events") || {})[i.type] && ue._data(l, "handle"),
                    r && r.apply(l, n),
                    (r = a && l[a]) && ue.acceptData(l) && r.apply && !1 === r.apply(l, n) && i.preventDefault();
                if (i.type = f,
                !s && !i.isDefaultPrevented() && (!u._default || !1 === u._default.apply(h.pop(), n)) && ue.acceptData(o) && a && o[f] && !ue.isWindow(o)) {
                    d = o[a],
                    d && (o[a] = null),
                    ue.event.triggered = f;
                    try {
                        o[f]()
                    } catch (e) {}
                    ue.event.triggered = t,
                    d && (o[a] = d)
                }
                return i.result
            }
        },
        dispatch: function(e) {
            e = ue.event.fix(e);
            var i, n, o, s, r, a = [], l = se.call(arguments), c = (ue._data(this, "events") || {})[e.type] || [], u = ue.event.special[e.type] || {};
            if (l[0] = e,
            e.delegateTarget = this,
            !u.preDispatch || !1 !== u.preDispatch.call(this, e)) {
                for (a = ue.event.handlers.call(this, e, c),
                i = 0; (s = a[i++]) && !e.isPropagationStopped(); )
                    for (e.currentTarget = s.elem,
                    r = 0; (o = s.handlers[r++]) && !e.isImmediatePropagationStopped(); )
                        (!e.namespace_re || e.namespace_re.test(o.namespace)) && (e.handleObj = o,
                        e.data = o.data,
                        (n = ((ue.event.special[o.origType] || {}).handle || o.handler).apply(s.elem, l)) !== t && !1 === (e.result = n) && (e.preventDefault(),
                        e.stopPropagation()));
                return u.postDispatch && u.postDispatch.call(this, e),
                e.result
            }
        },
        handlers: function(e, i) {
            var n, o, s, r, a = [], l = i.delegateCount, c = e.target;
            if (l && c.nodeType && (!e.button || "click" !== e.type))
                for (; c != this; c = c.parentNode || this)
                    if (1 === c.nodeType && (!0 !== c.disabled || "click" !== e.type)) {
                        for (s = [],
                        r = 0; l > r; r++)
                            o = i[r],
                            n = o.selector + " ",
                            s[n] === t && (s[n] = o.needsContext ? ue(n, this).index(c) >= 0 : ue.find(n, this, null, [c]).length),
                            s[n] && s.push(o);
                        s.length && a.push({
                            elem: c,
                            handlers: s
                        })
                    }
            return i.length > l && a.push({
                elem: this,
                handlers: i.slice(l)
            }),
            a
        },
        fix: function(e) {
            if (e[ue.expando])
                return e;
            var t, i, n, o = e.type, s = e, r = this.fixHooks[o];
            for (r || (this.fixHooks[o] = r = Ne.test(o) ? this.mouseHooks : Me.test(o) ? this.keyHooks : {}),
            n = r.props ? this.props.concat(r.props) : this.props,
            e = new ue.Event(s),
            t = n.length; t--; )
                i = n[t],
                e[i] = s[i];
            return e.target || (e.target = s.srcElement || G),
            3 === e.target.nodeType && (e.target = e.target.parentNode),
            e.metaKey = !!e.metaKey,
            r.filter ? r.filter(e, s) : e
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function(e, t) {
                return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode),
                e
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function(e, i) {
                var n, o, s, r = i.button, a = i.fromElement;
                return null == e.pageX && null != i.clientX && (o = e.target.ownerDocument || G,
                s = o.documentElement,
                n = o.body,
                e.pageX = i.clientX + (s && s.scrollLeft || n && n.scrollLeft || 0) - (s && s.clientLeft || n && n.clientLeft || 0),
                e.pageY = i.clientY + (s && s.scrollTop || n && n.scrollTop || 0) - (s && s.clientTop || n && n.clientTop || 0)),
                !e.relatedTarget && a && (e.relatedTarget = a === e.target ? i.toElement : a),
                e.which || r === t || (e.which = 1 & r ? 1 : 2 & r ? 3 : 4 & r ? 2 : 0),
                e
            }
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    if (this !== u() && this.focus)
                        try {
                            return this.focus(),
                            !1
                        } catch (e) {}
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    return this === u() && this.blur ? (this.blur(),
                    !1) : t
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    return ue.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(),
                    !1) : t
                },
                _default: function(e) {
                    return ue.nodeName(e.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(e) {
                    e.result !== t && (e.originalEvent.returnValue = e.result)
                }
            }
        },
        simulate: function(e, t, i, n) {
            var o = ue.extend(new ue.Event, i, {
                type: e,
                isSimulated: !0,
                originalEvent: {}
            });
            n ? ue.event.trigger(o, null, t) : ue.event.dispatch.call(t, o),
            o.isDefaultPrevented() && i.preventDefault()
        }
    },
    ue.removeEvent = G.removeEventListener ? function(e, t, i) {
        e.removeEventListener && e.removeEventListener(t, i, !1)
    }
    : function(e, t, i) {
        var n = "on" + t;
        e.detachEvent && (typeof e[n] === V && (e[n] = null),
        e.detachEvent(n, i))
    }
    ,
    ue.Event = function(e, i) {
        return this instanceof ue.Event ? (e && e.type ? (this.originalEvent = e,
        this.type = e.type,
        this.isDefaultPrevented = e.defaultPrevented || !1 === e.returnValue || e.getPreventDefault && e.getPreventDefault() ? l : c) : this.type = e,
        i && ue.extend(this, i),
        this.timeStamp = e && e.timeStamp || ue.now(),
        this[ue.expando] = !0,
        t) : new ue.Event(e,i)
    }
    ,
    ue.Event.prototype = {
        isDefaultPrevented: c,
        isPropagationStopped: c,
        isImmediatePropagationStopped: c,
        preventDefault: function() {
            var e = this.originalEvent;
            this.isDefaultPrevented = l,
            e && (e.preventDefault ? e.preventDefault() : e.returnValue = !1)
        },
        stopPropagation: function() {
            var e = this.originalEvent;
            this.isPropagationStopped = l,
            e && (e.stopPropagation && e.stopPropagation(),
            e.cancelBubble = !0)
        },
        stopImmediatePropagation: function() {
            this.isImmediatePropagationStopped = l,
            this.stopPropagation()
        }
    },
    ue.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout"
    }, function(e, t) {
        ue.event.special[e] = {
            delegateType: t,
            bindType: t,
            handle: function(e) {
                var i, n = this, o = e.relatedTarget, s = e.handleObj;
                return (!o || o !== n && !ue.contains(n, o)) && (e.type = s.origType,
                i = s.handler.apply(this, arguments),
                e.type = t),
                i
            }
        }
    }),
    ue.support.submitBubbles || (ue.event.special.submit = {
        setup: function() {
            return !ue.nodeName(this, "form") && (ue.event.add(this, "click._submit keypress._submit", function(e) {
                var i = e.target
                  , n = ue.nodeName(i, "input") || ue.nodeName(i, "button") ? i.form : t;
                n && !ue._data(n, "submitBubbles") && (ue.event.add(n, "submit._submit", function(e) {
                    e._submit_bubble = !0
                }),
                ue._data(n, "submitBubbles", !0))
            }),
            t)
        },
        postDispatch: function(e) {
            e._submit_bubble && (delete e._submit_bubble,
            this.parentNode && !e.isTrigger && ue.event.simulate("submit", this.parentNode, e, !0))
        },
        teardown: function() {
            return !ue.nodeName(this, "form") && (ue.event.remove(this, "._submit"),
            t)
        }
    }),
    ue.support.changeBubbles || (ue.event.special.change = {
        setup: function() {
            return Ae.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (ue.event.add(this, "propertychange._change", function(e) {
                "checked" === e.originalEvent.propertyName && (this._just_changed = !0)
            }),
            ue.event.add(this, "click._change", function(e) {
                this._just_changed && !e.isTrigger && (this._just_changed = !1),
                ue.event.simulate("change", this, e, !0)
            })),
            !1) : (ue.event.add(this, "beforeactivate._change", function(e) {
                var t = e.target;
                Ae.test(t.nodeName) && !ue._data(t, "changeBubbles") && (ue.event.add(t, "change._change", function(e) {
                    !this.parentNode || e.isSimulated || e.isTrigger || ue.event.simulate("change", this.parentNode, e, !0)
                }),
                ue._data(t, "changeBubbles", !0))
            }),
            t)
        },
        handle: function(e) {
            var i = e.target;
            return this !== i || e.isSimulated || e.isTrigger || "radio" !== i.type && "checkbox" !== i.type ? e.handleObj.handler.apply(this, arguments) : t
        },
        teardown: function() {
            return ue.event.remove(this, "._change"),
            !Ae.test(this.nodeName)
        }
    }),
    /*ue.support.focusinBubbles || ue.each({
        focus: "focusin",
        blur: "focusout"
    }, function(e, t) {
        var i = 0
          , n = function(e) {
            ue.event.simulate(t, e.target, ue.event.fix(e), !0)
        };
        ue.event.special[t] = {
            setup: function() {
                0 == i++ && G.addEventListener(e, n, !0)
            },
            teardown: function() {
                0 == --i && G.removeEventListener(e, n, !0)
            }
        }
    }),*/
    ue.fn.extend({
        on: function(e, i, n, o, s) {
            var r, a;
            if ("object" == typeof e) {
                "string" != typeof i && (n = n || i,
                i = t);
                for (r in e)
                    this.on(r, i, n, e[r], s);
                return this
            }
            if (null == n && null == o ? (o = i,
            n = i = t) : null == o && ("string" == typeof i ? (o = n,
            n = t) : (o = n,
            n = i,
            i = t)),
            !1 === o)
                o = c;
            else if (!o)
                return this;
            return 1 === s && (a = o,
            o = function(e) {
                return ue().off(e),
                a.apply(this, arguments)
            }
            ,
            o.guid = a.guid || (a.guid = ue.guid++)),
            this.each(function() {
                ue.event.add(this, e, o, n, i)
            })
        },
        one: function(e, t, i, n) {
            return this.on(e, t, i, n, 1)
        },
        off: function(e, i, n) {
            var o, s;
            if (e && e.preventDefault && e.handleObj)
                return o = e.handleObj,
                ue(e.delegateTarget).off(o.namespace ? o.origType + "." + o.namespace : o.origType, o.selector, o.handler),
                this;
            if ("object" == typeof e) {
                for (s in e)
                    this.off(s, i, e[s]);
                return this
            }
            return (!1 === i || "function" == typeof i) && (n = i,
            i = t),
            !1 === n && (n = c),
            this.each(function() {
                ue.event.remove(this, e, n, i)
            })
        },
        trigger: function(e, t) {
            return this.each(function() {
                ue.event.trigger(e, t, this)
            })
        },
        triggerHandler: function(e, i) {
            var n = this[0];
            return n ? ue.event.trigger(e, i, n, !0) : t
        }
    });
    var He = /^.[^:#\[\.,]*$/
      , je = /^(?:parents|prev(?:Until|All))/
      , Be = ue.expr.match.needsContext
      , Pe = {
        children: !0,
        contents: !0,
        next: !0,
        prev: !0
    };
    ue.fn.extend({
        find: function(e) {
            var t, i = [], n = this, o = n.length;
            if ("string" != typeof e)
                return this.pushStack(ue(e).filter(function() {
                    for (t = 0; o > t; t++)
                        if (ue.contains(n[t], this))
                            return !0
                }));
            for (t = 0; o > t; t++)
                ue.find(e, n[t], i);
            return i = this.pushStack(o > 1 ? ue.unique(i) : i),
            i.selector = this.selector ? this.selector + " " + e : e,
            i
        },
        has: function(e) {
            var t, i = ue(e, this), n = i.length;
            return this.filter(function() {
                for (t = 0; n > t; t++)
                    if (ue.contains(this, i[t]))
                        return !0
            })
        },
        not: function(e) {
            return this.pushStack(p(this, e || [], !0))
        },
        filter: function(e) {
            return this.pushStack(p(this, e || [], !1))
        },
        is: function(e) {
            return !!p(this, "string" == typeof e && Be.test(e) ? ue(e) : e || [], !1).length
        },
        closest: function(e, t) {
            for (var i, n = 0, o = this.length, s = [], r = Be.test(e) || "string" != typeof e ? ue(e, t || this.context) : 0; o > n; n++)
                for (i = this[n]; i && i !== t; i = i.parentNode)
                    if (11 > i.nodeType && (r ? r.index(i) > -1 : 1 === i.nodeType && ue.find.matchesSelector(i, e))) {
                        i = s.push(i);
                        break
                    }
            return this.pushStack(s.length > 1 ? ue.unique(s) : s)
        },
        index: function(e) {
            return e ? "string" == typeof e ? ue.inArray(this[0], ue(e)) : ue.inArray(e.jquery ? e[0] : e, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(e, t) {
            var i = "string" == typeof e ? ue(e, t) : ue.makeArray(e && e.nodeType ? [e] : e)
              , n = ue.merge(this.get(), i);
            return this.pushStack(ue.unique(n))
        },
        addBack: function(e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }
    }),
    ue.each({
        parent: function(e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null
        },
        parents: function(e) {
            return ue.dir(e, "parentNode")
        },
        parentsUntil: function(e, t, i) {
            return ue.dir(e, "parentNode", i)
        },
        next: function(e) {
            return d(e, "nextSibling")
        },
        prev: function(e) {
            return d(e, "previousSibling")
        },
        nextAll: function(e) {
            return ue.dir(e, "nextSibling")
        },
        prevAll: function(e) {
            return ue.dir(e, "previousSibling")
        },
        nextUntil: function(e, t, i) {
            return ue.dir(e, "nextSibling", i)
        },
        prevUntil: function(e, t, i) {
            return ue.dir(e, "previousSibling", i)
        },
        siblings: function(e) {
            return ue.sibling((e.parentNode || {}).firstChild, e)
        },
        children: function(e) {
            return ue.sibling(e.firstChild)
        },
        contents: function(e) {
            return ue.nodeName(e, "iframe") ? e.contentDocument || e.contentWindow.document : ue.merge([], e.childNodes)
        }
    }, function(e, t) {
        ue.fn[e] = function(i, n) {
            var o = ue.map(this, t, i);
            return "Until" !== e.slice(-5) && (n = i),
            n && "string" == typeof n && (o = ue.filter(n, o)),
            this.length > 1 && (Pe[e] || (o = ue.unique(o)),
            je.test(e) && (o = o.reverse())),
            this.pushStack(o)
        }
    }),
    ue.extend({
        filter: function(e, t, i) {
            var n = t[0];
            return i && (e = ":not(" + e + ")"),
            1 === t.length && 1 === n.nodeType ? ue.find.matchesSelector(n, e) ? [n] : [] : ue.find.matches(e, ue.grep(t, function(e) {
                return 1 === e.nodeType
            }))
        },
        dir: function(e, i, n) {
            for (var o = [], s = e[i]; s && 9 !== s.nodeType && (n === t || 1 !== s.nodeType || !ue(s).is(n)); )
                1 === s.nodeType && o.push(s),
                s = s[i];
            return o
        },
        sibling: function(e, t) {
            for (var i = []; e; e = e.nextSibling)
                1 === e.nodeType && e !== t && i.push(e);
            return i
        }
    });
    var Ie = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video"
      , ze = RegExp("<(?:" + Ie + ")[\\s/>]", "i")
      , We = /^\s+/
      , Re = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi
      , qe = /<([\w:]+)/
      , Fe = /<tbody/i
      , Xe = /<|&#?\w+;/
      , Ue = /<(?:script|style|link)/i
      , Ye = /^(?:checkbox|radio)$/i
      , Ve = /checked\s*(?:[^=]|=\s*.checked.)/i
      , Qe = /^$|\/(?:java|ecma)script/i
      , Ge = /^true\/(.*)/
      , Je = {
        option: [1, "<select multiple='multiple'>", "</select>"],
        legend: [1, "<fieldset>", "</fieldset>"],
        area: [1, "<map>", "</map>"],
        param: [1, "<object>", "</object>"],
        thead: [1, "<table>", "</table>"],
        tr: [2, "<table><tbody>", "</tbody></table>"],
        col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
        _default: ue.support.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
    }
      , Ke = h(G)
      , Ze = Ke.appendChild(G.createElement("div"));
    Je.optgroup = Je.option,
    Je.tbody = Je.tfoot = Je.colgroup = Je.caption = Je.thead,
    Je.th = Je.td,
    ue.fn.extend({
        text: function(e) {
            return ue.access(this, function(e) {
                return e === t ? ue.text(this) : this.empty().append((this[0] && this[0].ownerDocument || G).createTextNode(e))
            }, null, e, arguments.length)
        },
        append: function() {
            return this.domManip(arguments, function(e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    f(this, e).appendChild(e)
                }
            })
        },
        prepend: function() {
            return this.domManip(arguments, function(e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = f(this, e);
                    t.insertBefore(e, t.firstChild)
                }
            })
        },
        before: function() {
            return this.domManip(arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this)
            })
        },
        after: function() {
            return this.domManip(arguments, function(e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
            })
        },
        remove: function(e, t) {
            for (var i, n = e ? ue.filter(e, this) : this, o = 0; null != (i = n[o]); o++)
                t || 1 !== i.nodeType || ue.cleanData(b(i)),
                i.parentNode && (t && ue.contains(i.ownerDocument, i) && v(b(i, "script")),
                i.parentNode.removeChild(i));
            return this
        },
        empty: function() {
            for (var e, t = 0; null != (e = this[t]); t++) {
                for (1 === e.nodeType && ue.cleanData(b(e, !1)); e.firstChild; )
                    e.removeChild(e.firstChild);
                e.options && ue.nodeName(e, "select") && (e.options.length = 0)
            }
            return this
        },
        clone: function(e, t) {
            return e = null != e && e,
            t = null == t ? e : t,
            this.map(function() {
                return ue.clone(this, e, t)
            })
        },
        html: function(e) {
            return ue.access(this, function(e) {
                var i = this[0] || {}
                  , n = 0
                  , o = this.length;
                if (e === t)
                    return 1 === i.nodeType ? i.innerHTML.replace(/ jQuery\d+="(?:null|\d+)"/g, "") : t;
                if (!("string" != typeof e || Ue.test(e) || !ue.support.htmlSerialize && ze.test(e) || !ue.support.leadingWhitespace && We.test(e) || Je[(qe.exec(e) || ["", ""])[1].toLowerCase()])) {
                    e = e.replace(Re, "<$1></$2>");
                    try {
                        for (; o > n; n++)
                            i = this[n] || {},
                            1 === i.nodeType && (ue.cleanData(b(i, !1)),
                            i.innerHTML = e);
                        i = 0
                    } catch (e) {}
                }
                i && this.empty().append(e)
            }, null, e, arguments.length)
        },
        replaceWith: function() {
            var e = ue.map(this, function(e) {
                return [e.nextSibling, e.parentNode]
            })
              , t = 0;
            return this.domManip(arguments, function(i) {
                var n = e[t++]
                  , o = e[t++];
                o && (n && n.parentNode !== o && (n = this.nextSibling),
                ue(this).remove(),
                o.insertBefore(i, n))
            }, !0),
            t ? this : this.remove()
        },
        detach: function(e) {
            return this.remove(e, !0)
        },
        domManip: function(e, t, i) {
            e = ne.apply([], e);
            var n, o, s, r, a, l, c = 0, u = this.length, d = this, p = u - 1, h = e[0], f = ue.isFunction(h);
            if (f || !(1 >= u || "string" != typeof h || ue.support.checkClone) && Ve.test(h))
                return this.each(function(n) {
                    var o = d.eq(n);
                    f && (e[0] = h.call(this, n, o.html())),
                    o.domManip(e, t, i)
                });
            if (u && (l = ue.buildFragment(e, this[0].ownerDocument, !1, !i && this),
            n = l.firstChild,
            1 === l.childNodes.length && (l = n),
            n)) {
                for (r = ue.map(b(l, "script"), m),
                s = r.length; u > c; c++)
                    o = l,
                    c !== p && (o = ue.clone(o, !0, !0),
                    s && ue.merge(r, b(o, "script"))),
                    t.call(this[c], o, c);
                if (s)
                    for (a = r[r.length - 1].ownerDocument,
                    ue.map(r, g),
                    c = 0; s > c; c++)
                        o = r[c],
                        Qe.test(o.type || "") && !ue._data(o, "globalEval") && ue.contains(a, o) && (o.src ? ue._evalUrl(o.src) : ue.globalEval((o.text || o.textContent || o.innerHTML || "").replace(/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, "")));
                l = n = null
            }
            return this
        }
    }),
    ue.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(e, t) {
        ue.fn[e] = function(e) {
            for (var i, n = 0, o = [], s = ue(e), r = s.length - 1; r >= n; n++)
                i = n === r ? this : this.clone(!0),
                ue(s[n])[t](i),
                oe.apply(o, i.get());
            return this.pushStack(o)
        }
    }),
    ue.extend({
        clone: function(e, t, i) {
            var n, o, s, r, a, l = ue.contains(e.ownerDocument, e);
            if (ue.support.html5Clone || ue.isXMLDoc(e) || !ze.test("<" + e.nodeName + ">") ? s = e.cloneNode(!0) : (Ze.innerHTML = e.outerHTML,
            Ze.removeChild(s = Ze.firstChild)),
            !(ue.support.noCloneEvent && ue.support.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || ue.isXMLDoc(e)))
                for (n = b(s),
                a = b(e),
                r = 0; null != (o = a[r]); ++r)
                    n[r] && w(o, n[r]);
            if (t)
                if (i)
                    for (a = a || b(e),
                    n = n || b(s),
                    r = 0; null != (o = a[r]); r++)
                        y(o, n[r]);
                else
                    y(e, s);
            return n = b(s, "script"),
            n.length > 0 && v(n, !l && b(e, "script")),
            n = a = o = null,
            s
        },
        buildFragment: function(e, t, i, n) {
            for (var o, s, r, a, l, c, u, d = e.length, p = h(t), f = [], m = 0; d > m; m++)
                if ((s = e[m]) || 0 === s)
                    if ("object" === ue.type(s))
                        ue.merge(f, s.nodeType ? [s] : s);
                    else if (Xe.test(s)) {
                        for (a = a || p.appendChild(t.createElement("div")),
                        l = (qe.exec(s) || ["", ""])[1].toLowerCase(),
                        u = Je[l] || Je._default,
                        a.innerHTML = u[1] + s.replace(Re, "<$1></$2>") + u[2],
                        o = u[0]; o--; )
                            a = a.lastChild;
                        if (!ue.support.leadingWhitespace && We.test(s) && f.push(t.createTextNode(We.exec(s)[0])),
                        !ue.support.tbody)
                            for (s = "table" !== l || Fe.test(s) ? "<table>" !== u[1] || Fe.test(s) ? 0 : a : a.firstChild,
                            o = s && s.childNodes.length; o--; )
                                ue.nodeName(c = s.childNodes[o], "tbody") && !c.childNodes.length && s.removeChild(c);
                        for (ue.merge(f, a.childNodes),
                        a.textContent = ""; a.firstChild; )
                            a.removeChild(a.firstChild);
                        a = p.lastChild
                    } else
                        f.push(t.createTextNode(s));
            for (a && p.removeChild(a),
            ue.support.appendChecked || ue.grep(b(f, "input"), x),
            m = 0; s = f[m++]; )
                if ((!n || -1 === ue.inArray(s, n)) && (r = ue.contains(s.ownerDocument, s),
                a = b(p.appendChild(s), "script"),
                r && v(a),
                i))
                    for (o = 0; s = a[o++]; )
                        Qe.test(s.type || "") && i.push(s);
            return a = null,
            p
        },
        cleanData: function(e, t) {
            for (var i, n, o, s, r = 0, a = ue.expando, l = ue.cache, c = ue.support.deleteExpando, u = ue.event.special; null != (i = e[r]); r++)
                if ((t || ue.acceptData(i)) && (o = i[a],
                s = o && l[o])) {
                    if (s.events)
                        for (n in s.events)
                            u[n] ? ue.event.remove(i, n) : ue.removeEvent(i, n, s.handle);
                    l[o] && (delete l[o],
                    c ? delete i[a] : typeof i.removeAttribute !== V ? i.removeAttribute(a) : i[a] = null,
                    te.push(o))
                }
        },
        _evalUrl: function(e) {
            return ue.ajax({
                url: e,
                type: "GET",
                dataType: "script",
                async: !1,
                global: !1,
                throws: !0
            })
        }
    }),
    ue.fn.extend({
        wrapAll: function(e) {
            if (ue.isFunction(e))
                return this.each(function(t) {
                    ue(this).wrapAll(e.call(this, t))
                });
            if (this[0]) {
                var t = ue(e, this[0].ownerDocument).eq(0).clone(!0);
                this[0].parentNode && t.insertBefore(this[0]),
                t.map(function() {
                    for (var e = this; e.firstChild && 1 === e.firstChild.nodeType; )
                        e = e.firstChild;
                    return e
                }).append(this)
            }
            return this
        },
        wrapInner: function(e) {
            return ue.isFunction(e) ? this.each(function(t) {
                ue(this).wrapInner(e.call(this, t))
            }) : this.each(function() {
                var t = ue(this)
                  , i = t.contents();
                i.length ? i.wrapAll(e) : t.append(e)
            })
        },
        wrap: function(e) {
            var t = ue.isFunction(e);
            return this.each(function(i) {
                ue(this).wrapAll(t ? e.call(this, i) : e)
            })
        },
        unwrap: function() {
            return this.parent().each(function() {
                ue.nodeName(this, "body") || ue(this).replaceWith(this.childNodes)
            }).end()
        }
    });
    var et, tt, it, nt = /alpha\([^)]*\)/i, ot = /opacity\s*=\s*([^)]*)/, st = /^(top|right|bottom|left)$/, rt = /^(none|table(?!-c[ea]).+)/, at = /^margin/, lt = RegExp("^(" + de + ")(.*)$", "i"), ct = RegExp("^(" + de + ")(?!px)[a-z%]+$", "i"), ut = RegExp("^([+-])=(" + de + ")", "i"), dt = {
        BODY: "block"
    }, pt = {
        position: "absolute",
        visibility: "hidden",
        display: "block"
    }, ht = {
        letterSpacing: 0,
        fontWeight: 400
    }, ft = ["Top", "Right", "Bottom", "Left"], mt = ["Webkit", "O", "Moz", "ms"];
    ue.fn.extend({
        css: function(e, i) {
            return ue.access(this, function(e, i, n) {
                var o, s, r = {}, a = 0;
                if (ue.isArray(i)) {
                    for (s = tt(e),
                    o = i.length; o > a; a++)
                        r[i[a]] = ue.css(e, i[a], !1, s);
                    return r
                }
                return n !== t ? ue.style(e, i, n) : ue.css(e, i)
            }, e, i, arguments.length > 1)
        },
        show: function() {
            return k(this, !0)
        },
        hide: function() {
            return k(this)
        },
        toggle: function(e) {
            return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
                C(this) ? ue(this).show() : ue(this).hide()
            })
        }
    }),
    ue.extend({
        cssHooks: {
            opacity: {
                get: function(e, t) {
                    if (t) {
                        var i = it(e, "opacity");
                        return "" === i ? "1" : i
                    }
                }
            }
        },
        cssNumber: {
            columnCount: !0,
            fillOpacity: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            float: ue.support.cssFloat ? "cssFloat" : "styleFloat"
        },
        style: function(e, i, n, o) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var s, r, a, l = ue.camelCase(i), c = e.style;
                if (i = ue.cssProps[l] || (ue.cssProps[l] = S(c, l)),
                a = ue.cssHooks[i] || ue.cssHooks[l],
                n === t)
                    return a && "get"in a && (s = a.get(e, !1, o)) !== t ? s : c[i];
                if (r = typeof n,
                "string" === r && (s = ut.exec(n)) && (n = (s[1] + 1) * s[2] + parseFloat(ue.css(e, i)),
                r = "number"),
                !(null == n || "number" === r && isNaN(n) || ("number" !== r || ue.cssNumber[l] || (n += "px"),
                ue.support.clearCloneStyle || "" !== n || 0 !== i.indexOf("background") || (c[i] = "inherit"),
                a && "set"in a && (n = a.set(e, n, o)) === t)))
                    try {
                        c[i] = n
                    } catch (e) {}
            }
        },
        css: function(e, i, n, o) {
            var s, r, a, l = ue.camelCase(i);
            return i = ue.cssProps[l] || (ue.cssProps[l] = S(e.style, l)),
            a = ue.cssHooks[i] || ue.cssHooks[l],
            a && "get"in a && (r = a.get(e, !0, n)),
            r === t && (r = it(e, i, o)),
            "normal" === r && i in ht && (r = ht[i]),
            "" === n || n ? (s = parseFloat(r),
            !0 === n || ue.isNumeric(s) ? s || 0 : r) : r
        }
    }),
    e.getComputedStyle ? (tt = function(t) {
        return e.getComputedStyle(t, null)
    }
    ,
    it = function(e, i, n) {
        var o, s, r, a = n || tt(e), l = a ? a.getPropertyValue(i) || a[i] : t, c = e.style;
        return a && ("" !== l || ue.contains(e.ownerDocument, e) || (l = ue.style(e, i)),
        ct.test(l) && at.test(i) && (o = c.width,
        s = c.minWidth,
        r = c.maxWidth,
        c.minWidth = c.maxWidth = c.width = l,
        l = a.width,
        c.width = o,
        c.minWidth = s,
        c.maxWidth = r)),
        l
    }
    ) : G.documentElement.currentStyle && (tt = function(e) {
        return e.currentStyle
    }
    ,
    it = function(e, i, n) {
        var o, s, r, a = n || tt(e), l = a ? a[i] : t, c = e.style;
        return null == l && c && c[i] && (l = c[i]),
        ct.test(l) && !st.test(i) && (o = c.left,
        s = e.runtimeStyle,
        r = s && s.left,
        r && (s.left = e.currentStyle.left),
        c.left = "fontSize" === i ? "1em" : l,
        l = c.pixelLeft + "px",
        c.left = o,
        r && (s.left = r)),
        "" === l ? "auto" : l
    }
    ),
    ue.each(["height", "width"], function(e, i) {
        ue.cssHooks[i] = {
            get: function(e, n, o) {
                return n ? 0 === e.offsetWidth && rt.test(ue.css(e, "display")) ? ue.swap(e, pt, function() {
                    return $(e, i, o)
                }) : $(e, i, o) : t
            },
            set: function(e, t, n) {
                var o = n && tt(e);
                return T(e, t, n ? _(e, i, n, ue.support.boxSizing && "border-box" === ue.css(e, "boxSizing", !1, o), o) : 0)
            }
        }
    }),
    ue.support.opacity || (ue.cssHooks.opacity = {
        get: function(e, t) {
            return ot.test((t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : t ? "1" : ""
        },
        set: function(e, t) {
            var i = e.style
              , n = e.currentStyle
              , o = ue.isNumeric(t) ? "alpha(opacity=" + 100 * t + ")" : ""
              , s = n && n.filter || i.filter || "";
            i.zoom = 1,
            (t >= 1 || "" === t) && "" === ue.trim(s.replace(nt, "")) && i.removeAttribute && (i.removeAttribute("filter"),
            "" === t || n && !n.filter) || (i.filter = nt.test(s) ? s.replace(nt, o) : s + " " + o)
        }
    }),
    ue(function() {
        ue.support.reliableMarginRight || (ue.cssHooks.marginRight = {
            get: function(e, i) {
                return i ? ue.swap(e, {
                    display: "inline-block"
                }, it, [e, "marginRight"]) : t
            }
        }),
        !ue.support.pixelPosition && ue.fn.position && ue.each(["top", "left"], function(e, i) {
            ue.cssHooks[i] = {
                get: function(e, n) {
                    return n ? (n = it(e, i),
                    ct.test(n) ? ue(e).position()[i] + "px" : n) : t
                }
            }
        })
    }),
    ue.expr && ue.expr.filters && (ue.expr.filters.hidden = function(e) {
        return 0 >= e.offsetWidth && 0 >= e.offsetHeight || !ue.support.reliableHiddenOffsets && "none" === (e.style && e.style.display || ue.css(e, "display"))
    }
    ,
    ue.expr.filters.visible = function(e) {
        return !ue.expr.filters.hidden(e)
    }
    ),
    ue.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(e, t) {
        ue.cssHooks[e + t] = {
            expand: function(i) {
                for (var n = 0, o = {}, s = "string" == typeof i ? i.split(" ") : [i]; 4 > n; n++)
                    o[e + ft[n] + t] = s[n] || s[n - 2] || s[0];
                return o
            }
        },
        at.test(e) || (ue.cssHooks[e + t].set = T)
    });
    var gt = /\[\]$/
      , vt = /^(?:submit|button|image|reset|file)$/i
      , yt = /^(?:input|select|textarea|keygen)/i;
    ue.fn.extend({
        serialize: function() {
            return ue.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var e = ue.prop(this, "elements");
                return e ? ue.makeArray(e) : this
            }).filter(function() {
                var e = this.type;
                return this.name && !ue(this).is(":disabled") && yt.test(this.nodeName) && !vt.test(e) && (this.checked || !Ye.test(e))
            }).map(function(e, t) {
                var i = ue(this).val();
                return null == i ? null : ue.isArray(i) ? ue.map(i, function(e) {
                    return {
                        name: t.name,
                        value: e.replace(/\r?\n/g, "\r\n")
                    }
                }) : {
                    name: t.name,
                    value: i.replace(/\r?\n/g, "\r\n")
                }
            }).get()
        }
    }),
    ue.param = function(e, i) {
        var n, o = [], s = function(e, t) {
            t = ue.isFunction(t) ? t() : null == t ? "" : t,
            o[o.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t)
        };
        if (i === t && (i = ue.ajaxSettings && ue.ajaxSettings.traditional),
        ue.isArray(e) || e.jquery && !ue.isPlainObject(e))
            ue.each(e, function() {
                s(this.name, this.value)
            });
        else
            for (n in e)
                A(n, e[n], i, s);
        return o.join("&").replace(/%20/g, "+")
    }
    ,
    ue.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(e, t) {
        ue.fn[t] = function(e, i) {
            return arguments.length > 0 ? this.on(t, null, e, i) : this.trigger(t)
        }
    }),
    ue.fn.extend({
        hover: function(e, t) {
            return this.mouseenter(e).mouseleave(t || e)
        },
        bind: function(e, t, i) {
            return this.on(e, null, t, i)
        },
        unbind: function(e, t) {
            return this.off(e, null, t)
        },
        delegate: function(e, t, i, n) {
            return this.on(t, e, i, n)
        },
        undelegate: function(e, t, i) {
            return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", i)
        }
    });
    var wt, bt, xt = ue.now(), St = /\?/, Ct = /([?&])_=[^&]*/, kt = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm, Tt = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, _t = /^(?:GET|HEAD)$/, $t = /^([\w.+-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/, Et = ue.fn.load, Dt = {}, At = {}, Mt = "*/".concat("*");
    try {
        bt = Q.href
    } catch (e) {
        bt = G.createElement("a"),
        bt.href = "",
        bt = bt.href
    }
    wt = $t.exec(bt.toLowerCase()) || [],
    ue.fn.load = function(e, i, n) {
        if ("string" != typeof e && Et)
            return Et.apply(this, arguments);
        var o, s, r, a = this, l = e.indexOf(" ");
        return l >= 0 && (o = e.slice(l, e.length),
        e = e.slice(0, l)),
        ue.isFunction(i) ? (n = i,
        i = t) : i && "object" == typeof i && (r = "POST"),
        a.length > 0 && ue.ajax({
            url: e,
            type: r,
            dataType: "html",
            data: i
        }).done(function(e) {
            s = arguments,
            a.html(o ? ue("<div>").append(ue.parseHTML(e)).find(o) : e)
        }).complete(n && function(e, t) {
            a.each(n, s || [e.responseText, t, e])
        }
        ),
        this
    }
    ,
    ue.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
        ue.fn[t] = function(e) {
            return this.on(t, e)
        }
    }),
    ue.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: bt,
            type: "GET",
            isLocal: Tt.test(wt[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Mt,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": ue.parseJSON,
                "text xml": ue.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(e, t) {
            return t ? O(O(e, ue.ajaxSettings), t) : O(ue.ajaxSettings, e)
        },
        ajaxPrefilter: M(Dt),
        ajaxTransport: M(At),
        ajax: function(e, i) {
            function n(e, i, n, o) {
                var s, d, y, w, x, C = i;
                2 !== b && (b = 2,
                l && clearTimeout(l),
                u = t,
                a = o || "",
                S.readyState = e > 0 ? 4 : 0,
                s = e >= 200 && 300 > e || 304 === e,
                n && (w = L(p, S, n)),
                w = H(p, w, S, s),
                s ? (p.ifModified && (x = S.getResponseHeader("Last-Modified"),
                x && (ue.lastModified[r] = x),
                (x = S.getResponseHeader("etag")) && (ue.etag[r] = x)),
                204 === e || "HEAD" === p.type ? C = "nocontent" : 304 === e ? C = "notmodified" : (C = w.state,
                d = w.data,
                y = w.error,
                s = !y)) : (y = C,
                (e || !C) && (C = "error",
                0 > e && (e = 0))),
                S.status = e,
                S.statusText = (i || C) + "",
                s ? m.resolveWith(h, [d, C, S]) : m.rejectWith(h, [S, C, y]),
                S.statusCode(v),
                v = t,
                c && f.trigger(s ? "ajaxSuccess" : "ajaxError", [S, p, s ? d : y]),
                g.fireWith(h, [S, C]),
                c && (f.trigger("ajaxComplete", [S, p]),
                --ue.active || ue.event.trigger("ajaxStop")))
            }
            "object" == typeof e && (i = e,
            e = t),
            i = i || {};
            var o, s, r, a, l, c, u, d, p = ue.ajaxSetup({}, i), h = p.context || p, f = p.context && (h.nodeType || h.jquery) ? ue(h) : ue.event, m = ue.Deferred(), g = ue.Callbacks("once memory"), v = p.statusCode || {}, y = {}, w = {}, b = 0, x = "canceled", S = {
                readyState: 0,
                getResponseHeader: function(e) {
                    var t;
                    if (2 === b) {
                        if (!d)
                            for (d = {}; t = kt.exec(a); )
                                d[t[1].toLowerCase()] = t[2];
                        t = d[e.toLowerCase()]
                    }
                    return null == t ? null : t
                },
                getAllResponseHeaders: function() {
                    return 2 === b ? a : null
                },
                setRequestHeader: function(e, t) {
                    var i = e.toLowerCase();
                    return b || (e = w[i] = w[i] || e,
                    y[e] = t),
                    this
                },
                overrideMimeType: function(e) {
                    return b || (p.mimeType = e),
                    this
                },
                statusCode: function(e) {
                    var t;
                    if (e)
                        if (2 > b)
                            for (t in e)
                                v[t] = [v[t], e[t]];
                        else
                            S.always(e[S.status]);
                    return this
                },
                abort: function(e) {
                    var t = e || x;
                    return u && u.abort(t),
                    n(0, t),
                    this
                }
            };
            if (m.promise(S).complete = g.add,
            S.success = S.done,
            S.error = S.fail,
            p.url = ((e || p.url || bt) + "").replace(/#.*$/, "").replace(/^\/\//, wt[1] + "//"),
            p.type = i.method || i.type || p.method || p.type,
            p.dataTypes = ue.trim(p.dataType || "*").toLowerCase().match(pe) || [""],
            null == p.crossDomain && (o = $t.exec(p.url.toLowerCase()),
            p.crossDomain = !(!o || o[1] === wt[1] && o[2] === wt[2] && (o[3] || ("http:" === o[1] ? "80" : "443")) === (wt[3] || ("http:" === wt[1] ? "80" : "443")))),
            p.data && p.processData && "string" != typeof p.data && (p.data = ue.param(p.data, p.traditional)),
            N(Dt, p, i, S),
            2 === b)
                return S;
            c = p.global,
            c && 0 == ue.active++ && ue.event.trigger("ajaxStart"),
            p.type = p.type.toUpperCase(),
            p.hasContent = !_t.test(p.type),
            r = p.url,
            p.hasContent || (p.data && (r = p.url += (St.test(r) ? "&" : "?") + p.data,
            delete p.data),
            !1 === p.cache && (p.url = Ct.test(r) ? r.replace(Ct, "$1_=" + xt++) : r + (St.test(r) ? "&" : "?") + "_=" + xt++)),
            p.ifModified && (ue.lastModified[r] && S.setRequestHeader("If-Modified-Since", ue.lastModified[r]),
            ue.etag[r] && S.setRequestHeader("If-None-Match", ue.etag[r])),
            (p.data && p.hasContent && !1 !== p.contentType || i.contentType) && S.setRequestHeader("Content-Type", p.contentType),
            S.setRequestHeader("Accept", p.dataTypes[0] && p.accepts[p.dataTypes[0]] ? p.accepts[p.dataTypes[0]] + ("*" !== p.dataTypes[0] ? ", " + Mt + "; q=0.01" : "") : p.accepts["*"]);
            for (s in p.headers)
                S.setRequestHeader(s, p.headers[s]);
            if (p.beforeSend && (!1 === p.beforeSend.call(h, S, p) || 2 === b))
                return S.abort();
            x = "abort";
            for (s in {
                success: 1,
                error: 1,
                complete: 1
            })
                S[s](p[s]);
            if (u = N(At, p, i, S)) {
                S.readyState = 1,
                c && f.trigger("ajaxSend", [S, p]),
                p.async && p.timeout > 0 && (l = setTimeout(function() {
                    S.abort("timeout")
                }, p.timeout));
                try {
                    b = 1,
                    u.send(y, n)
                } catch (e) {
                    if (!(2 > b))
                        throw e;
                    n(-1, e)
                }
            } else
                n(-1, "No Transport");
            return S
        },
        getJSON: function(e, t, i) {
            return ue.get(e, t, i, "json")
        },
        getScript: function(e, i) {
            return ue.get(e, t, i, "script")
        }
    }),
    ue.each(["get", "post"], function(e, i) {
        ue[i] = function(e, n, o, s) {
            return ue.isFunction(n) && (s = s || o,
            o = n,
            n = t),
            ue.ajax({
                url: e,
                type: i,
                dataType: s,
                data: n,
                success: o
            })
        }
    }),
    ue.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function(e) {
                return ue.globalEval(e),
                e
            }
        }
    }),
    ue.ajaxPrefilter("script", function(e) {
        e.cache === t && (e.cache = !1),
        e.crossDomain && (e.type = "GET",
        e.global = !1)
    }),
    ue.ajaxTransport("script", function(e) {
        if (e.crossDomain) {
            var i, n = G.head || ue("head")[0] || G.documentElement;
            return {
                send: function(t, o) {
                    i = G.createElement("script"),
                    i.async = !0,
                    e.scriptCharset && (i.charset = e.scriptCharset),
                    i.src = e.url,
                    i.onload = i.onreadystatechange = function(e, t) {
                        (t || !i.readyState || /loaded|complete/.test(i.readyState)) && (i.onload = i.onreadystatechange = null,
                        i.parentNode && i.parentNode.removeChild(i),
                        i = null,
                        t || o(200, "success"))
                    }
                    ,
                    n.insertBefore(i, n.firstChild)
                },
                abort: function() {
                    i && i.onload(t, !0)
                }
            }
        }
    });
    var Nt = []
      , Ot = /(=)\?(?=&|$)|\?\?/;
    ue.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var e = Nt.pop() || ue.expando + "_" + xt++;
            return this[e] = !0,
            e
        }
    }),
    ue.ajaxPrefilter("json jsonp", function(i, n, o) {
        var s, r, a, l = !1 !== i.jsonp && (Ot.test(i.url) ? "url" : "string" == typeof i.data && !(i.contentType || "").indexOf("application/x-www-form-urlencoded") && Ot.test(i.data) && "data");
        return l || "jsonp" === i.dataTypes[0] ? (s = i.jsonpCallback = ue.isFunction(i.jsonpCallback) ? i.jsonpCallback() : i.jsonpCallback,
        l ? i[l] = i[l].replace(Ot, "$1" + s) : !1 !== i.jsonp && (i.url += (St.test(i.url) ? "&" : "?") + i.jsonp + "=" + s),
        i.converters["script json"] = function() {
            return a || ue.error(s + " was not called"),
            a[0]
        }
        ,
        i.dataTypes[0] = "json",
        r = e[s],
        e[s] = function() {
            a = arguments
        }
        ,
        o.always(function() {
            e[s] = r,
            i[s] && (i.jsonpCallback = n.jsonpCallback,
            Nt.push(s)),
            a && ue.isFunction(r) && r(a[0]),
            a = r = t
        }),
        "script") : t
    });
    var Lt, Ht, jt = 0, Bt = e.ActiveXObject && function() {
        var e;
        for (e in Lt)
            Lt[e](t, !0)
    }
    ;
    ue.ajaxSettings.xhr = e.ActiveXObject ? function() {
        return !this.isLocal && j() || B()
    }
    : j,
    Ht = ue.ajaxSettings.xhr(),
    ue.support.cors = !!Ht && "withCredentials"in Ht,
    (Ht = ue.support.ajax = !!Ht) && ue.ajaxTransport(function(i) {
        if (!i.crossDomain || ue.support.cors) {
            var n;
            return {
                send: function(o, s) {
                    var r, a, l = i.xhr();
                    if (i.username ? l.open(i.type, i.url, i.async, i.username, i.password) : l.open(i.type, i.url, i.async),
                    i.xhrFields)
                        for (a in i.xhrFields)
                            l[a] = i.xhrFields[a];
                    i.mimeType && l.overrideMimeType && l.overrideMimeType(i.mimeType),
                    i.crossDomain || o["X-Requested-With"] || (o["X-Requested-With"] = "XMLHttpRequest");
                    try {
                        for (a in o)
                            l.setRequestHeader(a, o[a])
                    } catch (e) {}
                    l.send(i.hasContent && i.data || null),
                    n = function(e, o) {
                        var a, c, u, d;
                        try {
                            if (n && (o || 4 === l.readyState))
                                if (n = t,
                                r && (l.onreadystatechange = ue.noop,
                                Bt && delete Lt[r]),
                                o)
                                    4 !== l.readyState && l.abort();
                                else {
                                    d = {},
                                    a = l.status,
                                    c = l.getAllResponseHeaders(),
                                    "string" == typeof l.responseText && (d.text = l.responseText);
                                    try {
                                        u = l.statusText
                                    } catch (e) {
                                        u = ""
                                    }
                                    a || !i.isLocal || i.crossDomain ? 1223 === a && (a = 204) : a = d.text ? 200 : 404
                                }
                        } catch (e) {
                            o || s(-1, e)
                        }
                        d && s(a, u, d, c)
                    }
                    ,
                    i.async ? 4 === l.readyState ? setTimeout(n) : (r = ++jt,
                    Bt && (Lt || (Lt = {},
                    ue(e).unload(Bt)),
                    Lt[r] = n),
                    l.onreadystatechange = n) : n()
                },
                abort: function() {
                    n && n(t, !0)
                }
            }
        }
    });
    var Pt, It, zt = /^(?:toggle|show|hide)$/, Wt = RegExp("^(?:([+-])=|)(" + de + ")([a-z%]*)$", "i"), Rt = /queueHooks$/, qt = [R], Ft = {
        "*": [function(e, t) {
            var i = this.createTween(e, t)
              , n = i.cur()
              , o = Wt.exec(t)
              , s = o && o[3] || (ue.cssNumber[e] ? "" : "px")
              , r = (ue.cssNumber[e] || "px" !== s && +n) && Wt.exec(ue.css(i.elem, e))
              , a = 1
              , l = 20;
            if (r && r[3] !== s) {
                s = s || r[3],
                o = o || [],
                r = +n || 1;
                do {
                    a = a || ".5",
                    r /= a,
                    ue.style(i.elem, e, r + s)
                } while (a !== (a = i.cur() / n) && 1 !== a && --l)
            }
            return o && (r = i.start = +r || +n || 0,
            i.unit = s,
            i.end = o[1] ? r + (o[1] + 1) * o[2] : +o[2]),
            i
        }
        ]
    };
    ue.Animation = ue.extend(z, {
        tweener: function(e, t) {
            ue.isFunction(e) ? (t = e,
            e = ["*"]) : e = e.split(" ");
            for (var i, n = 0, o = e.length; o > n; n++)
                i = e[n],
                Ft[i] = Ft[i] || [],
                Ft[i].unshift(t)
        },
        prefilter: function(e, t) {
            t ? qt.unshift(e) : qt.push(e)
        }
    }),
    ue.Tween = q,
    q.prototype = {
        constructor: q,
        init: function(e, t, i, n, o, s) {
            this.elem = e,
            this.prop = i,
            this.easing = o || "swing",
            this.options = t,
            this.start = this.now = this.cur(),
            this.end = n,
            this.unit = s || (ue.cssNumber[i] ? "" : "px")
        },
        cur: function() {
            var e = q.propHooks[this.prop];
            return e && e.get ? e.get(this) : q.propHooks._default.get(this)
        },
        run: function(e) {
            var t, i = q.propHooks[this.prop];
            return this.pos = t = this.options.duration ? ue.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : e,
            this.now = (this.end - this.start) * t + this.start,
            this.options.step && this.options.step.call(this.elem, this.now, this),
            i && i.set ? i.set(this) : q.propHooks._default.set(this),
            this
        }
    },
    q.prototype.init.prototype = q.prototype,
    q.propHooks = {
        _default: {
            get: function(e) {
                var t;
                return null == e.elem[e.prop] || e.elem.style && null != e.elem.style[e.prop] ? (t = ue.css(e.elem, e.prop, ""),
                t && "auto" !== t ? t : 0) : e.elem[e.prop]
            },
            set: function(e) {
                ue.fx.step[e.prop] ? ue.fx.step[e.prop](e) : e.elem.style && (null != e.elem.style[ue.cssProps[e.prop]] || ue.cssHooks[e.prop]) ? ue.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now
            }
        }
    },
    q.propHooks.scrollTop = q.propHooks.scrollLeft = {
        set: function(e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }
    },
    ue.each(["toggle", "show", "hide"], function(e, t) {
        var i = ue.fn[t];
        ue.fn[t] = function(e, n, o) {
            return null == e || "boolean" == typeof e ? i.apply(this, arguments) : this.animate(F(t, !0), e, n, o)
        }
    }),
    ue.fn.extend({
        fadeTo: function(e, t, i, n) {
            return this.filter(C).css("opacity", 0).show().end().animate({
                opacity: t
            }, e, i, n)
        },
        animate: function(e, t, i, n) {
            var o = ue.isEmptyObject(e)
              , s = ue.speed(t, i, n)
              , r = function() {
                var t = z(this, ue.extend({}, e), s);
                (o || ue._data(this, "finish")) && t.stop(!0)
            };
            return r.finish = r,
            o || !1 === s.queue ? this.each(r) : this.queue(s.queue, r)
        },
        stop: function(e, i, n) {
            var o = function(e) {
                var t = e.stop;
                delete e.stop,
                t(n)
            };
            return "string" != typeof e && (n = i,
            i = e,
            e = t),
            i && !1 !== e && this.queue(e || "fx", []),
            this.each(function() {
                var t = !0
                  , i = null != e && e + "queueHooks"
                  , s = ue.timers
                  , r = ue._data(this);
                if (i)
                    r[i] && r[i].stop && o(r[i]);
                else
                    for (i in r)
                        r[i] && r[i].stop && Rt.test(i) && o(r[i]);
                for (i = s.length; i--; )
                    s[i].elem !== this || null != e && s[i].queue !== e || (s[i].anim.stop(n),
                    t = !1,
                    s.splice(i, 1));
                (t || !n) && ue.dequeue(this, e)
            })
        },
        finish: function(e) {
            return !1 !== e && (e = e || "fx"),
            this.each(function() {
                var t, i = ue._data(this), n = i[e + "queue"], o = i[e + "queueHooks"], s = ue.timers, r = n ? n.length : 0;
                for (i.finish = !0,
                ue.queue(this, e, []),
                o && o.stop && o.stop.call(this, !0),
                t = s.length; t--; )
                    s[t].elem === this && s[t].queue === e && (s[t].anim.stop(!0),
                    s.splice(t, 1));
                for (t = 0; r > t; t++)
                    n[t] && n[t].finish && n[t].finish.call(this);
                delete i.finish
            })
        }
    }),
    ue.each({
        slideDown: F("show"),
        slideUp: F("hide"),
        slideToggle: F("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(e, t) {
        ue.fn[e] = function(e, i, n) {
            return this.animate(t, e, i, n)
        }
    }),
    ue.speed = function(e, t, i) {
        var n = e && "object" == typeof e ? ue.extend({}, e) : {
            complete: i || !i && t || ue.isFunction(e) && e,
            duration: e,
            easing: i && t || t && !ue.isFunction(t) && t
        };
        return n.duration = ue.fx.off ? 0 : "number" == typeof n.duration ? n.duration : n.duration in ue.fx.speeds ? ue.fx.speeds[n.duration] : ue.fx.speeds._default,
        (null == n.queue || !0 === n.queue) && (n.queue = "fx"),
        n.old = n.complete,
        n.complete = function() {
            ue.isFunction(n.old) && n.old.call(this),
            n.queue && ue.dequeue(this, n.queue)
        }
        ,
        n
    }
    ,
    ue.easing = {
        linear: function(e) {
            return e
        },
        swing: function(e) {
            return .5 - Math.cos(e * Math.PI) / 2
        }
    },
    ue.timers = [],
    ue.fx = q.prototype.init,
    ue.fx.tick = function() {
        var e, i = ue.timers, n = 0;
        for (Pt = ue.now(); i.length > n; n++)
            (e = i[n])() || i[n] !== e || i.splice(n--, 1);
        i.length || ue.fx.stop(),
        Pt = t
    }
    ,
    ue.fx.timer = function(e) {
        e() && ue.timers.push(e) && ue.fx.start()
    }
    ,
    ue.fx.interval = 13,
    ue.fx.start = function() {
        It || (It = setInterval(ue.fx.tick, ue.fx.interval))
    }
    ,
    ue.fx.stop = function() {
        clearInterval(It),
        It = null
    }
    ,
    ue.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    },
    ue.fx.step = {},
    ue.expr && ue.expr.filters && (ue.expr.filters.animated = function(e) {
        return ue.grep(ue.timers, function(t) {
            return e === t.elem
        }).length
    }
    ),
    ue.fn.offset = function(e) {
        if (arguments.length)
            return e === t ? this : this.each(function(t) {
                ue.offset.setOffset(this, e, t)
            });
        var i, n, o = {
            top: 0,
            left: 0
        }, s = this[0], r = s && s.ownerDocument;
        return r ? (i = r.documentElement,
        ue.contains(i, s) ? (typeof s.getBoundingClientRect !== V && (o = s.getBoundingClientRect()),
        n = X(r),
        {
            top: o.top + (n.pageYOffset || i.scrollTop) - (i.clientTop || 0),
            left: o.left + (n.pageXOffset || i.scrollLeft) - (i.clientLeft || 0)
        }) : o) : void 0
    }
    ,
    ue.offset = {
        setOffset: function(e, t, i) {
            var n = ue.css(e, "position");
            "static" === n && (e.style.position = "relative");
            var o, s, r = ue(e), a = r.offset(), l = ue.css(e, "top"), c = ue.css(e, "left"), u = ("absolute" === n || "fixed" === n) && ue.inArray("auto", [l, c]) > -1, d = {}, p = {};
            u ? (p = r.position(),
            o = p.top,
            s = p.left) : (o = parseFloat(l) || 0,
            s = parseFloat(c) || 0),
            ue.isFunction(t) && (t = t.call(e, i, a)),
            null != t.top && (d.top = t.top - a.top + o),
            null != t.left && (d.left = t.left - a.left + s),
            "using"in t ? t.using.call(e, d) : r.css(d)
        }
    },
    ue.fn.extend({
        position: function() {
            if (this[0]) {
                var e, t, i = {
                    top: 0,
                    left: 0
                }, n = this[0];
                return "fixed" === ue.css(n, "position") ? t = n.getBoundingClientRect() : (e = this.offsetParent(),
                t = this.offset(),
                ue.nodeName(e[0], "html") || (i = e.offset()),
                i.top += ue.css(e[0], "borderTopWidth", !0),
                i.left += ue.css(e[0], "borderLeftWidth", !0)),
                {
                    top: t.top - i.top - ue.css(n, "marginTop", !0),
                    left: t.left - i.left - ue.css(n, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                for (var e = this.offsetParent || J; e && !ue.nodeName(e, "html") && "static" === ue.css(e, "position"); )
                    e = e.offsetParent;
                return e || J
            })
        }
    }),
    ue.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(e, i) {
        var n = /Y/.test(i);
        ue.fn[e] = function(o) {
            return ue.access(this, function(e, o, s) {
                var r = X(e);
                return s === t ? r ? i in r ? r[i] : r.document.documentElement[o] : e[o] : (r ? r.scrollTo(n ? ue(r).scrollLeft() : s, n ? s : ue(r).scrollTop()) : e[o] = s,
                t)
            }, e, o, arguments.length, null)
        }
    }),
    ue.each({
        Height: "height",
        Width: "width"
    }, function(e, i) {
        ue.each({
            padding: "inner" + e,
            content: i,
            "": "outer" + e
        }, function(n, o) {
            ue.fn[o] = function(o, s) {
                var r = arguments.length && (n || "boolean" != typeof o)
                  , a = n || (!0 === o || !0 === s ? "margin" : "border");
                return ue.access(this, function(i, n, o) {
                    var s;
                    return ue.isWindow(i) ? i.document.documentElement["client" + e] : 9 === i.nodeType ? (s = i.documentElement,
                    Math.max(i.body["scroll" + e], s["scroll" + e], i.body["offset" + e], s["offset" + e], s["client" + e])) : o === t ? ue.css(i, n, a) : ue.style(i, n, o, a)
                }, i, r ? o : t, r, null)
            }
        })
    }),
    ue.fn.size = function() {
        return this.length
    }
    ,
    ue.fn.andSelf = ue.fn.addBack,
    "object" == typeof module && module && "object" == typeof module.exports ? module.exports = ue : (e.jQuery = e.$ = ue,
    "function" == typeof define && define.amd && define("jquery", [], function() {
        return ue
    }))
}(window),
$(".modal-wrapper").hide(),
function(e, t, i) {
    "function" == typeof define && define.amd ? define(["jquery"], function(n) {
        return i(n, e, t),
        n.mobile
    }) : i(e.jQuery, e, t)
}(this, document, function(e, t, i, n) {
    (function(e, t, i, n) {
        function o(e) {
            for (; e && void 0 !== e.originalEvent; )
                e = e.originalEvent;
            return e
        }
        function s(t, i) {
            var s, r, a, l, c, u, d, p, h, f = t.type;
            if (t = e.Event(t),
            t.type = i,
            s = t.originalEvent,
            r = e.event.props,
            f.search(/^(mouse|click)/) > -1 && (r = D),
            s)
                for (d = r.length,
                l; d; )
                    l = r[--d],
                    t[l] = s[l];
            if (f.search(/mouse(down|up)|click/) > -1 && !t.which && (t.which = 1),
            -1 !== f.search(/^touch/) && (a = o(s),
            f = a.touches,
            c = a.changedTouches,
            u = f && f.length ? f[0] : c && c.length ? c[0] : n,
            u))
                for (p = 0,
                h = $.length; p < h; p++)
                    l = $[p],
                    t[l] = u[l];
            return t
        }
        function r(t) {
            for (var i, n, o = {}; t; ) {
                i = e.data(t, k);
                for (n in i)
                    i[n] && (o[n] = o.hasVirtualBinding = !0);
                t = t.parentNode
            }
            return o
        }
        function a(t, i) {
            for (var n; t; ) {
                if ((n = e.data(t, k)) && (!i || n[i]))
                    return t;
                t = t.parentNode
            }
            return null
        }
        function l() {
            B = !1
        }
        function c() {
            B = !0
        }
        function u() {
            W = 0,
            H.length = 0,
            j = !1,
            c()
        }
        function d() {
            l()
        }
        function p() {
            h(),
            M = setTimeout(function() {
                M = 0,
                u()
            }, e.vmouse.resetTimerDuration)
        }
        function h() {
            M && (clearTimeout(M),
            M = 0)
        }
        function f(t, i, n) {
            var o;
            return (n && n[t] || !n && a(i.target, t)) && (o = s(i, t),
            e(i.target).trigger(o)),
            o
        }
        function m(t) {
            var i, n = e.data(t.target, T);
            !j && (!W || W !== n) && (i = f("v" + t.type, t)) && (i.isDefaultPrevented() && t.preventDefault(),
            i.isPropagationStopped() && t.stopPropagation(),
            i.isImmediatePropagationStopped() && t.stopImmediatePropagation())
        }
        function g(t) {
            var i, n, s, a = o(t).touches;
            a && 1 === a.length && (i = t.target,
            n = r(i),
            n.hasVirtualBinding && (W = z++,
            e.data(i, T, W),
            h(),
            d(),
            L = !1,
            s = o(t).touches[0],
            N = s.pageX,
            O = s.pageY,
            f("vmouseover", t, n),
            f("vmousedown", t, n)))
        }
        function v(e) {
            B || (L || f("vmousecancel", e, r(e.target)),
            L = !0,
            p())
        }
        function y(t) {
            if (!B) {
                var i = o(t).touches[0]
                  , n = L
                  , s = e.vmouse.moveDistanceThreshold
                  , a = r(t.target);
                L = L || Math.abs(i.pageX - N) > s || Math.abs(i.pageY - O) > s,
                L && !n && f("vmousecancel", t, a),
                f("vmousemove", t, a),
                p()
            }
        }
        function w(e) {
            if (!B) {
                c();
                var t, i, n = r(e.target);
                f("vmouseup", e, n),
                L || (t = f("vclick", e, n)) && t.isDefaultPrevented() && (i = o(e).changedTouches[0],
                H.push({
                    touchID: W,
                    x: i.clientX,
                    y: i.clientY
                }),
                j = !0),
                f("vmouseout", e, n),
                L = !1,
                p()
            }
        }
        function b(t) {
            var i, n = e.data(t, k);
            if (n)
                for (i in n)
                    if (n[i])
                        return !0;
            return !1
        }
        function x() {}
        var S, C, k = "virtualMouseBindings", T = "virtualTouchID", _ = "vmouseover vmousedown vmousemove vmouseup vclick vmouseout vmousecancel".split(" "), $ = "clientX clientY pageX pageY screenX screenY".split(" "), E = e.event.mouseHooks ? e.event.mouseHooks.props : [], D = e.event.props.concat(E), A = {}, M = 0, N = 0, O = 0, L = !1, H = [], j = !1, B = !1, P = "addEventListener"in i, I = e(i), z = 1, W = 0;
        for (e.vmouse = {
            moveDistanceThreshold: 10,
            clickDistanceThreshold: 10,
            resetTimerDuration: 1500
        },
        C = 0; C < _.length; C++)
            e.event.special[_[C]] = function(t) {
                var i = t.substr(1);
                return {
                    setup: function() {
                        b(this) || e.data(this, k, {}),
                        e.data(this, k)[t] = !0,
                        A[t] = (A[t] || 0) + 1,
                        1 === A[t] && I.bind(i, m),
                        e(this).bind(i, x),
                        P && (A.touchstart = (A.touchstart || 0) + 1,
                        1 === A.touchstart && I.bind("touchstart", g).bind("touchend", w).bind("touchmove", y).bind("scroll", v))
                    },
                    teardown: function() {
                        --A[t],
                        A[t] || I.unbind(i, m),
                        P && (--A.touchstart || I.unbind("touchstart", g).unbind("touchmove", y).unbind("touchend", w).unbind("scroll", v));
                        var n = e(this)
                          , o = e.data(this, k);
                        o && (o[t] = !1),
                        n.unbind(i, x),
                        b(this) || n.removeData(k)
                    }
                }
            }(_[C]);
        /*P && i.addEventListener("click", function(t) {
            var i, n, o, s, r, a = H.length, l = t.target;
            if (a)
                for (i = t.clientX,
                n = t.clientY,
                S = e.vmouse.clickDistanceThreshold,
                o = l; o; ) {
                    for (s = 0; s < a; s++)
                        if (r = H[s],
                        0,
                        o === l && Math.abs(r.x - i) < S && Math.abs(r.y - n) < S || e.data(o, T) === r.touchID)
                            return t.preventDefault(),
                            void t.stopPropagation();
                    o = o.parentNode
                }
        }, !0)*/
    })(e, 0, i),
    function(e) {
        e.mobile = {}
    }(e),
    function(e, t) {
        var n = {
            touch: "ontouchend"in i
        };
        e.mobile.support = e.mobile.support || {},
        e.extend(e.support, n),
        e.extend(e.mobile.support, n)
    }(e),
    function(e, t, n) {
        function o(t, i, o, s) {
            var r = o.type;
            o.type = i,
            s ? e.event.trigger(o, n, t) : e.event.dispatch.call(t, o),
            o.type = r
        }
        var s = e(i)
          , r = e.mobile.support.touch
          , a = "touchmove scroll"
          , l = r ? "touchstart" : "mousedown"
          , c = r ? "touchend" : "mouseup"
          , u = r ? "touchmove" : "mousemove";
        e.each("touchstart touchmove touchend tap taphold swipe swipeleft swiperight scrollstart scrollstop".split(" "), function(t, i) {
            e.fn[i] = function(e) {
                return e ? this.bind(i, e) : this.trigger(i)
            }
            ,
            e.attrFn && (e.attrFn[i] = !0)
        }),
        e.event.special.scrollstart = {
            enabled: !0,
            setup: function() {
                function t(e, t) {
                    i = t,
                    o(s, i ? "scrollstart" : "scrollstop", e)
                }
                var i, n, s = this, r = e(s);
                r.bind(a, function(o) {
                    e.event.special.scrollstart.enabled && (i || t(o, !0),
                    clearTimeout(n),
                    n = setTimeout(function() {
                        t(o, !1)
                    }, 50))
                })
            },
            teardown: function() {
                e(this).unbind(a)
            }
        },
        e.event.special.tap = {
            tapholdThreshold: 750,
            emitTapOnTaphold: !0,
            setup: function() {
                var t = this
                  , i = e(t)
                  , n = !1;
                i.bind("vmousedown", function(r) {
                    function a() {
                        clearTimeout(u)
                    }
                    function l() {
                        a(),
                        i.unbind("vclick", c).unbind("vmouseup", a),
                        s.unbind("vmousecancel", l)
                    }
                    function c(e) {
                        l(),
                        n || d !== e.target ? n && e.preventDefault() : o(t, "tap", e)
                    }
                    if (n = !1,
                    r.which && 1 !== r.which)
                        return !1;
                    var u, d = r.target;
                    i.bind("vmouseup", a).bind("vclick", c),
                    s.bind("vmousecancel", l),
                    u = setTimeout(function() {
                        e.event.special.tap.emitTapOnTaphold || (n = !0),
                        o(t, "taphold", e.Event("taphold", {
                            target: d
                        }))
                    }, e.event.special.tap.tapholdThreshold)
                })
            },
            teardown: function() {
                e(this).unbind("vmousedown").unbind("vclick").unbind("vmouseup"),
                s.unbind("vmousecancel")
            }
        },
        e.event.special.swipe = {
            scrollSupressionThreshold: 30,
            durationThreshold: 1e3,
            horizontalDistanceThreshold: 30,
            verticalDistanceThreshold: 30,
            getLocation: function(e) {
                var i = t.pageXOffset
                  , n = t.pageYOffset
                  , o = e.clientX
                  , s = e.clientY;
                return 0 === e.pageY && Math.floor(s) > Math.floor(e.pageY) || 0 === e.pageX && Math.floor(o) > Math.floor(e.pageX) ? (o -= i,
                s -= n) : (s < e.pageY - n || o < e.pageX - i) && (o = e.pageX - i,
                s = e.pageY - n),
                {
                    x: o,
                    y: s
                }
            },
            start: function(t) {
                var i = t.originalEvent.touches ? t.originalEvent.touches[0] : t
                  , n = e.event.special.swipe.getLocation(i);
                return {
                    time: (new Date).getTime(),
                    coords: [n.x, n.y],
                    origin: e(t.target)
                }
            },
            stop: function(t) {
                var i = t.originalEvent.touches ? t.originalEvent.touches[0] : t
                  , n = e.event.special.swipe.getLocation(i);
                return {
                    time: (new Date).getTime(),
                    coords: [n.x, n.y]
                }
            },
            handleSwipe: function(t, i, n, s) {
                if (i.time - t.time < e.event.special.swipe.durationThreshold && Math.abs(t.coords[0] - i.coords[0]) > e.event.special.swipe.horizontalDistanceThreshold && Math.abs(t.coords[1] - i.coords[1]) < e.event.special.swipe.verticalDistanceThreshold) {
                    var r = t.coords[0] > i.coords[0] ? "swipeleft" : "swiperight";
                    return o(n, "swipe", e.Event("swipe", {
                        target: s,
                        swipestart: t,
                        swipestop: i
                    }), !0),
                    o(n, r, e.Event(r, {
                        target: s,
                        swipestart: t,
                        swipestop: i
                    }), !0),
                    !0
                }
                return !1
            },
            eventInProgress: !1,
            setup: function() {
                var t, i = this, n = e(i), o = {};
                t = e.data(this, "mobile-events"),
                t || (t = {
                    length: 0
                },
                e.data(this, "mobile-events", t)),
                t.length++,
                t.swipe = o,
                o.start = function(t) {
                    if (!e.event.special.swipe.eventInProgress) {
                        e.event.special.swipe.eventInProgress = !0;
                        var n, r = e.event.special.swipe.start(t), a = t.target, l = !1;
                        o.move = function(t) {
                            r && !t.isDefaultPrevented() && (n = e.event.special.swipe.stop(t),
                            l || (l = e.event.special.swipe.handleSwipe(r, n, i, a)) && (e.event.special.swipe.eventInProgress = !1),
                            Math.abs(r.coords[0] - n.coords[0]) > e.event.special.swipe.scrollSupressionThreshold && t.preventDefault())
                        }
                        ,
                        o.stop = function() {
                            l = !0,
                            e.event.special.swipe.eventInProgress = !1,
                            s.off(u, o.move),
                            o.move = null
                        }
                        ,
                        s.on(u, o.move).one(c, o.stop)
                    }
                }
                ,
                n.on(l, o.start)
            },
            teardown: function() {
                var t, i;
                t = e.data(this, "mobile-events"),
                t && (i = t.swipe,
                delete t.swipe,
                0 === --t.length && e.removeData(this, "mobile-events")),
                i && (i.start && e(this).off(l, i.start),
                i.move && s.off(u, i.move),
                i.stop && s.off(c, i.stop))
            }
        },
        e.each({
            scrollstop: "scrollstart",
            taphold: "tap",
            swipeleft: "swipe.left",
            swiperight: "swipe.right"
        }, function(t, i) {
            e.event.special[t] = {
                setup: function() {
                    e(this).bind(i, e.noop)
                },
                teardown: function() {
                    e(this).unbind(i)
                }
            }
        })
    }(e, this)
}),
function(e) {
    function t() {
        !function() {
            return "ontouchstart"in window || navigator.maxTouchPoints
        }() ? (e("html").addClass("notouch"),
        e("html").removeClass("touch")) : (e("html").addClass("touch"),
        e("html").removeClass("notouch"))
    }
    e(document).ready(t),
    e(window).resize(t)
}(jQuery),
function(e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : e("object" == typeof exports ? require("jquery") : jQuery)
}(function(e) {
    var t, i = navigator.userAgent, n = /iphone/i.test(i), o = /chrome/i.test(i), s = /android/i.test(i);
    e.mask = {
        definitions: {
            9: "[0-9]",
            a: "[A-Za-z]",
            "*": "[A-Za-z0-9]"
        },
        autoclear: !0,
        dataName: "rawMaskFn",
        placeholder: "_"
    },
    e.fn.extend({
        caret: function(e, t) {
            var i;
            if (0 !== this.length && !this.is(":hidden"))
                return "number" == typeof e ? (t = "number" == typeof t ? t : e,
                this.each(function() {
                    this.setSelectionRange ? this.setSelectionRange(e, t) : this.createTextRange && (i = this.createTextRange(),
                    i.collapse(!0),
                    i.moveEnd("character", t),
                    i.moveStart("character", e),
                    i.select())
                })) : (this[0].setSelectionRange ? (e = this[0].selectionStart,
                t = this[0].selectionEnd) : document.selection && document.selection.createRange && (i = document.selection.createRange(),
                e = 0 - i.duplicate().moveStart("character", -1e5),
                t = e + i.text.length),
                {
                    begin: e,
                    end: t
                })
        },
        unmask: function() {
            return this.trigger("unmask")
        },
        mask: function(i, r) {
            var a, l, c, u, d, p, h, f;
            if (!i && this.length > 0) {
                a = e(this[0]);
                var m = a.data(e.mask.dataName);
                return m ? m() : void 0
            }
            return r = e.extend({
                autoclear: e.mask.autoclear,
                placeholder: e.mask.placeholder,
                completed: null
            }, r),
            l = e.mask.definitions,
            c = [],
            u = h = i.length,
            d = null,
            e.each(i.split(""), function(e, t) {
                "?" == t ? (h--,
                u = e) : l[t] ? (c.push(new RegExp(l[t])),
                null === d && (d = c.length - 1),
                u > e && (p = c.length - 1)) : c.push(null)
            }),
            this.trigger("unmask").each(function() {
                function a() {
                    if (r.completed) {
                        for (var e = d; p >= e; e++)
                            if (c[e] && E[e] === m(e))
                                return;
                        r.completed.call($)
                    }
                }
                function m(e) {
                    return r.placeholder.charAt(e < r.placeholder.length ? e : 0)
                }
                function g(e) {
                    for (; ++e < h && !c[e]; )
                        ;
                    return e
                }
                function v(e) {
                    for (; --e >= 0 && !c[e]; )
                        ;
                    return e
                }
                function y(e, t) {
                    var i, n;
                    if (!(0 > e)) {
                        for (i = e,
                        n = g(t); h > i; i++)
                            if (c[i]) {
                                if (!(h > n && c[i].test(E[n])))
                                    break;
                                E[i] = E[n],
                                E[n] = m(n),
                                n = g(n)
                            }
                        T(),
                        $.caret(Math.max(d, e))
                    }
                }
                function w(e) {
                    var t, i, n, o;
                    for (t = e,
                    i = m(e); h > t; t++)
                        if (c[t]) {
                            if (n = g(t),
                            o = E[t],
                            E[t] = i,
                            !(h > n && c[n].test(o)))
                                break;
                            i = o
                        }
                }
                function b() {
                    var e = $.val()
                      , t = $.caret();
                    if (f && f.length && f.length > e.length) {
                        for (_(!0); t.begin > 0 && !c[t.begin - 1]; )
                            t.begin--;
                        if (0 === t.begin)
                            for (; t.begin < d && !c[t.begin]; )
                                t.begin++;
                        $.caret(t.begin, t.begin)
                    } else {
                        for (_(!0); t.begin < h && !c[t.begin]; )
                            t.begin++;
                        $.caret(t.begin, t.begin)
                    }
                    a()
                }
                function x() {
                    _(),
                    $.val() != A && $.change()
                }
                function S(e) {
                    if (!$.prop("readonly")) {
                        var t, i, o, s = e.which || e.keyCode;
                        f = $.val(),
                        8 === s || 46 === s || n && 127 === s ? (t = $.caret(),
                        i = t.begin,
                        o = t.end,
                        o - i == 0 && (i = 46 !== s ? v(i) : o = g(i - 1),
                        o = 46 === s ? g(o) : o),
                        k(i, o),
                        y(i, o - 1),
                        e.preventDefault()) : 13 === s ? x.call(this, e) : 27 === s && ($.val(A),
                        $.caret(0, _()),
                        e.preventDefault())
                    }
                }
                function C(t) {
                    if (!$.prop("readonly")) {
                        var i, n, o, r = t.which || t.keyCode, l = $.caret();
                        if (!(t.ctrlKey || t.altKey || t.metaKey || 32 > r) && r && 13 !== r) {
                            if (l.end - l.begin != 0 && (k(l.begin, l.end),
                            y(l.begin, l.end - 1)),
                            i = g(l.begin - 1),
                            h > i && (n = String.fromCharCode(r),
                            c[i].test(n))) {
                                if (w(i),
                                E[i] = n,
                                T(),
                                o = g(i),
                                s) {
                                    var u = function() {
                                        e.proxy(e.fn.caret, $, o)()
                                    };
                                    setTimeout(u, 0);
                                } else
                                    $.caret(o);
                                l.begin <= p && a();
                            }
                            t.preventDefault();
                        }
                    }
                }
                function k(e, t) {
                    var i;
                    for (i = e; t > i && h > i; i++)
                        c[i] && (E[i] = m(i));
                }
                function T() {
                    $.val(E.join(""));
                }
                function _(e) {
                    var t, i, n, o = $.val(), s = -1;
                    for (t = 0,
                    n = 0; h > t; t++)
                        if (c[t]) {
                            for (E[t] = m(t); n++ < o.length; )
                                if (i = o.charAt(n - 1),
                                c[t].test(i)) {
                                    E[t] = i,
                                    s = t;
                                    break
                                }
                            if (n > o.length) {
                                k(t + 1, h);
                                break;
                            }
                        } else
                            E[t] === o.charAt(n) && n++,
                            u > t && (s = t);
                    return e ? T() : u > s + 1 ? r.autoclear || E.join("") === D ? ($.val() && $.val(""),
                    k(0, h)) : T() : (T(),
                    $.val($.val().substring(0, s + 1))),
                    u ? t : d
                }
                var $ = e(this)
                  , E = e.map(i.split(""), function(e, t) {
                    return "?" != e ? l[e] ? m(t) : e : void 0
                })
                  , D = E.join("")
                  , A = $.val();
                $.data(e.mask.dataName, function() {
                    return e.map(E, function(e, t) {
                        return c[t] && e != m(t) ? e : null
                    }).join("")
                }),
                $.one("unmask", function() {
                    $.off(".mask").removeData(e.mask.dataName)
                }).on("focus.mask", function() {
                    if (!$.prop("readonly")) {
                        clearTimeout(t);
                        var e;
                        A = $.val(),
                        e = _(),
                        t = setTimeout(function() {
                            $.get(0) === document.activeElement && (T(),
                            e == i.replace("?", "").length ? $.caret(0, e) : $.caret(e))
                        }, 10)
                    }
                }).on("blur.mask", x).on("keydown.mask", S).on("keypress.mask", C).on("input.mask paste.mask", function() {
                    $.prop("readonly") || setTimeout(function() {
                        var e = _(!0);
                        $.caret(e),
                        a()
                    }, 0)
                }),
                o && s && $.off("input.mask").on("input.mask", b),
                _()
            })
        }
    })
}),
jQuery(function(e) {
    e(".maskphone").mask("8 (999) 999-99-99")
}),
jQuery(function(e) {
    e(".birthdate").mask("99.99.9999 г")
}),
jQuery(function(e) {
    e(".smscode").mask("99-99-99-99")
}),
jQuery(function(e) {
    e(".cardnum").mask("9999-9999")
}),
function(e) {
    function t() {
        function t() {
            if (e(this).hasClass("numbr") || e(this).hasClass("what-the-size")) {
				//return preventDefault
				return true;
			}
            if (e("html").hasClass("modal-page2")) {
                var t = e(this).closest(".modal-wrapper2")
                  , i = e(t).find(".modal2");
                return ("modal-wrapper2" == this.classList || e(this).hasClass("modal-close2")) && (e("html").removeClass("modal-page2"),
                e(t).css({
                    "overflow-y": "hidden"
                }).removeClass("modal-open"),
                e(i).animate({
                    top: 0,
                    opacity: 0
                }, 0),
                e(t).hide(0),
                e(".modal-open .modal").animate({
                    top: 50,
                    opacity: 1
                }, 700)),
                !1
            }
            if (e("html").hasClass("modal-page")) {
                var t = e(this).closest(".modal-wrapper")
                  , i = e(t).find(".modal");
                return ("modal-close" == this.classList || "modal-close obut" == this.classList || "modal-wrapper" == this.classList || e(this).hasClass("modal-close")) && (e("html").css({
                    "overflow-y": "scroll",
                    "padding-right": "0px"
                }).removeClass("modal-page"),
                //console.log(t),
                //console.log(e(t)),
                //console.log(i),
                //t=this,
                //t.addClass("testclass"),
                $(this).closest(".modal-open").css({
                    "overflow-y": "hidden"
                }).removeClass("modal-open").fadeOut(700),

                e(t).css({
                    "overflow-y": "hidden"
                }).removeClass("modal-open"),
                e(i).animate({
                    top: 0,
                    opacity: 0
                }, 700),
                e(".blured").removeClass("blured"),
                e(t).fadeOut(700)),
                e(".modal-wrapper2").removeClass("modal-wrapper2").addClass("modal-wrapper"),
                e(".modal2").removeClass("modal2").addClass("modal"),
                e(".modal-close2").removeClass("modal-close2").addClass("modal-close"),
                !1
            }
            return !1
        }
        e(".modal-wrapper").hide(),
        e("a[modal]").click(function() {
            var t = e(this).attr("href")
              , i = "17px";
            return !!e("html").hasClass("touch") && (i = "0px"),
            e("html").hasClass("modal-page") ? e("html").hasClass("modal-page") && (e("html").css({
                "overflow-y": "hidden",
                "padding-right": i
            }).addClass("modal-page2"),
            e(t).removeClass("modal-wrapper").addClass("modal-wrapper2"),
            e(t).find(".modal").removeClass("modal").addClass("modal2"),
            e(t).find(".modal-close").removeClass("modal-close").addClass("modal-close2"),
            e(t).addClass("modal-open").find(".modal.box").css({
                "-webkit-overflow-scrolling": "touch"
            })) : (e("html").css({
                "overflow-y": "hidden",
                "padding-right": i
            }).addClass("modal-page"),
            e(t).addClass("modal-open")),
            e(t).css({
                "overflow-y": "scroll"
            }),
            e("body>*").not(".modal-wrapper").not(".modal-wrapper *").not(".modal-wrapper2").not(".modal-wrapper2 *").addClass("blured"),
            e(t).fadeIn(700),
            e(t).find(".modal").animate({
                top: 50,
                opacity: 1
            }, 700),
            e("html").hasClass("modal-page2") && (e(".modal").animate({
                top: 0,
                opacity: 0
            }, 700),
            e(t).find(".modal2").animate({
                top: 50,
                opacity: 1
            }, 700)),
            !1
        }),
        e(".modal-close").click(t),
        e(".modal-wrapper").click(t),
        e(".modal-wrapper *").click(t),
        e(".modal-close2").click(t),
        e(".modal-wrapper2").click(t),
        e(".modal-wrapper2 *").click(t)
    }
    e(document).ready(t)
}(jQuery),
function(e) {
    function t() {
        e("[toggle]").each(function() {
            console.log("toggle", e(this).attr("toggle"));
            var t = JSON.parse(e(this).attr("toggle"))
              , i = e(this)
              , n = t.src
              , o = t.stay
              , s = t.autohide
              , r = t.animation;
            //console.log("toggle", t);
            o ? e(this).hasClass("active") ? e(n).slideDown() : e(n).not("[toggle]").slideUp() : e(this).hasClass("active") ? e(n).slideDown() : e(n).slideUp();
            var a = !1;
            if (s && e("html").hasClass("notouch")) {
                var l = function() {
                    a || ("slide" === r ? e(n).slideUp() : e(n).hide())
                }
                  , c = function() {
                    setTimeout(l, 2e3)
                };
                e(i).mouseenter(function() {
                    clearTimeout(l),
                    a = !0
                }),
                e(n).mouseenter(function() {
                    clearTimeout(l),
                    a = !0
                }),
                e(i).mouseleave(function() {
                    a = !1,
                    c()
                }),
                e(n).mouseleave(function() {
                    a = !1,
                    c()
                })
            }
            //console.log("bind", i, e(i));
            e(i).click(function() {
            //e(i).on("click", function() {
            //$(document).on('click', GetSelector(i), function(){
                console.log("click", this, n);
                var trgs = e(this).find(n);
                console.log("trgs", trgs);
                //if(trgs.length < 1) return false;
                if(trgs.length < 1) trgs = e(n);
                e(this).hasClass("active") ? e(this).removeClass("active") : e(this).addClass("active"),
                trgs.each(function() {
                    "slide" === r ? e(this).css("opacity") > 0 && "none" !== e(this).css("display") ? e(this).slideUp() : e(this).slideDown() : e(this).css("opacity") > 0 && "none" !== e(this).css("display") ? e(this).hide() : e(this).show()
                })
            })
        })
    }
    e(document).ready(t)
}(jQuery),
$("[animate]").length > 0 && function(e) {
    function t() {
        e.fn.viewportChecker = function(t) {
            var i = {
                classToAdd: "visible",
                offset: 100,
                callbackFunction: function(e) {}
            };
            e.extend(i, t);
            var n = this
              , o = e(window).height();
            this.checkElements = function() {
                var t = -1 != navigator.userAgent.toLowerCase().indexOf("webkit") ? "body" : "html"
                  , s = e(t).scrollTop()
                  , r = s + o;
                n.each(function() {
                    var t = e(this);
                    if (!t.hasClass(i.classToAdd)) {
                        var n = Math.round(t.offset().top) + i.offset
                          , o = n + t.height();
                        n < r && o > s && (t.addClass(i.classToAdd),
                        i.callbackFunction(t))
                    }
                })
            }
            ,
            e(window).scroll(this.checkElements),
            this.checkElements(),
            e(window).resize(function(e) {
                o = e.currentTarget.innerHeight
            })
        }
        ,
        e("[animate]").addClass("hidden").viewportChecker({
            classToAdd: "visible animated",
            offset: 0
        })
    }
    e(document).ready(t)
}(jQuery),
$("[switcher]").length > 0 && function(e) {
    function t() {
        e("[switcher]").each(function() {
            if(e(this).hasClass("nojs")) return 1;
            var t = JSON.parse(e(this).attr("switcher"))
              , i = e(this).addClass("switcher").attr({
                "sw-rel": t.src
            })
              , n = e(t.src).addClass("switcher").css({
                "list-style": "none"
            })
              , o = 0;
            e(i).find("a").each(function() {
                e(this).attr({
                    "sw-rel": o
                }),
                o++
            }),
            e(n).find("li").each(function() {
                e(this).attr({
                    num: e(this).index()
                }).hide()
            }),
            e(i).find("a").first().addClass("active"),
            e(n).find("li").first().addClass("active"),
            e("li.active").show()
        }),
        e("a[sw-rel]").click(function() {
            var t = e(this).closest(".switcher")
              , i = e(t).attr("sw-rel") ? e(t).attr("sw-rel") : "#" + e(t).attr("id")
              , n = JSON.parse(e('[sw-rel="' + i + '"]').attr("switcher"))
              , o = e(t).attr("sw-rel") ? e(t).find("a").length - 1 : e('[sw-rel="' + i + '"]').find("a").length - 1
              , s = +e(i).find("li.active").attr("num")
              , r = e(this).attr("sw-rel");
            t.find("a").removeClass("active"),
            e(this).addClass("active"),
            "next" === r ? r = s === o ? 0 : s + 1 : "prev" === r && (r = 0 === s ? o : s - 1),
            r !== s && (e(i + " li.active").hide().removeClass("active"),
            "fade" === n.animation ? e(i + ' li[num="' + r + '"]').fadeIn().addClass("active") : n.animation ? e(i + ' li[num="' + r + '"]').slideDown().addClass("active") : e(i + ' li[num="' + r + '"]').show().addClass("active"))
        })
    }
    e(document).ready(t)
}(jQuery),
$(".slider").length > 0 && (!function(e) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], e) : "undefined" != typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
}(function(e) {
    "use strict";
    var t = window.Slick || {};
    t = function() {
        function t(t, n) {
            var o, s = this;
            s.defaults = {
                accessibility: !0,
                adaptiveHeight: !1,
                appendArrows: e(t),
                appendDots: e(t),
                arrows: !0,
                asNavFor: null,
                prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">◄</button>',
                nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">►</button>',
                autoplay: !1,
                autoplaySpeed: 3e3,
                centerMode: !1,
                centerPadding: "50px",
                cssEase: "ease",
                customPaging: function(t, i) {
                    return e('<button type="button" data-role="none" role="button" tabindex="0" />').text(i + 1)
                },
                dots: !1,
                dotsClass: "slick-dots",
                draggable: !0,
                easing: "linear",
                edgeFriction: .35,
                fade: !1,
                focusOnSelect: !1,
                infinite: !0,
                initialSlide: 0,
                lazyLoad: "ondemand",
                mobileFirst: !1,
                pauseOnHover: !0,
                pauseOnFocus: !0,
                pauseOnDotsHover: !1,
                respondTo: "window",
                responsive: null,
                rows: 1,
                rtl: !1,
                slide: "",
                slidesPerRow: 1,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 500,
                swipe: !0,
                swipeToSlide: !1,
                touchMove: !0,
                touchThreshold: 5,
                useCSS: !0,
                useTransform: !0,
                variableWidth: !1,
                vertical: !1,
                verticalSwiping: !1,
                waitForAnimate: !0,
                zIndex: 1e3
            },
            s.initials = {
                animating: !1,
                dragging: !1,
                autoPlayTimer: null,
                currentDirection: 0,
                currentLeft: null,
                currentSlide: 0,
                direction: 1,
                $dots: null,
                listWidth: null,
                listHeight: null,
                loadIndex: 0,
                $nextArrow: null,
                $prevArrow: null,
                slideCount: null,
                slideWidth: null,
                $slideTrack: null,
                $slides: null,
                sliding: !1,
                slideOffset: 0,
                swipeLeft: null,
                $list: null,
                touchObject: {},
                transformsEnabled: !1,
                unslicked: !1
            },
            e.extend(s, s.initials),
            s.activeBreakpoint = null,
            s.animType = null,
            s.animProp = null,
            s.breakpoints = [],
            s.breakpointSettings = [],
            s.cssTransitions = !1,
            s.focussed = !1,
            s.interrupted = !1,
            s.hidden = "hidden",
            s.paused = !0,
            s.positionProp = null,
            s.respondTo = null,
            s.rowCount = 1,
            s.shouldClick = !0,
            s.$slider = e(t),
            s.$slidesCache = null,
            s.transformType = null,
            s.transitionType = null,
            s.visibilityChange = "visibilitychange",
            s.windowWidth = 0,
            s.windowTimer = null,
            o = e(t).data("slick") || {},
            s.options = e.extend({}, s.defaults, n, o),
            s.currentSlide = s.options.initialSlide,
            s.originalSettings = s.options,
            void 0 !== document.mozHidden ? (s.hidden = "mozHidden",
            s.visibilityChange = "mozvisibilitychange") : void 0 !== document.webkitHidden && (s.hidden = "webkitHidden",
            s.visibilityChange = "webkitvisibilitychange"),
            s.autoPlay = e.proxy(s.autoPlay, s),
            s.autoPlayClear = e.proxy(s.autoPlayClear, s),
            s.autoPlayIterator = e.proxy(s.autoPlayIterator, s),
            s.changeSlide = e.proxy(s.changeSlide, s),
            s.clickHandler = e.proxy(s.clickHandler, s),
            s.selectHandler = e.proxy(s.selectHandler, s),
            s.setPosition = e.proxy(s.setPosition, s),
            s.swipeHandler = e.proxy(s.swipeHandler, s),
            s.dragHandler = e.proxy(s.dragHandler, s),
            s.keyHandler = e.proxy(s.keyHandler, s),
            s.instanceUid = i++,
            s.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/,
            s.registerBreakpoints(),
            s.init(!0)
        }
        var i = 0;
        return t
    }(),
    t.prototype.activateADA = function() {
        this.$slideTrack.find(".slick-active").attr({
            "aria-hidden": "false"
        }).find("a, input, button, select").attr({
            tabindex: "0"
        })
    }
    ,
    t.prototype.addSlide = t.prototype.slickAdd = function(t, i, n) {
        var o = this;
        if ("boolean" == typeof i)
            n = i,
            i = null;
        else if (i < 0 || i >= o.slideCount)
            return !1;
        o.unload(),
        "number" == typeof i ? 0 === i && 0 === o.$slides.length ? e(t).appendTo(o.$slideTrack) : n ? e(t).insertBefore(o.$slides.eq(i)) : e(t).insertAfter(o.$slides.eq(i)) : !0 === n ? e(t).prependTo(o.$slideTrack) : e(t).appendTo(o.$slideTrack),
        o.$slides = o.$slideTrack.children(this.options.slide),
        o.$slideTrack.children(this.options.slide).detach(),
        o.$slideTrack.append(o.$slides),
        o.$slides.each(function(t, i) {
            e(i).attr("data-slick-index", t)
        }),
        o.$slidesCache = o.$slides,
        o.reinit()
    }
    ,
    t.prototype.animateHeight = function() {
        var e = this;
        if (1 === e.options.slidesToShow && !0 === e.options.adaptiveHeight && !1 === e.options.vertical) {
            var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
            e.$list.animate({
                height: t
            }, e.options.speed)
        }
    }
    ,
    t.prototype.animateSlide = function(t, i) {
        var n = {}
          , o = this;
        o.animateHeight(),
        !0 === o.options.rtl && !1 === o.options.vertical && (t = -t),
        !1 === o.transformsEnabled ? !1 === o.options.vertical ? o.$slideTrack.animate({
            left: t
        }, o.options.speed, o.options.easing, i) : o.$slideTrack.animate({
            top: t
        }, o.options.speed, o.options.easing, i) : !1 === o.cssTransitions ? (!0 === o.options.rtl && (o.currentLeft = -o.currentLeft),
        e({
            animStart: o.currentLeft
        }).animate({
            animStart: t
        }, {
            duration: o.options.speed,
            easing: o.options.easing,
            step: function(e) {
                e = Math.ceil(e),
                !1 === o.options.vertical ? (n[o.animType] = "translate(" + e + "px, 0px)",
                o.$slideTrack.css(n)) : (n[o.animType] = "translate(0px," + e + "px)",
                o.$slideTrack.css(n))
            },
            complete: function() {
                i && i.call()
            }
        })) : (o.applyTransition(),
        t = Math.ceil(t),
        //console.log("t", t),
        !1 === o.options.vertical ? n[o.animType] = "translate3d(" + t + "px, 0px, 0px)" : n[o.animType] = "translate3d(0px," + t + "px, 0px)",
        o.$slideTrack.css(n),
        i && setTimeout(function() {
            o.disableTransition(),
            i.call()
        }, o.options.speed))
    }
    ,
    t.prototype.getNavTarget = function() {
        var t = this
          , i = t.options.asNavFor;
        return i && null !== i && (i = e(i).not(t.$slider)),
        i
    }
    ,
    t.prototype.asNavFor = function(t) {
        var i = this
          , n = i.getNavTarget();
        null !== n && "object" == typeof n && n.each(function() {
            var i = e(this).slick("getSlick");
            i.unslicked || i.slideHandler(t, !0)
        })
    }
    ,
    t.prototype.applyTransition = function(e) {
        var t = this
          , i = {};
        !1 === t.options.fade ? i[t.transitionType] = t.transformType + " " + t.options.speed + "ms " + t.options.cssEase : i[t.transitionType] = "opacity " + t.options.speed + "ms " + t.options.cssEase,
        !1 === t.options.fade ? t.$slideTrack.css(i) : t.$slides.eq(e).css(i)
    }
    ,
    t.prototype.autoPlay = function() {
        var e = this;
        e.autoPlayClear(),
        e.slideCount > e.options.slidesToShow && (e.autoPlayTimer = setInterval(e.autoPlayIterator, e.options.autoplaySpeed))
    }
    ,
    t.prototype.autoPlayClear = function() {
        var e = this;
        e.autoPlayTimer && clearInterval(e.autoPlayTimer)
    }
    ,
    t.prototype.autoPlayIterator = function() {
        var e = this
          , t = e.currentSlide + e.options.slidesToScroll;
        e.paused || e.interrupted || e.focussed || (!1 === e.options.infinite && (1 === e.direction && e.currentSlide + 1 === e.slideCount - 1 ? e.direction = 0 : 0 === e.direction && (t = e.currentSlide - e.options.slidesToScroll,
        e.currentSlide - 1 == 0 && (e.direction = 1))),
        e.slideHandler(t))
    }
    ,
    t.prototype.buildArrows = function() {
        var t = this;
        !0 === t.options.arrows && (t.$prevArrow = e(t.options.prevArrow).addClass("slick-arrow"),
        t.$nextArrow = e(t.options.nextArrow).addClass("slick-arrow"),
        t.slideCount > t.options.slidesToShow ? (t.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),
        t.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),
        t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.prependTo(t.options.appendArrows),
        t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.appendTo(t.options.appendArrows),
        !0 !== t.options.infinite && t.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : t.$prevArrow.add(t.$nextArrow).addClass("slick-hidden").attr({
            "aria-disabled": "true",
            tabindex: "-1"
        }))
    }
    ,
    t.prototype.buildDots = function() {
        var t, i, n = this;
        if (!0 === n.options.dots && n.slideCount > n.options.slidesToShow) {
            for (n.$slider.addClass("slick-dotted"),
            i = e("<ul />").addClass(n.options.dotsClass),
            t = 0; t <= n.getDotCount(); t += 1)
                i.append(e("<li />").append(n.options.customPaging.call(this, n, t)));
            n.$dots = i.appendTo(n.options.appendDots),
            n.$dots.find("li").first().addClass("slick-active").attr("aria-hidden", "false")
        }
    }
    ,
    t.prototype.buildOut = function() {
        var t = this;
        t.$slides = t.$slider.children(t.options.slide + ":not(.slick-cloned)").addClass("slick-slide"),
        t.slideCount = t.$slides.length,
        t.$slides.each(function(t, i) {
            e(i).attr("data-slick-index", t).data("originalStyling", e(i).attr("style") || "")
        }),
        t.$slider.addClass("slick-slider"),
        t.$slideTrack = 0 === t.slideCount ? e('<div class="slick-track"/>').appendTo(t.$slider) : t.$slides.wrapAll('<div class="slick-track"/>').parent(),
        t.$list = t.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(),
        t.$slideTrack.css("opacity", 0),
        !0 !== t.options.centerMode && !0 !== t.options.swipeToSlide || (t.options.slidesToScroll = 1),
        e("img[data-lazy]", t.$slider).not("[src]").addClass("slick-loading"),
        t.setupInfinite(),
        t.buildArrows(),
        t.buildDots(),
        t.updateDots(),
        t.setSlideClasses("number" == typeof t.currentSlide ? t.currentSlide : 0),
        !0 === t.options.draggable && t.$list.addClass("draggable")
    }
    ,
    t.prototype.buildRows = function() {
        var e, t, i, n, o, s, r, a = this;
        if (n = document.createDocumentFragment(),
        s = a.$slider.children(),
        a.options.rows > 1) {
            for (r = a.options.slidesPerRow * a.options.rows,
            o = Math.ceil(s.length / r),
            e = 0; e < o; e++) {
                var l = document.createElement("div");
                for (t = 0; t < a.options.rows; t++) {
                    var c = document.createElement("div");
                    for (i = 0; i < a.options.slidesPerRow; i++) {
                        var u = e * r + (t * a.options.slidesPerRow + i);
                        s.get(u) && c.appendChild(s.get(u))
                    }
                    l.appendChild(c)
                }
                n.appendChild(l)
            }
            a.$slider.empty().append(n),
            a.$slider.children().children().children().css({
                width: 100 / a.options.slidesPerRow + "%",
                display: "inline-block"
            })
        }
    }
    ,
    t.prototype.checkResponsive = function(t, i) {
        var n, o, s, r = this, a = !1, l = r.$slider.width(), c = window.innerWidth || e(window).width();
        if ("window" === r.respondTo ? s = c : "slider" === r.respondTo ? s = l : "min" === r.respondTo && (s = Math.min(c, l)),
        r.options.responsive && r.options.responsive.length && null !== r.options.responsive) {
            o = null;
            for (n in r.breakpoints)
                r.breakpoints.hasOwnProperty(n) && (!1 === r.originalSettings.mobileFirst ? s < r.breakpoints[n] && (o = r.breakpoints[n]) : s > r.breakpoints[n] && (o = r.breakpoints[n]));
            null !== o ? null !== r.activeBreakpoint ? (o !== r.activeBreakpoint || i) && (r.activeBreakpoint = o,
            "unslick" === r.breakpointSettings[o] ? r.unslick(o) : (r.options = e.extend({}, r.originalSettings, r.breakpointSettings[o]),
            !0 === t && (r.currentSlide = r.options.initialSlide),
            r.refresh(t)),
            a = o) : (r.activeBreakpoint = o,
            "unslick" === r.breakpointSettings[o] ? r.unslick(o) : (r.options = e.extend({}, r.originalSettings, r.breakpointSettings[o]),
            !0 === t && (r.currentSlide = r.options.initialSlide),
            r.refresh(t)),
            a = o) : null !== r.activeBreakpoint && (r.activeBreakpoint = null,
            r.options = r.originalSettings,
            !0 === t && (r.currentSlide = r.options.initialSlide),
            r.refresh(t),
            a = o),
            t || !1 === a || r.$slider.trigger("breakpoint", [r, a])
        }
    }
    ,
    t.prototype.changeSlide = function(t, i) {
        var n, o, s, r = this, a = e(t.currentTarget);
        switch (a.is("a") && t.preventDefault(),
        a.is("li") || (a = a.closest("li")),
        s = r.slideCount % r.options.slidesToScroll != 0,
        n = s ? 0 : (r.slideCount - r.currentSlide) % r.options.slidesToScroll,
        t.data.message) {
        case "previous":
            o = 0 === n ? r.options.slidesToScroll : r.options.slidesToShow - n,
            r.slideCount > r.options.slidesToShow && r.slideHandler(r.currentSlide - o, !1, i);
            break;
        case "next":
            o = 0 === n ? r.options.slidesToScroll : n,
            r.slideCount > r.options.slidesToShow && r.slideHandler(r.currentSlide + o, !1, i);
            break;
        case "index":
            var l = 0 === t.data.index ? 0 : t.data.index || a.index() * r.options.slidesToScroll;
            r.slideHandler(r.checkNavigable(l), !1, i),
            a.children().trigger("focus");
            break;
        default:
            return
        }
    }
    ,
    t.prototype.checkNavigable = function(e) {
        var t, i, n = this;
        if (t = n.getNavigableIndexes(),
        i = 0,
        e > t[t.length - 1])
            e = t[t.length - 1];
        else
            for (var o in t) {
                if (e < t[o]) {
                    e = i;
                    break
                }
                i = t[o]
            }
        return e
    }
    ,
    t.prototype.cleanUpEvents = function() {
        var t = this;
        t.options.dots && null !== t.$dots && e("li", t.$dots).off("click.slick", t.changeSlide).off("mouseenter.slick", e.proxy(t.interrupt, t, !0)).off("mouseleave.slick", e.proxy(t.interrupt, t, !1)),
        t.$slider.off("focus.slick blur.slick"),
        !0 === t.options.arrows && t.slideCount > t.options.slidesToShow && (t.$prevArrow && t.$prevArrow.off("click.slick", t.changeSlide),
        t.$nextArrow && t.$nextArrow.off("click.slick", t.changeSlide)),
        t.$list.off("touchstart.slick mousedown.slick", t.swipeHandler),
        t.$list.off("touchmove.slick mousemove.slick", t.swipeHandler),
        t.$list.off("touchend.slick mouseup.slick", t.swipeHandler),
        t.$list.off("touchcancel.slick mouseleave.slick", t.swipeHandler),
        t.$list.off("click.slick", t.clickHandler),
        e(document).off(t.visibilityChange, t.visibility),
        t.cleanUpSlideEvents(),
        !0 === t.options.accessibility && t.$list.off("keydown.slick", t.keyHandler),
        !0 === t.options.focusOnSelect && e(t.$slideTrack).children().off("click.slick", t.selectHandler),
        e(window).off("orientationchange.slick.slick-" + t.instanceUid, t.orientationChange),
        e(window).off("resize.slick.slick-" + t.instanceUid, t.resize),
        e("[draggable!=true]", t.$slideTrack).off("dragstart", t.preventDefault),
        e(window).off("load.slick.slick-" + t.instanceUid, t.setPosition),
        e(document).off("ready.slick.slick-" + t.instanceUid, t.setPosition)
    }
    ,
    t.prototype.cleanUpSlideEvents = function() {
        var t = this;
        t.$list.off("mouseenter.slick", e.proxy(t.interrupt, t, !0)),
        t.$list.off("mouseleave.slick", e.proxy(t.interrupt, t, !1))
    }
    ,
    t.prototype.cleanUpRows = function() {
        var e, t = this;
        t.options.rows > 1 && (e = t.$slides.children().children(),
        e.removeAttr("style"),
        t.$slider.empty().append(e))
    }
    ,
    t.prototype.clickHandler = function(e) {
        !1 === this.shouldClick && (e.stopImmediatePropagation(),
        e.stopPropagation(),
        e.preventDefault())
    }
    ,
    t.prototype.destroy = function(t) {
        var i = this;
        i.autoPlayClear(),
        i.touchObject = {},
        i.cleanUpEvents(),
        e(".slick-cloned", i.$slider).detach(),
        i.$dots && i.$dots.remove(),
        i.$prevArrow && i.$prevArrow.length && (i.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""),
        i.htmlExpr.test(i.options.prevArrow) && i.$prevArrow.remove()),
        i.$nextArrow && i.$nextArrow.length && (i.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""),
        i.htmlExpr.test(i.options.nextArrow) && i.$nextArrow.remove()),
        i.$slides && (i.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function() {
            e(this).attr("style", e(this).data("originalStyling"))
        }),
        i.$slideTrack.children(this.options.slide).detach(),
        i.$slideTrack.detach(),
        i.$list.detach(),
        i.$slider.append(i.$slides)),
        i.cleanUpRows(),
        i.$slider.removeClass("slick-slider"),
        i.$slider.removeClass("slick-initialized"),
        i.$slider.removeClass("slick-dotted"),
        i.unslicked = !0,
        t || i.$slider.trigger("destroy", [i])
    }
    ,
    t.prototype.disableTransition = function(e) {
        var t = this
          , i = {};
        i[t.transitionType] = "",
        !1 === t.options.fade ? t.$slideTrack.css(i) : t.$slides.eq(e).css(i)
    }
    ,
    t.prototype.fadeSlide = function(e, t) {
        var i = this;
        !1 === i.cssTransitions ? (i.$slides.eq(e).css({
            zIndex: i.options.zIndex
        }),
        i.$slides.eq(e).animate({
            opacity: 1
        }, i.options.speed, i.options.easing, t)) : (i.applyTransition(e),
        i.$slides.eq(e).css({
            opacity: 1,
            zIndex: i.options.zIndex
        }),
        t && setTimeout(function() {
            i.disableTransition(e),
            t.call()
        }, i.options.speed))
    }
    ,
    t.prototype.fadeSlideOut = function(e) {
        var t = this;
        !1 === t.cssTransitions ? t.$slides.eq(e).animate({
            opacity: 0,
            zIndex: t.options.zIndex - 2
        }, t.options.speed, t.options.easing) : (t.applyTransition(e),
        t.$slides.eq(e).css({
            opacity: 0,
            zIndex: t.options.zIndex - 2
        }))
    }
    ,
    t.prototype.filterSlides = t.prototype.slickFilter = function(e) {
        var t = this;
        null !== e && (t.$slidesCache = t.$slides,
        t.unload(),
        t.$slideTrack.children(this.options.slide).detach(),
        t.$slidesCache.filter(e).appendTo(t.$slideTrack),
        t.reinit())
    }
    ,
    t.prototype.focusHandler = function() {
        var t = this;
        t.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*:not(.slick-arrow)", function(i) {
            i.stopImmediatePropagation();
            var n = e(this);
            setTimeout(function() {
                t.options.pauseOnFocus && (t.focussed = n.is(":focus"),
                t.autoPlay())
            }, 0)
        })
    }
    ,
    t.prototype.getCurrent = t.prototype.slickCurrentSlide = function() {
        return this.currentSlide
    }
    ,
    t.prototype.getDotCount = function() {
        var e = this
          , t = 0
          , i = 0
          , n = 0;
        if (!0 === e.options.infinite)
            for (; t < e.slideCount; )
                ++n,
                t = i + e.options.slidesToScroll,
                i += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
        else if (!0 === e.options.centerMode)
            n = e.slideCount;
        else if (e.options.asNavFor)
            for (; t < e.slideCount; )
                ++n,
                t = i + e.options.slidesToScroll,
                i += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
        else
            n = 1 + Math.ceil((e.slideCount - e.options.slidesToShow) / e.options.slidesToScroll);
        return n - 1
    }
    ,
    t.prototype.getLeft = function(e) {
        var t, i, n, o = this, s = 0;
        //console.log("o", o);
        //console.log("infinite", o.options.infinite);
        //console.log("slideCount", o.slideCount);
        //console.log("slidesToShow", o.options.slidesToShow);
        return o.slideOffset = 0,
        i = o.$slides.first().outerHeight(!0),
        !0 === o.options.infinite ? (o.slideCount > o.options.slidesToShow && (o.slideOffset = o.slideWidth * o.options.slidesToShow * -1,
        s = i * o.options.slidesToShow * -1),
        o.slideCount % o.options.slidesToScroll != 0 && e + o.options.slidesToScroll > o.slideCount && o.slideCount > o.options.slidesToShow && (e > o.slideCount ? (o.slideOffset = (o.options.slidesToShow - (e - o.slideCount)) * o.slideWidth * -1,
        s = (o.options.slidesToShow - (e - o.slideCount)) * i * -1) : (o.slideOffset = o.slideCount % o.options.slidesToScroll * o.slideWidth * -1,
        s = o.slideCount % o.options.slidesToScroll * i * -1))) : e + o.options.slidesToShow > o.slideCount && (o.slideOffset = (e + o.options.slidesToShow - o.slideCount) * o.slideWidth,
        s = (e + o.options.slidesToShow - o.slideCount) * i),
        o.slideCount <= o.options.slidesToShow && (o.slideOffset = 0,
        s = 0),
        !0 === o.options.centerMode && !0 === o.options.infinite ? o.slideOffset += o.slideWidth * Math.floor(o.options.slidesToShow / 2) - o.slideWidth : !0 === o.options.centerMode && (o.slideOffset = 0,
        o.slideOffset += o.slideWidth * Math.floor(o.options.slidesToShow / 2)),
        t = !1 === o.options.vertical ? e * o.slideWidth * -1 + o.slideOffset : e * i * -1 + s,
        !0 === o.options.variableWidth && (n = o.slideCount <= o.options.slidesToShow || !1 === o.options.infinite ? o.$slideTrack.children(".slick-slide").eq(e) : o.$slideTrack.children(".slick-slide").eq(e + o.options.slidesToShow),
        t = !0 === o.options.rtl ? n[0] ? -1 * (o.$slideTrack.width() - n[0].offsetLeft - n.width()) : 0 : n[0] ? -1 * n[0].offsetLeft : 0,
        !0 === o.options.centerMode && (n = o.slideCount <= o.options.slidesToShow || !1 === o.options.infinite ? o.$slideTrack.children(".slick-slide").eq(e) : o.$slideTrack.children(".slick-slide").eq(e + o.options.slidesToShow + 1),
        t = !0 === o.options.rtl ? n[0] ? -1 * (o.$slideTrack.width() - n[0].offsetLeft - n.width()) : 0 : n[0] ? -1 * n[0].offsetLeft : 0,
        t += (o.$list.width() - n.outerWidth()) / 2)),
        t
    }
    ,
    t.prototype.getOption = t.prototype.slickGetOption = function(e) {
        return this.options[e]
    }
    ,
    t.prototype.getNavigableIndexes = function() {
        var e, t = this, i = 0, n = 0, o = [];
        for (!1 === t.options.infinite ? e = t.slideCount : (i = -1 * t.options.slidesToScroll,
        n = -1 * t.options.slidesToScroll,
        e = 2 * t.slideCount); i < e; )
            o.push(i),
            i = n + t.options.slidesToScroll,
            n += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow;
        return o
    }
    ,
    t.prototype.getSlick = function() {
        return this
    }
    ,
    t.prototype.getSlideCount = function() {
        var t, i, n = this;
        return i = !0 === n.options.centerMode ? n.slideWidth * Math.floor(n.options.slidesToShow / 2) : 0,
        !0 === n.options.swipeToSlide ? (n.$slideTrack.find(".slick-slide").each(function(o, s) {
            if (s.offsetLeft - i + e(s).outerWidth() / 2 > -1 * n.swipeLeft)
                return t = s,
                !1
        }),
        Math.abs(e(t).attr("data-slick-index") - n.currentSlide) || 1) : n.options.slidesToScroll
    }
    ,
    t.prototype.goTo = t.prototype.slickGoTo = function(e, t) {
        this.changeSlide({
            data: {
                message: "index",
                index: parseInt(e)
            }
        }, t)
    }
    ,
    t.prototype.init = function(t) {
        var i = this;
        e(i.$slider).hasClass("slick-initialized") || (e(i.$slider).addClass("slick-initialized"),
        i.buildRows(),
        i.buildOut(),
        i.setProps(),
        i.startLoad(),
        i.loadSlider(),
        i.initializeEvents(),
        i.updateArrows(),
        i.updateDots(),
        i.checkResponsive(!0),
        i.focusHandler()),
        t && i.$slider.trigger("init", [i]),
        !0 === i.options.accessibility && i.initADA(),
        i.options.autoplay && (i.paused = !1,
        i.autoPlay())
    }
    ,
    t.prototype.initADA = function() {
        var t = this;
        t.$slides.add(t.$slideTrack.find(".slick-cloned")).attr({
            "aria-hidden": "true",
            tabindex: "-1"
        }).find("a, input, button, select").attr({
            tabindex: "-1"
        }),
        t.$slideTrack.attr("role", "listbox"),
        t.$slides.not(t.$slideTrack.find(".slick-cloned")).each(function(i) {
            e(this).attr({
                role: "option",
                "aria-describedby": "slick-slide" + t.instanceUid + i
            })
        }),
        null !== t.$dots && t.$dots.attr("role", "tablist").find("li").each(function(i) {
            e(this).attr({
                role: "presentation",
                "aria-selected": "false",
                "aria-controls": "navigation" + t.instanceUid + i,
                id: "slick-slide" + t.instanceUid + i
            })
        }).first().attr("aria-selected", "true").end().find("button").attr("role", "button").end().closest("div").attr("role", "toolbar"),
        t.activateADA()
    }
    ,
    t.prototype.initArrowEvents = function() {
        var e = this;
        !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.off("click.slick").on("click.slick", {
            message: "previous"
        }, e.changeSlide),
        e.$nextArrow.off("click.slick").on("click.slick", {
            message: "next"
        }, e.changeSlide))
    }
    ,
    t.prototype.initDotEvents = function() {
        var t = this;
        !0 === t.options.dots && t.slideCount > t.options.slidesToShow && e("li", t.$dots).on("click.slick", {
            message: "index"
        }, t.changeSlide),
        !0 === t.options.dots && !0 === t.options.pauseOnDotsHover && e("li", t.$dots).on("mouseenter.slick", e.proxy(t.interrupt, t, !0)).on("mouseleave.slick", e.proxy(t.interrupt, t, !1))
    }
    ,
    t.prototype.initSlideEvents = function() {
        var t = this;
        t.options.pauseOnHover && (t.$list.on("mouseenter.slick", e.proxy(t.interrupt, t, !0)),
        t.$list.on("mouseleave.slick", e.proxy(t.interrupt, t, !1)))
    }
    ,
    t.prototype.initializeEvents = function() {
        var t = this;
        t.initArrowEvents(),
        t.initDotEvents(),
        t.initSlideEvents(),
        t.$list.on("touchstart.slick mousedown.slick", {
            action: "start"
        }, t.swipeHandler),
        t.$list.on("touchmove.slick mousemove.slick", {
            action: "move"
        }, t.swipeHandler),
        t.$list.on("touchend.slick mouseup.slick", {
            action: "end"
        }, t.swipeHandler),
        t.$list.on("touchcancel.slick mouseleave.slick", {
            action: "end"
        }, t.swipeHandler),
        t.$list.on("click.slick", t.clickHandler),
        e(document).on(t.visibilityChange, e.proxy(t.visibility, t)),
        !0 === t.options.accessibility && t.$list.on("keydown.slick", t.keyHandler),
        !0 === t.options.focusOnSelect && e(t.$slideTrack).children().on("click.slick", t.selectHandler),
        e(window).on("orientationchange.slick.slick-" + t.instanceUid, e.proxy(t.orientationChange, t)),
        e(window).on("resize.slick.slick-" + t.instanceUid, e.proxy(t.resize, t)),
        e("[draggable!=true]", t.$slideTrack).on("dragstart", t.preventDefault),
        e(window).on("load.slick.slick-" + t.instanceUid, t.setPosition),
        e(document).on("ready.slick.slick-" + t.instanceUid, t.setPosition)
    }
    ,
    t.prototype.initUI = function() {
        var e = this;
        !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.show(),
        e.$nextArrow.show()),
        !0 === e.options.dots && e.slideCount > e.options.slidesToShow && e.$dots.show()
    }
    ,
    t.prototype.keyHandler = function(e) {
        var t = this;
        e.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === e.keyCode && !0 === t.options.accessibility ? t.changeSlide({
            data: {
                message: !0 === t.options.rtl ? "next" : "previous"
            }
        }) : 39 === e.keyCode && !0 === t.options.accessibility && t.changeSlide({
            data: {
                message: !0 === t.options.rtl ? "previous" : "next"
            }
        }))
    }
    ,
    t.prototype.lazyLoad = function() {
        function t(t) {
            e("img[data-lazy]", t).each(function() {
                var t = e(this)
                  , i = e(this).attr("data-lazy")
                  , n = document.createElement("img");
                n.onload = function() {
                    t.animate({
                        opacity: 0
                    }, 100, function() {
                        t.attr("src", i).animate({
                            opacity: 1
                        }, 200, function() {
                            t.removeAttr("data-lazy").removeClass("slick-loading")
                        }),
                        r.$slider.trigger("lazyLoaded", [r, t, i])
                    })
                }
                ,
                n.onerror = function() {
                    t.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),
                    r.$slider.trigger("lazyLoadError", [r, t, i])
                }
                ,
                n.src = i
            })
        }
        var i, n, o, s, r = this;
        !0 === r.options.centerMode ? !0 === r.options.infinite ? (o = r.currentSlide + (r.options.slidesToShow / 2 + 1),
        s = o + r.options.slidesToShow + 2) : (o = Math.max(0, r.currentSlide - (r.options.slidesToShow / 2 + 1)),
        s = r.options.slidesToShow / 2 + 1 + 2 + r.currentSlide) : (o = r.options.infinite ? r.options.slidesToShow + r.currentSlide : r.currentSlide,
        s = Math.ceil(o + r.options.slidesToShow),
        !0 === r.options.fade && (o > 0 && o--,
        s <= r.slideCount && s++)),
        i = r.$slider.find(".slick-slide").slice(o, s),
        t(i),
        r.slideCount <= r.options.slidesToShow ? (n = r.$slider.find(".slick-slide"),
        t(n)) : r.currentSlide >= r.slideCount - r.options.slidesToShow ? (n = r.$slider.find(".slick-cloned").slice(0, r.options.slidesToShow),
        t(n)) : 0 === r.currentSlide && (n = r.$slider.find(".slick-cloned").slice(-1 * r.options.slidesToShow),
        t(n))
    }
    ,
    t.prototype.loadSlider = function() {
        var e = this;
        e.setPosition(),
        e.$slideTrack.css({
            opacity: 1
        }),
        e.$slider.removeClass("slick-loading"),
        e.initUI(),
        "progressive" === e.options.lazyLoad && e.progressiveLazyLoad()
    }
    ,
    t.prototype.next = t.prototype.slickNext = function() {
        this.changeSlide({
            data: {
                message: "next"
            }
        })
    }
    ,
    t.prototype.orientationChange = function() {
        var e = this;
        e.checkResponsive(),
        e.setPosition()
    }
    ,
    t.prototype.pause = t.prototype.slickPause = function() {
        var e = this;
        e.autoPlayClear(),
        e.paused = !0
    }
    ,
    t.prototype.play = t.prototype.slickPlay = function() {
        var e = this;
        e.autoPlay(),
        e.options.autoplay = !0,
        e.paused = !1,
        e.focussed = !1,
        e.interrupted = !1
    }
    ,
    t.prototype.postSlide = function(e) {
        var t = this;
        t.unslicked || (t.$slider.trigger("afterChange", [t, e]),
        t.animating = !1,
        t.setPosition(),
        t.swipeLeft = null,
        t.options.autoplay && t.autoPlay(),
        !0 === t.options.accessibility && t.initADA())
    }
    ,
    t.prototype.prev = t.prototype.slickPrev = function() {
        this.changeSlide({
            data: {
                message: "previous"
            }
        })
    }
    ,
    t.prototype.preventDefault = function(e) {
        e.preventDefault()
    }
    ,
    t.prototype.progressiveLazyLoad = function(t) {
        t = t || 1;
        var i, n, o, s = this, r = e("img[data-lazy]", s.$slider);
        r.length ? (i = r.first(),
        n = i.attr("data-lazy"),
        o = document.createElement("img"),
        o.onload = function() {
            i.attr("src", n).removeAttr("data-lazy").removeClass("slick-loading"),
            !0 === s.options.adaptiveHeight && s.setPosition(),
            s.$slider.trigger("lazyLoaded", [s, i, n]),
            s.progressiveLazyLoad()
        }
        ,
        o.onerror = function() {
            t < 3 ? setTimeout(function() {
                s.progressiveLazyLoad(t + 1)
            }, 500) : (i.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),
            s.$slider.trigger("lazyLoadError", [s, i, n]),
            s.progressiveLazyLoad())
        }
        ,
        o.src = n) : s.$slider.trigger("allImagesLoaded", [s])
    }
    ,
    t.prototype.refresh = function(t) {
        var i, n, o = this;
        n = o.slideCount - o.options.slidesToShow,
        !o.options.infinite && o.currentSlide > n && (o.currentSlide = n),
        o.slideCount <= o.options.slidesToShow && (o.currentSlide = 0),
        i = o.currentSlide,
        o.destroy(!0),
        e.extend(o, o.initials, {
            currentSlide: i
        }),
        o.init(),
        t || o.changeSlide({
            data: {
                message: "index",
                index: i
            }
        }, !1)
    }
    ,
    t.prototype.registerBreakpoints = function() {
        var t, i, n, o = this, s = o.options.responsive || null;
        if ("array" === e.type(s) && s.length) {
            o.respondTo = o.options.respondTo || "window";
            for (t in s)
                if (n = o.breakpoints.length - 1,
                i = s[t].breakpoint,
                s.hasOwnProperty(t)) {
                    for (; n >= 0; )
                        o.breakpoints[n] && o.breakpoints[n] === i && o.breakpoints.splice(n, 1),
                        n--;
                    o.breakpoints.push(i),
                    o.breakpointSettings[i] = s[t].settings
                }
            o.breakpoints.sort(function(e, t) {
                return o.options.mobileFirst ? e - t : t - e
            })
        }
    }
    ,
    t.prototype.reinit = function() {
        var t = this;
        t.$slides = t.$slideTrack.children(t.options.slide).addClass("slick-slide"),
        t.slideCount = t.$slides.length,
        t.currentSlide >= t.slideCount && 0 !== t.currentSlide && (t.currentSlide = t.currentSlide - t.options.slidesToScroll),
        t.slideCount <= t.options.slidesToShow && (t.currentSlide = 0),
        t.registerBreakpoints(),
        t.setProps(),
        t.setupInfinite(),
        t.buildArrows(),
        t.updateArrows(),
        t.initArrowEvents(),
        t.buildDots(),
        t.updateDots(),
        t.initDotEvents(),
        t.cleanUpSlideEvents(),
        t.initSlideEvents(),
        t.checkResponsive(!1, !0),
        !0 === t.options.focusOnSelect && e(t.$slideTrack).children().on("click.slick", t.selectHandler),
        t.setSlideClasses("number" == typeof t.currentSlide ? t.currentSlide : 0),
        t.setPosition(),
        t.focusHandler(),
        t.paused = !t.options.autoplay,
        t.autoPlay(),
        t.$slider.trigger("reInit", [t])
    }
    ,
    t.prototype.resize = function() {
        var t = this;
        e(window).width() !== t.windowWidth && (clearTimeout(t.windowDelay),
        t.windowDelay = window.setTimeout(function() {
            t.windowWidth = e(window).width(),
            t.checkResponsive(),
            t.unslicked || t.setPosition()
        }, 50))
    }
    ,
    t.prototype.removeSlide = t.prototype.slickRemove = function(e, t, i) {
        var n = this;
        if ("boolean" == typeof e ? (t = e,
        e = !0 === t ? 0 : n.slideCount - 1) : e = !0 === t ? --e : e,
        n.slideCount < 1 || e < 0 || e > n.slideCount - 1)
            return !1;
        n.unload(),
        !0 === i ? n.$slideTrack.children().remove() : n.$slideTrack.children(this.options.slide).eq(e).remove(),
        n.$slides = n.$slideTrack.children(this.options.slide),
        n.$slideTrack.children(this.options.slide).detach(),
        n.$slideTrack.append(n.$slides),
        n.$slidesCache = n.$slides,
        n.reinit()
    }
    ,
    t.prototype.setCSS = function(e) {
        //console.log("e", e);
        if (e == -20800)
            e = -208;
        //if(e == -5200) e=-0;
        var t, i, n = this, o = {};
        !0 === n.options.rtl && (e = -e),
        t = "left" == n.positionProp ? Math.ceil(e) + "px" : "0px",
        i = "top" == n.positionProp ? Math.ceil(e) + "px" : "0px",
        o[n.positionProp] = e,
        !1 === n.transformsEnabled ? n.$slideTrack.css(o) : (o = {},
        //console.log("t2", t),
        !1 === n.cssTransitions ? (o[n.animType] = "translate(" + t + ", " + i + ")",
        n.$slideTrack.css(o)) : (o[n.animType] = "translate3d(" + t + ", " + i + ", 0px)",
        n.$slideTrack.css(o)))
    }
    ,
    t.prototype.setDimensions = function() {
        var e = this;
        !1 === e.options.vertical ? !0 === e.options.centerMode && e.$list.css({
            padding: "0px " + e.options.centerPadding
        }) : (e.$list.height(e.$slides.first().outerHeight(!0) * e.options.slidesToShow),
        !0 === e.options.centerMode && e.$list.css({
            padding: e.options.centerPadding + " 0px"
        })),
        e.listWidth = e.$list.width(),
        e.listHeight = e.$list.height(),
        !1 === e.options.vertical && !1 === e.options.variableWidth ? (e.slideWidth = Math.ceil(e.listWidth / e.options.slidesToShow),
        e.$slideTrack.width(Math.ceil(e.slideWidth * e.$slideTrack.children(".slick-slide").length))) : !0 === e.options.variableWidth ? e.$slideTrack.width(5e3 * e.slideCount) : (e.slideWidth = Math.ceil(e.listWidth),
        e.$slideTrack.height(Math.ceil(e.$slides.first().outerHeight(!0) * e.$slideTrack.children(".slick-slide").length)));
        var t = e.$slides.first().outerWidth(!0) - e.$slides.first().width();
        !1 === e.options.variableWidth && e.$slideTrack.children(".slick-slide").width(e.slideWidth - t)
    }
    ,
    t.prototype.setFade = function() {
        var t, i = this;
        i.$slides.each(function(n, o) {
            t = i.slideWidth * n * -1,
            !0 === i.options.rtl ? e(o).css({
                position: "relative",
                right: t,
                top: 0,
                zIndex: i.options.zIndex - 2,
                opacity: 0
            }) : e(o).css({
                position: "relative",
                left: t,
                top: 0,
                zIndex: i.options.zIndex - 2,
                opacity: 0
            })
        }),
        i.$slides.eq(i.currentSlide).css({
            zIndex: i.options.zIndex - 1,
            opacity: 1
        })
    }
    ,
    t.prototype.setHeight = function() {
        var e = this;
        if (1 === e.options.slidesToShow && !0 === e.options.adaptiveHeight && !1 === e.options.vertical) {
            var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
            e.$list.css("height", t)
        }
    }
    ,
    t.prototype.setOption = t.prototype.slickSetOption = function() {
        var t, i, n, o, s, r = this, a = !1;
        if ("object" === e.type(arguments[0]) ? (n = arguments[0],
        a = arguments[1],
        s = "multiple") : "string" === e.type(arguments[0]) && (n = arguments[0],
        o = arguments[1],
        a = arguments[2],
        "responsive" === arguments[0] && "array" === e.type(arguments[1]) ? s = "responsive" : void 0 !== arguments[1] && (s = "single")),
        "single" === s)
            r.options[n] = o;
        else if ("multiple" === s)
            e.each(n, function(e, t) {
                r.options[e] = t
            });
        else if ("responsive" === s)
            for (i in o)
                if ("array" !== e.type(r.options.responsive))
                    r.options.responsive = [o[i]];
                else {
                    for (t = r.options.responsive.length - 1; t >= 0; )
                        r.options.responsive[t].breakpoint === o[i].breakpoint && r.options.responsive.splice(t, 1),
                        t--;
                    r.options.responsive.push(o[i])
                }
        a && (r.unload(),
        r.reinit())
    }
    ,
    t.prototype.setPosition = function() {
        var e = this;
        e.setDimensions(),
        e.setHeight(),
        //console.log("this", this),
        //console.log("e2", e),
        !1 === e.options.fade ? e.setCSS(e.getLeft(e.currentSlide)) : e.setFade(),
        e.$slider.trigger("setPosition", [e])
    }
    ,
    t.prototype.setProps = function() {
        var e = this
          , t = document.body.style;
        e.positionProp = !0 === e.options.vertical ? "top" : "left",
        "top" === e.positionProp ? e.$slider.addClass("slick-vertical") : e.$slider.removeClass("slick-vertical"),
        void 0 === t.WebkitTransition && void 0 === t.MozTransition && void 0 === t.msTransition || !0 === e.options.useCSS && (e.cssTransitions = !0),
        e.options.fade && ("number" == typeof e.options.zIndex ? e.options.zIndex < 3 && (e.options.zIndex = 3) : e.options.zIndex = e.defaults.zIndex),
        void 0 !== t.OTransform && (e.animType = "OTransform",
        e.transformType = "-o-transform",
        e.transitionType = "OTransition",
        void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)),
        void 0 !== t.MozTransform && (e.animType = "MozTransform",
        e.transformType = "-moz-transform",
        e.transitionType = "MozTransition",
        void 0 === t.perspectiveProperty && void 0 === t.MozPerspective && (e.animType = !1)),
        void 0 !== t.webkitTransform && (e.animType = "webkitTransform",
        e.transformType = "-webkit-transform",
        e.transitionType = "webkitTransition",
        void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)),
        void 0 !== t.msTransform && (e.animType = "msTransform",
        e.transformType = "-ms-transform",
        e.transitionType = "msTransition",
        void 0 === t.msTransform && (e.animType = !1)),
        void 0 !== t.transform && !1 !== e.animType && (e.animType = "transform",
        e.transformType = "transform",
        e.transitionType = "transition"),
        e.transformsEnabled = e.options.useTransform && null !== e.animType && !1 !== e.animType
    }
    ,
    t.prototype.setSlideClasses = function(e) {
        var t, i, n, o, s = this;
        i = s.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"),
        s.$slides.eq(e).addClass("slick-current"),
        !0 === s.options.centerMode ? (t = Math.floor(s.options.slidesToShow / 2),
        !0 === s.options.infinite && (e >= t && e <= s.slideCount - 1 - t ? s.$slides.slice(e - t, e + t + 1).addClass("slick-active").attr("aria-hidden", "false") : (n = s.options.slidesToShow + e,
        i.slice(n - t + 1, n + t + 2).addClass("slick-active").attr("aria-hidden", "false")),
        0 === e ? i.eq(i.length - 1 - s.options.slidesToShow).addClass("slick-center") : e === s.slideCount - 1 && i.eq(s.options.slidesToShow).addClass("slick-center")),
        s.$slides.eq(e).addClass("slick-center")) : e >= 0 && e <= s.slideCount - s.options.slidesToShow ? s.$slides.slice(e, e + s.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : i.length <= s.options.slidesToShow ? i.addClass("slick-active").attr("aria-hidden", "false") : (o = s.slideCount % s.options.slidesToShow,
        n = !0 === s.options.infinite ? s.options.slidesToShow + e : e,
        s.options.slidesToShow == s.options.slidesToScroll && s.slideCount - e < s.options.slidesToShow ? i.slice(n - (s.options.slidesToShow - o), n + o).addClass("slick-active").attr("aria-hidden", "false") : i.slice(n, n + s.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false")),
        "ondemand" === s.options.lazyLoad && s.lazyLoad()
    }
    ,
    t.prototype.setupInfinite = function() {
        var t, i, n, o = this;
        if (!0 === o.options.fade && (o.options.centerMode = !1),
        !0 === o.options.infinite && !1 === o.options.fade && (i = null,
        o.slideCount > o.options.slidesToShow)) {
            for (n = !0 === o.options.centerMode ? o.options.slidesToShow + 1 : o.options.slidesToShow,
            t = o.slideCount; t > o.slideCount - n; t -= 1)
                i = t - 1,
                e(o.$slides[i]).clone(!0).attr("id", "").attr("data-slick-index", i - o.slideCount).prependTo(o.$slideTrack).addClass("slick-cloned");
            for (t = 0; t < n; t += 1)
                i = t,
                e(o.$slides[i]).clone(!0).attr("id", "").attr("data-slick-index", i + o.slideCount).appendTo(o.$slideTrack).addClass("slick-cloned");
            o.$slideTrack.find(".slick-cloned").find("[id]").each(function() {
                e(this).attr("id", "")
            })
        }
    }
    ,
    t.prototype.interrupt = function(e) {
        var t = this;
        e || t.autoPlay(),
        t.interrupted = e
    }
    ,
    t.prototype.selectHandler = function(t) {
        var i = this
          , n = e(t.target).is(".slick-slide") ? e(t.target) : e(t.target).parents(".slick-slide")
          , o = parseInt(n.attr("data-slick-index"));
        if (o || (o = 0),
        i.slideCount <= i.options.slidesToShow)
            return i.setSlideClasses(o),
            void i.asNavFor(o);
        i.slideHandler(o)
    }
    ,
    t.prototype.slideHandler = function(e, t, i) {
        var n, o, s, r, a, l = null, c = this;
        if (t = t || !1,
        (!0 !== c.animating || !0 !== c.options.waitForAnimate) && !(!0 === c.options.fade && c.currentSlide === e || c.slideCount <= c.options.slidesToShow)) {
            if (!1 === t && c.asNavFor(e),
            n = e,
            l = c.getLeft(n),
            r = c.getLeft(c.currentSlide),
            c.currentLeft = null === c.swipeLeft ? r : c.swipeLeft,
            !1 === c.options.infinite && !1 === c.options.centerMode && (e < 0 || e > c.getDotCount() * c.options.slidesToScroll))
                return void (!1 === c.options.fade && (n = c.currentSlide,
                !0 !== i ? c.animateSlide(r, function() {
                    c.postSlide(n)
                }) : c.postSlide(n)));
            if (!1 === c.options.infinite && !0 === c.options.centerMode && (e < 0 || e > c.slideCount - c.options.slidesToScroll))
                return void (!1 === c.options.fade && (n = c.currentSlide,
                !0 !== i ? c.animateSlide(r, function() {
                    c.postSlide(n)
                }) : c.postSlide(n)));
            if (c.options.autoplay && clearInterval(c.autoPlayTimer),
            o = n < 0 ? c.slideCount % c.options.slidesToScroll != 0 ? c.slideCount - c.slideCount % c.options.slidesToScroll : c.slideCount + n : n >= c.slideCount ? c.slideCount % c.options.slidesToScroll != 0 ? 0 : n - c.slideCount : n,
            c.animating = !0,
            c.$slider.trigger("beforeChange", [c, c.currentSlide, o]),
            s = c.currentSlide,
            c.currentSlide = o,
            c.setSlideClasses(c.currentSlide),
            c.options.asNavFor && (a = c.getNavTarget(),
            a = a.slick("getSlick"),
            a.slideCount <= a.options.slidesToShow && a.setSlideClasses(c.currentSlide)),
            c.updateDots(),
            c.updateArrows(),
            !0 === c.options.fade)
                return !0 !== i ? (c.fadeSlideOut(s),
                c.fadeSlide(o, function() {
                    c.postSlide(o)
                })) : c.postSlide(o),
                void c.animateHeight();
            !0 !== i ? c.animateSlide(l, function() {
                c.postSlide(o)
            }) : c.postSlide(o)
        }
    }
    ,
    t.prototype.startLoad = function() {
        var e = this;
        !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.hide(),
        e.$nextArrow.hide()),
        !0 === e.options.dots && e.slideCount > e.options.slidesToShow && e.$dots.hide(),
        e.$slider.addClass("slick-loading")
    }
    ,
    t.prototype.swipeDirection = function() {
        var e, t, i, n, o = this;
        return e = o.touchObject.startX - o.touchObject.curX,
        t = o.touchObject.startY - o.touchObject.curY,
        i = Math.atan2(t, e),
        n = Math.round(180 * i / Math.PI),
        n < 0 && (n = 360 - Math.abs(n)),
        n <= 45 && n >= 0 ? !1 === o.options.rtl ? "left" : "right" : n <= 360 && n >= 315 ? !1 === o.options.rtl ? "left" : "right" : n >= 135 && n <= 225 ? !1 === o.options.rtl ? "right" : "left" : !0 === o.options.verticalSwiping ? n >= 35 && n <= 135 ? "down" : "up" : "vertical"
    }
    ,
    t.prototype.swipeEnd = function(e) {
        var t, i, n = this;
        if (n.dragging = !1,
        n.interrupted = !1,
        n.shouldClick = !(n.touchObject.swipeLength > 10),
        void 0 === n.touchObject.curX)
            return !1;
        if (!0 === n.touchObject.edgeHit && n.$slider.trigger("edge", [n, n.swipeDirection()]),
        n.touchObject.swipeLength >= n.touchObject.minSwipe) {
            switch (i = n.swipeDirection()) {
            case "left":
            case "down":
                t = n.options.swipeToSlide ? n.checkNavigable(n.currentSlide + n.getSlideCount()) : n.currentSlide + n.getSlideCount(),
                n.currentDirection = 0;
                break;
            case "right":
            case "up":
                t = n.options.swipeToSlide ? n.checkNavigable(n.currentSlide - n.getSlideCount()) : n.currentSlide - n.getSlideCount(),
                n.currentDirection = 1
            }
            "vertical" != i && (n.slideHandler(t),
            n.touchObject = {},
            n.$slider.trigger("swipe", [n, i]))
        } else
            n.touchObject.startX !== n.touchObject.curX && (n.slideHandler(n.currentSlide),
            n.touchObject = {})
    }
    ,
    t.prototype.swipeHandler = function(e) {
        var t = this;
        if (!(!1 === t.options.swipe || "ontouchend"in document && !1 === t.options.swipe || !1 === t.options.draggable && -1 !== e.type.indexOf("mouse")))
            switch (t.touchObject.fingerCount = e.originalEvent && void 0 !== e.originalEvent.touches ? e.originalEvent.touches.length : 1,
            t.touchObject.minSwipe = t.listWidth / t.options.touchThreshold,
            !0 === t.options.verticalSwiping && (t.touchObject.minSwipe = t.listHeight / t.options.touchThreshold),
            e.data.action) {
            case "start":
                t.swipeStart(e);
                break;
            case "move":
                t.swipeMove(e);
                break;
            case "end":
                t.swipeEnd(e)
            }
    }
    ,
    t.prototype.swipeMove = function(e) {
        var t, i, n, o, s, r = this;
        return s = void 0 !== e.originalEvent ? e.originalEvent.touches : null,
        !(!r.dragging || s && 1 !== s.length) && (t = r.getLeft(r.currentSlide),
        r.touchObject.curX = void 0 !== s ? s[0].pageX : e.clientX,
        r.touchObject.curY = void 0 !== s ? s[0].pageY : e.clientY,
        r.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(r.touchObject.curX - r.touchObject.startX, 2))),
        !0 === r.options.verticalSwiping && (r.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(r.touchObject.curY - r.touchObject.startY, 2)))),
        "vertical" !== (i = r.swipeDirection()) ? (void 0 !== e.originalEvent && r.touchObject.swipeLength > 4 && e.preventDefault(),
        o = (!1 === r.options.rtl ? 1 : -1) * (r.touchObject.curX > r.touchObject.startX ? 1 : -1),
        !0 === r.options.verticalSwiping && (o = r.touchObject.curY > r.touchObject.startY ? 1 : -1),
        n = r.touchObject.swipeLength,
        r.touchObject.edgeHit = !1,
        !1 === r.options.infinite && (0 === r.currentSlide && "right" === i || r.currentSlide >= r.getDotCount() && "left" === i) && (n = r.touchObject.swipeLength * r.options.edgeFriction,
        r.touchObject.edgeHit = !0),
        !1 === r.options.vertical ? r.swipeLeft = t + n * o : r.swipeLeft = t + n * (r.$list.height() / r.listWidth) * o,
        !0 === r.options.verticalSwiping && (r.swipeLeft = t + n * o),
        //console.log("e3", e),
        !0 !== r.options.fade && !1 !== r.options.touchMove && (!0 === r.animating ? (r.swipeLeft = null,
        !1) : void r.setCSS(r.swipeLeft))) : void 0)
    }
    ,
    t.prototype.swipeStart = function(e) {
        var t, i = this;
        if (i.interrupted = !0,
        1 !== i.touchObject.fingerCount || i.slideCount <= i.options.slidesToShow)
            return i.touchObject = {},
            !1;
        void 0 !== e.originalEvent && void 0 !== e.originalEvent.touches && (t = e.originalEvent.touches[0]),
        i.touchObject.startX = i.touchObject.curX = void 0 !== t ? t.pageX : e.clientX,
        i.touchObject.startY = i.touchObject.curY = void 0 !== t ? t.pageY : e.clientY,
        i.dragging = !0
    }
    ,
    t.prototype.unfilterSlides = t.prototype.slickUnfilter = function() {
        var e = this;
        null !== e.$slidesCache && (e.unload(),
        e.$slideTrack.children(this.options.slide).detach(),
        e.$slidesCache.appendTo(e.$slideTrack),
        e.reinit())
    }
    ,
    t.prototype.unload = function() {
        var t = this;
        e(".slick-cloned", t.$slider).remove(),
        t.$dots && t.$dots.remove(),
        t.$prevArrow && t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.remove(),
        t.$nextArrow && t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.remove(),
        t.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
    }
    ,
    t.prototype.unslick = function(e) {
        var t = this;
        t.$slider.trigger("unslick", [t, e]),
        t.destroy()
    }
    ,
    t.prototype.updateArrows = function() {
        var e = this;
        Math.floor(e.options.slidesToShow / 2),
        !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && !e.options.infinite && (e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"),
        e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"),
        0 === e.currentSlide ? (e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"),
        e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - e.options.slidesToShow && !1 === e.options.centerMode ? (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"),
        e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - 1 && !0 === e.options.centerMode && (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"),
        e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")))
    }
    ,
    t.prototype.updateDots = function() {
        var e = this;
        null !== e.$dots && (e.$dots.find("li").removeClass("slick-active").attr("aria-hidden", "true"),
        e.$dots.find("li").eq(Math.floor(e.currentSlide / e.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden", "false"))
    }
    ,
    t.prototype.visibility = function() {
        var e = this;
        e.options.autoplay && (document[e.hidden] ? e.interrupted = !0 : e.interrupted = !1)
    }
    ,
    e.fn.slick = function() {
        var e, i, n = this, o = arguments[0], s = Array.prototype.slice.call(arguments, 1), r = n.length;
        for (e = 0; e < r; e++)
            if ("object" == typeof o || void 0 === o ? n[e].slick = new t(n[e],o) : i = n[e].slick[o].apply(n[e].slick, s),
            void 0 !== i)
                return i;
        return n
    }
}),
//console.log("pre slider main"),
//$(".slider.main").slick({autoplay:true,infinite:!0,autoplaySpeed:2e3,slidesToShow:1,variableWidth:false,dots:!1,arrows:!0,responsive:[{breakpoint:1179,settings:{autoplaySpeed:4e3,arrows:!1}}]}),
mailOwl = $(".slider.main").slick({
    autoplay: true,
    infinite: true,
    autoplaySpeed: 3e3,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: true,
    responsive: [{
        breakpoint: 1179,
        settings: {
            autoplaySpeed: 4e3
        }
    }, {
        breakpoint: 768,
        settings: {
            autoplaySpeed: 4e3,
            arrows: false,
            dots: true
        }
    }]
}),
//console.log("post slider main"),
$(".slider.slider-2").slick({
    autoplay: !1,
    infinite: !0,
    slidesToShow: 2,
    slidesToScroll: 1
}),
$(".slider.slider-4").slick({
    slidesToShow: 4,
    arrows: !0,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1179,
        settings: {
            slidesToShow: 3,
            arrows: !1
        }
    },
    {
        breakpoint: 767,
        settings: {
            slidesToShow: 2,
            arrows: !1
        }
    },
    {
        breakpoint: 370,
        settings: {
            slidesToShow: 1,
            arrows: !1
        }
    }]
}),
$(".slider.slider-5").slick({
    autoplay: !0,
    autoplaySpeed: 8e3,
    infinite: !0,
    slidesToShow: 5,
    arrows: !0,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1179,
        settings: {
            autoplaySpeed: 2e3,
            slidesToShow: 4,
            arrows: !1
        }
    },
    {
        breakpoint: 767,
        settings: {
            autoplaySpeed: 2e3,
            slidesToShow: 2,
            arrows: !1
        }
    }]
}),
$sldrl = $(".slider-left .slider.slider-ew50"),
$sldrl.slick({
    autoplay: !0,
    autoplaySpeed: 8e3,
    infinite: !0,
    slidesToShow: $sldrl.data('c2s'),
    arrows: !0,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1240,
        settings: {
            //slidesToShow: Math.floor($sldrl.data('c2s') - ($sldrl.data('c2s')/3)),
            slidesToShow: ($sldrl.closest('.slider-half').hasClass('.slider-full') ? 3 : ($sldrl.data('c2s') == '1' ? 1 : 2)),
        }
    },{
        breakpoint: 767,
        settings: {
            //slidesToShow: Math.floor($sldrl.data('c2s')/2),
            slidesToShow: ($sldrl.closest('.slider-half').hasClass('.slider-full') ? 2 : 1),
        }
    }]
}),
$sldrr = $(".slider-right .slider.slider-ew50"),
$sldrr.slick({
    autoplay: !0,
    autoplaySpeed: 8e3,
    infinite: !0,
    slidesToShow: $sldrr.data('c2s'),
    arrows: !0,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1240,
        settings: {
            //slidesToShow: Math.floor($sldrl.data('c2s') - ($sldrl.data('c2s')/3)),
            slidesToShow: ($sldrr.closest('.slider-half').hasClass('.slider-full') ? 3 : ($sldrr.data('c2s') == '1' ? 1 : 2)),
        }
    },{
        breakpoint: 767,
        settings: {
            //slidesToShow: Math.floor($sldrl.data('c2s')/2),
            slidesToShow: ($sldrr.closest('.slider-half').hasClass('.slider-full') ? 2 : 1),
        }
    }]
})
),
$(".what-the-size").length > 0 && (
    //$(".what-the-size-in>span,.what-the-size-in1>span").click(
    $(document).on('click', '.what-the-size-in>span,.what-the-size-in1>span', 
    function() {
    var $p = $(this).closest(".what-the-size");
    $p.addClass("choice").find(".numbr").text($(this).html());
    $(".achtung").fadeOut();
	
	 $(".achtung22").fadeOut();
	
    var t = JSON.parse($p.attr('toggle'));
    //console.log('6567', t);
    /*try{
        console.log(t["func"]);
        console.log(window[t["func"]]);
    }catch(e){
        console.log(e);
    }*/
    if(t["func"] != 'undefined' && t["func"].length > 0){
        //console.log('6575');
        if(typeof(window[t["func"]]) != "undefined"){
            //console.log('v1', window[t["func"]]);
            window[t["func"]](this);
        }else if(typeof(t["func"]) != "undefined"){
            //console.log('v2', t["func"]);
            try{
                t["func"](this);
            }catch(e){
                try{
                    eval(t["func"]);
                }catch(e){
                    console.log('Can`t call: ', t["func"]);
                }
            }
        }
    }
    //console.log('6578', t["func"], window[t["func"]]);
    /*if (typeof(window[t["func"]]) != "undefined") {
        if(t["func"] != undefined && t["func"].length > 0 && window[t["func"]]) window[t["func"]](this);
        //if(window[t["func"]] != undefined && window[t["func"]].length > 0)
            //eval(window[t["func"]]);
    }*/
}),

$("#closewind").click(function(e) {
   
 return $(".achtung22").fadeOut(),
        !1
	
}),
/*
$("#buy").click(function(e) {
    if ($(".what-the-size").hasClass("choice")){
        return $(".achtung22").fadeIn(),
            !1,
            setTimeout(function(){$('.achtung22').fadeOut()},5000);
    }
		
    if ($(".razmer44").hasClass("razmer33")){
        return $(".achtung22").fadeIn(),
            !1,
            setTimeout(function(){$('.achtung22').fadeOut()},5000);
    }
}),
*/
$(".what-the-size-in1>span").click(function() {
    $(this).closest(".what-the-size").addClass("choice").find(".numbr").text($(this).html()),
    $(".achtung1").fadeOut()
}),
$(".item-buy").click(function() {
    if (!$(".what-the-size").hasClass("choice"))
        return $(".achtung1").fadeIn(),
        !1
})),
$(".img-hvr").length > 0 && ($(".img-hvr .large").each(function() {
    $(this).height($(this).width())
}),
$(".img-hvr .small>div").each(function() {
    $(this).height($(this).width())
}),
$(".img-hvr .small>div").hover(function() {
    $(this).closest(".img-hvr").find(".large").attr({
        style: "background:url(" + $(this).attr("large") + ") center/contain no-repeat;height:" + $(this).closest(".img-hvr").find(".large").height() + "px"
    })
})),
$(".collect-wrap-more").length > 0 && ($(document).ready(collection),
$(window).resize(collection),
$(".collect-wrap-more").click(collection)),
$(".tog200").length > 0) {
    var n = 0;
    $(".tog200").each(function() {
        var e = $(this).offset().top;
        var html_part = '<p class="ptb10 txt-c"><a class="box tog200-';
        $(this).attr({
            tp: e
        }),
        $(this).children().each(function() {
            var t = $(this).offset().top;
            $(this).attr({
                tp: t
            }),
            t - e > 150 && $(this).addClass("tog200-" + n)
        }),
        $(this).append(html_part + n + '" toggle=\'{"src":".tog200-' + n + '","stay":"true", "animation":"slide"}\'>›</a></p>'),
        n++
    })
}
if ($("input").is("#gift-name1i") && ($("#gift-name1i").keyup(function() {
    $("#gift-name1").html($("#gift-name1i").val())
}),
$("#gift-name2i").keyup(function() {
    $("#gift-name2").html($("#gift-name2i").val())
})),
$("div").is("#slider-range")) {
    !function(e, t) {
        function i(t, i) {
            var o, s, r, a = t.nodeName.toLowerCase();
            return "area" === a ? (o = t.parentNode,
            s = o.name,
            !(!t.href || !s || "map" !== o.nodeName.toLowerCase()) && (!!(r = e("img[usemap=#" + s + "]")[0]) && n(r))) : (/input|select|textarea|button|object/.test(a) ? !t.disabled : "a" === a ? t.href || i : i) && n(t)
        }
        function n(t) {
            return e.expr.filters.visible(t) && !e(t).parents().addBack().filter(function() {
                return "hidden" === e.css(this, "visibility")
            }).length
        }
        var o = 0
          , s = /^ui-id-\d+$/;
        e.ui = e.ui || {},
        e.extend(e.ui, {
            version: "@VERSION",
            keyCode: {
                BACKSPACE: 8,
                COMMA: 188,
                DELETE: 46,
                DOWN: 40,
                END: 35,
                ENTER: 13,
                ESCAPE: 27,
                HOME: 36,
                LEFT: 37,
                PAGE_DOWN: 34,
                PAGE_UP: 33,
                PERIOD: 190,
                RIGHT: 39,
                SPACE: 32,
                TAB: 9,
                UP: 38
            }
        }),
        e.fn.extend({
            focus: function(t) {
                return function(i, n) {
                    return "number" == typeof i ? this.each(function() {
                        var t = this;
                        setTimeout(function() {
                            e(t).focus(),
                            n && n.call(t)
                        }, i)
                    }) : t.apply(this, arguments)
                }
            }(e.fn.focus),
            scrollParent: function() {
                var t;
                return t = e.ui.ie && /(static|relative)/.test(this.css("position")) || /absolute/.test(this.css("position")) ? this.parents().filter(function() {
                    return /(relative|absolute|fixed)/.test(e.css(this, "position")) && /(auto|scroll)/.test(e.css(this, "overflow") + e.css(this, "overflow-y") + e.css(this, "overflow-x"))
                }).eq(0) : this.parents().filter(function() {
                    return /(auto|scroll)/.test(e.css(this, "overflow") + e.css(this, "overflow-y") + e.css(this, "overflow-x"))
                }).eq(0),
                /fixed/.test(this.css("position")) || !t.length ? e(this[0].ownerDocument || document) : t
            },
            uniqueId: function() {
                return this.each(function() {
                    this.id || (this.id = "ui-id-" + ++o)
                })
            },
            removeUniqueId: function() {
                return this.each(function() {
                    s.test(this.id) && e(this).removeAttr("id")
                })
            }
        }),
        e.extend(e.expr[":"], {
            data: e.expr.createPseudo ? e.expr.createPseudo(function(t) {
                return function(i) {
                    return !!e.data(i, t)
                }
            }) : function(t, i, n) {
                return !!e.data(t, n[3])
            }
            ,
            focusable: function(t) {
                return i(t, !isNaN(e.attr(t, "tabindex")))
            },
            tabbable: function(t) {
                var n = e.attr(t, "tabindex")
                  , o = isNaN(n);
                return (o || n >= 0) && i(t, !o)
            }
        }),
        e("<a>").outerWidth(1).jquery || e.each(["Width", "Height"], function(t, i) {
            function n(t, i, n, s) {
                return e.each(o, function() {
                    i -= parseFloat(e.css(t, "padding" + this)) || 0,
                    n && (i -= parseFloat(e.css(t, "border" + this + "Width")) || 0),
                    s && (i -= parseFloat(e.css(t, "margin" + this)) || 0)
                }),
                i
            }
            var o = "Width" === i ? ["Left", "Right"] : ["Top", "Bottom"]
              , s = i.toLowerCase()
              , r = {
                innerWidth: e.fn.innerWidth,
                innerHeight: e.fn.innerHeight,
                outerWidth: e.fn.outerWidth,
                outerHeight: e.fn.outerHeight
            };
            e.fn["inner" + i] = function(t) {
                return void 0 === t ? r["inner" + i].call(this) : this.each(function() {
                    e(this).css(s, n(this, t) + "px")
                })
            }
            ,
            e.fn["outer" + i] = function(t, o) {
                return "number" != typeof t ? r["outer" + i].call(this, t) : this.each(function() {
                    e(this).css(s, n(this, t, !0, o) + "px")
                })
            }
        }),
        e.fn.addBack || (e.fn.addBack = function(e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }
        ),
        e("<a>").data("a-b", "a").removeData("a-b").data("a-b") && (e.fn.removeData = function(t) {
            return function(i) {
                return arguments.length ? t.call(this, e.camelCase(i)) : t.call(this)
            }
        }(e.fn.removeData)),
        e.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()),
        e.support.selectstart = "onselectstart"in document.createElement("div"),
        e.fn.extend({
            disableSelection: function() {
                return this.bind((e.support.selectstart ? "selectstart" : "mousedown") + ".ui-disableSelection", function(e) {
                    e.preventDefault()
                })
            },
            enableSelection: function() {
                return this.unbind(".ui-disableSelection")
            },
            zIndex: function(t) {
                if (void 0 !== t)
                    return this.css("zIndex", t);
                if (this.length)
                    for (var i, n, o = e(this[0]); o.length && o[0] !== document; ) {
                        if (("absolute" === (i = o.css("position")) || "relative" === i || "fixed" === i) && (n = parseInt(o.css("zIndex"), 10),
                        !isNaN(n) && 0 !== n))
                            return n;
                        o = o.parent()
                    }
                return 0
            }
        }),
        e.ui.plugin = {
            add: function(t, i, n) {
                var o, s = e.ui[t].prototype;
                for (o in n)
                    s.plugins[o] = s.plugins[o] || [],
                    s.plugins[o].push([i, n[o]])
            },
            call: function(e, t, i, n) {
                var o, s = e.plugins[t];
                if (s && (n || e.element[0].parentNode && 11 !== e.element[0].parentNode.nodeType))
                    for (o = 0; o < s.length; o++)
                        e.options[s[o][0]] && s[o][1].apply(e.element, i)
            }
        }
    }(jQuery),
    function(e, t) {
        var i = 0
          , n = Array.prototype.slice
          , o = e.cleanData;
        e.cleanData = function(t) {
            for (var i, n = 0; null != (i = t[n]); n++)
                try {
                    e(i).triggerHandler("remove")
                } catch (e) {}
            o(t)
        }
        ,
        e.widget = function(t, i, n) {
            var o, s, r, a, l = {}, c = t.split(".")[0];
            return t = t.split(".")[1],
            o = c + "-" + t,
            n || (n = i,
            i = e.Widget),
            e.expr[":"][o.toLowerCase()] = function(t) {
                return !!e.data(t, o)
            }
            ,
            e[c] = e[c] || {},
            s = e[c][t],
            r = e[c][t] = function(e, t) {
                if (!this._createWidget)
                    return new r(e,t);
                arguments.length && this._createWidget(e, t)
            }
            ,
            e.extend(r, s, {
                version: n.version,
                _proto: e.extend({}, n),
                _childConstructors: []
            }),
            a = new i,
            a.options = e.widget.extend({}, a.options),
            e.each(n, function(t, n) {
                if (!e.isFunction(n))
                    return void (l[t] = n);
                l[t] = function() {
                    var e = function() {
                        return i.prototype[t].apply(this, arguments)
                    }
                      , o = function(e) {
                        return i.prototype[t].apply(this, e)
                    };
                    return function() {
                        var t, i = this._super, s = this._superApply;
                        return this._super = e,
                        this._superApply = o,
                        t = n.apply(this, arguments),
                        this._super = i,
                        this._superApply = s,
                        t
                    }
                }()
            }),
            r.prototype = e.widget.extend(a, {
                widgetEventPrefix: s ? a.widgetEventPrefix || t : t
            }, l, {
                constructor: r,
                namespace: c,
                widgetName: t,
                widgetFullName: o
            }),
            s ? (e.each(s._childConstructors, function(t, i) {
                var n = i.prototype;
                e.widget(n.namespace + "." + n.widgetName, r, i._proto)
            }),
            delete s._childConstructors) : i._childConstructors.push(r),
            e.widget.bridge(t, r),
            r
        }
        ,
        e.widget.extend = function(t) {
            for (var i, o, s = n.call(arguments, 1), r = 0, a = s.length; r < a; r++)
                for (i in s[r])
                    o = s[r][i],
                    s[r].hasOwnProperty(i) && void 0 !== o && (e.isPlainObject(o) ? t[i] = e.isPlainObject(t[i]) ? e.widget.extend({}, t[i], o) : e.widget.extend({}, o) : t[i] = o);
            return t
        }
        ,
        e.widget.bridge = function(t, i) {
            var o = i.prototype.widgetFullName || t;
            e.fn[t] = function(s) {
                var r = "string" == typeof s
                  , a = n.call(arguments, 1)
                  , l = this;
                return s = !r && a.length ? e.widget.extend.apply(null, [s].concat(a)) : s,
                r ? this.each(function() {
                    var i, n = e.data(this, o);
                    return "instance" === s ? (l = n,
                    !1) : n ? e.isFunction(n[s]) && "_" !== s.charAt(0) ? (i = n[s].apply(n, a),
                    i !== n && void 0 !== i ? (l = i && i.jquery ? l.pushStack(i.get()) : i,
                    !1) : void 0) : e.error("no such method '" + s + "' for " + t + " widget instance") : e.error("cannot call methods on " + t + " prior to initialization; attempted to call method '" + s + "'")
                }) : this.each(function() {
                    var t = e.data(this, o);
                    t ? t.option(s || {})._init() : e.data(this, o, new i(s,this))
                }),
                l
            }
        }
        ,
        e.Widget = function() {}
        ,
        e.Widget._childConstructors = [],
        e.Widget.prototype = {
            widgetName: "widget",
            widgetEventPrefix: "",
            defaultElement: "<div>",
            options: {
                disabled: !1,
                create: null
            },
            _createWidget: function(t, n) {
                n = e(n || this.defaultElement || this)[0],
                this.element = e(n),
                this.uuid = i++,
                this.eventNamespace = "." + this.widgetName + this.uuid,
                this.options = e.widget.extend({}, this.options, this._getCreateOptions(), t),
                this.bindings = e(),
                this.hoverable = e(),
                this.focusable = e(),
                n !== this && (e.data(n, this.widgetFullName, this),
                this._on(!0, this.element, {
                    remove: function(e) {
                        e.target === n && this.destroy()
                    }
                }),
                this.document = e(n.style ? n.ownerDocument : n.document || n),
                this.window = e(this.document[0].defaultView || this.document[0].parentWindow)),
                this._create(),
                this._trigger("create", null, this._getCreateEventData()),
                this._init()
            },
            _getCreateOptions: e.noop,
            _getCreateEventData: e.noop,
            _create: e.noop,
            _init: e.noop,
            destroy: function() {
                this._destroy(),
                this.element.unbind(this.eventNamespace).removeData(this.widgetFullName).removeData(e.camelCase(this.widgetFullName)),
                this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled ui-state-disabled"),
                this.bindings.unbind(this.eventNamespace),
                this.hoverable.removeClass("ui-state-hover"),
                this.focusable.removeClass("ui-state-focus")
            },
            _destroy: e.noop,
            widget: function() {
                return this.element
            },
            option: function(t, i) {
                var n, o, s, r = t;
                if (0 === arguments.length)
                    return e.widget.extend({}, this.options);
                if ("string" == typeof t)
                    if (r = {},
                    n = t.split("."),
                    t = n.shift(),
                    n.length) {
                        for (o = r[t] = e.widget.extend({}, this.options[t]),
                        s = 0; s < n.length - 1; s++)
                            o[n[s]] = o[n[s]] || {},
                            o = o[n[s]];
                        if (t = n.pop(),
                        void 0 === i)
                            return void 0 === o[t] ? null : o[t];
                        o[t] = i
                    } else {
                        if (void 0 === i)
                            return void 0 === this.options[t] ? null : this.options[t];
                        r[t] = i
                    }
                return this._setOptions(r),
                this
            },
            _setOptions: function(e) {
                var t;
                for (t in e)
                    this._setOption(t, e[t]);
                return this
            },
            _setOption: function(e, t) {
                return this.options[e] = t,
                "disabled" === e && (this.widget().toggleClass(this.widgetFullName + "-disabled", !!t),
                this.hoverable.removeClass("ui-state-hover"),
                this.focusable.removeClass("ui-state-focus")),
                this
            },
            enable: function() {
                return this._setOptions({
                    disabled: !1
                })
            },
            disable: function() {
                return this._setOptions({
                    disabled: !0
                })
            },
            _on: function(t, i, n) {
                var o, s = this;
                "boolean" != typeof t && (n = i,
                i = t,
                t = !1),
                n ? (i = o = e(i),
                this.bindings = this.bindings.add(i)) : (n = i,
                i = this.element,
                o = this.widget()),
                e.each(n, function(n, r) {
                    function a() {
                        if (t || !0 !== s.options.disabled && !e(this).hasClass("ui-state-disabled"))
                            return ("string" == typeof r ? s[r] : r).apply(s, arguments)
                    }
                    "string" != typeof r && (a.guid = r.guid = r.guid || a.guid || e.guid++);
                    var l = n.match(/^(\w+)\s*(.*)$/)
                      , c = l[1] + s.eventNamespace
                      , u = l[2];
                    u ? o.delegate(u, c, a) : i.bind(c, a)
                })
            },
            _off: function(e, t) {
                t = (t || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace,
                e.unbind(t).undelegate(t)
            },
            _delay: function(e, t) {
                function i() {
                    return ("string" == typeof e ? n[e] : e).apply(n, arguments)
                }
                var n = this;
                return setTimeout(i, t || 0)
            },
            _hoverable: function(t) {
                this.hoverable = this.hoverable.add(t),
                this._on(t, {
                    mouseenter: function(t) {
                        e(t.currentTarget).addClass("ui-state-hover")
                    },
                    mouseleave: function(t) {
                        e(t.currentTarget).removeClass("ui-state-hover")
                    }
                })
            },
            _focusable: function(t) {
                this.focusable = this.focusable.add(t),
                this._on(t, {
                    focusin: function(t) {
                        e(t.currentTarget).addClass("ui-state-focus")
                    },
                    focusout: function(t) {
                        e(t.currentTarget).removeClass("ui-state-focus")
                    }
                })
            },
            _trigger: function(t, i, n) {
                var o, s, r = this.options[t];
                if (n = n || {},
                i = e.Event(i),
                i.type = (t === this.widgetEventPrefix ? t : this.widgetEventPrefix + t).toLowerCase(),
                i.target = this.element[0],
                s = i.originalEvent)
                    for (o in s)
                        o in i || (i[o] = s[o]);
                return this.element.trigger(i, n),
                !(e.isFunction(r) && !1 === r.apply(this.element[0], [i].concat(n)) || i.isDefaultPrevented())
            }
        },
        e.each({
            show: "fadeIn",
            hide: "fadeOut"
        }, function(t, i) {
            e.Widget.prototype["_" + t] = function(n, o, s) {
                "string" == typeof o && (o = {
                    effect: o
                });
                var r, a = o ? !0 === o || "number" == typeof o ? i : o.effect || i : t;
                o = o || {},
                "number" == typeof o && (o = {
                    duration: o
                }),
                r = !e.isEmptyObject(o),
                o.complete = s,
                o.delay && n.delay(o.delay),
                r && e.effects && e.effects.effect[a] ? n[t](o) : a !== t && n[a] ? n[a](o.duration, o.easing, s) : n.queue(function(i) {
                    e(this)[t](),
                    s && s.call(n[0]),
                    i()
                })
            }
        })
    }(jQuery),
    function(e, t) {
        var i = !1;
        e(document).mouseup(function() {
            i = !1
        }),
        e.widget("ui.mouse", {
            version: "@VERSION",
            options: {
                cancel: "input,textarea,button,select,option",
                distance: 1,
                delay: 0
            },
            _mouseInit: function() {
                var t = this;
                this.element.bind("mousedown." + this.widgetName, function(e) {
                    return t._mouseDown(e)
                }).bind("click." + this.widgetName, function(i) {
                    if (!0 === e.data(i.target, t.widgetName + ".preventClickEvent"))
                        return e.removeData(i.target, t.widgetName + ".preventClickEvent"),
                        i.stopImmediatePropagation(),
                        !1
                }),
                this.started = !1
            },
            _mouseDestroy: function() {
                this.element.unbind("." + this.widgetName),
                this._mouseMoveDelegate && this.document.unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate)
            },
            _mouseDown: function(t) {
                if (!i) {
                    this._mouseStarted && this._mouseUp(t),
                    this._mouseDownEvent = t;
                    var n = this
                      , o = 1 === t.which
                      , s = !("string" != typeof this.options.cancel || !t.target.nodeName) && e(t.target).closest(this.options.cancel).length;
                    return !(o && !s && this._mouseCapture(t)) || (this.mouseDelayMet = !this.options.delay,
                    this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function() {
                        n.mouseDelayMet = !0
                    }, this.options.delay)),
                    this._mouseDistanceMet(t) && this._mouseDelayMet(t) && (this._mouseStarted = !1 !== this._mouseStart(t),
                    !this._mouseStarted) ? (t.preventDefault(),
                    !0) : (!0 === e.data(t.target, this.widgetName + ".preventClickEvent") && e.removeData(t.target, this.widgetName + ".preventClickEvent"),
                    this._mouseMoveDelegate = function(e) {
                        return n._mouseMove(e)
                    }
                    ,
                    this._mouseUpDelegate = function(e) {
                        return n._mouseUp(e)
                    }
                    ,
                    this.document.bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate),
                    t.preventDefault(),
                    i = !0,
                    !0))
                }
            },
            _mouseMove: function(t) {
                return e.ui.ie && (!document.documentMode || document.documentMode < 9) && !t.button ? this._mouseUp(t) : t.which ? this._mouseStarted ? (this._mouseDrag(t),
                t.preventDefault()) : (this._mouseDistanceMet(t) && this._mouseDelayMet(t) && (this._mouseStarted = !1 !== this._mouseStart(this._mouseDownEvent, t),
                this._mouseStarted ? this._mouseDrag(t) : this._mouseUp(t)),
                !this._mouseStarted) : this._mouseUp(t)
            },
            _mouseUp: function(t) {
                return this.document.unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate),
                this._mouseStarted && (this._mouseStarted = !1,
                t.target === this._mouseDownEvent.target && e.data(t.target, this.widgetName + ".preventClickEvent", !0),
                this._mouseStop(t)),
                i = !1,
                !1
            },
            _mouseDistanceMet: function(e) {
                return Math.max(Math.abs(this._mouseDownEvent.pageX - e.pageX), Math.abs(this._mouseDownEvent.pageY - e.pageY)) >= this.options.distance
            },
            _mouseDelayMet: function() {
                return this.mouseDelayMet
            },
            _mouseStart: function() {},
            _mouseDrag: function() {},
            _mouseStop: function() {},
            _mouseCapture: function() {
                return !0
            }
        })
    }(jQuery),
    function(e, t) {
        e.widget("ui.slider", e.ui.mouse, {
            version: "@VERSION",
            widgetEventPrefix: "slide",
            options: {
                animate: !1,
                distance: 0,
                max: 100,
                min: 0,
                orientation: "horizontal",
                range: !1,
                step: 1,
                value: 0,
                values: null,
                change: null,
                slide: null,
                start: null,
                stop: null
            },
            _create: function() {
                this._keySliding = !1,
                this._mouseSliding = !1,
                this._animateOff = !0,
                this._handleIndex = null,
                this._detectOrientation(),
                this._mouseInit(),
                this.element.addClass("ui-slider ui-slider-" + this.orientation + " ui-widget ui-widget-content ui-corner-all"),
                this._refresh(),
                this._setOption("disabled", this.options.disabled),
                this._animateOff = !1
            },
            _refresh: function() {
                this._createRange(),
                this._createHandles(),
                this._setupEvents(),
                this._refreshValue()
            },
            _createHandles: function() {
                var t, i, n = this.options, o = this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"), s = [];
                for (i = n.values && n.values.length || 1,
                o.length > i && (o.slice(i).remove(),
                o = o.slice(0, i)),
                t = o.length; t < i; t++)
                    s.push("<a class='ui-slider-handle ui-state-default ui-corner-all' href='#'></a>");
                this.handles = o.add(e(s.join("")).appendTo(this.element)),
                this.handle = this.handles.eq(0),
                this.handles.each(function(t) {
                    e(this).data("ui-slider-handle-index", t)
                })
            },
            _createRange: function() {
                var t = this.options
                  , i = "";
                t.range ? (!0 === t.range && (t.values ? t.values.length && 2 !== t.values.length ? t.values = [t.values[0], t.values[0]] : e.isArray(t.values) && (t.values = t.values.slice(0)) : t.values = [this._valueMin(), this._valueMin()]),
                this.range && this.range.length ? this.range.removeClass("ui-slider-range-min ui-slider-range-max").css({
                    left: "",
                    bottom: ""
                }) : (this.range = e("<div></div>").appendTo(this.element),
                i = "ui-slider-range ui-widget-header ui-corner-all"),
                this.range.addClass(i + ("min" === t.range || "max" === t.range ? " ui-slider-range-" + t.range : ""))) : (this.range && this.range.remove(),
                this.range = null)
            },
            _setupEvents: function() {
                var e = this.handles.add(this.range).filter("a");
                this._off(e),
                this._on(e, this._handleEvents),
                this._hoverable(e),
                this._focusable(e)
            },
            _destroy: function() {
                this.handles.remove(),
                this.range && this.range.remove(),
                this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-widget ui-widget-content ui-corner-all"),
                this._mouseDestroy()
            },
            _mouseCapture: function(t) {
                var i, n, o, s, r, a, l, c = this, u = this.options;
                return !u.disabled && (this.elementSize = {
                    width: this.element.outerWidth(),
                    height: this.element.outerHeight()
                },
                this.elementOffset = this.element.offset(),
                i = {
                    x: t.pageX,
                    y: t.pageY
                },
                n = this._normValueFromMouse(i),
                o = this._valueMax() - this._valueMin() + 1,
                this.handles.each(function(t) {
                    var i = Math.abs(n - c.values(t));
                    (o > i || o === i && (t === c._lastChangedValue || c.values(t) === u.min)) && (o = i,
                    s = e(this),
                    r = t)
                }),
                !1 !== this._start(t, r) && (this._mouseSliding = !0,
                this._handleIndex = r,
                s.addClass("ui-state-active").focus(),
                a = s.offset(),
                l = !e(t.target).parents().addBack().is(".ui-slider-handle"),
                this._clickOffset = l ? {
                    left: 0,
                    top: 0
                } : {
                    left: t.pageX - a.left - s.width() / 2,
                    top: t.pageY - a.top - s.height() / 2 - (parseInt(s.css("borderTopWidth"), 10) || 0) - (parseInt(s.css("borderBottomWidth"), 10) || 0) + (parseInt(s.css("marginTop"), 10) || 0)
                },
                this.handles.hasClass("ui-state-hover") || this._slide(t, r, n),
                this._animateOff = !0,
                !0))
            },
            _mouseStart: function() {
                return !0
            },
            _mouseDrag: function(e) {
                var t = {
                    x: e.pageX,
                    y: e.pageY
                }
                  , i = this._normValueFromMouse(t);
                return this._slide(e, this._handleIndex, i),
                !1
            },
            _mouseStop: function(e) {
                return this.handles.removeClass("ui-state-active"),
                this._mouseSliding = !1,
                this._stop(e, this._handleIndex),
                this._change(e, this._handleIndex),
                this._handleIndex = null,
                this._clickOffset = null,
                this._animateOff = !1,
                !1
            },
            _detectOrientation: function() {
                this.orientation = "vertical" === this.options.orientation ? "vertical" : "horizontal"
            },
            _normValueFromMouse: function(e) {
                var t, i, n, o, s;
                return "horizontal" === this.orientation ? (t = this.elementSize.width,
                i = e.x - this.elementOffset.left - (this._clickOffset ? this._clickOffset.left : 0)) : (t = this.elementSize.height,
                i = e.y - this.elementOffset.top - (this._clickOffset ? this._clickOffset.top : 0)),
                n = i / t,
                n > 1 && (n = 1),
                n < 0 && (n = 0),
                "vertical" === this.orientation && (n = 1 - n),
                o = this._valueMax() - this._valueMin(),
                s = this._valueMin() + n * o,
                this._trimAlignValue(s)
            },
            _start: function(e, t) {
                var i = {
                    handle: this.handles[t],
                    value: this.value()
                };
                return this.options.values && this.options.values.length && (i.value = this.values(t),
                i.values = this.values()),
                this._trigger("start", e, i)
            },
            _slide: function(e, t, i) {
                var n, o, s;
                this.options.values && this.options.values.length ? (n = this.values(t ? 0 : 1),
                2 === this.options.values.length && !0 === this.options.range && (0 === t && i > n || 1 === t && i < n) && (i = n),
                i !== this.values(t) && (o = this.values(),
                o[t] = i,
                s = this._trigger("slide", e, {
                    handle: this.handles[t],
                    value: i,
                    values: o
                }),
                n = this.values(t ? 0 : 1),
                !1 !== s && this.values(t, i, !0))) : i !== this.value() && !1 !== (s = this._trigger("slide", e, {
                    handle: this.handles[t],
                    value: i
                })) && this.value(i)
            },
            _stop: function(e, t) {
                var i = {
                    handle: this.handles[t],
                    value: this.value()
                };
                this.options.values && this.options.values.length && (i.value = this.values(t),
                i.values = this.values()),
                this._trigger("stop", e, i)
            },
            _change: function(e, t) {
                if (!this._keySliding && !this._mouseSliding) {
                    var i = {
                        handle: this.handles[t],
                        value: this.value()
                    };
                    this.options.values && this.options.values.length && (i.value = this.values(t),
                    i.values = this.values()),
                    this._lastChangedValue = t,
                    this._trigger("change", e, i)
                }
            },
            value: function(e) {
                return arguments.length ? (this.options.value = this._trimAlignValue(e),
                this._refreshValue(),
                void this._change(null, 0)) : this._value()
            },
            values: function(t, i) {
                var n, o, s;
                if (arguments.length > 1)
                    return this.options.values[t] = this._trimAlignValue(i),
                    this._refreshValue(),
                    void this._change(null, t);
                if (!arguments.length)
                    return this._values();
                if (!e.isArray(arguments[0]))
                    return this.options.values && this.options.values.length ? this._values(t) : this.value();
                for (n = this.options.values,
                o = arguments[0],
                s = 0; s < n.length; s += 1)
                    n[s] = this._trimAlignValue(o[s]),
                    this._change(null, s);
                this._refreshValue()
            },
            _setOption: function(t, i) {
                var n, o = 0;
                switch ("range" === t && !0 === this.options.range && ("min" === i ? (this.options.value = this._values(0),
                this.options.values = null) : "max" === i && (this.options.value = this._values(this.options.values.length - 1),
                this.options.values = null)),
                e.isArray(this.options.values) && (o = this.options.values.length),
                "disabled" === t && this.element.toggleClass("ui-state-disabled", !!i),
                this._super(t, i),
                t) {
                case "orientation":
                    this._detectOrientation(),
                    this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-" + this.orientation),
                    this._refreshValue();
                    break;
                case "value":
                    this._animateOff = !0,
                    this._refreshValue(),
                    this._change(null, 0),
                    this._animateOff = !1;
                    break;
                case "values":
                    for (this._animateOff = !0,
                    this._refreshValue(),
                    n = 0; n < o; n += 1)
                        this._change(null, n);
                    this._animateOff = !1;
                    break;
                case "min":
                case "max":
                    this._animateOff = !0,
                    this._refreshValue(),
                    this._animateOff = !1;
                    break;
                case "range":
                    this._animateOff = !0,
                    this._refresh(),
                    this._animateOff = !1
                }
            },
            _value: function() {
                var e = this.options.value;
                return e = this._trimAlignValue(e)
            },
            _values: function(e) {
                var t, i, n;
                if (arguments.length)
                    return t = this.options.values[e],
                    t = this._trimAlignValue(t);
                if (this.options.values && this.options.values.length) {
                    for (i = this.options.values.slice(),
                    n = 0; n < i.length; n += 1)
                        i[n] = this._trimAlignValue(i[n]);
                    return i
                }
                return []
            },
            _trimAlignValue: function(e) {
                if (e <= this._valueMin())
                    return this._valueMin();
                if (e >= this._valueMax())
                    return this._valueMax();
                var t = this.options.step > 0 ? this.options.step : 1
                  , i = (e - this._valueMin()) % t
                  , n = e - i;
                return 2 * Math.abs(i) >= t && (n += i > 0 ? t : -t),
                parseFloat(n.toFixed(5))
            },
            _valueMin: function() {
                return this.options.min
            },
            _valueMax: function() {
                return this.options.max
            },
            _refreshValue: function() {
                var t, i, n, o, s, r = this.options.range, a = this.options, l = this, c = !this._animateOff && a.animate, u = {};
                this.options.values && this.options.values.length ? this.handles.each(function(n) {
                    i = (l.values(n) - l._valueMin()) / (l._valueMax() - l._valueMin()) * 100,
                    u["horizontal" === l.orientation ? "left" : "bottom"] = i + "%",
                    e(this).stop(1, 1)[c ? "animate" : "css"](u, a.animate),
                    !0 === l.options.range && ("horizontal" === l.orientation ? (0 === n && l.range.stop(1, 1)[c ? "animate" : "css"]({
                        left: i + "%"
                    }, a.animate),
                    1 === n && l.range[c ? "animate" : "css"]({
                        width: i - t + "%"
                    }, {
                        queue: !1,
                        duration: a.animate
                    })) : (0 === n && l.range.stop(1, 1)[c ? "animate" : "css"]({
                        bottom: i + "%"
                    }, a.animate),
                    1 === n && l.range[c ? "animate" : "css"]({
                        height: i - t + "%"
                    }, {
                        queue: !1,
                        duration: a.animate
                    }))),
                    t = i
                }) : (n = this.value(),
                o = this._valueMin(),
                s = this._valueMax(),
                i = s !== o ? (n - o) / (s - o) * 100 : 0,
                u["horizontal" === this.orientation ? "left" : "bottom"] = i + "%",
                this.handle.stop(1, 1)[c ? "animate" : "css"](u, a.animate),
                "min" === r && "horizontal" === this.orientation && this.range.stop(1, 1)[c ? "animate" : "css"]({
                    width: i + "%"
                }, a.animate),
                "max" === r && "horizontal" === this.orientation && this.range[c ? "animate" : "css"]({
                    width: 100 - i + "%"
                }, {
                    queue: !1,
                    duration: a.animate
                }),
                "min" === r && "vertical" === this.orientation && this.range.stop(1, 1)[c ? "animate" : "css"]({
                    height: i + "%"
                }, a.animate),
                "max" === r && "vertical" === this.orientation && this.range[c ? "animate" : "css"]({
                    height: 100 - i + "%"
                }, {
                    queue: !1,
                    duration: a.animate
                }))
            },
            _handleEvents: {
                keydown: function(t) {
                    var i, n, o, s = e(t.target).data("ui-slider-handle-index");
                    switch (t.keyCode) {
                    case e.ui.keyCode.HOME:
                    case e.ui.keyCode.END:
                    case e.ui.keyCode.PAGE_UP:
                    case e.ui.keyCode.PAGE_DOWN:
                    case e.ui.keyCode.UP:
                    case e.ui.keyCode.RIGHT:
                    case e.ui.keyCode.DOWN:
                    case e.ui.keyCode.LEFT:
                        if (t.preventDefault(),
                        !this._keySliding && (this._keySliding = !0,
                        e(t.target).addClass("ui-state-active"),
                        !1 === this._start(t, s)))
                            return
                    }
                    switch (o = this.options.step,
                    i = n = this.options.values && this.options.values.length ? this.values(s) : this.value(),
                    t.keyCode) {
                    case e.ui.keyCode.HOME:
                        n = this._valueMin();
                        break;
                    case e.ui.keyCode.END:
                        n = this._valueMax();
                        break;
                    case e.ui.keyCode.PAGE_UP:
                        n = this._trimAlignValue(i + (this._valueMax() - this._valueMin()) / 5);
                        break;
                    case e.ui.keyCode.PAGE_DOWN:
                        n = this._trimAlignValue(i - (this._valueMax() - this._valueMin()) / 5);
                        break;
                    case e.ui.keyCode.UP:
                    case e.ui.keyCode.RIGHT:
                        if (i === this._valueMax())
                            return;
                        n = this._trimAlignValue(i + o);
                        break;
                    case e.ui.keyCode.DOWN:
                    case e.ui.keyCode.LEFT:
                        if (i === this._valueMin())
                            return;
                        n = this._trimAlignValue(i - o)
                    }
                    this._slide(t, s, n)
                },
                click: function(e) {
                    e.preventDefault()
                },
                keyup: function(t) {
                    var i = e(t.target).data("ui-slider-handle-index");
                    this._keySliding && (this._keySliding = !1,
                    this._stop(t, i),
                    this._change(t, i),
                    e(t.target).removeClass("ui-state-active"))
                }
            }
        })
    }(jQuery);
    var sl_min = $("#slider-range").attr("sl_min")
      , sl_max = $("#slider-range").attr("sl_max")
      , sl_min_val = $("#slider-range").attr("sl_min_val")
      , sl_max_val = $("#slider-range").attr("sl_max_val");
    $("#slider-range").slider({
        min: sl_min,
        max: sl_max,
        values: [sl_min_val, sl_max_val],
        step: 5,
        range: !0,
        slide: function(e, t) {
            $("#fr").val(t.values[0]),
            $("#to").val(t.values[1])
        }
    }),
    $("#fr").val($("#slider-range").slider("values", 0)),
    $("#to").val($("#slider-range").slider("values", 1)),
    $("#fr").keyup(function() {
        $("#fr").val() >= r_min && $("#fr").val() <= $("#slider-range").slider("values", 1) && $("#slider-range").slider("values", 0, $("#fr").val())
    }),
    $("#to").keyup(function() {
        $("#to").val() >= $("#slider-range").slider("values", 0) && $("#slider-range").slider("values", 1, $("#to").val())
    });
    var r_min = $("#slider-range").slider("option", "min")
      , r_max = $("#slider-range").slider("option", "max")
      , r_cen = Math.round((r_max - r_min) / 2);
    $("#slid-txt1").html(r_min),
    $("#slid-txt2").html(r_cen),
    $("#slid-txt3").html(r_max),
    $("#slider-range1").slider({
        min: sl_min,
        max: sl_max,
        values: [sl_min_val, sl_max_val],
        step: 5,
        range: !0,
        slide: function(e, t) {
            $("#fr1").val(t.values[0]),
            $("#to1").val(t.values[1])
        }
    }),
    $("#fr1").val($("#slider-range1").slider("values", 0)),
    $("#to1").val($("#slider-range1").slider("values", 1)),
    $("#fr1").keyup(function() {
        $("#fr1").val() >= r_min && $("#fr1").val() <= $("#slider-range1").slider("values", 1) && $("#slider-range1").slider("values", 0, $("#fr1").val())
    }),
    $("#to1").keyup(function() {
        $("#to1").val() >= $("#slider-range1").slider("values", 0) && $("#slider-range1").slider("values", 1, $("#to1").val())
    });
    var r_min1 = $("#slider-range1").slider("option", "min")
      , r_max1 = $("#slider-range1").slider("option", "max")
      , r_cen1 = Math.round((r_max1 - r_min1) / 2);
    $("#slid-txt11").html(r_min1),
    $("#slid-txt12").html(r_cen1),
    $("#slid-txt13").html(r_max1)
}
$(".mCustomScrollbar").length > 0 && (function(e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e : e(jQuery)
}(function(e) {
    function t(t) {
        var r = t || window.event
          , a = l.call(arguments, 1)
          , c = 0
          , d = 0
          , p = 0
          , h = 0
          , f = 0
          , m = 0;
        if (t = e.event.fix(r),
        t.type = "mousewheel",
        "detail"in r && (p = -1 * r.detail),
        "wheelDelta"in r && (p = r.wheelDelta),
        "wheelDeltaY"in r && (p = r.wheelDeltaY),
        "wheelDeltaX"in r && (d = -1 * r.wheelDeltaX),
        "axis"in r && r.axis === r.HORIZONTAL_AXIS && (d = -1 * p,
        p = 0),
        c = 0 === p ? d : p,
        "deltaY"in r && (p = -1 * r.deltaY,
        c = p),
        "deltaX"in r && (d = r.deltaX,
        0 === p && (c = -1 * d)),
        0 !== p || 0 !== d) {
            if (1 === r.deltaMode) {
                var g = e.data(this, "mousewheel-line-height");
                c *= g,
                p *= g,
                d *= g
            } else if (2 === r.deltaMode) {
                var v = e.data(this, "mousewheel-page-height");
                c *= v,
                p *= v,
                d *= v
            }
            if (h = Math.max(Math.abs(p), Math.abs(d)),
            (!s || s > h) && (s = h,
            n(r, h) && (s /= 40)),
            n(r, h) && (c /= 40,
            d /= 40,
            p /= 40),
            c = Math[c >= 1 ? "floor" : "ceil"](c / s),
            d = Math[d >= 1 ? "floor" : "ceil"](d / s),
            p = Math[p >= 1 ? "floor" : "ceil"](p / s),
            u.settings.normalizeOffset && this.getBoundingClientRect) {
                var y = this.getBoundingClientRect();
                f = t.clientX - y.left,
                m = t.clientY - y.top
            }
            return t.deltaX = d,
            t.deltaY = p,
            t.deltaFactor = s,
            t.offsetX = f,
            t.offsetY = m,
            t.deltaMode = 0,
            a.unshift(t, c, d, p),
            o && clearTimeout(o),
            o = setTimeout(i, 200),
            (e.event.dispatch || e.event.handle).apply(this, a)
        }
    }
    function i() {
        s = null
    }
    function n(e, t) {
        return u.settings.adjustOldDeltas && "mousewheel" === e.type && t % 120 == 0
    }
    var o, s, r = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"], a = "onwheel"in document || document.documentMode >= 9 ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"], l = Array.prototype.slice;
    if (e.event.fixHooks)
        for (var c = r.length; c; )
            e.event.fixHooks[r[--c]] = e.event.mouseHooks;
    var u = e.event.special.mousewheel = {
        version: "3.1.12",
        setup: function() {
            if (this.addEventListener)
                for (var i = a.length; i; )
                    this.addEventListener(a[--i], t, !1);
            else
                this.onmousewheel = t;
            e.data(this, "mousewheel-line-height", u.getLineHeight(this)),
            e.data(this, "mousewheel-page-height", u.getPageHeight(this))
        },
        teardown: function() {
            if (this.removeEventListener)
                for (var i = a.length; i; )
                    this.removeEventListener(a[--i], t, !1);
            else
                this.onmousewheel = null;
            e.removeData(this, "mousewheel-line-height"),
            e.removeData(this, "mousewheel-page-height")
        },
        getLineHeight: function(t) {
            var i = e(t)
              , n = i["offsetParent"in e.fn ? "offsetParent" : "parent"]();
            return n.length || (n = e("body")),
            parseInt(n.css("fontSize"), 10) || parseInt(i.css("fontSize"), 10) || 16
        },
        getPageHeight: function(t) {
            return e(t).height()
        },
        settings: {
            adjustOldDeltas: !0,
            normalizeOffset: !0
        }
    };
    e.fn.extend({
        mousewheel: function(e) {
            return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
        },
        unmousewheel: function(e) {
            return this.unbind("mousewheel", e)
        }
    })
}),
function(e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e : e(jQuery)
}(function(e) {
    function t(t) {
        var r = t || window.event
          , a = l.call(arguments, 1)
          , c = 0
          , d = 0
          , p = 0
          , h = 0
          , f = 0
          , m = 0;
        if (t = e.event.fix(r),
        t.type = "mousewheel",
        "detail"in r && (p = -1 * r.detail),
        "wheelDelta"in r && (p = r.wheelDelta),
        "wheelDeltaY"in r && (p = r.wheelDeltaY),
        "wheelDeltaX"in r && (d = -1 * r.wheelDeltaX),
        "axis"in r && r.axis === r.HORIZONTAL_AXIS && (d = -1 * p,
        p = 0),
        c = 0 === p ? d : p,
        "deltaY"in r && (p = -1 * r.deltaY,
        c = p),
        "deltaX"in r && (d = r.deltaX,
        0 === p && (c = -1 * d)),
        0 !== p || 0 !== d) {
            if (1 === r.deltaMode) {
                var g = e.data(this, "mousewheel-line-height");
                c *= g,
                p *= g,
                d *= g
            } else if (2 === r.deltaMode) {
                var v = e.data(this, "mousewheel-page-height");
                c *= v,
                p *= v,
                d *= v
            }
            if (h = Math.max(Math.abs(p), Math.abs(d)),
            (!s || s > h) && (s = h,
            n(r, h) && (s /= 40)),
            n(r, h) && (c /= 40,
            d /= 40,
            p /= 40),
            c = Math[c >= 1 ? "floor" : "ceil"](c / s),
            d = Math[d >= 1 ? "floor" : "ceil"](d / s),
            p = Math[p >= 1 ? "floor" : "ceil"](p / s),
            u.settings.normalizeOffset && this.getBoundingClientRect) {
                var y = this.getBoundingClientRect();
                f = t.clientX - y.left,
                m = t.clientY - y.top
            }
            return t.deltaX = d,
            t.deltaY = p,
            t.deltaFactor = s,
            t.offsetX = f,
            t.offsetY = m,
            t.deltaMode = 0,
            a.unshift(t, c, d, p),
            o && clearTimeout(o),
            o = setTimeout(i, 200),
            (e.event.dispatch || e.event.handle).apply(this, a)
        }
    }
    function i() {
        s = null
    }
    function n(e, t) {
        return u.settings.adjustOldDeltas && "mousewheel" === e.type && t % 120 == 0
    }
    var o, s, r = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"], a = "onwheel"in document || document.documentMode >= 9 ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"], l = Array.prototype.slice;
    if (e.event.fixHooks)
        for (var c = r.length; c; )
            e.event.fixHooks[r[--c]] = e.event.mouseHooks;
    var u = e.event.special.mousewheel = {
        version: "3.1.12",
        setup: function() {
            if (this.addEventListener)
                for (var i = a.length; i; )
                    this.addEventListener(a[--i], t, !1);
            else
                this.onmousewheel = t;
            e.data(this, "mousewheel-line-height", u.getLineHeight(this)),
            e.data(this, "mousewheel-page-height", u.getPageHeight(this))
        },
        teardown: function() {
            if (this.removeEventListener)
                for (var i = a.length; i; )
                    this.removeEventListener(a[--i], t, !1);
            else
                this.onmousewheel = null;
            e.removeData(this, "mousewheel-line-height"),
            e.removeData(this, "mousewheel-page-height")
        },
        getLineHeight: function(t) {
            var i = e(t)
              , n = i["offsetParent"in e.fn ? "offsetParent" : "parent"]();
            return n.length || (n = e("body")),
            parseInt(n.css("fontSize"), 10) || parseInt(i.css("fontSize"), 10) || 16
        },
        getPageHeight: function(t) {
            return e(t).height()
        },
        settings: {
            adjustOldDeltas: !0,
            normalizeOffset: !0
        }
    };
    e.fn.extend({
        mousewheel: function(e) {
            return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
        },
        unmousewheel: function(e) {
            return this.unbind("mousewheel", e)
        }
    })
}),
function(e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : "undefined" != typeof module && module.exports ? module.exports = e : e(jQuery, window, document)
}(function(e) {
    !function(t) {
        var i = "function" == typeof define && define.amd
          , n = "undefined" != typeof module && module.exports
          , o = "https:" == document.location.protocol ? "https:" : "http:";
        i || (n ? require("jquery-mousewheel")(e) : e.event.special.mousewheel || e("head").append(decodeURI("%3Cscript src=" + o + "//cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js%3E%3C/script%3E"))),
        function() {
            var t, i = "mCustomScrollbar", n = "mCS", o = ".mCustomScrollbar", s = {
                setTop: 0,
                setLeft: 0,
                axis: "y",
                scrollbarPosition: "inside",
                scrollInertia: 950,
                autoDraggerLength: !0,
                alwaysShowScrollbar: 0,
                snapOffset: 0,
                mouseWheel: {
                    enable: !0,
                    scrollAmount: "auto",
                    axis: "y",
                    deltaFactor: "auto",
                    disableOver: ["select", "option", "keygen", "datalist", "textarea"]
                },
                scrollButtons: {
                    scrollType: "stepless",
                    scrollAmount: "auto"
                },
                keyboard: {
                    enable: !0,
                    scrollType: "stepless",
                    scrollAmount: "auto"
                },
                contentTouchScroll: 25,
                documentTouchScroll: !0,
                advanced: {
                    autoScrollOnFocus: "input,textarea,select,button,datalist,keygen,a[tabindex],area,object,[contenteditable='true']",
                    updateOnContentResize: !0,
                    updateOnImageLoad: "auto",
                    autoUpdateTimeout: 60
                },
                theme: "light",
                callbacks: {
                    onTotalScrollOffset: 0,
                    onTotalScrollBackOffset: 0,
                    alwaysTriggerOffsets: !0
                }
            }, r = 0, a = {}, l = window.attachEvent && !window.addEventListener ? 1 : 0, c = !1, u = ["mCSB_dragger_onDrag", "mCSB_scrollTools_onDrag", "mCS_img_loaded", "mCS_disabled", "mCS_destroyed", "mCS_no_scrollbar", "mCS-autoHide", "mCS-dir-rtl", "mCS_no_scrollbar_y", "mCS_no_scrollbar_x", "mCS_y_hidden", "mCS_x_hidden", "mCSB_draggerContainer", "mCSB_buttonUp", "mCSB_buttonDown", "mCSB_buttonLeft", "mCSB_buttonRight"], d = {
                init: function(t) {
                    var t = e.extend(!0, {}, s, t)
                      , i = p.call(this);
                    if (t.live) {
                        var l = t.liveSelector || this.selector || o
                          , c = e(l);
                        if ("off" === t.live)
                            return void f(l);
                        a[l] = setTimeout(function() {
                            c.mCustomScrollbar(t),
                            "once" === t.live && c.length && f(l)
                        }, 500)
                    } else
                        f(l);
                    return t.setWidth = t.set_width ? t.set_width : t.setWidth,
                    t.setHeight = t.set_height ? t.set_height : t.setHeight,
                    t.axis = t.horizontalScroll ? "x" : m(t.axis),
                    t.scrollInertia = t.scrollInertia > 0 && t.scrollInertia < 17 ? 17 : t.scrollInertia,
                    "object" != typeof t.mouseWheel && 1 == t.mouseWheel && (t.mouseWheel = {
                        enable: !0,
                        scrollAmount: "auto",
                        axis: "y",
                        preventDefault: !1,
                        deltaFactor: "auto",
                        normalizeDelta: !1,
                        invert: !1
                    }),
                    t.mouseWheel.scrollAmount = t.mouseWheelPixels ? t.mouseWheelPixels : t.mouseWheel.scrollAmount,
                    t.mouseWheel.normalizeDelta = t.advanced.normalizeMouseWheelDelta ? t.advanced.normalizeMouseWheelDelta : t.mouseWheel.normalizeDelta,
                    t.scrollButtons.scrollType = g(t.scrollButtons.scrollType),
                    h(t),
                    e(i).each(function() {
                        var i = e(this);
                        if (!i.data(n)) {
                            i.data(n, {
                                idx: ++r,
                                opt: t,
                                scrollRatio: {
                                    y: null,
                                    x: null
                                },
                                overflowed: null,
                                contentReset: {
                                    y: null,
                                    x: null
                                },
                                bindEvents: !1,
                                tweenRunning: !1,
                                sequential: {},
                                langDir: i.css("direction"),
                                cbOffsets: null,
                                trigger: null,
                                poll: {
                                    size: {
                                        o: 0,
                                        n: 0
                                    },
                                    img: {
                                        o: 0,
                                        n: 0
                                    },
                                    change: {
                                        o: 0,
                                        n: 0
                                    }
                                }
                            });
                            var o = i.data(n)
                              , s = o.opt
                              , a = i.data("mcs-axis")
                              , l = i.data("mcs-scrollbar-position")
                              , c = i.data("mcs-theme");
                            a && (s.axis = a),
                            l && (s.scrollbarPosition = l),
                            c && (s.theme = c,
                            h(s)),
                            v.call(this),
                            o && s.callbacks.onCreate && "function" == typeof s.callbacks.onCreate && s.callbacks.onCreate.call(this),
                            e("#mCSB_" + o.idx + "_container img:not(." + u[2] + ")").addClass(u[2]),
                            d.update.call(null, i)
                        }
                    })
                },
                update: function(t, i) {
                    var o = t || p.call(this);
                    return e(o).each(function() {
                        var t = e(this);
                        if (t.data(n)) {
                            var o = t.data(n)
                              , s = o.opt
                              , r = e("#mCSB_" + o.idx + "_container")
                              , a = e("#mCSB_" + o.idx)
                              , l = [e("#mCSB_" + o.idx + "_dragger_vertical"), e("#mCSB_" + o.idx + "_dragger_horizontal")];
                            if (!r.length)
                                return;
                            o.tweenRunning && V(t),
                            i && o && s.callbacks.onBeforeUpdate && "function" == typeof s.callbacks.onBeforeUpdate && s.callbacks.onBeforeUpdate.call(this),
                            t.hasClass(u[3]) && t.removeClass(u[3]),
                            t.hasClass(u[4]) && t.removeClass(u[4]),
                            a.css("max-height", "none"),
                            a.height() !== t.height() && a.css("max-height", t.height()),
                            w.call(this),
                            "y" === s.axis || s.advanced.autoExpandHorizontalScroll || r.css("width", y(r)),
                            o.overflowed = k.call(this),
                            E.call(this),
                            s.autoDraggerLength && x.call(this),
                            S.call(this),
                            _.call(this);
                            var c = [Math.abs(r[0].offsetTop), Math.abs(r[0].offsetLeft)];
                            "x" !== s.axis && (o.overflowed[0] ? l[0].height() > l[0].parent().height() ? T.call(this) : (Q(t, c[0].toString(), {
                                dir: "y",
                                dur: 0,
                                overwrite: "none"
                            }),
                            o.contentReset.y = null) : (T.call(this),
                            "y" === s.axis ? $.call(this) : "yx" === s.axis && o.overflowed[1] && Q(t, c[1].toString(), {
                                dir: "x",
                                dur: 0,
                                overwrite: "none"
                            }))),
                            "y" !== s.axis && (o.overflowed[1] ? l[1].width() > l[1].parent().width() ? T.call(this) : (Q(t, c[1].toString(), {
                                dir: "x",
                                dur: 0,
                                overwrite: "none"
                            }),
                            o.contentReset.x = null) : (T.call(this),
                            "x" === s.axis ? $.call(this) : "yx" === s.axis && o.overflowed[0] && Q(t, c[0].toString(), {
                                dir: "y",
                                dur: 0,
                                overwrite: "none"
                            }))),
                            i && o && (2 === i && s.callbacks.onImageLoad && "function" == typeof s.callbacks.onImageLoad ? s.callbacks.onImageLoad.call(this) : 3 === i && s.callbacks.onSelectorChange && "function" == typeof s.callbacks.onSelectorChange ? s.callbacks.onSelectorChange.call(this) : s.callbacks.onUpdate && "function" == typeof s.callbacks.onUpdate && s.callbacks.onUpdate.call(this)),
                            U.call(this)
                        }
                    })
                },
                scrollTo: function(t, i) {
                    if (void 0 !== t && null != t) {
                        var o = p.call(this);
                        return e(o).each(function() {
                            var o = e(this);
                            if (o.data(n)) {
                                var s = o.data(n)
                                  , r = s.opt
                                  , a = {
                                    trigger: "external",
                                    scrollInertia: r.scrollInertia,
                                    scrollEasing: "mcsEaseInOut",
                                    moveDragger: !1,
                                    timeout: 60,
                                    callbacks: !0,
                                    onStart: !0,
                                    onUpdate: !0,
                                    onComplete: !0
                                }
                                  , l = e.extend(!0, {}, a, i)
                                  , c = F.call(this, t)
                                  , u = l.scrollInertia > 0 && l.scrollInertia < 17 ? 17 : l.scrollInertia;
                                c[0] = X.call(this, c[0], "y"),
                                c[1] = X.call(this, c[1], "x"),
                                l.moveDragger && (c[0] *= s.scrollRatio.y,
                                c[1] *= s.scrollRatio.x),
                                l.dur = oe() ? 0 : u,
                                setTimeout(function() {
                                    null !== c[0] && void 0 !== c[0] && "x" !== r.axis && s.overflowed[0] && (l.dir = "y",
                                    l.overwrite = "all",
                                    Q(o, c[0].toString(), l)),
                                    null !== c[1] && void 0 !== c[1] && "y" !== r.axis && s.overflowed[1] && (l.dir = "x",
                                    l.overwrite = "none",
                                    Q(o, c[1].toString(), l))
                                }, l.timeout)
                            }
                        })
                    }
                },
                stop: function() {
                    var t = p.call(this);
                    return e(t).each(function() {
                        var t = e(this);
                        t.data(n) && V(t)
                    })
                },
                disable: function(t) {
                    var i = p.call(this);
                    return e(i).each(function() {
                        var i = e(this);
                        i.data(n) && (i.data(n),
                        U.call(this, "remove"),
                        $.call(this),
                        t && T.call(this),
                        E.call(this, !0),
                        i.addClass(u[3]))
                    })
                },
                destroy: function() {
                    var t = p.call(this);
                    return e(t).each(function() {
                        var o = e(this);
                        if (o.data(n)) {
                            var s = o.data(n)
                              , r = s.opt
                              , a = e("#mCSB_" + s.idx)
                              , l = e("#mCSB_" + s.idx + "_container")
                              , c = e(".mCSB_" + s.idx + "_scrollbar");
                            r.live && f(r.liveSelector || e(t).selector),
                            U.call(this, "remove"),
                            $.call(this),
                            T.call(this),
                            o.removeData(n),
                            Z(this, "mcs"),
                            c.remove(),
                            l.find("img." + u[2]).removeClass(u[2]),
                            a.replaceWith(l.contents()),
                            o.removeClass(i + " _" + n + "_" + s.idx + " " + u[6] + " " + u[7] + " " + u[5] + " " + u[3]).addClass(u[4])
                        }
                    })
                }
            }, p = function() {
                return "object" != typeof e(this) || e(this).length < 1 ? o : this
            }, h = function(t) {
                var i = ["rounded", "rounded-dark", "rounded-dots", "rounded-dots-dark"]
                  , n = ["rounded-dots", "rounded-dots-dark", "3d", "3d-dark", "3d-thick", "3d-thick-dark", "inset", "inset-dark", "inset-2", "inset-2-dark", "inset-3", "inset-3-dark"]
                  , o = ["minimal", "minimal-dark"]
                  , s = ["minimal", "minimal-dark"]
                  , r = ["minimal", "minimal-dark"];
                t.autoDraggerLength = !(e.inArray(t.theme, i) > -1) && t.autoDraggerLength,
                t.autoExpandScrollbar = !(e.inArray(t.theme, n) > -1) && t.autoExpandScrollbar,
                t.scrollButtons.enable = !(e.inArray(t.theme, o) > -1) && t.scrollButtons.enable,
                t.autoHideScrollbar = e.inArray(t.theme, s) > -1 || t.autoHideScrollbar,
                t.scrollbarPosition = e.inArray(t.theme, r) > -1 ? "outside" : t.scrollbarPosition
            }, f = function(e) {
                a[e] && (clearTimeout(a[e]),
                Z(a, e))
            }, m = function(e) {
                return "yx" === e || "xy" === e || "auto" === e ? "yx" : "x" === e || "horizontal" === e ? "x" : "y"
            }, g = function(e) {
                return "stepped" === e || "pixels" === e || "step" === e || "click" === e ? "stepped" : "stepless"
            }, v = function() {
                var t = e(this)
                  , o = t.data(n)
                  , s = o.opt
                  , r = s.autoExpandScrollbar ? " " + u[1] + "_expand" : ""
                  , a = ["<div id='mCSB_" + o.idx + "_scrollbar_vertical' class='mCSB_scrollTools mCSB_" + o.idx + "_scrollbar mCS-" + s.theme + " mCSB_scrollTools_vertical" + r + "'><div class='" + u[12] + "'><div id='mCSB_" + o.idx + "_dragger_vertical' class='mCSB_dragger' style='position:absolute;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>", "<div id='mCSB_" + o.idx + "_scrollbar_horizontal' class='mCSB_scrollTools mCSB_" + o.idx + "_scrollbar mCS-" + s.theme + " mCSB_scrollTools_horizontal" + r + "'><div class='" + u[12] + "'><div id='mCSB_" + o.idx + "_dragger_horizontal' class='mCSB_dragger' style='position:absolute;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>"]
                  , l = "yx" === s.axis ? "mCSB_vertical_horizontal" : "x" === s.axis ? "mCSB_horizontal" : "mCSB_vertical"
                  , c = "yx" === s.axis ? a[0] + a[1] : "x" === s.axis ? a[1] : a[0]
                  , d = "yx" === s.axis ? "<div id='mCSB_" + o.idx + "_container_wrapper' class='mCSB_container_wrapper' />" : ""
                  , p = s.autoHideScrollbar ? " " + u[6] : ""
                  , h = "x" !== s.axis && "rtl" === o.langDir ? " " + u[7] : "";
                s.setWidth && t.css("width", s.setWidth),
                s.setHeight && t.css("height", s.setHeight),
                s.setLeft = "y" !== s.axis && "rtl" === o.langDir ? "989999px" : s.setLeft,
                t.addClass(i + " _" + n + "_" + o.idx + p + h).wrapInner("<div id='mCSB_" + o.idx + "' class='mCustomScrollBox mCS-" + s.theme + " " + l + "'><div id='mCSB_" + o.idx + "_container' class='mCSB_container' style='position:relative; top:" + s.setTop + "; left:" + s.setLeft + ";' dir='" + o.langDir + "' /></div>");
                var f = e("#mCSB_" + o.idx)
                  , m = e("#mCSB_" + o.idx + "_container");
                "y" === s.axis || s.advanced.autoExpandHorizontalScroll || m.css("width", y(m)),
                "outside" === s.scrollbarPosition ? ("static" === t.css("position") && t.css("position", "relative"),
                t.css("overflow", "visible"),
                f.addClass("mCSB_outside").after(c)) : (f.addClass("mCSB_inside").append(c),
                m.wrap(d)),
                b.call(this);
                var g = [e("#mCSB_" + o.idx + "_dragger_vertical"), e("#mCSB_" + o.idx + "_dragger_horizontal")];
                g[0].css("min-height", g[0].height()),
                g[1].css("min-width", g[1].width())
            }, y = function(t) {
                var i = [t[0].scrollWidth, Math.max.apply(Math, t.children().map(function() {
                    return e(this).outerWidth(!0)
                }).get())]
                  , n = t.parent().width();
                return i[0] > n ? i[0] : i[1] > n ? i[1] : "100%"
            }, w = function() {
                var t = e(this)
                  , i = t.data(n)
                  , o = i.opt
                  , s = e("#mCSB_" + i.idx + "_container");
                if (o.advanced.autoExpandHorizontalScroll && "y" !== o.axis) {
                    s.css({
                        width: "auto",
                        "min-width": 0,
                        "overflow-x": "scroll"
                    });
                    var r = Math.ceil(s[0].scrollWidth);
                    3 === o.advanced.autoExpandHorizontalScroll || 2 !== o.advanced.autoExpandHorizontalScroll && r > s.parent().width() ? s.css({
                        width: r,
                        "min-width": "100%",
                        "overflow-x": "inherit"
                    }) : s.css({
                        "overflow-x": "inherit",
                        position: "absolute"
                    }).wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />").css({
                        width: Math.ceil(s[0].getBoundingClientRect().right + .4) - Math.floor(s[0].getBoundingClientRect().left),
                        "min-width": "100%",
                        position: "relative"
                    }).unwrap()
                }
            }, b = function() {
                var t = e(this)
                  , i = t.data(n)
                  , o = i.opt
                  , s = e(".mCSB_" + i.idx + "_scrollbar:first")
                  , r = ie(o.scrollButtons.tabindex) ? "tabindex='" + o.scrollButtons.tabindex + "'" : ""
                  , a = ["<a href='#' class='" + u[13] + "' " + r + " />", "<a href='#' class='" + u[14] + "' " + r + " />", "<a href='#' class='" + u[15] + "' " + r + " />", "<a href='#' class='" + u[16] + "' " + r + " />"]
                  , l = ["x" === o.axis ? a[2] : a[0], "x" === o.axis ? a[3] : a[1], a[2], a[3]];
                o.scrollButtons.enable && s.prepend(l[0]).append(l[1]).next(".mCSB_scrollTools").prepend(l[2]).append(l[3])
            }, x = function() {
                var t = e(this)
                  , i = t.data(n)
                  , o = e("#mCSB_" + i.idx)
                  , s = e("#mCSB_" + i.idx + "_container")
                  , r = [e("#mCSB_" + i.idx + "_dragger_vertical"), e("#mCSB_" + i.idx + "_dragger_horizontal")]
                  , a = [o.height() / s.outerHeight(!1), o.width() / s.outerWidth(!1)]
                  , c = [parseInt(r[0].css("min-height")), Math.round(a[0] * r[0].parent().height()), parseInt(r[1].css("min-width")), Math.round(a[1] * r[1].parent().width())]
                  , u = l && c[1] < c[0] ? c[0] : c[1]
                  , d = l && c[3] < c[2] ? c[2] : c[3];
                r[0].css({
                    height: u,
                    "max-height": r[0].parent().height() - 10
                }).find(".mCSB_dragger_bar").css({
                    "line-height": c[0] + "px"
                }),
                r[1].css({
                    width: d,
                    "max-width": r[1].parent().width() - 10
                })
            }, S = function() {
                var t = e(this)
                  , i = t.data(n)
                  , o = e("#mCSB_" + i.idx)
                  , s = e("#mCSB_" + i.idx + "_container")
                  , r = [e("#mCSB_" + i.idx + "_dragger_vertical"), e("#mCSB_" + i.idx + "_dragger_horizontal")]
                  , a = [s.outerHeight(!1) - o.height(), s.outerWidth(!1) - o.width()]
                  , l = [a[0] / (r[0].parent().height() - r[0].height()), a[1] / (r[1].parent().width() - r[1].width())];
                i.scrollRatio = {
                    y: l[0],
                    x: l[1]
                }
            }, C = function(e, t, i) {
                var n = i ? u[0] + "_expanded" : ""
                  , o = e.closest(".mCSB_scrollTools");
                "active" === t ? (e.toggleClass(u[0] + " " + n),
                o.toggleClass(u[1]),
                e[0]._draggable = e[0]._draggable ? 0 : 1) : e[0]._draggable || ("hide" === t ? (e.removeClass(u[0]),
                o.removeClass(u[1])) : (e.addClass(u[0]),
                o.addClass(u[1])))
            }, k = function() {
                var t = e(this)
                  , i = t.data(n)
                  , o = e("#mCSB_" + i.idx)
                  , s = e("#mCSB_" + i.idx + "_container")
                  , r = null == i.overflowed ? s.height() : s.outerHeight(!1)
                  , a = null == i.overflowed ? s.width() : s.outerWidth(!1)
                  , l = s[0].scrollHeight
                  , c = s[0].scrollWidth;
                return l > r && (r = l),
                c > a && (a = c),
                [r > o.height(), a > o.width()]
            }, T = function() {
                var t = e(this)
                  , i = t.data(n)
                  , o = i.opt
                  , s = e("#mCSB_" + i.idx)
                  , r = e("#mCSB_" + i.idx + "_container")
                  , a = [e("#mCSB_" + i.idx + "_dragger_vertical"), e("#mCSB_" + i.idx + "_dragger_horizontal")];
                if (V(t),
                ("x" !== o.axis && !i.overflowed[0] || "y" === o.axis && i.overflowed[0]) && (a[0].add(r).css("top", 0),
                Q(t, "_resetY")),
                "y" !== o.axis && !i.overflowed[1] || "x" === o.axis && i.overflowed[1]) {
                    var l = dx = 0;
                    "rtl" === i.langDir && (l = s.width() - r.outerWidth(!1),
                    dx = Math.abs(l / i.scrollRatio.x)),
                    r.css("left", l),
                    a[1].css("left", dx),
                    Q(t, "_resetX")
                }
            }, _ = function() {
                function t() {
                    r = setTimeout(function() {
                        e.event.special.mousewheel ? (clearTimeout(r),
                        O.call(i[0])) : t()
                    }, 100)
                }
                var i = e(this)
                  , o = i.data(n)
                  , s = o.opt;
                if (!o.bindEvents) {
                    if (A.call(this),
                    s.contentTouchScroll && M.call(this),
                    N.call(this),
                    s.mouseWheel.enable) {
                        var r;
                        t()
                    }
                    P.call(this),
                    z.call(this),
                    s.advanced.autoScrollOnFocus && I.call(this),
                    s.scrollButtons.enable && W.call(this),
                    s.keyboard.enable && R.call(this),
                    o.bindEvents = !0
                }
            }, $ = function() {
                var t = e(this)
                  , i = t.data(n)
                  , o = i.opt
                  , s = n + "_" + i.idx
                  , r = ".mCSB_" + i.idx + "_scrollbar"
                  , a = e("#mCSB_" + i.idx + ",#mCSB_" + i.idx + "_container,#mCSB_" + i.idx + "_container_wrapper," + r + " ." + u[12] + ",#mCSB_" + i.idx + "_dragger_vertical,#mCSB_" + i.idx + "_dragger_horizontal," + r + ">a")
                  , l = e("#mCSB_" + i.idx + "_container");
                o.advanced.releaseDraggableSelectors && a.add(e(o.advanced.releaseDraggableSelectors)),
                o.advanced.extraDraggableSelectors && a.add(e(o.advanced.extraDraggableSelectors)),
                i.bindEvents && (e(document).add(e(!H() || top.document)).unbind("." + s),
                a.each(function() {
                    e(this).unbind("." + s)
                }),
                clearTimeout(t[0]._focusTimeout),
                Z(t[0], "_focusTimeout"),
                clearTimeout(i.sequential.step),
                Z(i.sequential, "step"),
                clearTimeout(l[0].onCompleteTimeout),
                Z(l[0], "onCompleteTimeout"),
                i.bindEvents = !1)
            }, E = function(t) {
                var i = e(this)
                  , o = i.data(n)
                  , s = o.opt
                  , r = e("#mCSB_" + o.idx + "_container_wrapper")
                  , a = r.length ? r : e("#mCSB_" + o.idx + "_container")
                  , l = [e("#mCSB_" + o.idx + "_scrollbar_vertical"), e("#mCSB_" + o.idx + "_scrollbar_horizontal")]
                  , c = [l[0].find(".mCSB_dragger"), l[1].find(".mCSB_dragger")];
                "x" !== s.axis && (o.overflowed[0] && !t ? (l[0].add(c[0]).add(l[0].children("a")).css("display", "block"),
                a.removeClass(u[8] + " " + u[10])) : (s.alwaysShowScrollbar ? (2 !== s.alwaysShowScrollbar && c[0].css("display", "none"),
                a.removeClass(u[10])) : (l[0].css("display", "none"),
                a.addClass(u[10])),
                a.addClass(u[8]))),
                "y" !== s.axis && (o.overflowed[1] && !t ? (l[1].add(c[1]).add(l[1].children("a")).css("display", "block"),
                a.removeClass(u[9] + " " + u[11])) : (s.alwaysShowScrollbar ? (2 !== s.alwaysShowScrollbar && c[1].css("display", "none"),
                a.removeClass(u[11])) : (l[1].css("display", "none"),
                a.addClass(u[11])),
                a.addClass(u[9]))),
                o.overflowed[0] || o.overflowed[1] ? i.removeClass(u[5]) : i.addClass(u[5])
            }, D = function(t) {
                var i = t.type
                  , n = t.target.ownerDocument !== document && null !== frameElement ? [e(frameElement).offset().top, e(frameElement).offset().left] : null
                  , o = H() && t.target.ownerDocument !== top.document && null !== frameElement ? [e(t.view.frameElement).offset().top, e(t.view.frameElement).offset().left] : [0, 0];
                switch (i) {
                case "pointerdown":
                case "MSPointerDown":
                case "pointermove":
                case "MSPointerMove":
                case "pointerup":
                case "MSPointerUp":
                    return n ? [t.originalEvent.pageY - n[0] + o[0], t.originalEvent.pageX - n[1] + o[1], !1] : [t.originalEvent.pageY, t.originalEvent.pageX, !1];
                case "touchstart":
                case "touchmove":
                case "touchend":
                    var s = t.originalEvent.touches[0] || t.originalEvent.changedTouches[0]
                      , r = t.originalEvent.touches.length || t.originalEvent.changedTouches.length;
                    return t.target.ownerDocument !== document ? [s.screenY, s.screenX, r > 1] : [s.pageY, s.pageX, r > 1];
                default:
                    return n ? [t.pageY - n[0] + o[0], t.pageX - n[1] + o[1], !1] : [t.pageY, t.pageX, !1]
                }
            }, A = function() {
                function t(e, t, n, o) {
                    if (h[0].idleTimer = u.scrollInertia < 233 ? 250 : 0,
                    i.attr("id") === p[1])
                        var s = "x"
                          , l = (i[0].offsetLeft - t + o) * a.scrollRatio.x;
                    else
                        var s = "y"
                          , l = (i[0].offsetTop - e + n) * a.scrollRatio.y;
                    Q(r, l.toString(), {
                        dir: s,
                        drag: !0
                    })
                }
                var i, o, s, r = e(this), a = r.data(n), u = a.opt, d = n + "_" + a.idx, p = ["mCSB_" + a.idx + "_dragger_vertical", "mCSB_" + a.idx + "_dragger_horizontal"], h = e("#mCSB_" + a.idx + "_container"), f = e("#" + p[0] + ",#" + p[1]), m = u.advanced.releaseDraggableSelectors ? f.add(e(u.advanced.releaseDraggableSelectors)) : f, g = u.advanced.extraDraggableSelectors ? e(!H() || top.document).add(e(u.advanced.extraDraggableSelectors)) : e(!H() || top.document);
                f.bind("contextmenu." + d, function(e) {
                    e.preventDefault()
                }).bind("mousedown." + d + " touchstart." + d + " pointerdown." + d + " MSPointerDown." + d, function(t) {
                    if (t.stopImmediatePropagation(),
                    t.preventDefault(),
                    ee(t)) {
                        c = !0,
                        l && (document.onselectstart = function() {
                            return !1
                        }
                        ),
                        j.call(h, !1),
                        V(r),
                        i = e(this);
                        var n = i.offset()
                          , a = D(t)[0] - n.top
                          , d = D(t)[1] - n.left
                          , p = i.height() + n.top
                          , f = i.width() + n.left;
                        p > a && a > 0 && f > d && d > 0 && (o = a,
                        s = d),
                        C(i, "active", u.autoExpandScrollbar)
                    }
                }).bind("touchmove." + d, function(e) {
                    e.stopImmediatePropagation(),
                    e.preventDefault();
                    var n = i.offset()
                      , r = D(e)[0] - n.top
                      , a = D(e)[1] - n.left;
                    t(o, s, r, a)
                }),
                e(document).add(g).bind("mousemove." + d + " pointermove." + d + " MSPointerMove." + d, function(e) {
                    if (i) {
                        var n = i.offset()
                          , r = D(e)[0] - n.top
                          , a = D(e)[1] - n.left;
                        if (o === r && s === a)
                            return;
                        t(o, s, r, a)
                    }
                }).add(m).bind("mouseup." + d + " touchend." + d + " pointerup." + d + " MSPointerUp." + d, function() {
                    i && (C(i, "active", u.autoExpandScrollbar),
                    i = null),
                    c = !1,
                    l && (document.onselectstart = null),
                    j.call(h, !0)
                })
            }, M = function() {
                function i(e) {
                    if (!te(e) || c || D(e)[2])
                        return void (t = 0);
                    t = 1,
                    S = 0,
                    C = 0,
                    u = 1,
                    k.removeClass("mCS_touch_action");
                    var i = A.offset();
                    d = D(e)[0] - i.top,
                    p = D(e)[1] - i.left,
                    B = [D(e)[0], D(e)[1]]
                }
                function o(e) {
                    if (te(e) && !c && !D(e)[2] && (_.documentTouchScroll || e.preventDefault(),
                    e.stopImmediatePropagation(),
                    (!C || S) && u)) {
                        g = J();
                        var t = E.offset()
                          , i = D(e)[0] - t.top
                          , n = D(e)[1] - t.left
                          , o = "mcsLinearOut";
                        if (N.push(i),
                        O.push(n),
                        B[2] = Math.abs(D(e)[0] - B[0]),
                        B[3] = Math.abs(D(e)[1] - B[1]),
                        T.overflowed[0])
                            var s = M[0].parent().height() - M[0].height()
                              , r = d - i > 0 && i - d > -s * T.scrollRatio.y && (2 * B[3] < B[2] || "yx" === _.axis);
                        if (T.overflowed[1])
                            var a = M[1].parent().width() - M[1].width()
                              , h = p - n > 0 && n - p > -a * T.scrollRatio.x && (2 * B[2] < B[3] || "yx" === _.axis);
                        r || h ? (z || e.preventDefault(),
                        S = 1) : (C = 1,
                        k.addClass("mCS_touch_action")),
                        z && e.preventDefault(),
                        b = "yx" === _.axis ? [d - i, p - n] : "x" === _.axis ? [null, p - n] : [d - i, null],
                        A[0].idleTimer = 250,
                        T.overflowed[0] && l(b[0], L, o, "y", "all", !0),
                        T.overflowed[1] && l(b[1], L, o, "x", j, !0)
                    }
                }
                function s(e) {
                    if (!te(e) || c || D(e)[2])
                        return void (t = 0);
                    t = 1,
                    e.stopImmediatePropagation(),
                    V(k),
                    m = J();
                    var i = E.offset();
                    h = D(e)[0] - i.top,
                    f = D(e)[1] - i.left,
                    N = [],
                    O = []
                }
                function r(e) {
                    if (te(e) && !c && !D(e)[2]) {
                        u = 0,
                        e.stopImmediatePropagation(),
                        S = 0,
                        C = 0,
                        v = J();
                        var t = E.offset()
                          , i = D(e)[0] - t.top
                          , n = D(e)[1] - t.left;
                        if (!(v - g > 30)) {
                            w = 1e3 / (v - m);
                            var o = "mcsEaseOut"
                              , s = 2.5 > w
                              , r = s ? [N[N.length - 2], O[O.length - 2]] : [0, 0];
                            y = s ? [i - r[0], n - r[1]] : [i - h, n - f];
                            var d = [Math.abs(y[0]), Math.abs(y[1])];
                            w = s ? [Math.abs(y[0] / 4), Math.abs(y[1] / 4)] : [w, w];
                            var p = [Math.abs(A[0].offsetTop) - y[0] * a(d[0] / w[0], w[0]), Math.abs(A[0].offsetLeft) - y[1] * a(d[1] / w[1], w[1])];
                            b = "yx" === _.axis ? [p[0], p[1]] : "x" === _.axis ? [null, p[1]] : [p[0], null],
                            x = [4 * d[0] + _.scrollInertia, 4 * d[1] + _.scrollInertia];
                            var k = parseInt(_.contentTouchScroll) || 0;
                            b[0] = d[0] > k ? b[0] : 0,
                            b[1] = d[1] > k ? b[1] : 0,
                            T.overflowed[0] && l(b[0], x[0], o, "y", j, !1),
                            T.overflowed[1] && l(b[1], x[1], o, "x", j, !1)
                        }
                    }
                }
                function a(e, t) {
                    var i = [1.5 * t, 2 * t, t / 1.5, t / 2];
                    return e > 90 ? t > 4 ? i[0] : i[3] : e > 60 ? t > 3 ? i[3] : i[2] : e > 30 ? t > 8 ? i[1] : t > 6 ? i[0] : t > 4 ? t : i[2] : t > 8 ? t : i[3]
                }
                function l(e, t, i, n, o, s) {
                    e && Q(k, e.toString(), {
                        dur: t,
                        scrollEasing: i,
                        dir: n,
                        overwrite: o,
                        drag: s
                    })
                }
                var u, d, p, h, f, m, g, v, y, w, b, x, S, C, k = e(this), T = k.data(n), _ = T.opt, $ = n + "_" + T.idx, E = e("#mCSB_" + T.idx), A = e("#mCSB_" + T.idx + "_container"), M = [e("#mCSB_" + T.idx + "_dragger_vertical"), e("#mCSB_" + T.idx + "_dragger_horizontal")], N = [], O = [], L = 0, j = "yx" === _.axis ? "none" : "all", B = [], P = A.find("iframe"), I = ["touchstart." + $ + " pointerdown." + $ + " MSPointerDown." + $, "touchmove." + $ + " pointermove." + $ + " MSPointerMove." + $, "touchend." + $ + " pointerup." + $ + " MSPointerUp." + $], z = void 0 !== document.body.style.touchAction && "" !== document.body.style.touchAction;
                A.bind(I[0], function(e) {
                    i(e)
                }).bind(I[1], function(e) {
                    o(e)
                }),
                E.bind(I[0], function(e) {
                    s(e)
                }).bind(I[2], function(e) {
                    r(e)
                }),
                P.length && P.each(function() {
                    e(this).bind("load", function() {
                        H(this) && e(this.contentDocument || this.contentWindow.document).bind(I[0], function(e) {
                            i(e),
                            s(e)
                        }).bind(I[1], function(e) {
                            o(e)
                        }).bind(I[2], function(e) {
                            r(e)
                        })
                    })
                })
            }, N = function() {
                function i() {
                    return window.getSelection ? window.getSelection().toString() : document.selection && "Control" != document.selection.type ? document.selection.createRange().text : 0
                }
                function o(e, t, i) {
                    u.type = i && s ? "stepped" : "stepless",
                    u.scrollAmount = 10,
                    q(r, e, t, "mcsLinearOut", i ? 60 : null)
                }
                var s, r = e(this), a = r.data(n), l = a.opt, u = a.sequential, d = n + "_" + a.idx, p = e("#mCSB_" + a.idx + "_container"), h = p.parent();
                p.bind("mousedown." + d, function() {
                    t || s || (s = 1,
                    c = !0)
                }).add(document).bind("mousemove." + d, function(e) {
                    if (!t && s && i()) {
                        var n = p.offset()
                          , r = D(e)[0] - n.top + p[0].offsetTop
                          , c = D(e)[1] - n.left + p[0].offsetLeft;
                        r > 0 && r < h.height() && c > 0 && c < h.width() ? u.step && o("off", null, "stepped") : ("x" !== l.axis && a.overflowed[0] && (0 > r ? o("on", 38) : r > h.height() && o("on", 40)),
                        "y" !== l.axis && a.overflowed[1] && (0 > c ? o("on", 37) : c > h.width() && o("on", 39)))
                    }
                }).bind("mouseup." + d + " dragend." + d, function() {
                    t || (s && (s = 0,
                    o("off", null)),
                    c = !1)
                })
            }, O = function() {
                function t(t, n) {
                    if (V(i),
                    !B(i, t.target)) {
                        var r = "auto" !== s.mouseWheel.deltaFactor ? parseInt(s.mouseWheel.deltaFactor) : l && t.deltaFactor < 100 ? 100 : t.deltaFactor || 100
                          , u = s.scrollInertia;
                        if ("x" === s.axis || "x" === s.mouseWheel.axis)
                            var d = "x"
                              , p = [Math.round(r * o.scrollRatio.x), parseInt(s.mouseWheel.scrollAmount)]
                              , h = "auto" !== s.mouseWheel.scrollAmount ? p[1] : p[0] >= a.width() ? .9 * a.width() : p[0]
                              , f = Math.abs(e("#mCSB_" + o.idx + "_container")[0].offsetLeft)
                              , m = c[1][0].offsetLeft
                              , g = c[1].parent().width() - c[1].width()
                              , v = "y" === s.mouseWheel.axis ? t.deltaY || n : t.deltaX;
                        else
                            var d = "y"
                              , p = [Math.round(r * o.scrollRatio.y), parseInt(s.mouseWheel.scrollAmount)]
                              , h = "auto" !== s.mouseWheel.scrollAmount ? p[1] : p[0] >= a.height() ? .9 * a.height() : p[0]
                              , f = Math.abs(e("#mCSB_" + o.idx + "_container")[0].offsetTop)
                              , m = c[0][0].offsetTop
                              , g = c[0].parent().height() - c[0].height()
                              , v = t.deltaY || n;
                        "y" === d && !o.overflowed[0] || "x" === d && !o.overflowed[1] || ((s.mouseWheel.invert || t.webkitDirectionInvertedFromDevice) && (v = -v),
                        s.mouseWheel.normalizeDelta && (v = 0 > v ? -1 : 1),
                        (v > 0 && 0 !== m || 0 > v && m !== g || s.mouseWheel.preventDefault) && (t.stopImmediatePropagation(),
                        t.preventDefault()),
                        t.deltaFactor < 5 && !s.mouseWheel.normalizeDelta && (h = t.deltaFactor,
                        u = 17),
                        Q(i, (f - v * h).toString(), {
                            dir: d,
                            dur: u
                        }))
                    }
                }
                if (e(this).data(n)) {
                    var i = e(this)
                      , o = i.data(n)
                      , s = o.opt
                      , r = n + "_" + o.idx
                      , a = e("#mCSB_" + o.idx)
                      , c = [e("#mCSB_" + o.idx + "_dragger_vertical"), e("#mCSB_" + o.idx + "_dragger_horizontal")]
                      , u = e("#mCSB_" + o.idx + "_container").find("iframe");
                    u.length && u.each(function() {
                        e(this).bind("load", function() {
                            H(this) && e(this.contentDocument || this.contentWindow.document).bind("mousewheel." + r, function(e, i) {
                                t(e, i)
                            })
                        })
                    }),
                    a.bind("mousewheel." + r, function(e, i) {
                        t(e, i)
                    })
                }
            }, L = new Object, H = function(t) {
                var i = !1
                  , n = !1
                  , o = null;
                if (void 0 === t ? n = "#empty" : void 0 !== e(t).attr("id") && (n = e(t).attr("id")),
                !1 !== n && void 0 !== L[n])
                    return L[n];
                if (t) {
                    try {
                        var s = t.contentDocument || t.contentWindow.document;
                        o = s.body.innerHTML
                    } catch (e) {}
                    i = null !== o
                } else {
                    try {
                        var s = top.document;
                        o = s.body.innerHTML
                    } catch (e) {}
                    i = null !== o
                }
                return !1 !== n && (L[n] = i),
                i
            }, j = function(e) {
                var t = this.find("iframe");
                if (t.length) {
                    var i = e ? "auto" : "none";
                    t.css("pointer-events", i)
                }
            }, B = function(t, i) {
                var o = i.nodeName.toLowerCase()
                  , s = t.data(n).opt.mouseWheel.disableOver
                  , r = ["select", "textarea"];
                return e.inArray(o, s) > -1 && !(e.inArray(o, r) > -1 && !e(i).is(":focus"))
            }, P = function() {
                var t, i = e(this), o = i.data(n), s = n + "_" + o.idx, r = e("#mCSB_" + o.idx + "_container"), a = r.parent();
                e(".mCSB_" + o.idx + "_scrollbar ." + u[12]).bind("mousedown." + s + " touchstart." + s + " pointerdown." + s + " MSPointerDown." + s, function(i) {
                    c = !0,
                    e(i.target).hasClass("mCSB_dragger") || (t = 1)
                }).bind("touchend." + s + " pointerup." + s + " MSPointerUp." + s, function() {
                    c = !1
                }).bind("click." + s, function(n) {
                    if (t && (t = 0,
                    e(n.target).hasClass(u[12]) || e(n.target).hasClass("mCSB_draggerRail"))) {
                        V(i);
                        var s = e(this)
                          , l = s.find(".mCSB_dragger");
                        if (s.parent(".mCSB_scrollTools_horizontal").length > 0) {
                            if (!o.overflowed[1])
                                return;
                            var c = "x"
                              , d = n.pageX > l.offset().left ? -1 : 1
                              , p = Math.abs(r[0].offsetLeft) - d * (.9 * a.width())
                        } else {
                            if (!o.overflowed[0])
                                return;
                            var c = "y"
                              , d = n.pageY > l.offset().top ? -1 : 1
                              , p = Math.abs(r[0].offsetTop) - d * (.9 * a.height())
                        }
                        Q(i, p.toString(), {
                            dir: c,
                            scrollEasing: "mcsEaseInOut"
                        })
                    }
                })
            }, I = function() {
                var t = e(this)
                  , i = t.data(n)
                  , o = i.opt
                  , s = n + "_" + i.idx
                  , r = e("#mCSB_" + i.idx + "_container")
                  , a = r.parent();
                r.bind("focusin." + s, function() {
                    var i = e(document.activeElement)
                      , n = r.find(".mCustomScrollBox").length;
                    i.is(o.advanced.autoScrollOnFocus) && (V(t),
                    clearTimeout(t[0]._focusTimeout),
                    t[0]._focusTimer = n ? 17 * n : 0,
                    t[0]._focusTimeout = setTimeout(function() {
                        var e = [ne(i)[0], ne(i)[1]]
                          , n = [r[0].offsetTop, r[0].offsetLeft]
                          , s = [n[0] + e[0] >= 0 && n[0] + e[0] < a.height() - i.outerHeight(!1), n[1] + e[1] >= 0 && n[0] + e[1] < a.width() - i.outerWidth(!1)]
                          , l = "yx" !== o.axis || s[0] || s[1] ? "all" : "none";
                        "x" === o.axis || s[0] || Q(t, e[0].toString(), {
                            dir: "y",
                            scrollEasing: "mcsEaseInOut",
                            overwrite: l,
                            dur: 0
                        }),
                        "y" === o.axis || s[1] || Q(t, e[1].toString(), {
                            dir: "x",
                            scrollEasing: "mcsEaseInOut",
                            overwrite: l,
                            dur: 0
                        })
                    }, t[0]._focusTimer))
                })
            }, z = function() {
                var t = e(this)
                  , i = t.data(n)
                  , o = n + "_" + i.idx
                  , s = e("#mCSB_" + i.idx + "_container").parent();
                s.bind("scroll." + o, function() {
                    0 === s.scrollTop() && 0 === s.scrollLeft() || e(".mCSB_" + i.idx + "_scrollbar").css("visibility", "hidden")
                })
            }, W = function() {
                var t = e(this)
                  , i = t.data(n)
                  , o = i.opt
                  , s = i.sequential
                  , r = n + "_" + i.idx
                  , a = ".mCSB_" + i.idx + "_scrollbar";
                e(a + ">a").bind("contextmenu." + r, function(e) {
                    e.preventDefault()
                }).bind("mousedown." + r + " touchstart." + r + " pointerdown." + r + " MSPointerDown." + r + " mouseup." + r + " touchend." + r + " pointerup." + r + " MSPointerUp." + r + " mouseout." + r + " pointerout." + r + " MSPointerOut." + r + " click." + r, function(n) {
                    function r(e, i) {
                        s.scrollAmount = o.scrollButtons.scrollAmount,
                        q(t, e, i)
                    }
                    if (n.preventDefault(),
                    ee(n)) {
                        var a = e(this).attr("class");
                        switch (s.type = o.scrollButtons.scrollType,
                        n.type) {
                        case "mousedown":
                        case "touchstart":
                        case "pointerdown":
                        case "MSPointerDown":
                            if ("stepped" === s.type)
                                return;
                            c = !0,
                            i.tweenRunning = !1,
                            r("on", a);
                            break;
                        case "mouseup":
                        case "touchend":
                        case "pointerup":
                        case "MSPointerUp":
                        case "mouseout":
                        case "pointerout":
                        case "MSPointerOut":
                            if ("stepped" === s.type)
                                return;
                            c = !1,
                            s.dir && r("off", a);
                            break;
                        case "click":
                            if ("stepped" !== s.type || i.tweenRunning)
                                return;
                            r("on", a)
                        }
                    }
                })
            }, R = function() {
                function t(t) {
                    function n(e, t) {
                        r.type = s.keyboard.scrollType,
                        r.scrollAmount = s.keyboard.scrollAmount,
                        "stepped" === r.type && o.tweenRunning || q(i, e, t)
                    }
                    switch (t.type) {
                    case "blur":
                        o.tweenRunning && r.dir && n("off", null);
                        break;
                    case "keydown":
                    case "keyup":
                        var a = t.keyCode ? t.keyCode : t.which
                          , l = "on";
                        if ("x" !== s.axis && (38 === a || 40 === a) || "y" !== s.axis && (37 === a || 39 === a)) {
                            if ((38 === a || 40 === a) && !o.overflowed[0] || (37 === a || 39 === a) && !o.overflowed[1])
                                return;
                            "keyup" === t.type && (l = "off"),
                            e(document.activeElement).is(d) || (t.preventDefault(),
                            t.stopImmediatePropagation(),
                            n(l, a))
                        } else if (33 === a || 34 === a) {
                            if ((o.overflowed[0] || o.overflowed[1]) && (t.preventDefault(),
                            t.stopImmediatePropagation()),
                            "keyup" === t.type) {
                                V(i);
                                var p = 34 === a ? -1 : 1;
                                if ("x" === s.axis || "yx" === s.axis && o.overflowed[1] && !o.overflowed[0])
                                    var h = "x"
                                      , f = Math.abs(c[0].offsetLeft) - p * (.9 * u.width());
                                else
                                    var h = "y"
                                      , f = Math.abs(c[0].offsetTop) - p * (.9 * u.height());
                                Q(i, f.toString(), {
                                    dir: h,
                                    scrollEasing: "mcsEaseInOut"
                                })
                            }
                        } else if ((35 === a || 36 === a) && !e(document.activeElement).is(d) && ((o.overflowed[0] || o.overflowed[1]) && (t.preventDefault(),
                        t.stopImmediatePropagation()),
                        "keyup" === t.type)) {
                            if ("x" === s.axis || "yx" === s.axis && o.overflowed[1] && !o.overflowed[0])
                                var h = "x"
                                  , f = 35 === a ? Math.abs(u.width() - c.outerWidth(!1)) : 0;
                            else
                                var h = "y"
                                  , f = 35 === a ? Math.abs(u.height() - c.outerHeight(!1)) : 0;
                            Q(i, f.toString(), {
                                dir: h,
                                scrollEasing: "mcsEaseInOut"
                            })
                        }
                    }
                }
                var i = e(this)
                  , o = i.data(n)
                  , s = o.opt
                  , r = o.sequential
                  , a = n + "_" + o.idx
                  , l = e("#mCSB_" + o.idx)
                  , c = e("#mCSB_" + o.idx + "_container")
                  , u = c.parent()
                  , d = "input,textarea,select,datalist,keygen,[contenteditable='true']"
                  , p = c.find("iframe")
                  , h = ["blur." + a + " keydown." + a + " keyup." + a];
                p.length && p.each(function() {
                    e(this).bind("load", function() {
                        H(this) && e(this.contentDocument || this.contentWindow.document).bind(h[0], function(e) {
                            t(e)
                        })
                    })
                }),
                l.attr("tabindex", "0").bind(h[0], function(e) {
                    t(e)
                })
            }, q = function(t, i, o, s, r) {
                function a(e) {
                    c.snapAmount && (d.scrollAmount = c.snapAmount instanceof Array ? "x" === d.dir[0] ? c.snapAmount[1] : c.snapAmount[0] : c.snapAmount);
                    var i = "stepped" !== d.type
                      , n = r || (e ? i ? f / 1.5 : m : 1e3 / 60)
                      , o = e ? i ? 7.5 : 40 : 2.5
                      , u = [Math.abs(p[0].offsetTop), Math.abs(p[0].offsetLeft)]
                      , h = [l.scrollRatio.y > 10 ? 10 : l.scrollRatio.y, l.scrollRatio.x > 10 ? 10 : l.scrollRatio.x]
                      , g = "x" === d.dir[0] ? u[1] + d.dir[1] * (h[1] * o) : u[0] + d.dir[1] * (h[0] * o)
                      , v = "x" === d.dir[0] ? u[1] + d.dir[1] * parseInt(d.scrollAmount) : u[0] + d.dir[1] * parseInt(d.scrollAmount)
                      , y = "auto" !== d.scrollAmount ? v : g
                      , w = s || (e ? i ? "mcsLinearOut" : "mcsEaseInOut" : "mcsLinear")
                      , b = !!e;
                    return e && 17 > n && (y = "x" === d.dir[0] ? u[1] : u[0]),
                    Q(t, y.toString(), {
                        dir: d.dir[0],
                        scrollEasing: w,
                        dur: n,
                        onComplete: b
                    }),
                    e ? void (d.dir = !1) : (clearTimeout(d.step),
                    void (d.step = setTimeout(function() {
                        a()
                    }, n)))
                }
                var l = t.data(n)
                  , c = l.opt
                  , d = l.sequential
                  , p = e("#mCSB_" + l.idx + "_container")
                  , h = "stepped" === d.type
                  , f = c.scrollInertia < 26 ? 26 : c.scrollInertia
                  , m = c.scrollInertia < 1 ? 17 : c.scrollInertia;
                switch (i) {
                case "on":
                    if (d.dir = [o === u[16] || o === u[15] || 39 === o || 37 === o ? "x" : "y", o === u[13] || o === u[15] || 38 === o || 37 === o ? -1 : 1],
                    V(t),
                    ie(o) && "stepped" === d.type)
                        return;
                    a(h);
                    break;
                case "off":
                    (function() {
                        clearTimeout(d.step),
                        Z(d, "step"),
                        V(t)
                    })(),
                    (h || l.tweenRunning && d.dir) && a(!0)
                }
            }, F = function(t) {
                var i = e(this).data(n).opt
                  , o = [];
                return "function" == typeof t && (t = t()),
                t instanceof Array ? o = t.length > 1 ? [t[0], t[1]] : "x" === i.axis ? [null, t[0]] : [t[0], null] : (o[0] = t.y ? t.y : t.x || "x" === i.axis ? null : t,
                o[1] = t.x ? t.x : t.y || "y" === i.axis ? null : t),
                "function" == typeof o[0] && (o[0] = o[0]()),
                "function" == typeof o[1] && (o[1] = o[1]()),
                o
            }, X = function(t, i) {
                if (null != t && void 0 !== t) {
                    var o = e(this)
                      , s = o.data(n)
                      , r = s.opt
                      , a = e("#mCSB_" + s.idx + "_container")
                      , l = a.parent()
                      , c = typeof t;
                    i || (i = "x" === r.axis ? "x" : "y");
                    var u = "x" === i ? a.outerWidth(!1) - l.width() : a.outerHeight(!1) - l.height()
                      , p = "x" === i ? a[0].offsetLeft : a[0].offsetTop
                      , h = "x" === i ? "left" : "top";
                    switch (c) {
                    case "function":
                        return t();
                    case "object":
                        var f = t.jquery ? t : e(t);
                        if (!f.length)
                            return;
                        return "x" === i ? ne(f)[1] : ne(f)[0];
                    case "string":
                    case "number":
                        if (ie(t))
                            return Math.abs(t);
                        if (-1 !== t.indexOf("%"))
                            return Math.abs(u * parseInt(t) / 100);
                        if (-1 !== t.indexOf("-="))
                            return Math.abs(p - parseInt(t.split("-=")[1]));
                        if (-1 !== t.indexOf("+=")) {
                            var m = p + parseInt(t.split("+=")[1]);
                            return m >= 0 ? 0 : Math.abs(m)
                        }
                        if (-1 !== t.indexOf("px") && ie(t.split("px")[0]))
                            return Math.abs(t.split("px")[0]);
                        if ("top" === t || "left" === t)
                            return 0;
                        if ("bottom" === t)
                            return Math.abs(l.height() - a.outerHeight(!1));
                        if ("right" === t)
                            return Math.abs(l.width() - a.outerWidth(!1));
                        if ("first" === t || "last" === t) {
                            var f = a.find(":" + t);
                            return "x" === i ? ne(f)[1] : ne(f)[0]
                        }
                        return e(t).length ? "x" === i ? ne(e(t))[1] : ne(e(t))[0] : (a.css(h, t),
                        void d.update.call(null, o[0]))
                    }
                }
            }, U = function(t) {
                function i() {
                    return clearTimeout(p[0].autoUpdate),
                    0 === a.parents("html").length ? void (a = null) : void (p[0].autoUpdate = setTimeout(function() {
                        return c.advanced.updateOnSelectorChange && (l.poll.change.n = s(),
                        l.poll.change.n !== l.poll.change.o) ? (l.poll.change.o = l.poll.change.n,
                        void r(3)) : c.advanced.updateOnContentResize && (l.poll.size.n = a[0].scrollHeight + a[0].scrollWidth + p[0].offsetHeight + a[0].offsetHeight + a[0].offsetWidth,
                        l.poll.size.n !== l.poll.size.o) ? (l.poll.size.o = l.poll.size.n,
                        void r(1)) : !c.advanced.updateOnImageLoad || "auto" === c.advanced.updateOnImageLoad && "y" === c.axis || (l.poll.img.n = p.find("img").length,
                        l.poll.img.n === l.poll.img.o) ? void ((c.advanced.updateOnSelectorChange || c.advanced.updateOnContentResize || c.advanced.updateOnImageLoad) && i()) : (l.poll.img.o = l.poll.img.n,
                        void p.find("img").each(function() {
                            o(this)
                        }))
                    }, c.advanced.autoUpdateTimeout))
                }
                function o(t) {
                    function i() {
                        this.onload = null,
                        e(t).addClass(u[2]),
                        r(2)
                    }
                    if (e(t).hasClass(u[2]))
                        return void r();
                    var n = new Image;
                    n.onload = function(e, t) {
                        return function() {
                            return t.apply(e, arguments)
                        }
                    }(n, i),
                    n.src = t.src
                }
                function s() {
                    !0 === c.advanced.updateOnSelectorChange && (c.advanced.updateOnSelectorChange = "*");
                    var e = 0
                      , t = p.find(c.advanced.updateOnSelectorChange);
                    return c.advanced.updateOnSelectorChange && t.length > 0 && t.each(function() {
                        e += this.offsetHeight + this.offsetWidth
                    }),
                    e
                }
                function r(e) {
                    clearTimeout(p[0].autoUpdate),
                    d.update.call(null, a[0], e)
                }
                var a = e(this)
                  , l = a.data(n)
                  , c = l.opt
                  , p = e("#mCSB_" + l.idx + "_container");
                return t ? (clearTimeout(p[0].autoUpdate),
                void Z(p[0], "autoUpdate")) : void i()
            }, Y = function(e, t, i) {
                return Math.round(e / t) * t - i
            }, V = function(t) {
                var i = t.data(n);
                e("#mCSB_" + i.idx + "_container,#mCSB_" + i.idx + "_container_wrapper,#mCSB_" + i.idx + "_dragger_vertical,#mCSB_" + i.idx + "_dragger_horizontal").each(function() {
                    K.call(this)
                })
            }, Q = function(t, i, o) {
                function s(e) {
                    return l && c.callbacks[e] && "function" == typeof c.callbacks[e]
                }
                function r() {
                    return [c.callbacks.alwaysTriggerOffsets || b >= x[0] + k, c.callbacks.alwaysTriggerOffsets || -T >= b]
                }
                function a() {
                    var e = [h[0].offsetTop, h[0].offsetLeft]
                      , i = [y[0].offsetTop, y[0].offsetLeft]
                      , n = [h.outerHeight(!1), h.outerWidth(!1)]
                      , s = [p.height(), p.width()];
                    t[0].mcs = {
                        content: h,
                        top: e[0],
                        left: e[1],
                        draggerTop: i[0],
                        draggerLeft: i[1],
                        topPct: Math.round(100 * Math.abs(e[0]) / (Math.abs(n[0]) - s[0])),
                        leftPct: Math.round(100 * Math.abs(e[1]) / (Math.abs(n[1]) - s[1])),
                        direction: o.dir
                    }
                }
                var l = t.data(n)
                  , c = l.opt
                  , u = {
                    trigger: "internal",
                    dir: "y",
                    scrollEasing: "mcsEaseOut",
                    drag: !1,
                    dur: c.scrollInertia,
                    overwrite: "all",
                    callbacks: !0,
                    onStart: !0,
                    onUpdate: !0,
                    onComplete: !0
                }
                  , o = e.extend(u, o)
                  , d = [o.dur, o.drag ? 0 : o.dur]
                  , p = e("#mCSB_" + l.idx)
                  , h = e("#mCSB_" + l.idx + "_container")
                  , f = h.parent()
                  , m = c.callbacks.onTotalScrollOffset ? F.call(t, c.callbacks.onTotalScrollOffset) : [0, 0]
                  , g = c.callbacks.onTotalScrollBackOffset ? F.call(t, c.callbacks.onTotalScrollBackOffset) : [0, 0];
                if (l.trigger = o.trigger,
                0 === f.scrollTop() && 0 === f.scrollLeft() || (e(".mCSB_" + l.idx + "_scrollbar").css("visibility", "visible"),
                f.scrollTop(0).scrollLeft(0)),
                "_resetY" !== i || l.contentReset.y || (s("onOverflowYNone") && c.callbacks.onOverflowYNone.call(t[0]),
                l.contentReset.y = 1),
                "_resetX" !== i || l.contentReset.x || (s("onOverflowXNone") && c.callbacks.onOverflowXNone.call(t[0]),
                l.contentReset.x = 1),
                "_resetY" !== i && "_resetX" !== i) {
                    if (!l.contentReset.y && t[0].mcs || !l.overflowed[0] || (s("onOverflowY") && c.callbacks.onOverflowY.call(t[0]),
                    l.contentReset.x = null),
                    !l.contentReset.x && t[0].mcs || !l.overflowed[1] || (s("onOverflowX") && c.callbacks.onOverflowX.call(t[0]),
                    l.contentReset.x = null),
                    c.snapAmount) {
                        var v = c.snapAmount instanceof Array ? "x" === o.dir ? c.snapAmount[1] : c.snapAmount[0] : c.snapAmount;
                        i = Y(i, v, c.snapOffset)
                    }
                    switch (o.dir) {
                    case "x":
                        var y = e("#mCSB_" + l.idx + "_dragger_horizontal")
                          , w = "left"
                          , b = h[0].offsetLeft
                          , x = [p.width() - h.outerWidth(!1), y.parent().width() - y.width()]
                          , S = [i, 0 === i ? 0 : i / l.scrollRatio.x]
                          , k = m[1]
                          , T = g[1]
                          , _ = k > 0 ? k / l.scrollRatio.x : 0
                          , $ = T > 0 ? T / l.scrollRatio.x : 0;
                        break;
                    case "y":
                        var y = e("#mCSB_" + l.idx + "_dragger_vertical")
                          , w = "top"
                          , b = h[0].offsetTop
                          , x = [p.height() - h.outerHeight(!1), y.parent().height() - y.height()]
                          , S = [i, 0 === i ? 0 : i / l.scrollRatio.y]
                          , k = m[0]
                          , T = g[0]
                          , _ = k > 0 ? k / l.scrollRatio.y : 0
                          , $ = T > 0 ? T / l.scrollRatio.y : 0
                    }
                    S[1] < 0 || 0 === S[0] && 0 === S[1] ? S = [0, 0] : S[1] >= x[1] ? S = [x[0], x[1]] : S[0] = -S[0],
                    t[0].mcs || (a(),
                    s("onInit") && c.callbacks.onInit.call(t[0])),
                    clearTimeout(h[0].onCompleteTimeout),
                    G(y[0], w, Math.round(S[1]), d[1], o.scrollEasing),
                    !l.tweenRunning && (0 === b && S[0] >= 0 || b === x[0] && S[0] <= x[0]) || G(h[0], w, Math.round(S[0]), d[0], o.scrollEasing, o.overwrite, {
                        onStart: function() {
                            o.callbacks && o.onStart && !l.tweenRunning && (s("onScrollStart") && (a(),
                            c.callbacks.onScrollStart.call(t[0])),
                            l.tweenRunning = !0,
                            C(y),
                            l.cbOffsets = r())
                        },
                        onUpdate: function() {
                            o.callbacks && o.onUpdate && s("whileScrolling") && (a(),
                            c.callbacks.whileScrolling.call(t[0]))
                        },
                        onComplete: function() {
                            if (o.callbacks && o.onComplete) {
                                "yx" === c.axis && clearTimeout(h[0].onCompleteTimeout);
                                var e = h[0].idleTimer || 0;
                                h[0].onCompleteTimeout = setTimeout(function() {
                                    s("onScroll") && (a(),
                                    c.callbacks.onScroll.call(t[0])),
                                    s("onTotalScroll") && S[1] >= x[1] - _ && l.cbOffsets[0] && (a(),
                                    c.callbacks.onTotalScroll.call(t[0])),
                                    s("onTotalScrollBack") && S[1] <= $ && l.cbOffsets[1] && (a(),
                                    c.callbacks.onTotalScrollBack.call(t[0])),
                                    l.tweenRunning = !1,
                                    h[0].idleTimer = 0,
                                    C(y, "hide")
                                }, e)
                            }
                        }
                    })
                }
            }, G = function(e, t, i, n, o, s, r) {
                function a() {
                    w.stop || (g || p.call(),
                    g = J() - m,
                    l(),
                    g >= w.time && (w.time = g > w.time ? g + u - (g - w.time) : g + u - 1,
                    w.time < g + 1 && (w.time = g + 1)),
                    w.time < n ? w.id = d(a) : f.call())
                }
                function l() {
                    n > 0 ? (w.currVal = c(w.time, v, b, n, o),
                    y[t] = Math.round(w.currVal) + "px") : y[t] = i + "px",
                    h.call()
                }
                function c(e, t, i, n, o) {
                    switch (o) {
                    case "linear":
                    case "mcsLinear":
                        return i * e / n + t;
                    case "mcsLinearOut":
                        return e /= n,
                        e--,
                        i * Math.sqrt(1 - e * e) + t;
                    case "easeInOutSmooth":
                        return e /= n / 2,
                        1 > e ? i / 2 * e * e + t : (e--,
                        -i / 2 * (e * (e - 2) - 1) + t);
                    case "easeInOutStrong":
                        return e /= n / 2,
                        1 > e ? i / 2 * Math.pow(2, 10 * (e - 1)) + t : (e--,
                        i / 2 * (2 - Math.pow(2, -10 * e)) + t);
                    case "easeInOut":
                    case "mcsEaseInOut":
                        return e /= n / 2,
                        1 > e ? i / 2 * e * e * e + t : (e -= 2,
                        i / 2 * (e * e * e + 2) + t);
                    case "easeOutSmooth":
                        return e /= n,
                        e--,
                        -i * (e * e * e * e - 1) + t;
                    case "easeOutStrong":
                        return i * (1 - Math.pow(2, -10 * e / n)) + t;
                    case "easeOut":
                    case "mcsEaseOut":
                    default:
                        var s = (e /= n) * e
                          , r = s * e;
                        return t + i * (.499999999999997 * r * s + -2.5 * s * s + 5.5 * r + -6.5 * s + 4 * e)
                    }
                }
                e._mTween || (e._mTween = {
                    top: {},
                    left: {}
                });
                var u, d, r = r || {}, p = r.onStart || function() {}
                , h = r.onUpdate || function() {}
                , f = r.onComplete || function() {}
                , m = J(), g = 0, v = e.offsetTop, y = e.style, w = e._mTween[t];
                "left" === t && (v = e.offsetLeft);
                var b = i - v;
                w.stop = 0,
                "none" !== s && function() {
                    null != w.id && (window.requestAnimationFrame ? window.cancelAnimationFrame(w.id) : clearTimeout(w.id),
                    w.id = null)
                }(),
                function() {
                    u = 1e3 / 60,
                    w.time = g + u,
                    d = window.requestAnimationFrame ? window.requestAnimationFrame : function(e) {
                        return l(),
                        setTimeout(e, .01)
                    }
                    ,
                    w.id = d(a)
                }()
            }, J = function() {
                return window.performance && window.performance.now ? window.performance.now() : window.performance && window.performance.webkitNow ? window.performance.webkitNow() : Date.now ? Date.now() : (new Date).getTime()
            }, K = function() {
                var e = this;
                e._mTween || (e._mTween = {
                    top: {},
                    left: {}
                });
                for (var t = ["top", "left"], i = 0; i < t.length; i++) {
                    var n = t[i];
                    e._mTween[n].id && (window.requestAnimationFrame ? window.cancelAnimationFrame(e._mTween[n].id) : clearTimeout(e._mTween[n].id),
                    e._mTween[n].id = null,
                    e._mTween[n].stop = 1)
                }
            }, Z = function(e, t) {
                try {
                    delete e[t]
                } catch (i) {
                    e[t] = null
                }
            }, ee = function(e) {
                return !(e.which && 1 !== e.which)
            }, te = function(e) {
                var t = e.originalEvent.pointerType;
                return !(t && "touch" !== t && 2 !== t)
            }, ie = function(e) {
                return !isNaN(parseFloat(e)) && isFinite(e)
            }, ne = function(e) {
                var t = e.parents(".mCSB_container");
                return [e.offset().top - t.offset().top, e.offset().left - t.offset().left]
            }, oe = function() {
                var e = function() {
                    var e = ["webkit", "moz", "ms", "o"];
                    if ("hidden"in document)
                        return "hidden";
                    for (var t = 0; t < e.length; t++)
                        if (e[t] + "Hidden"in document)
                            return e[t] + "Hidden";
                    return null
                }();
                return !!e && document[e]
            };
            e.fn[i] = function(t) {
                return d[t] ? d[t].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof t && t ? void e.error("Method " + t + " does not exist") : d.init.apply(this, arguments)
            }
            ,
            e[i] = function(t) {
                return d[t] ? d[t].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof t && t ? void e.error("Method " + t + " does not exist") : d.init.apply(this, arguments)
            }
            ,
            e[i].defaults = s,
            window[i] = !0,
            e(window).bind("load", function() {
                e(o)[i](),
                e.extend(e.expr[":"], {
                    mcsInView: e.expr[":"].mcsInView || function(t) {
                        var i, n, o = e(t), s = o.parents(".mCSB_container");
                        if (s.length)
                            return i = s.parent(),
                            n = [s[0].offsetTop, s[0].offsetLeft],
                            n[0] + ne(o)[0] >= 0 && n[0] + ne(o)[0] < i.height() - o.outerHeight(!1) && n[1] + ne(o)[1] >= 0 && n[1] + ne(o)[1] < i.width() - o.outerWidth(!1)
                    }
                    ,
                    mcsInSight: e.expr[":"].mcsInSight || function(t, i, n) {
                        var o, s, r, a, l = e(t), c = l.parents(".mCSB_container"), u = "exact" === n[3] ? [[1, 0], [1, 0]] : [[.9, .1], [.6, .4]];
                        if (c.length)
                            return o = [l.outerHeight(!1), l.outerWidth(!1)],
                            r = [c[0].offsetTop + ne(l)[0], c[0].offsetLeft + ne(l)[1]],
                            s = [c.parent()[0].offsetHeight, c.parent()[0].offsetWidth],
                            a = [o[0] < s[0] ? u[0] : u[1], o[1] < s[1] ? u[0] : u[1]],
                            r[0] - s[0] * a[0][0] < 0 && r[0] + o[0] - s[0] * a[0][1] >= 0 && r[1] - s[1] * a[1][0] < 0 && r[1] + o[1] - s[1] * a[1][1] >= 0
                    }
                    ,
                    mcsOverflow: e.expr[":"].mcsOverflow || function(t) {
                        var i = e(t).data(n);
                        if (i)
                            return i.overflowed[0] || i.overflowed[1]
                    }
                })
            })
        }()
    }()
})),
function(e) {
    function t() {
        var t = e("#submenu").width()
          , i = t * e("#sub-wrap>div>div").length;
        e("#sub-wrap").width(i);
        var n = 0;
        e("#submenu-nav .cansub").each(function() {
            e(this).attr({
                eq: n
            }),
            n++
        });
        var o = function() {
            onhover || (e("#submenu").fadeOut(300),
            e(".open").removeClass("open"))
        }
          , s = function() {
            setTimeout(o, 1e3)
        };
        e("#submenu-nav .cansub").mouseenter(function() {
            clearTimeout(o),
            onhover = !0
        }),
        e("#submenu").mouseenter(function() {
            clearTimeout(o),
            onhover = !0
        }),
        e("#submenu-nav .cansub").mouseleave(function() {
            onhover = !1,
            s()
        }),
        e("#submenu").mouseleave(function() {
            onhover = !1,
            s()
        }),
        e("#submenu-nav .cansub").hover(function() {
            e("#submenu").fadeIn(300),
            n = 0 - e(this).attr("eq") * t,
            e("#submenu").hasClass("open") ? e("#sub-wrap").animate({
                margin: "0 0 0 " + n + "px"
            }, 200) : e("#sub-wrap").css({
                margin: "0 0 0 " + n + "px"
            }),
            e("#submenu-nav .cansub").removeClass("open"),
            e(this).addClass("open"),
            e("#submenu").addClass("open")
        })
    }
    e(document).ready(t),
    e(window).resize(t),
    e("#menu-cities .letter").each(function() {
        var t = e(this).find("a:first").html().split("")[0];
        e(this).prepend('<span class="lettr ylw1">' + t + "</span>")
    }),
    setTimeout("$('.nojq').fadeOut(500)", 500)
}(jQuery);
if ($('div').is('.coll-in-wrap')) {
    $('.coll-in-sml').height($('.coll-in-sml').width())
    $('.coll-in-big').height($('.coll-in-big').width())
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function favUpdateHandlers() {
    console.log("yc_del click");
    $.ajax({
        url: '/ajax/your_choice.php?act=del&id=' + $(this).data('id'),
        //url: $(this).attr('href'),
        //context: document.body,
        success: function(data) {
            //console.log($('#favorites'));
            //console.log(data);
            //console.log($(data).find('#favorites-in').html());
            //$('#favorites').html($(data).find('#favorites-in').html());
            $('#favorites').html(data);
        }
    });

    return false;
}

function parseGetParams() {
    var $_GET = {};
    var __GET = window.location.search.substring(1).split("&");
    for (var i = 0; i < __GET.length; i++) {
        var getVar = __GET[i].split("=");
        $_GET[getVar[0]] = typeof (getVar[1]) == "undefined" ? "" : getVar[1];
    }
    return $_GET;
}

function SetPageSort(obj, pt) {
    console.log(obj);
    console.log($(obj).data('key'));
    console.log(document.location);
    var s = '';
    var n = $(obj).data('key');
    var ptxt = '';
    if (pt == 1)
        ptxt = 'cat_sort';
    else if (pt == 2)
        ptxt = 'coll_sort';
    if (document.location.search.length > 0) {
        var prms = parseGetParams();
        console.log(prms);
        console.log(prms[ptxt]);
        if (prms[ptxt] && prms[ptxt].length > 0) {
            console.log('Perebor');
            for (var k in prms) {
                console.log(k, prms[k]);
                if (k != ptxt) {
                    s += (s.length > 0 ? '&' : '?') + k + '=' + prms[k];
                } else {
                    if (prms[k] == n)
                        return false;
                }
            }
            s += (s.length > 0 ? '&' : '?');
        } else {
            console.log('ok');
            s = document.location.search + '&';
        }
    } else {
        console.log('no params');
        s = '?';
    }
    console.log('---------------');
    console.log(s);
    var newHref = document.location.origin + document.location.pathname + s + ptxt + '=' + n;
    console.log(newHref);
    document.location.href = newHref;
}

function SetPageSize(obj) {
    console.log(obj);
    console.log(document.location);
    var s = '';
    var n = $(obj).text();
    if (document.location.search.length > 0) {
        var prms = parseGetParams();
        console.log(prms);
        console.log(prms["page_size"]);
        if (prms["page_size"] && prms["page_size"].length > 0) {
            console.log('Perebor');
            for (var k in prms) {
                console.log(k, prms[k]);
                if (k != "page_size") {
                    s += (s.length > 0 ? '&' : '?') + k + '=' + prms[k];
                } else {
                    if (prms[k] == n)
                        return false;
                }
            }
            s += (s.length > 0 ? '&' : '?');
        } else {
            console.log('ok');
            s = document.location.search + '&';
        }
    } else {
        console.log('no params');
        s = '?';
    }
    console.log('---------------');
    console.log(s);
    var newHref = document.location.origin + document.location.pathname + s + 'page_size=' + n;
    console.log(newHref);
    //document.location.href = newHref;
}

function SetMetall(obj) {
    console.log(obj);
    console.log(document.location);
    var s = '';
    var n = $(obj).data('key');
    if (document.location.search.length > 0) {
        var prms = parseGetParams();
        console.log(prms);
        console.log(prms["metall"]);
        if (prms["metall"] && prms["metall"].length > 0) {
            console.log('Perebor');
            for (var k in prms) {
                console.log(k, prms[k]);
                if (k != "metall") {
                    s += (s.length > 0 ? '&' : '?') + k + '=' + prms[k];
                } else {
                    if (prms[k] == n)
                        return false;
                }
            }
            s += (s.length > 0 ? '&' : '?');
        } else {
            console.log('ok');
            s = document.location.search + '&';
        }
    } else {
        console.log('no params');
        s = '?';
    }
    console.log('---------------');
    console.log(s);
    var newHref = document.location.origin + document.location.pathname + s + 'metall=' + n;
    console.log(newHref);
    document.location.href = newHref;
}

function SetRowSize(obj) {
    console.log(obj);
    //var ps = getCookie("page_size");
    var s = '';
    var n = $(obj).data('key');
    console.log(n);
    if (n != "m" && n != "b")
        n = "m";
    console.log(n);
    $('#catcat,#content').removeClass('row-m row-b').addClass('row-' + n);
    setCookie("page_size", n, 365);
    //console.log(ps);
    /*if (username != "" && username != null) {
        }
	console.log('---------------');
	console.log(s);
	var newHref = document.location.origin + document.location.pathname + s + 'page_size=' + n;
	console.log(newHref);*/
    //document.location.href = newHref;
}

function BasketItemSetSize(obj, id, sel){
    console.log(obj, id, sel);
    $.ajax({
        url: '/ajax/basket.php?cmd=size&id=' + id + '&size=' + $(obj).text(),
        //url: $(this).attr('href'),
        //context: document.body,
        success: function(data) {
            //console.log($('#favorites'));
            console.log(data);
            //console.log($(data).find('#favorites-in').html());
            //$('#favorites').html($(data).find('#favorites-in').html());
            //$('#favorites').html(data);
            //document.location.reload();
            recalcBasketAjax({});
        }
    });
}

$(document).ready(function() {
    $('.your_choice').on('click', function() {
        console.log("yc_add click");
        var $t = $(this);
        if ($t.hasClass('inliked'))
            return false;
        $.ajax({
            url: '/ajax/your_choice.php?act=add&id=' + $(this).data('id'),
            success: function(data) {
                console.log($t);
                //console.log(data);
                console.log(data.trim());
                $('#favorites').html(data);
                //console.log($('#favorites'));
                //console.log(data);
                //console.log($(data).find('#favorites-in').html());
                //$('#favorites').html($(data).find('#favorites-in').html());
                if (data.trim() == 'ok') {/*$t.addClass('inliked');
					$.ajax({
						url: '/ajax/your_choice.php?act=show&id=true',
						success: function(data){
							$('#favorites').html(data);
						}
					});*/
                }
            }
        });

        return false;
    });
    $('#favorites A.yc_del').on('click', favUpdateHandlers);
});

/*$(window).resize(function() {
	var $vss = $('#content .wa .vis-s:first');
	console.log($vss);
	console.log($vss.is(':visible'));
	console.log(mailOwl);
	if($vss.is(':visible') == true){ // Mobile
		mobileMode = true;
		//mailOwl.data('slick', '{"dots": true}').attr("data-slick", '{"dots": true}');
	}else{ // PC
		mobileMode = false;
		//mailOwl.data('slick', '{"dots": false}').attr("data-slick", '{"dots": false}');
		
	}
});*/
