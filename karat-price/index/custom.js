var touchMoving = false;
var DoNotToggle = false;
var ShowMoreLoading = false;
var ScrollTopArrBusy = false;
var YaHitLink = document.location.href;
var FilterHOver = false;
var MobScrollToFilter = false;
var Debug1 = false;
//var filterTopPosDiff = 30;
var filterTopPosDiff = 20;

$(function() {

	$("div.data-href").click(function () {
		window.location.href = $(this).attr('data-url');
	});

	$('#sort_name').change(function () {
		
		$.get('/ajax/filter.php', 'iID=sort' + '&value=' + $(this).val(), function (data) {
			window.location.reload();
			//alert(data);
		});
		return false;
		
	});	
	
	$('#count_elements').change(function () {
		
		$.get('/ajax/filter.php', 'iID=count_elements' + '&value=' + $(this).val(), function (data) {
			window.location.reload();
			//alert(data);
		});
		return false;
		
	});	
	
	$('.choose_city').change(function () {
		
		city = $(this).val();
		$('.city_shop').hide();
		$('.city_shop_block').show();
		$('#c'+city).show();
		$('.city_shop').find('select').attr('NAME', '');
		$('#c'+city).find('select').attr('NAME', 'shop');
		return false;
		
	});	
	
	$('.choose_city2').change(function () {
		
		city = $(this).val();
		$('.city_shop2').hide();
		$('.city_shop_block2').show();
		$('#d'+city).show();
		$('.city_shop2').find('select').attr('NAME', '');
		$('#d'+city).find('select').attr('NAME', 'shop');
		return false;
		
	});
	
	$('.filter_section input').click(function () {
		window.location.href = $(this).attr('data-url');
	});
	
	$('input[name="size"]').click(function () {
		$("#product_size").val($(this).val());
		$("#product_size2").val($(this).val());
	});

	
	$(".ajax_form_nal").validate({

       rules:{

            name:{
                required: true
            },

            phone:{
                required: true
            },
			city:{
                required: true
            },
       },

       messages:{

            name:{
                required: "Это поле обязательно для заполнения"
            },

            phone:{
                required: "Это поле обязательно для заполнения"
            },
			
			city:{
                required: "Выберите город"
            },
       },
	   
	   submitHandler: function (form){
            var str = $(".ajax_form_nal").serialize();

		
			$.post("/ajax/form_nal.php",
				   {'form_data': str},
				   function(result){ 

						$(".ajax_form_nal").hide();
						$(".ajax_form_nal").next().show();
				  }
				);
		
			
         },


    });
		

	$(".ajax_form_dev").validate({

       rules:{

            name:{
                required: true
            },

            phone:{
                required: true
            },
			city:{
                required: true
            },
       },

       messages:{

            name:{
                required: "Это поле обязательно для заполнения"
            },

            phone:{
                required: "Это поле обязательно для заполнения"
            },
			
			city:{
                required: "Выберите город"
            },
       },
	   
	   submitHandler: function (form){
            var str = $(".ajax_form_dev").serialize();

		
			$.post("/ajax/form_dev.php",
				   {'form_data': str},
				   function(result){ 
						$(".ajax_form_dev").hide();
						$(".ajax_form_dev").next().show();
				  }
				);
		
			
         },


    });	


	$(".ajax_form_card").validate({

       rules:{

            name:{
                required: false
            },

            phone:{
                required: true
            },
			number:{
                required: true
            },
       },

       messages:{

            name:{
                required: "Это поле обязательно для заполнения"
            },

            phone:{
                required: "<span>Введите Телефон</span>"
            },
			
			number:{
                required: "<span>Введите номер карты</span>"
            },
       },
	   
	   submitHandler: function (form){
            console.log(form);
            /*return false;
            var str = $(".ajax_form_card").serialize();

		
			$.post("/ajax/form_bal.php",
				   {'form_data': str},
				   function(result){ 
                        console.log(result);
						$(".ajax_form_card").hide();
						$(".ajax_form_card").next().show();
                        jData = JSON.parse(result);
                        console.log(jData);
				  }
				);*/
		
            var $this = $(form);

            datasend = {};
            datasend.number = $this.find('input[name="number"]').val();
            datasend.name = $this.find('input[name="name"]').val();
            datasend.phone = $this.find('input[name="phone"]').val();
            //console.log(datasend);
            $.ajax({
                type: "POST",
                url: '/ajax/form_bal.php',
                dataType: "html",
                data: datasend,
                success: function(data){
                    console.log(data);

                    jData = JSON.parse(data);
                    console.log(jData);

                    var $m = $this.closest('.formBlock__form,.jk-banner-balls').find('.message');
                    console.log($m);
                    if(typeof(jData.MESSAGE) != "undefined"){
                        $m.html(jData.MESSAGE);
                    }else{
                        $m.html('<span class="tfull">Сумма бонусов н</span><span class="tmini">Н</span>а Вашей карте: '+jData.COUNT+' руб.');
                    }
                }
            });

            return false;
			
         },


    });		
			
    //------------------is Mobile and Tablet---------------------\\
    var isMobile = { 

        Android: function() {
        return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }

    };

    //------------------Mobile toggle menu---------------------\\
    $(".toggle-bars").on('click', function(event) {
        $(this).closest('.menuWrap').toggleClass('in');
        event.preventDefault();
    });

    //------------------slide---------------------\\


    //------------------Mobile toggle menu about---------------------\\
    $('.menuAbout__toggle').on('click', function() {
        var elem = $(this),
            elemParent = elem.closest('.menuAbout__wrap');

            elemParent.toggleClass('in');
            elemParent.find('.menuAbout').slideToggle();

            if (elemParent.hasClass('in')) {
                elem.text('Свернуть меню');
            } else {
                elem.text('Развернуть меню');
            }
    });

    //------------------Mobile toggle filter---------------------\\
    $('.filter__toggle').on('click', function() {
        var elem = $(this),
            elemParent = elem.closest('.filter__wrap');

            elemParent.toggleClass('in');
            elemParent.find('.filter').slideToggle();

            if (elemParent.hasClass('in')) {
                elem.text('Свернуть фильтр');
            } else {
                elem.text('Развернуть фильтр');
            }
    });

    //------------------Catalog---------------------\\


    //console.log($('FORM.smartfilter'));
    $('FORM.smartfilter').unbind('submit');
    $('FORM.smartfilter').unbind();
    $('FORM.smartfilter').submit(function(){
        //console.log("submit");
        //smartFilter.doload(this);
        smartFilterMod.doload(this);
        /*$('.smartfilter .bx-filter-container-modef .bx-filter-popup-result A').click();
        console.log($('.smartfilter .bx-filter-container-modef .bx-filter-popup-result A'));
        console.log($('.smartfilter .bx-filter-container-modef .bx-filter-popup-result'));
        console.log($('.smartfilter .bx-filter-container-modef'));
        console.log($('.smartfilter'));
        $('.smartfilter .bx-filter-container-modef .bx-filter-popup-result A').trigger("click");*/
		//console.log($('.smartfilter .bx-filter-container-modef .bx-filter-popup-result A').attr('href'));
///        document.location.href = $('.smartfilter .bx-filter-container-modef .bx-filter-popup-result A').attr('href');
        return false;
    });
    $('FORM.smartfilter A#set_filter_a').click(function(){
        console.log("FORM.smart_filter click");
        //smartFilter.doload(this);
        smartFilterMod.doload(this);
        /*$('.smartfilter .bx-filter-container-modef .bx-filter-popup-result A').click();
        console.log($('.smartfilter .bx-filter-container-modef .bx-filter-popup-result A'));
        console.log($('.smartfilter .bx-filter-container-modef .bx-filter-popup-result'));
        console.log($('.smartfilter .bx-filter-container-modef'));
        console.log($('.smartfilter'));
        $('.smartfilter .bx-filter-container-modef .bx-filter-popup-result A').trigger("click");*/
///		var lnk = $('.smartfilter .bx-filter-container-modef .bx-filter-popup-result A').attr('href');
		//console.log(lnk);
///        if(lnk) document.location.href = lnk;
        return false;
    });
    /*$.get(
        result.FILTER_AJAX_URL, 
        function (data) {
            $('.catalog-list').html($(data).find('.catalog-list').html());
        }
    );*/
    
    $('.go2sub').click(function(){
        console.log("go2sub");
        var lnk = $('.smartfilter .bx-filter-container-modef .bx-filter-popup-result A').attr('href');
        console.log(lnk);
        if(typeof(lnk) == 'undefined'){
            lnk = $(this).data('url') + '?' + window.location.search.substring(1);
        }else{
            var p = lnk.split('?');
            console.log(p);
            lnk = $(this).data('url') + '?' + p[1];
        }
        console.log('Go to: ' + lnk);
        document.location.href = lnk;
        return false;
    });

    $('.coll-item').touchstart(function(){
        touchMoving = false;
        //if($(this).hasClass('active')){
            //document.location.href=$(this).find('A').attr('href');
        //}else{
            $('.coll-item.touch').removeClass('touch');
            $(this).addClass('touch');
            //return false;
        //}
    });
    
    $('.coll-item').touchmove(function(){
        touchMoving = true;
    });
    
    $('.coll-item').touchend(function(){
        if(touchMoving == false && $(this).hasClass('active')){
            document.location.href=$(this).find('A').attr('href');
        }else if($(this).hasClass('touch')){
            $('.coll-item').removeClass('active touch');
            $(this).addClass('active');
        }
        touchMoving = false;
    });
    
    $('.coll-item').click(function(){
        if($(this).hasClass('active') == false){
            document.location.href=$(this).find('A').attr('href');
        }
        /*if($(this).hasClass('active')){
            document.location.href=$(this).attr('href');
        }else{
            $('.coll-item.active').removeClass('active');
            $(this).addClass('active');
            return false;
        }*/
    });
    
    $('.togshowfull').click(function(){
        $(this).closest('.tog-small-txt-block').find('.tog-small-txt').css('max-height','none');
        $(this).hide();
    });
    
    $('#auth_form FORM').submit(function(){
        //var str = $(this).serialize();
        //console.log(str);
        
        var $this = $(this);

        datasend = {};
        datasend.PHONE = $this.find('input[name="PHONE"]').val();
        datasend.PASSWORD = $this.find('input[name="PASSWORD"]').val();
        //console.log(datasend);
        $.ajax({
            type: "POST",
            url: '/ajax/auth.php',
            dataType: "html",
            data: datasend,
            success: function(data){
                //$("#bid").html(getBasketHTML(out));
                console.log(data);

                jData = JSON.parse(data);
                //alert( user.friends[1] ); // 1
                console.log(jData);
                
                if(jData.RESULT === "1"){
                    document.location.reload();
                }else{
                    $this.find('.message').html(jData.MESSAGE);
                }
            }
        });
    
        return false;
    });
    
    $('.search .submit.obut').click(function(){
        $(this).closest('FORM').submit();
    });
    
    //$('.city-set-block A[do_select]').unbind('click');
    //$('.city-set-block A[do_select]').unbind();
    //$('.city-set-block A[do_select]').click(function(){
    $('.city-set-block A:not([do_select]):not(.modal-close)').unbind('click');
    $('.city-set-block A:not([do_select]):not(.modal-close)').unbind();
    $('.city-set-block A:not([do_select]):not(.modal-close)').click(function(){
        var $this = $(this);
        var $city = $this.data('city');
        if(typeof($city) == 'undefined' || $city.length < 1) $city = $this.text();
        console.log($city);
        console.log(typeof($city));
        if(typeof($city) != 'undefined' && $city.length > 1){
            /*if(typeof(bShops) != 'undefined'){
                
            }else{*/
                var $form = $this.closest('.city-set-block').find('FORM');
                console.log($form);
                $form.find('INPUT').val($city);
                $form.submit();
            /*}*/
        }
        return false;
    });
    
    $('.catalog-filters .filter-row .view-row').click(function(){
        var $this = $(this);
        console.log(this);
        var n = 'm';
        if($this.hasClass('view-row-1')){
            $this.removeClass('view-row-1').addClass('view-row-2');
            n = 'm';
        }else{
            $this.removeClass('view-row-2').addClass('view-row-1');
            n = 'b';
        }
        $('#catcat,#content').removeClass('row-m row-b').addClass('row-' + n);
        setCookie("page_size", n, 365);
        return false;
    });
    
    $('.popup-window').magnificPopup({
		type:'inline',
		midClick: true
    });
    
    $('*[popup]').magnificPopup({
		type:'inline',
		midClick: true
    });
    
    /*$('.shop-list-tbl .all-shops').on('click', function(){
        var $this = $(this);
console.log($this);
        var $items = $this.closest('.shop-list-tbl').find('.all-shops-item');
console.log($items);
        if($this.hasClass('active')){
            $this.removeClass('active');
            $items.removeClass('active');
            $items.hide();
        }else{
            $this.addClass('active');
            $items.addClass('active');
            $items.show();
        }
        return false;
    });*/
    
    $('.shop-list-tbl-mob .sl-city .officeItem .more').on('click', sl_office_more);
    
    /*console.log('ymaps: ', ymaps);
    try{
        ymaps.ready(function () {
            console.log('ymaps.ready');
        });
    }catch(e){
        console.log(e);
    }*/
    
    var locationSets = getCookie("location_sets");
    //console.log('locationSets: ', locationSets);
    //console.log('locationSets: ', typeof(locationSets));
    if(locationSets == ''){
        /*var gls = supports_geolocation();
        console.log('supports_geolocation: ', gls);
        //if(supports_geolocation()){
        if(gls){
            var glr = get_location();
            console.log('get_location: ', glr);
        }else{
            setCookie("location_sets", false);
        }*/
        try{
            ymaps.ready(function () {
                //var glr = 
                    get_location();
                //console.log('get_location: ', glr);
            });
        }catch(e){
            console.log(e);
        }
    }

    $(document).on('click', '.all-sizes-list LI', function(){
        var $this = $(this);
        var sSize = $this.data('size').toString();
        var $p = $this.closest('.all-sizes-list');
        var bIsTop = ($p.closest('.catalog_item').length > 0);
        //console.log("clicked on top: ", bIsTop);
        $p.find('LI').removeClass('active');
        $this.addClass('active');
        $('.all-sizes-list').each(function(i, e){
            //console.log(i, e);
            var $sp = $(e).closest('.all-sizes-list');
            var $t = $sp.closest('.catalog_item');
            if((bIsTop && $t.length < 1) || (!bIsTop && $t.length > 0)){
                //console.log("Is target!");
                //console.log($sp.find('LI[data-size="'+sSize+'"]'));
                $sp.find('LI').removeClass('active');
                $sp.find('LI[data-size="'+sSize+'"]').addClass("active");
            }
        });
        
        var ic = 0;
        if(sSize != -1){
            $('.shop-list-tbl .count_shops').addClass('hide-by-size');
            $('.shop-list-tbl TR').each(function(i, e){
                var $this = $(e);
                var s = '';
                if(typeof($this.data('sizes')) != 'undefined'){
                    //console.log("Sizes: ", $this.data('sizes'));
                    s = $this.data('sizes').toString().split(',');
                    //console.log("Sizes: ", s);
                    //console.log("Have: ", s.indexOf(sSize));
                    if(s.indexOf(sSize) < 0){
                        $this.addClass('hide-by-size');
                    }else{
                        $this.removeClass('hide-by-size');
                    }
                }
                //console.log("Sizes: ", $(e).data('sizes'));
                if(typeof($this.data('size')) != 'undefined'){
                    //console.log("Sizes: ", $this.data('sizes'));
                    //s = $this.data('size').toString();
                    //console.log("Size: ", s);
                    //console.log("Have: ", s.indexOf(sSize));
                    if($this.data('size').toString() != sSize){
                        $this.addClass('hide-by-size');
                    }else{
                        $this.removeClass('hide-by-size');
                        ic++;
                    }
                }
            });
        }else{
            $('.shop-list-tbl TR').removeClass('hide-by-size');
            $('.shop-list-tbl .count_shops').removeClass('hide-by-size');
        }
        if(sSize != -1 && ic < 1){
            DoNotToggle = true;
            $('.shop-list-tbl .all-sizes-list LI[data-size="-1"]').click();
            setTimeout(function(){
                DoNotToggle = false;
                $('.shop-list-tbl TH.sl-size').find('.all-sizes-list').toggle(1000);
            }, 1000);
        }
    });
    
    $(document).on('click', '.shop-list-tbl TH.sl-size', function(){
        if(!DoNotToggle) $(this).find('.all-sizes-list').toggle(200);
    });
    
    $(document).on('click', ".show_more", show_more);
    $(document).on('click', '.scroll-top', function(){
        //console.log("ScrollTop");
        $("html, body").animate({scrollTop: $('#catalog_items').position().top}, 500);
    });

    /*$(document).scroll(function(){
        //var $t = $('.show_more_block_pos');
        var $t = $('#catalog_items');
        
        if($t.length < 1) return false;
        
        var $this = $(this);
        var c = $t.position().top;
        var h = c + $t.height();
        var w = $(window).height();
        //var half = Math.floor(w/2);
        var s = $this.scrollTop() + w;
        //var t = h - half;
        var t = h - w;
        
        //console.log('sm-pos: ', h, ' / ', s, ' >= ', t, ' max: ', h);
        //console.log('scroll-pos: ', s, ' / ', t);
        
        if(s >= t){
            show_more();
        }
        
        //console.log(ScrollTopArrBusy);
        if(!ScrollTopArrBusy){
            var $st = $('.scroll-top');
            if(s > (c+w+w)){
                if($st.length > 0 && $st.css('opacity') == 0/* && !$st.is(':visible') && !$st.is(':not(:hidden)')*-/){
                    //console.log('show');
                    ScrollTopArrBusy = true;
                    $st.animate({opacity: 1}, 300, function(){ScrollTopArrBusy = false;});
                    //$st.fadeIn(3000, function(){ScrollTopArrBusy = false;});
                }
            }else{
                if($st.length > 0 && $st.css('opacity') == 1/* && $st.is(':visible') && $st.is(':not(:hidden)')*-/){
                    //console.log('hide');
                    ScrollTopArrBusy = true;
                    //$st.fadeOut(3000, function(){ScrollTopArrBusy = false;});
                    $st.animate({opacity: 0}, 300, function(){ScrollTopArrBusy = false;});
                }
            }
        }
    });
    
    $(document).scroll();*/

    $(document).on('click', '.filter-button', function (){
        if(Debug1) console.log("Click 4");
        //console.log(': ', this);
        var $this = $(this);
        //var $p = $this.closest(".catalog-filters");
        var $p = $('.catalog-filters:visible');
        var h = 0;
        if(!$this.hasClass('active')){
            h = $p.find('.fb-content').height();
            if($p.hasClass('cf-mobile')){
                //var cs = $this.scrollTop();
                var cs = $(document).scrollTop();
                var $filters = $('#catalog_items');
                if($filters.length > 0){
                    //var fp = $filters.position().top - 30;
                    var fp = $('.filter-line-pointer').position().top-filterTopPosDiff;
                    //console.log('scroll-pos: ', cs, ' / ', fp, ' / ', $filters.position().top, ' / ', $(document).scrollTop());
                    if(cs < fp){
                        $("html, body").animate({scrollTop: fp + 10}, 200, function(){MobScrollToFilter = false;});
                        MobScrollToFilter = true;
                    }
                }
            }
        }
        //console.log('h: ', h, ' wh: ', $(window).height());
        var $fb = $p.find('.filter-body');
        if(h > 0){
            //var $thisS = $p.find('.sort-button');
            var $thisS = $('#catalog_items .filter-line .sort-button');
            if($thisS.hasClass('active')){
                $thisS.click();
                setTimeout(function(){$('.filter-button').click()}, 300);
                return false;
            }
            $fb.css({'height':'0px'}).addClass('active').closest('.catalog-filters').addClass('active');
            //if($p.hasClass('cf-mobile')) $fb.find('.fb-content').css({'max-height':($(window).height() - 65)+'px'});
        }
        $this.toggleClass('active');
        $fb.animate({height:h+'px'}, 500, function(){
            if(h == 0) $fb.removeClass('active').closest('.catalog-filters').removeClass('active');
            else if($p.hasClass('cf-mobile')) $fb.css({'height':''});
            //$('BODY').toggleClass('noscroll');
            var $cfb = $('.filter-body.active,.filter-body.active');
            if($cfb.length > 0){
                $('BODY').addClass('noscroll');
                if($p.hasClass('cf-mobile')){
                    $('#catalog_items').addClass('scrolled');
                    //console.log($cfb.position(), $cfb.offset(), $cfb.height());
                    var $b = $p.find('.filter-wrap-bg');
                    if($b.length > 0){
                        var bh = $p.height() + 67;
                        var wh = $(window).height();
                        var bgh = wh - bh;
                        console.log('rs: ', bh, wh, bgh);
                        if(bgh < 0) bgh = 0;
                        $b.css({'top':(wh - bgh) + 'px'});
                        //$b.css({'top':(67+$cfb.height())+'px'});
                    }
                }
            }else{
                //console.log('Remove noscroll');
                $('BODY').removeClass('noscroll');
                if($p.hasClass('cf-mobile')){
                    //console.log('Remove scrolled');
                    $('#catalog_items').removeClass('scrolled');
                }
            }
            $(document).scroll();
        });
    });
    
    $(document).on('click', '.filter-close', function (){
        if(Debug1) console.log("Click 3");
        var $this = $(this);
        //var $fb = $('.catalog-filters.cf-pc .filter-button');
        //var $fb = $this.closest('.catalog-filters').find('.filter-button');
        var $fb = $('.filter-line .filter-button');
        //console.log($fb);
        if($fb.hasClass('active')) $fb.click();
    });
    
    $(document).on('click', '.catalog-filters .fb-menu-left LI[data-menu]', function (){
        if(Debug1) console.log("Click 2", this);
        var $this = $(this);
        var $p = $this.parent();
        var $c = $this.closest('.fb-content');
        var $m = $this.closest('.catalog-filters');
        if(Debug1) console.log("cf: ", $m);
        var m = $this.data('menu');
        var $t = $c.find('.fb-menu-right[data-menu="'+m+'"]');
        var h = 0;
        
        if($m.hasClass('cf-mobile')){
            if(Debug1) console.log("Is Mobile");
            if($this.hasClass('active')){
                $t.animate({
                    'height':'0px'
                }, 500, function(){
                    $(this).hide();
                    $this.removeClass('active');
                    var bh = $m.height() + 67;
                    var wh = $(window).height();
                    var bgh = wh - bh;
                    console.log('rs: ', bh, wh, bgh);
                    if(bgh < 0) bgh = 0;
                    var $b = $m.find('.filter-wrap-bg');
                    if($b.length > 0){
                        $b.css({'top':(wh - bgh) + 'px'});
                    }
                });
            }else{
                $c.find('.fb-menu-left>LI.active[data-menu]').click();
                $t.css({
                    'height':'auto',
                    'position':'absolute'
                });
                h = $t.height();
                if(h > 300) h = 300;
                $t.css({
                    'height':'0',
                    'position':'static'
                });
                $t.show().animate({
                    'height':h+'px'
                }, 500, function(){
                    $this.addClass('active');
                    $(this).css({'height':''});
                    var bh = $m.height() + 67;
                    var wh = $(window).height();
                    var bgh = wh - bh;
                    console.log('rs: ', bh, wh, bgh);
                    if(bgh < 0) bgh = 0;
                    var $b = $m.find('.filter-wrap-bg');
                    if($b.length > 0){
                        $b.css({'top':(wh - bgh) + 'px'});
                    }
                });
            }
        }else{
            $c.find('.fb-menu-right').hide();
            var $am = $c.find('.fb-menu-right[data-menu="'+m+'"]');
            $am.show();
            $p.find('LI').removeClass('active');
            $this.addClass('active');
            
            if(m == 'SECTION'){
                //console.log($am);
                var $liA = $am.find('LI.active');
                if($liA.length > 0){
                    $c.find('.fb-menu-right[data-menu="SECTION_L2"][data-pid="'+$liA.data('sid')+'"]').show();
                }
            }
        }
        
        return false;
    });
    
    $(document).on('click', '.catalog-filters .fb-menu-right LI:not(.disable)', function (){
        if(Debug1) console.log("Click 1");
        //console.log("SF do select");
        var $this = $(this);
        var $p = $this.parent();
        var m = $p.data('menu');
        var $t = $('#catalog_items .filter-body .fb-menu-right[data-menu="'+m+'"] LI[data-id="'+$this.data('id')+'"]');
        var $input = $('#catalog_items .filter-body INPUT#'+$this.data('id')+',#catalog_items .filter-body INPUT[name="'+m+'['+$this.data('id')+']"]');
        
        if(m == 'I_TYPE'){
            if($this.hasClass('active')) return false;
            $('#catalog_items .filter-body .fb-menu-right[data-menu="'+m+'"] LI').removeClass('active').find('INPUT').removeAttr('checked').removeProp('checked');
            $('.filter-line .filter-active .filter-aitem[data-menu="'+m+'"] .item').remove();
        }
        
        if(m == 'SECTION'){
            if($this.hasClass('active')) return false;
            $('#catalog_items .filter-body .fb-menu-right[data-menu="'+m+'"] LI').removeClass('active').find('INPUT').removeAttr('checked').removeProp('checked');
            $('.filter-line .filter-active .filter-aitem[data-menu="'+m+'"] .item').remove();
            $('#catalog_items .filter-body .fb-menu-right[data-menu="'+m+'_L2"]').hide();
            $('#catalog_items .filter-body .fb-menu-right[data-menu="'+m+'_L2"][data-pid="'+$this.data('sid')+'"]').show();
            var $form = $this.closest('FORM');
            $form.find('INPUT[name="SECTION_ID"]').val($this.data('sid'));
            $form.find('INPUT[name="SECTION_CODE"]').val($this.data('scode'));
            
            $('#catalog_items .filter-body .fb-menu-right[data-menu="SECTION_L2"] LI').removeClass('active').find('INPUT').removeAttr('checked').removeProp('checked');
            $('.filter-line .filter-active .filter-aitem[data-menu="SECTION_L2"] .item').remove();
            //return false;
        }else if(m == 'SECTION_L2'){
            if($this.hasClass('active')){
                var $p2 = $this.closest('UL');
                var $form = $this.closest('FORM');
                $form.find('INPUT[name="SECTION_ID"]').val($p2.data('pid'));
                $form.find('INPUT[name="SECTION_CODE"]').val($form.find('.fb-menu-right[data-menu="SECTION"] LI[data-sid="'+$p2.data('pid')+'"]').data('scode'));
                $('#catalog_items .filter-body .fb-menu-right[data-menu="'+m+'"] LI').removeClass('active').find('INPUT').removeAttr('checked').removeProp('checked');
                $('.filter-line .filter-active .filter-aitem[data-menu="'+m+'"] .item').remove();
                DoUpdateCatalogItems();
                return false;
            }
            $('#catalog_items .filter-body .fb-menu-right[data-menu="'+m+'"] LI').removeClass('active').find('INPUT').removeAttr('checked').removeProp('checked');
            $('.filter-line .filter-active .filter-aitem[data-menu="'+m+'"] .item').remove();
            //$('#catalog_items .filter-body .fb-menu-right[data-menu="'+m+'_L2"]').hide();
            //$('#catalog_items .filter-body .fb-menu-right[data-menu="'+m+'_L2"][data-pid="'+$this.data('sid')+'"]').show();
            var $form = $this.closest('FORM');
            $form.find('INPUT[name="SECTION_ID"]').val($this.data('sid'));
            $form.find('INPUT[name="SECTION_CODE"]').val($this.data('scode'));
            //return false;
        }else if(m == 'PRICE_DISC'){
            //var $p2 = $this.closest('UL');
            //$p2.find('.min-price').val($this.data('min'));
            //$p2.find('.max-price').val($this.data('max'));
            var $form = $this.closest('FORM');
            $form.find('.min-price').val($this.data('min'));
            $form.find('.max-price').val($this.data('max'));
            window['trackBarmf-price'].onInputChange();
            DoUpdateCatalogItems();
            return false;
        }
        
        /*var v = $this.data('value');
        console.log(v);
        //var jData = JSON.parse(v);
        //console.log(jData);
        if(typeof(v) !== 'undefined'){
            for(var k in v){
                $('#catalog_items .filter-body INPUT[name="'+k+'"]').val(v[k]);
            }
        }*/
            
        if(!$this.hasClass('active')){
            //console.log("Is not active");
            var $itemNew = $('<div class="item" data-id="'+$this.data('id')+'">'+$this.html()+'<b>&#10006;</b></div>').hide();
            $itemNew.find('.count').remove();
            $itemNew.find('input').remove();
            //$('.filter-line .filter-active .filter-aitem[data-menu="'+m+'"]').append('<div class="item" data-id="'+$this.data('id')+'">'+$this.text()+'<b>&#10006;</b></div>');
            //console.log("Append ", $itemNew, ' to ', $('.filter-line .filter-active .filter-aitem[data-menu="'+m+'"]'));
            $('.filter-line .filter-active .filter-aitem[data-menu="'+m+'"]').append($itemNew);
            $itemNew.fadeIn();
            $('.filter-line .filter-active .filter-aitem .item').on('click', filterItemRemove);
            $t.addClass('active');
            $input.attr('checked', 'checked').prop('checked', 'checked');
        }else{
            //console.log("Is active");
            $('.filter-line .filter-active .filter-aitem[data-menu="'+m+'"] .item[data-id="'+$this.data('id')+'"]').click();
            $t.removeClass('active');
            $input.removeAttr('checked').removeProp('checked');
        }
        
        DoUpdateCatalogItems();
        //smartFilter.click(this);
        
        //$(this).find('input').click();
        
        return false;
    });
    
    $(document).on('click', '.filter-line .filter-active .filter-aitem .item', filterItemRemove);
    
    $(document).on('click', '.sort-button', function (){
        if(Debug1) console.log("Click 5");
        //console.log($('.catalog-filters:visible'));
        var $this = $(this);
        //var $c = $this.closest('.catalog-filters');
        var $c = $('.catalog-filters:visible');
        var h = 0;
        if(!$this.hasClass('active')) h = $c.find('.sb-content').height();
        var $sb = $c.find('.sort-body');
        if(h > 0){
            //var $thisF = $c.find('.filter-button');
            var $thisF = $('#catalog_items .filter-line .filter-button');
            if($thisF.hasClass('active')){
                $thisF.click();
                setTimeout(function(){$('#catalog_items .filter-line .sort-button').click()}, 600);
                return false;
            }
            $sb.css({'height':'0px'}).addClass('active');
        }
        $this.toggleClass('active');
        $sb.animate({height:h+'px'}, 200, function(){
            if(h == 0) $sb.removeClass('active');
            else if($c.hasClass('cf-mobile')) $sb.css({'height':''});
            //$('BODY').toggleClass('noscroll');
            if($('.filter-body.active,.filter-body.active').length > 0) $('BODY').addClass('noscroll');
            else $('BODY').removeClass('noscroll');
            
            $(document).scroll();
        });
    });
    
    $(document).on('click', '.catalog-filters .sb-sort-list LI', function (){
        if(Debug1) console.log("Click 6");
        var $this = $(this);
        var $p = $this.parent();
        if(!$this.hasClass('active')){
            SetPageSort(this, 1);
            $p.find('LI').removeClass('active');
            $this.addClass('active');
        }
        var $sb = $this.closest('.catalog-filters').find('.sort-body');
        $sb.animate({height:'0px'}, 500, function(){
            $sb.removeClass('active');
            $('.sort-button').removeClass('active');
        });
        
        $('.filter-line .sort-button').text($this.text());
    });

    $(document).scroll(function(e){
        if(Debug1) console.log("Scroll 1", e);
        var $this = $(this);
        var cs = $this.scrollTop();
        //var $filters = $('#filters');
        var $filters = $('#catalog_items');
        
        if($filters.length < 1) return false;
        
        //var fp = $filters.position().top-30;
        var fp = $('.filter-line-pointer').position().top-filterTopPosDiff;
        //var $fba = $('#filters-mob .filter-button.active');
        var $fba = $('.filter-line .filter-button.active');
        
        //console.log('scroll-pos: ', cs, ' / ', fp, ' / ', $fba.length);
        
        if($fba.length > 0){
            if(cs < fp && MobScrollToFilter == false){
                //$("html, body").css({scrollTop: fp + 10});
                /*$("html, body").scrollTop(fp + 10);
                cs = $this.scrollTop();
                fp = $('.filter-line-pointer').position().top-filterTopPosDiff;*/
                //console.log('scroll-pos-m: ', cs, ' / ', fp, ' / ', $fba.length);
            }
        }
        if($fba.length < 1 || $filters.find('.catalog-filters.cf-mobile.active').length < 1){
            //console.log('fba', $fba.length, $filters.find('.catalog-filters.cf-mobile.active').length);
            if(cs >= fp){
                $filters.addClass('scrolled');
                //if($fba.length > 0 && $(window).width() < 930) $('html').addClass('scrolled');
                //else $('html').removeClass('scrolled');
            }else{
                $filters.removeClass('scrolled');
                $('html').removeClass('scrolled');
            }
        }
        
        
        var $t = $('#catalog_items');
        
        if($t.length < 1) return false;
        
        var c = $t.position().top;
        var h = c + $t.height();
        var w = $(window).height();
        var s = $this.scrollTop() + w;
        var t = h - w;
        
        //console.log('sm-pos: ', h, ' / ', s, ' >= ', t, ' max: ', h);
        //console.log('scroll-pos: ', s, ' / ', t);
        
        if(s >= t){
            show_more();
        }
        
        //console.log(ScrollTopArrBusy);
        if(!ScrollTopArrBusy){
            var $st = $('.scroll-top');
            if(s > (c+w+w)){
                if($st.length > 0 && $st.css('opacity') == 0/* && !$st.is(':visible') && !$st.is(':not(:hidden)')*/){
                    //console.log('show');
                    ScrollTopArrBusy = true;
                    $st.animate({opacity: 1}, 300, function(){ScrollTopArrBusy = false;});
                    //$st.fadeIn(3000, function(){ScrollTopArrBusy = false;});
                }
            }else{
                if($st.length > 0 && $st.css('opacity') == 1/* && $st.is(':visible') && $st.is(':not(:hidden)')*/){
                    //console.log('hide');
                    ScrollTopArrBusy = true;
                    //$st.fadeOut(3000, function(){ScrollTopArrBusy = false;});
                    $st.animate({opacity: 0}, 300, function(){ScrollTopArrBusy = false;});
                }
            }
        }
        
        
        return;
        
        //var $t = $('.show_more_block_pos');
        var $t = $('#catalog_items');
        var c = $t.position().top;
        var h = c + $t.height();
        var w = $(window).height();
        //var half = Math.floor(w/2);
        var s = cs + w;
        //var t = h - half;
        var t = h - w;
        
        //console.log('sm-pos: ', h, ' / ', s, ' >= ', t, ' max: ', h);
        
        if(s >= t){
            show_more();
        }
        
        //console.log(ScrollTopArrBusy);
        if(!ScrollTopArrBusy){
            var $st = $('.scroll-top');
            if(s > (c+w+w)){
                if($st.length > 0 && $st.css('opacity') == 0/* && !$st.is(':visible') && !$st.is(':not(:hidden)')*/){
                    //console.log('show');
                    ScrollTopArrBusy = true;
                    $st.animate({opacity: 1}, 300, function(){ScrollTopArrBusy = false;});
                    //$st.fadeIn(3000, function(){ScrollTopArrBusy = false;});
                }
            }else{
                if($st.length > 0 && $st.css('opacity') == 1/* && $st.is(':visible') && $st.is(':not(:hidden)')*/){
                    //console.log('hide');
                    ScrollTopArrBusy = true;
                    //$st.fadeOut(3000, function(){ScrollTopArrBusy = false;});
                    $st.animate({opacity: 0}, 300, function(){ScrollTopArrBusy = false;});
                }
            }
        }
    });
    
    $(window).resize(function(){
        var $fa = $('.catalog-filters .filter-close');
        //console.log('fa: ', $fa);
        if($fa.length > 0){
            $fa.click();
        }
        $(document).scroll();
    });
    
    $(document).scroll();
    
    $('.mob-menu-button').click(function(){
        console.log('mob-menu-button click!');
        $('html').css({'overflow-y':'hidden'});
    });
    $('#m-menu .close-link').click(function(){
        $('.mob-menu-button').click();
        $('html').css({'overflow-y':''});
    });

    $('#m-menu .mob-menu-main .childrens').click(function (){
        var $this = $(this);
        var mId = $this.data('id');
        var $main = $('#m-menu .mob-menu-main');
        var $sub = $('#m-menu .mob-menu-sub[data-id="'+mId+'"]');
        
        $sub.css({
            position:'absolute',
            left:'100%',
            top:'0'
        });
        
        $main.css({
            position:'relative'
        });
        
        $main.animate({left:'-100%'}, 500, function(){
            $(this).hide();
        });
        
        $sub.show().animate({left:'0%'}, 500, function(){
            $(this).css({
                position:'static'
            });
        });
        
        return false;
    });

    $('#m-menu .mob-menu-sub .back-link').click(function (){
        var $this = $(this);
        var $main = $('#m-menu .mob-menu-main');
        var $sub = $this.closest('.mob-menu-sub');
        
        $sub.css({
            position:'absolute',
            left:'0%',
            top:'0'
        });
        
        $main.css({
            position:'relative'
        });
        
        $main.show().animate({left:'0%'}, 500, function(){
            $(this).css({
                position:'static'
            });
        });
        
        $sub.show().animate({left:'100%'}, 500, function(){
            $(this).hide();
        });
        
        return false;
    });
    
    if(typeof(arFilterAct) !== 'undefined'){
        for(var mId in arFilterAct){
            for(var iId in arFilterAct[mId]){
                console.log('['+mId+']['+iId+'] = ', arFilterAct[mId][iId]);
                var $t = $('#catalog_items .filter-body .fb-menu-right[data-menu="'+mId+'"] LI[data-id="'+iId+'"]');

                var $itemNew = $('<div class="item" data-id="'+iId+'">'+arFilterAct[mId][iId]+'<b>&#10006;</b></div>').hide();
                $('.filter-line .filter-active .filter-aitem[data-menu="'+mId+'"]').append($itemNew);
                $itemNew.fadeIn();
                $('.filter-line .filter-active .filter-aitem .item').on('click', filterItemRemove);
                $t.addClass('active');
            }
        }
    }
    
    $('#catalog_items .filter-body .fb-menu-right LI.active INPUT[checked="checked"]').each(function(i){
        var $this = $(this);
        
        if($this.attr('name') == 'STONE_COUNT_NO') return;
        
        var $li = $this.parent();
        var $lic = $li.clone();
        var $p = $li.parent();
        
        $lic.find('.count,input').remove();
        
        var $itemNew = $('<div class="item" data-id="'+$li.data('id')+'">'+$lic.text()+'<b>&#10006;</b></div>').hide();
        $('.filter-line .filter-active .filter-aitem[data-menu="'+$p.data('menu')+'"]').append($itemNew);
        $itemNew.fadeIn();
        //console.log($this, $p, $itemNew, ' ');
    });
    if($('.filter-line .filter-active .filter-aitem .item').length > 0) $('#catalog_items .filter-aclear').addClass('active');
    
    $('.filter-line .filter-active .filter-aitem .item').on('click', filterItemRemove);
    //console.log($t);
    
    $('#catalog_items .filter-aclear').click(function(){
        $('.filter-line .filter-active .filter-aitem .item').each(function(i){
            var $this = $(this);
            var $p = $this.parent();
            var m = $p.data('menu');
            var $l = $('.catalog-filters .fb-menu-right[data-menu="'+m+'"] LI[data-id="'+$this.data('id')+'"]');

            if($l.hasClass('active') == false) return false;

            $l.removeClass('active').find('input').removeAttr('checked').removeProp('checked');
            $this.fadeOut(300, function(){
                $this.remove();
                if($('.filter-line .filter-active .filter-aitem .item').length > 0) $('#catalog_items .filter-aclear').addClass('active');
                else $('#catalog_items .filter-aclear').removeClass('active');
            });

            if(m == 'SECTION'){
                var $form = $this.closest('#catalog_items').find('FORM.smartfilter');
                //console.log('SECTION_CODE[1215]: ', $this, $form, $form.find('INPUT[name="SECTION_CODE"]'), $form.find('INPUT'));
                $form.find('INPUT[name="SECTION_ID"]').val('');
                $form.find('INPUT[name="SECTION_CODE"]').val('');
                //return false;
            }else if(m == 'SECTION_L2'){
                var $p2 = $l.closest('UL');
                var $form = $this.closest('#catalog_items').find('FORM.smartfilter');
                $form.find('INPUT[name="SECTION_ID"]').val($p2.data('pid'));
                $form.find('INPUT[name="SECTION_CODE"]').val($form.find('.fb-menu-right[data-menu="SECTION"] LI[data-sid="'+$p2.data('pid')+'"]').data('scode'));
            }
        });
        
        DoUpdateCatalogItems();
        
        return false;
        
    });
    
    //$('#catalog_items .catalog-filters .filter-body').hover(
    $('#catalog_items .catalog-filters.cf-pc').hover(
        function(){
            //console.log('FEnter');
            FilterHOver = true;
        },
        function(){
            //console.log('FOut');
            FilterHOver = false;
            
            setTimeout(FilterHide, 500);
        }
    );

    function FilterHide(){
        //console.log('Hide');
        if(!FilterHOver){
            var $f = $('.filter-button.active');
            if($f.length > 0){
                $f.trigger('click');
            }
        }
    }
    
    function getAllEvents(element){
        var result = [];
        for(var key in element) {
            if (key.indexOf('on') === 0) {
                result.push(key.slice(2));
            }
        }
        return result.join(' ');
    }

    /*$('body').append('<div id="debug" style="position:fixed;left:0;bottom:0;background:#EEE;min-width:100%;min-height:10px;max-height:50%;overflow:auto;"></div>');
    var el = $('.fb-content');
    el.bind(getAllEvents(el[0]), function(e) {
        console.log('fb-content get event: ', e);
        $('#debug').append('<div>'+e.timeStamp+': '+e.type+'</div>').scrollTop(99999);
    });*/
    /*
    pointerover
    pointerenter
    pointerdown
    touchstart
    gotpointercapture
    pointerup
    lostpointercapture
    pointerout
    pointerleave
    touchend
    mouseover
    mouseenter
    mousemove
    mousedown
    selectstart
    mouseup
    */

    // anchor
    $(".anchor").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $(this).find('input').prop('checked', true);
        $('body,html').animate({scrollTop: top}, 1000);
    });
    
    $("#get_shops").click(function(){
        $(".catalog_item .item-data .ymap").show();
        $(this).hide();
    });
    
    /*$('BODY,BODY *').touchstart(function(e){
        console.log("touchstart: ", e);
        //console.log("touchstart: ", e.currentTarget);
        sleep(1000);
    });
    $('BODY,BODY *').scroll(function(e){
        console.log("scroll: ", e);
    });
    $('BODY,BODY *').click(function(e){
        console.log("click: ", e);
        sleep(1000);
    });*/
    
    /*$('BODY').touchstart(function(e){
		console.log('touchstart: ', e);
    });*/
    $('BODY').touchend(function(e){
		//console.log('touchend: ', e);
		var $this = $(this);
		if($(e['target']).closest('#catcat').length > 0 && $('BODY').hasClass('noscroll')){
			//console.log($('.filter-line .sort-button.active,.filter-line .filter-button.active'));
			$('.filter-line .sort-button.active,.filter-line .filter-button.active').click();
		}
    });
    
    $('#catalog_items .catalog-filters.cf-mobile .fb-menu-left LI .costRange INPUT').click(function(){
        return false;
    });
    
    $('#catalog_items .catalog-filters.cf-mobile .fb-menu-left LI[data-id="range"]').click(function(){
        return false;
    });
    
}); // End of document ready

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

function filterItemRemove(){
    if(Debug1) console.log("filterItemRemove");
    var $this = $(this);
    var $p = $this.parent();
    var m = $p.data('menu');
    var $l = $('.catalog-filters .fb-menu-right[data-menu="'+m+'"] LI[data-id="'+$this.data('id')+'"]');
    
    if($l.hasClass('active') == false) return false;
    
    $l.removeClass('active').find('input').removeAttr('checked').removeProp('checked');
    $this.fadeOut(300, function(){
        $this.remove();
        if($('.filter-line .filter-active .filter-aitem .item').length > 0) $('#catalog_items .filter-aclear').addClass('active');
        else $('#catalog_items .filter-aclear').removeClass('active');
    });
    
    if(m == 'SECTION'){
        var $form = $this.closest('FORM');
        $form.find('INPUT[name="SECTION_ID"]').val('');
        $form.find('INPUT[name="SECTION_CODE"]').val('');
        //return false;
    }else if(m == 'SECTION_L2'){
        var $p2 = $l.closest('UL');
        var $form = $this.closest('FORM');
        $form.find('INPUT[name="SECTION_ID"]').val($p2.data('pid'));
        $form.find('INPUT[name="SECTION_CODE"]').val($form.find('.fb-menu-right[data-menu="SECTION"] LI[data-sid="'+$p2.data('pid')+'"]').data('scode'));
        //$('#catalog_items .filter-body .fb-menu-right[data-menu="'+m+'"] LI').removeClass('active').find('INPUT').removeAttr('checked').removeProp('checked');
        //$('.filter-line .filter-active .filter-aitem[data-menu="'+m+'"] .item').remove();
        
        
        //var $form = $this.closest('FORM');
        //$form.find('INPUT[name="SECTION_ID"]').val('');
        //$form.find('INPUT[name="SECTION_CODE"]').val('');
        //return false;
    }
    
    DoUpdateCatalogItems();
};
function show_more() {
    if(ShowMoreLoading) return false;
    //console.log("Show more");
    
    var $test = $('<div class="test_main"><div class="test_2">222</div><div class="test_3">333</div></div>')
    //	console.log('test: ', $test);
    //	console.log('test.find: ', $test.find('.test_2'));
    //var $pag = $(this).closest(".pagination");
    //var $cat = $pag.find("#catcat");
    //var $cati = $(this).closest("#catalog_items");
    var $cati = $("#catalog_items");
    var $cat = $cati.find("#catcat");
    var $pagi = $cati.parent().find(".pagination");
    var sAa = $pagi.find("A.active").text();
    sAaN = sAa;
    sAaN++;
    var $pnLinks = $pagi.find(".prev_next_links");
    //console.log($pag);
    //	console.log($cati);
    //	console.log($cat);
    //	console.log($pnLinks);
    if ($pnLinks.length > 0) {
        var lNext = $pnLinks.data('next');
        //		console.log(lNext);
        if (lNext && lNext.length > 0) {
            $cati.parent().find(".loader").show();
            ShowMoreLoading = true;
            $.ajax({
                type: "POST",
                url: lNext,
                data: {
                    'ajax_get_page': 'y'
                },
                dataType: "html",
                success: function(data) {
                    var $data = $(data);
                    var dt = $data.find('.pagination');
                    var AppendBlock = $data.find('#catcat .catalog_item');
                    var $pagiNew = $data.find(".pagination>p");
                    var lNextNew = $pagiNew.find(".prev_next_links").attr('data-next');
                    $pagi.find("p").html($pagiNew);
                    $cat.append(AppendBlock);
                    if (!lNextNew || lNextNew.length < 1){
                        $cati.parent().find(".show_more_block").hide();
                        $cati.parent().find("#catalog-finish").show();
                    }else{
                        
                    }
                    $cati.parent().find(".loader").hide();
                    ShowMoreLoading = false;
                    if(typeof(yaCounter38054255) != 'undefined'){
                        //console.log("yaHit: ", lNext);
                        yaCounter38054255.hit(lNext, {title: $('H1').text()+' Страница '+sAaN, referer: YaHitLink});
                        YaHitLink = lNext;
                    }else{
                        //console.log("Not yaHit.", yaCounter38054255);
                    }
                    //yaCounterXXXXXX.hit('http://example.com#contacts', {title: 'Контакты', referer: 'http://example.com/#main'});
                }
            });
        }
    }
};



if ($('#slideRange').length) {
    // Vanilla
    var am2min = document.getElementById('arrFilter_P1_MIN');
    var am2max = document.getElementById('arrFilter_P1_MAX');
    var noS = document.getElementById('slideRange');

	min_value = $('#min_value').val()*1;
	max_value = $('#max_value').val()*1;
	set_min_value = $('#arrFilter_P1_MIN').val()*1;
	set_max_value = $('#arrFilter_P1_MAX').val()*1;
	
	if ( set_min_value == 0 ){
		set_min_value = min_value;
		$('#arrFilter_P1_MIN').val('');
	}
		
	if ( set_max_value == 0 )
		set_max_value = max_value;	
	
    noUiSlider.create(noS, {
        start: [set_min_value, set_max_value],
        connect: true,
        range: {
            'min': min_value,
            'max': max_value
        }
    });

    noS.noUiSlider.on('update', function( values, handle ) {

        var value = values[handle];

        if ( handle ) {
            am2max.value = Math.round(value);
        } else {
            am2min.value = Math.round(value);
        }
    });

    am2min.addEventListener('change', function(){
        noS.noUiSlider.set([this.value, null]);
    });

    am2max.addEventListener('change', function(){
        noS.noUiSlider.set([null, this.value]);
    });
}

//$(window).resize(sl_mob_resize);

function sl_mob_resize() {
    var w = $(window).width();
    if(w < 1000){
        console.log(w);
        $('.shop-list-tbl .sl-city .officeItem.txt-l .buy__result_item .buy__result_link .shop_addr .tline').css({'max-width':(w-60)+'px'});
    }
}

function sl_office_more(){
    console.log(this);
    console.log($(this).closest('.officeItem'));
    $(this).closest('.officeItem').addClass('active').find('.more').removeClass('more');
    
}

function supports_geolocation() {
    var r = false;
    try{
        r = !!navigator.geolocation;
    }catch(e){
        console.log(e);
    }
    return r;
}

function get_location() {
    //return navigator.geolocation.getCurrentPosition(GetCoords, GetCoordsError);
    //console.log(ymaps.geolocation);
    var latitude = false;
    var longitude = false;
    var r = false;
    
    try{
        latitude = ymaps.geolocation.latitude;
        longitude = ymaps.geolocation.longitude;
    }catch(e){
        console.log(e);
    }
    
    //latitude = 53.9;
    //longitude = 27.5667;
    //console.log('Lat: ', latitude);
    //console.log('Lon: ', longitude);
    if(latitude && longitude){
        $.get('/ajax/set_location.php', 'lat=' + latitude + '&lon=' + longitude, function (data) {
        });
    }
    
    try{
        r = ymaps.geolocation;
    }catch(e){
        console.log(e);
    }
    
    return r;
}

function GetCoords(position) {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    // Посмотрим карту или сделаем что-нибудь интересное!
    //console.log('Lat: ', latitude);
    //console.log('Lon: ', longitude);
    if(latitude && longitude){
		$.get('/ajax/set_location.php', 'lat=' + latitude + '&lon=' + longitude, function (data) {
			//window.location.reload();
			//alert(data);
            /*console.log('data: ', data);
            jData = JSON.parse(data);
            console.log(jData);
            try {
                if(jData.RESULT == 1) window.location.reload();
            } catch (e) {}*/
        });
    }
}

function GetCoordsError(err) {
    console.log('GetCoordsError: ', err);
    if (err.code == 1) {
        // пользователь сказал нет!
        setCookie("location_sets", false);
    }
}
/*

http://htmlbook.ru/html5/geolocation

function get_location() {
  if (Modernizr.geolocation) {
    navigator.geolocation.getCurrentPosition(show_map);
  } else {
   // Нет встроенной поддержки
  }
}

Свойство	Тип	Замечание
coords.latitude	double	В градусах
coords.longitude	double	В градусах
coords.altitude	double или null	В метрах
coords.accuracy	double	В метрах
coords.altitudeAccuracy	double или null	В метрах
coords.heading	double или null	Градусы по часовой стрелке от севера
coords.speed	double или null	В метрах в секунду
timestamp	DOMTimeStamp	Как у объекта Date()


navigator.geolocation.getCurrentPosition(show_map, handle_error)

Свойство	Тип	Замечание
code	short	Перечисленные значения
message	DOMString	Не предназначено для пользователей

function GetCoords(err) {
if (err.code == 1) {
// пользователь сказал нет!
}
}
*/

function DoUpdateCatalogItems(){
    //console.log($('#catalog_items FORM.smartfilter INPUT[type="submit"]'));
    $('#catalog_items FORM.smartfilter INPUT[type="submit"]').click();
    //console.log(this);
    //console.log($('#catalog_items FORM.smartfilter'));
    $('#catalog_items FORM.smartfilter').submit();
    
    if($('.filter-line .filter-active .filter-aitem .item').length > 0) $('#catalog_items .filter-aclear').addClass('active');
    else $('#catalog_items .filter-aclear').removeClass('active');
    
    return;

    var url = '';
    //var arFilter = {};
    //console.log($('#catalog_items FORM.smartfilter'));
    //$('#catalog_items FORM.smartfilter').submit();
    //$('FORM.smartfilter A#set_filter_a').click();
    
    return;
    
    var formData = new FormData($('#catalog_items FORM.smartfilter')[0]);
    //console.log($('#catalog_items FORM.smartfilter')[0]);
    //console.log(formData);
    
    /*$.ajax({
        url: '/catalog/',
        data: formData,
        processData: false,
        contentType: false,
        type: 'GET',
        success: function (data) {
            console.log('data: ', data);
        }
    });*/
    
    var s = $('#catalog_items FORM.smartfilter').serialize();
    s = s.replace(/(bxajaxid\=)([\w]*)(&)/g, '');

    //console.log(s);
    $.get('/catalog/?ajax_get_page=y&set_filter=y&'+s, function (data) {
        //console.log('data: ', data);
        
        var $cati = $("#catalog_items");
        var $cat = $cati.find("#catcat");
        var $pagi = $cati.parent().find(".pagination");
        var $data = $(data);
        //var dt = $data.find('.pagination');
        var AppendBlock = $data.find('#catcat .catalog_item');
        var $pagiNew = $data.find(".pagination>p");
        var lNextNew = $pagiNew.find(".prev_next_links").attr('data-next');
        $pagi.find("p").html($pagiNew);
        //$cat.append(AppendBlock);
        console.log('data: ', AppendBlock);
        $cat.html(AppendBlock);
        console.log('ntadd: ', $data.find('.ntadd'));
        $cat.append($data.find('.ntadd'));
        if (!lNextNew || lNextNew.length < 1){
            $cati.parent().find(".show_more_block").hide();
        }

        return;
    });
        
    return;
    
    var $items = $('.catalog-filters .fb-menu-right LI.active');
    //console.log('Filters items1: ', $items1.length);
    //var $items = $('#filters .filter-active .filter-aitem .item');
    //console.log('Filters items: ', $items.length);
    //console.log($items1);
    $items.each(function(i){
        var $this = $(this);
        var $p = $this.parent();
        var m = $p.data('menu');
        var id = $this.data('id');
        //console.log('['+m+'] '+id);
        //if(typeof(arFilter[m]) == 'undefined') arFilter[m] = {};
        //arFilter[m][Object.keys(arFilter[m]).length] = id;
        url += m+'[]='+id+'&';
    });
    /*console.log('arFilter: ', arFilter);
    for(var mId in arFilter){
        console.log('mId: ', mId);
    }*/
    url = url.substr(0, url.length - 1);
    if(true/*url.length > 3*/){
        //url = '/ajax/catalog_filter_data.php?'+url;
        //url = '/ajax/catalog_filtered.php?'+url;
        url = '/catalog/?ajax_get_page=y&'+url;
        //console.log('url: ', url);
		$.get(url, function (data) {
            //var l;
            //var $item;
			//window.location.reload();
			//alert(data);
            //console.log('data: ', data);
            
            var $cati = $("#catalog_items");
            var $cat = $cati.find("#catcat");
            var $pagi = $cati.parent().find(".pagination");
            var $data = $(data);
            //var dt = $data.find('.pagination');
            var AppendBlock = $data.find('#catcat .catalog_item');
            var $pagiNew = $data.find(".pagination>p");
            var lNextNew = $pagiNew.find(".prev_next_links").attr('data-next');
            $pagi.find("p").html($pagiNew);
            //$cat.append(AppendBlock);
            //console.log('data: ', AppendBlock);
            $cat.html(AppendBlock);
            //console.log('ntadd: ', $data.find('.ntadd'));
            $cat.append($data.find('.ntadd'));
            if (!lNextNew || lNextNew.length < 1){
                $cati.parent().find(".show_more_block").hide();
            }
            
            return;
            jData = JSON.parse(data);
            //console.log(jData);
            CatalogParseCounts(jData);
            /*try {
                for(var mId in jData){
                    l = Object.keys(jData[mId]).length;
                    console.log('mId: ', mId, '(', l, ')');
                    if(l > 0){
                        for(var iId in jData[mId]){
                            console.log("\t", iId, ': ', jData[mId][iId]);
                            console.log('.catalog-filters .filter-body .fb-content UL[data-menu="'+mId+'"] LI[data-id="'+iId+'"]');
                            $item = $('.catalog-filters .filter-body .fb-content UL[data-menu="'+mId+'"] LI[data-id="'+iId+'"]');
                            console.log($item);
                            $item.find('.count').text(jData[mId][iId]);
                            if(jData[mId][iId] < 1) $item.addClass('zero');
                            else $item.removeClass('zero');
                        }
                    }
                }
                //if(jData.RESULT == 1) window.location.reload();
            } catch (e) {}*/
        });
    }
}

function CatalogParseCounts(jData){
    //console.log("CatalogParseCounts: ", jData);
    //console.log(arFilterValToName);
    var l;
    var $item;
    var mId;
    try {
        $('.catalog-filters .filter-body .fb-content UL[data-menu] LI[data-id]').addClass('disable');
        mId = 'SMART';
        for(var iId in jData[mId]){
            if(jData[mId][iId] < 1) continue;
            $item = $('.catalog-filters .filter-body .fb-content UL LI[data-id="'+iId+'"]');
            //console.log($item);
            $item.find('.count').text(jData[mId][iId]);
            if(jData[mId][iId] < 1) $item.addClass('zero');
            else $item.removeClass('zero disable');
        }
        for(var mId in jData){
            if(mId == 'SMART'){
                /*l = Object.keys(jData[mId]).length;
                if(l > 0){
                    for(var iId in jData[mId]){
                        $item = $('.catalog-filters .filter-body .fb-content UL LI[data-id="'+iId+'"]');
                        //console.log($item);
                        $item.find('.count').text(jData[mId][iId]);
                        if(jData[mId][iId] < 1) $item.addClass('zero');
                        else $item.removeClass('zero');
                    }
                }*/
            }else{
                l = Object.keys(jData[mId]).length;
                //console.log('mId: ', mId, '(', l, ')');
                if(l > 0){
                    for(var iId in jData[mId]){
                        if(typeof(arFilterValToName[mId]) == 'undefined' && jData[mId][iId] < 1) continue;
                        //console.log("\t"+'['+mId+']'+ iId, ': ', jData[mId][iId]);
                        //console.log('.catalog-filters .filter-body .fb-content UL[data-menu="'+mId+'"] LI[data-id="'+iId+'"]');
                        $item = $('.catalog-filters .filter-body .fb-content UL[data-menu="'+mId+'"] LI[data-id="'+iId+'"]');
                        if($item.length < 1){
                            //console.log($item);
                            $item = $('.catalog-filters .filter-body .fb-content UL[data-menu="'+mId+'"] LI[data-id="'+arFilterValToName[mId][iId]+'"]');
                            //console.log($item);
                        }
                        //console.log(iId ,' ', $item.length);
                        //console.log($item.find('.count').length);
                        $item.find('.count').text(jData[mId][iId]);
                        if(jData[mId][iId] < 1) $item.addClass('zero disable');
                        else $item.removeClass('zero disable');
                    }
                }
            }
        }
        //if(jData.RESULT == 1) window.location.reload();
    } catch (e) {}
    $('.catalog-filters .filter-body .fb-content UL.fb-menu-right LI.disable .count').text('0');
}

function SelShopByMap(id){
    var $this = $(this);
    console.log(id);
    iShopByMap = id;
    $('#map .modal-close').click();
    return false;
}

