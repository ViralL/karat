$(document).ready(function(){

    $('#buy,#buy_once,.buy_nabor_item').click(
        function(){
			try{
				yaCounter38054255.reachGoal('bay');
			}catch(e){
				console.log(e.message, e);
			}
			var $this = $(this);
            var $wts = $this.closest('.item-data').find(".what-the-size.ring-size");
            if($wts.length > 0 && !$wts.hasClass("choice")){
                $(".achtung").fadeIn();
                return false;
            }
            var cPrice = parseInt($(".hrmb .basket_icon .hm-counter .hmc_val"));
            if(isNaN(cPrice)) cPrice = 0;
            //if (!$(".what-the-size").hasClass("choice"))
            $.ajax({
                type: "GET",
                url: '/ajax/basket.php?cmd=add&id='+$this.data("id")+"&size="+$wts.find(".numbr").text(),
                dataType: "html",
                success: function(data){
                    //$("#bid").html(getBasketHTML(out));
                    //console.log(data);
                    $(".hrmb").html(data);
                    var $v = $(".hrmb .basket_icon .hm-counter .hmc_val");
                    var $m = $(".mob .header-menu .hm-counter .hmc_val");
                    $m.text($v.text());
                    if($v.text() !== "0"){
                        var $a = $m.closest(".basket-empty");
                        if($a.length > 0){
                            $a.removeClass("basket-empty");
                            $a.find("IMG").attr("SRC", $(".hrmb .basket_icon IMG").attr("SRC"));
                        }
                    }
                    var newPrice = parseInt($v.text());
                    if(isNaN(newPrice)) newPrice = 0;
                    console.log(newPrice, cPrice);
                    if(newPrice > cPrice){
						try{
							yaCounter38054255.reachGoal('item_add');
						}catch(e){
							console.log(e.message, e);
						}
                    }
                    
                    /*if ($(".what-the-size").hasClass("choice")){
                        return $(".achtung22").fadeIn(),
                            !1,
                            setTimeout(function(){$('.achtung22').fadeOut()},5000);
                    }*/

                    $(".achtung22").fadeIn();
                    setTimeout(function(){$('.achtung22').fadeOut()},5000);
                    //if ($(".razmer44").hasClass("razmer33")){
                    //}
					var $mo = $this.closest('.modal-open');
					if($mo.length > 0) $mo.find('.modal-close').click();
                }

            });
            return false
        }
    );
    
    
    $('#buy_both').click(
        function(){
			try{
				yaCounter38054255.reachGoal('bay');
			}catch(e){
				console.log(e.message, e);
			}
			var $this = $(this);
            var $wts = $this.closest('.item-data').find(".what-the-size.ring-size");
            if($wts.length > 0 && !$wts.hasClass("choice")){
                $(".achtung").fadeIn();
                return false;
            }
            var cPrice = parseInt($(".hrmb .basket_icon .hm-counter .hmc_val"));
            if(isNaN(cPrice)) cPrice = 0;
            //if (!$(".what-the-size").hasClass("choice"))
			//console.log('DEBUG');
			//console.log($(this).closest('.item-data'));
			//console.log($wts);
			//console.log($wts.find(".numbr"));
            //return false;
            $.ajax({
                type: "GET",
                url: '/ajax/basket.php?cmd=add&id='+$this.data("para")+"&size="+$wts.find(".numbr").text()+'&id2='+$this.data("id")+"&size2="+$('#buy').closest('.item-data').find(".what-the-size.ring-size .numbr").text(),
                dataType: "html",
                success: function(data){
                    //$("#bid").html(getBasketHTML(out));
                    //console.log(data);
                    $(".hrmb").html(data);
                    var $v = $(".hrmb .basket_icon .hm-counter .hmc_val");
                    var $m = $(".mob .header-menu .hm-counter .hmc_val");
                    $m.text($v.text());
                    if($v.text() !== "0"){
                        var $a = $m.closest(".basket-empty");
                        if($a.length > 0){
                            $a.removeClass("basket-empty");
                            $a.find("IMG").attr("SRC", $(".hrmb .basket_icon IMG").attr("SRC"));
                        }
                    }
                    var newPrice = parseInt($v.text());
                    if(isNaN(newPrice)) newPrice = 0;
                    console.log(newPrice, cPrice);
                    if(newPrice > cPrice){
						try{
							yaCounter38054255.reachGoal('item_add');
						}catch(e){
							console.log(e.message, e);
						}
                    }
                    
                    /*if ($(".what-the-size").hasClass("choice")){
                        return $(".achtung22").fadeIn(),
                            !1,
                            setTimeout(function(){$('.achtung22').fadeOut()},5000);
                    }*/

                    $(".achtung22").fadeIn();
                    setTimeout(function(){$('.achtung22').fadeOut()},5000);
                    //if ($(".razmer44").hasClass("razmer33")){
                    //}
					var $mo = $this.closest('.modal-open');
					if($mo.length > 0) $mo.find('.modal-close').click();
                }

            });
            return false
        }
    );
    
    
    $('#get_stock').click(
        function(){
            $('html, body').animate({
                scrollTop: $('A[name="shop_list"]').offset().top
            }, 500);
            /*setTimeout(function(){
                $('html, body').animate({
                    scrollTop: $('A[name="shop_list"]').offset().top
                }, 500);
            },50);*/
            return false;
            
			var $this = $(this);
            var $wts = $this.closest('.item-data').find(".what-the-size.ring-size");
            if($wts.length > 0 && !$wts.hasClass("choice")){
                $(".achtung").fadeIn();
                return false;
            }
            $(".stock_list").html('');
            $.ajax({
                type: "GET",
                url: '/ajax/stock.php?id='+$this.data("id")+'&size='+$wts.find(".numbr").text(),
                dataType: "html",
                success: function(data){
                    $(".stock_list").html('<button title="Close (Esc)" type="button" class="mfp-close">×</button>'+data);
                    //sl_mob_resize();
                    /*$('html, body').animate({
                        scrollTop: $('A[name="shop_list"]').offset().top
                    }, 500);
                    setTimeout(function(){
                        $('html, body').animate({
                            scrollTop: $('A[name="shop_list"]').offset().top
                        }, 500);
                    },50);*/
                }

            });
            /*$('html, body').animate({
                scrollTop: $('A[name="shop_list"]').offset().top
            }, 500);*/
            /*$.magnificPopup.open({
                items: {
                    src: $('.stock_list')
                },
                type:'inline',
                preloader:true,
                tLoading: 'Загрузка...',
                tClose: 'Закрыть (Esc)',
                //modal: true,
                midClick: true,
                showCloseBtn: true,
                closeBtnInside: true,
                /*closeOnContentClick: true,*-/
                closeOnBgClick: false
                /*callbacks: {
                    close: function() {
                        alert('test');
                    }
                }*-/
            });
            $.magnificPopup.instance.updateStatus('loading');*/
            return false;
        }
    );
    
    
    $('.basket_shops_once:not(.active)').click(
        function(){
			var $this = $(this);
            $this.addClass('active');
            $('#basket_form_tbl .tr-b-shops').show();
            $('html, body').animate({
                scrollTop: $('A[name="order_form"]').offset().top
            }, 300);
            $('#basket_form_tbl INPUT[name="ONCE_SHOP"]').val("1");
            return false;
        }
    );
    
    /*$('.shop-list-tbl-mob .sl-price').click(function(){
        console.log(this);
        console.log($(this).closest('TR').find('.sl-buy A'));
        $(this).closest('TR').find('.sl-buy A').click();
    });*/
    
    $('.shop-list-tbl .sl-buy A').click(sl_buy);
    /*$('.sh-op-list-tbl .sl-buy A').click(function(){
        var $this = $(this);
        var id = $this.data('id');
        var key = $this.data('key');
        var shop = $this.data('shop');
        var size = $this.data('size');
        console.log($this);
        $.ajax({
            url: '/ajax/basket.php?cmd=add&id=' + id + '&size=' + size + '&shop=' + shop + '&key=' + key,
            //url: $(this).attr('href'),
            //context: document.body,
            success: function(data) {
                //console.log($('#favorites'));
                console.log(data);
                //console.log($(data).find('#favorites-in').html());
                //$('#favorites').html($(data).find('#favorites-in').html());
                //$('#favorites').html(data);
                //document.location.reload();
                //recalcBasketAjax({});
            }
        });
    });*/

    $('#basket_form .checkout').click(function(){
		try{
			yaCounter38054255.reachGoal('order_click');
		}catch(e){
			console.log(e.message, e);
		}
        datasend = {};
        datasend.USER_NAME = $('#basket_form input[name="USER_NAME"]').val();
        datasend.USER_PHONE = $('#basket_form input[name="USER_PHONE"]').val();
        datasend.USER_EMAIL = $('#basket_form input[name="USER_EMAIL"]').val();
        datasend.ONCE_SHOP = $('#basket_form *[name="ONCE_SHOP"]').val();
        datasend.SHOP = $('#basket_form *[name="SHOP"]').val();
        datasend.COMMENT = $('#basket_form textarea[name="COMMENT"]').val();
        //datasend.city_value = $('.log-other-city-select').find('option:selected').text();
        console.log($('#basket_form textarea[name="COMMENT"]'));
        console.log($('#basket_form textarea'));
        console.log(datasend);
        $.ajax({
              type: "POST",
              url: '/basket/order.php',
              dataType: "html",
              data: datasend,
              success: function(data){
                        //$("#bid").html(getBasketHTML(out));
                        //console.log(data);
                        
                        jData = JSON.parse(data);
                        //alert( user.friends[1] ); // 1
                        //console.log(jData);
                        
                        //$('#basket_form TD').removeClass('error');
                        $('#basket_form INPUT,#basket_form SELECT,#basket_form .tr-b-shops>TD').removeClass('error');
                        $('#basket_form .bft-cu-txt-error').addClass('hidden');
                        if(jData.ERROR && jData.RESULT == -1){
                            for(var eKey in jData.ERROR){
                                console.log(eKey);
                                //$('#basket_form *[name="' + eKey + '"]').closest('TD').addClass('error');
                                $('#basket_form *[name="' + eKey + '"]').addClass('error');
                                if(eKey == 'SHOP') $('#basket_form .tr-b-shops>TD').addClass('error');
                            }
                            $('#basket_form .checkout-btn').addClass('error');
                            $('#basket_form .bft-cu-txt-error').removeClass('hidden');
                        }else if(jData.RESULT == 1){
                            //alert("OK");
                            $("#content H1").text('Спасибо, Ваш заказ принят!');
                            var $bf = $('#basket_form');
                            var $bfp = $bf.parent();
                            $bf.remove();
                            for(var rKey in jData){
                                if(rKey == 'RESULT') continue;
                                console.log(rKey);
                                try{
                                    yaCounter38054255.reachGoal('order_done');
                                }catch(e){
                                    console.log(e.message, e);
                                }
                                //document.location.href="done.php?id="+jData.ORDER_ID+"&summ="+jData.ORDER_SUMM;
                                $bfp.append(jData[rKey]['ORDER_DONE_HTML']);
                                //$bfp.append('<p>Ваш заказ <a>№'+jData.ORDER_ID+'</a> от '+jData.ORDER_DATE+' на сумму <span>'+jData.ORDER_PRICE+'</span> успешно принят.</p><br><p>В ближайшее время наш менеджер с Вами свяжется для уточнения всех деталей.</p><br><br>');
                            }
                        }else{
                            alert("Что-то пошло не так!");
                        }
                        
                        //$(".hrmb").html(data);
                        
                        return false;
              }

        });
        return false
    });
    
    $('#basket_form SELECT[name="CITY"]').change(function(){
        console.log('SELECT[name="CITY"]');
        //console.log($(this).val());
        var cId = $(this).find('OPTION:selected').val();
        SetCityShowMetro(cId);
    });
    
    $('#basket_form SELECT[name="METRO"]').change(function(){
        console.log('SELECT[name="METRO"]');
        //console.log($(this).val());
        var $metro = $(this).find('OPTION:selected');
        var mId = $metro.val();
        var cId = $('#basket_form_tbl .b-city INPUT[name="CITY"]').val();
        SetMetroShowShop(cId, mId, $metro.text());
    });
    
    //$('#basket_form SELECT[name="CITY"]').trigger("change");
    //$('#basket_form SELECT[name="METRO"]').trigger("change");
    
    $("#basket_form_container .yc-items .yc-item").click(function(){
        //console.log(this);
        //console.log($(this).data('id'));
        //console.log($(this).data('bkey'));
        var cmd = 'add';
        var id = $(this).data("id");
        //var cmd = ($(this).find('.minus') ? 'del' : 'add');
        if($(this).find('.minus').length > 0){
            return false;
            cmd = 'del';
            id = $(this).data("bkey");
        }
        //console.log('/ajax/basket.php?cmd='+cmd+'&id='+id+"&size=16,5");
        $.ajax({
            type: "GET",
            url: '/ajax/basket.php?cmd='+cmd+'&id='+id+"&size=16,5",
            dataType: "html",
            success: function(data){
                //$("#bid").html(getBasketHTML(out));
                //console.log(data);
                //$(".hrmb").html(data);
                document.location.reload();
            }

        });
        return false;
    });
    
    $("#basket_items .basket-hdel").click(function(){
        $.ajax({
            type: "GET",
            url: '/ajax/basket.php?cmd=del&id='+$(this).data("bkey"),
            dataType: "html",
            success: function(data){
                //$("#bid").html(getBasketHTML(out));
                //console.log(data);
                //$(".hrmb").html(data);
                document.location.reload();
            }

        });
        return false;
    });
    
    $("#basket_form_tbl A.show_comment").click(function(){
        var $lnk = $(this);
        var $ta = $lnk.closest("TD").find("TEXTAREA");
        if($ta.is(":visible") !== false){
            console.log("hide");
            $ta.animate({
                height: "0px"
            }, 0, function() {
                $ta.hide();
                $lnk.text("Раскрыть");
            });
        }else{
            console.log("show");
            $ta.show().animate({
                height: "150px"
            }, 0, function() {
                $lnk.text("Скрыть");
            });
        }
    });
    
    $(document).on('click', '#basket_form .b-shops .shoplist .officeItem', function(){
        console.log('ShopClick');
        console.log(this);
        $('#basket_form INPUT[name="SHOP"]').val($(this).data('shop'));
        $('#basket_form .b-shops .shoplist .officeItem.active').removeClass('active');
        $(this).addClass('active');
        console.log($('#basket_form .b-shops .shop-info'));
        //console.log('Вы выбрали магазин: <span>'+$(this).find(bShops[$(this).data('city')][$(this).data('shop')]['NAME'].decodeHtml())+'</span>');
        console.log(bShops[$(this).data('city')][$(this).data('shop')]['NAME'].decodeHtml());
        $('#basket_form .b-shops .shop-info').html('Вы выбрали магазин: <span>'+bShops[$(this).data('city')][$(this).data('shop')]['NAME'].decodeHtml()+'</span>');
    });
    
    $('#basket_form_container .basket_left #basket_form_tbl .b-no-metro').click(function(){
        var cId = $('#basket_form_tbl .b-city INPUT[name="CITY"]').val();
        var $mb = $('#basket_form_container .basket_left #basket_form_tbl .b-metro');
        
        if($(this).find('INPUT').is(':checked')){
            $mb.addClass('disabled');
            SetMetroShowShop(cId, false, false);
        }else{
            SetCityShowMetro(cId);
            $mb.removeClass('disabled');
        }
    });
    
    $('.mfp-content #closewind').click(function(){
        $.magnificPopup.close();
    });
    
});

function SetCityShowMetro(cId){
    console.log('SetCityShowMetro');
    //console.log(cId);
    //console.log(bMetro[cId]);
    //console.log(bShops[cId]);

    var $m = $('#basket_form SELECT[name="METRO"]');
    $m.find("OPTION").remove();
    var $mn = $('#basket_form .b-metro .what-the-size');
    var $mnin = $mn.find('.what-the-size-in1');
    $mnin.find('.changeraz').remove();
    console.log($mn);
    var mc = 0;
    var sMetro = false;
    for(metro in bMetro[cId]){
        $m.append('<option value="' + bMetro[cId][metro] + '"'+(sMetro === false ? ' selected="selected"' : '')+'>' + metro + '</option>');
        $mnin.append('<span class="box w-100 changeraz" data-key="' + bMetro[cId][metro] + '">' + metro + '</span>');
        if(sMetro === false){
            sMetro = metro;
            $mn.find('.numbr').text(metro);
        }
        mc++;
    }
    if(mc < 1){
        $m.closest('TD').hide();
        $m.closest('TR').find('.reserv').show();
    }else{
        $m.closest('TR').find('.reserv').hide();
        $m.closest('TD').show();
    }

    //console.log(sMetro);
    //console.log(bMetro[cId][sMetro]);
    //console.log($('#basket_form SELECT[name="METRO"]'));
    //$('#basket_form SELECT[name="METRO"]').val(bMetro[cId][sMetro]).trigger("change");
    //$('#basket_form SELECT[name="METRO"]').trigger("change");
    //$('#basket_form SELECT[name="METRO"]').change();
    var mId = false;
    if(sMetro !== false) mId = bMetro[cId][sMetro];
    SetMetroShowShop(cId, mId, sMetro);
}

function MetroChange(cId, obj){
    console.log('MetroChange');
    console.log(cId);
    console.log(obj);
    var $obj = $(obj);
    SetMetroShowShop(cId, $obj.data('key').toString(), $obj.text());
}

function SetMetroShowShop(cId, mId, mName){
    console.log('SetMetroShowShop');
    //console.log($(this).val());
//    var mId = $(this).find('OPTION:selected').val();
    console.log(cId);
    console.log(mId);
    console.log(mName);
    //console.log(mId.split(','));
    //var Id = parseInt(mId);
    if(mId == false){
        var Ids = [];
        for(shopId in bShops[cId]){
            Ids.push(shopId);
        }
    }else{
        var Ids = mId.split(',');
    }
    //var Id = Ids.shift();
    var Id = Ids[0];
    //console.log(Id);
    console.log(Ids);
    //console.log(Ids.length);
    //console.log(bMetro[mId]);
    //console.log(bShops[cId]);

    var shopCount = 0;
    //var $s = $('#basket_form SELECT[name="SHOP"]');
    //$s.find('option').remove();
    var $shoplist = $('#basket_form .b-shops .shoplist');
    $shoplist.html('');
    var $empty = $('#basket_form .b-shops .shop-empty .officeItem');
    var $new;
    var shopslink = $('.shopslink').attr('href');
    var selShopId = $('#basket_form *[name="SHOP"]').val();
    console.log($shoplist);
    //for(shopId in bShops[mId]){
    for(var i=0; i<Ids.length; i++){
    //for(shopId in Ids){
        shopId = Ids[i];
        //$s.append('<option value="' + shopId + '">' + bShops[cId][shopId]['NAME'] + '</option>');
        $new = $empty.clone();
        $new.attr('data-shop', shopId).data('shop', shopId);
        if(selShopId == shopId) $new.addClass('active');
        $new.find('.buy__result_link A').attr('href', shopslink+shopId+'/').text(bShops[cId][shopId]['NAME'].decodeHtml());
        $new.find('.buy__result_info').text('');
        if(/*mName == false*/bShops[cId][shopId]['METRO'] == '') $new.find('.metro').hide();
        else $new.find('.metro .metro-name').text(bShops[cId][shopId]['METRO'].decodeHtml());
        $new.find('.adress').text(bShops[cId][shopId]['ADRES'].decodeHtml());
        $new.find('.phone').text(bShops[cId][shopId]['PHONE'].decodeHtml());
        $new.find('.time').text(bShops[cId][shopId]['WORK_TIME'].decodeHtml());
        console.log($new);
        $new.appendTo($shoplist);
        //$(this).clone().css('color','red').appendTo('body');
        shopCount++;
    }
    //$s.val(Id);
    $('#basket_form_tbl .b-shops .b-s-count').text('Магазинов найдено: '+shopCount+'.');
    if(shopCount == 1 && !$('#basket_form *[name="SHOP"]').val()){
        setTimeout(function(){$('#basket_form .b-shops .shoplist .officeItem:first').click();},500);
        
        console.log('Try autoselect: ');
        console.log($('#basket_form .b-shops .shoplist .officeItem:first'));
    }
}


String.prototype.escapeHTML = function() {
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };

  return this.replace(/[&<>"']/g, function(m) { return map[m]; });
}

String.prototype.decodeHtml = function() {
    var map =
    {
        '&amp;': '&',
        '&lt;': '<',
        '&gt;': '>',
        '&quot;': '"',
        '&#039;': "'"
    };
    return this.replace(/&amp;|&lt;|&gt;|&quot;|&#039;/g, function(m) {return map[m];});
}

function sl_buy(){
    var $this = $(this);
    var id = $this.data('id');
    var key = $this.data('key');
    var shop = $this.data('shop');
    var size = $this.data('size');
    //console.log($this);
    var cPrice = parseInt($(".hrmb .basket_icon .hm-counter .hmc_val"));
    if(isNaN(cPrice)) cPrice = 0;
    $.ajax({
        url: '/ajax/basket.php?cmd=add&id=' + id + '&size=' + size + '&shop=' + shop + '&key=' + key,
        //url: $(this).attr('href'),
        //context: document.body,
        success: function(data) {
            //console.log($('#favorites'));
            //console.log(data);
            //console.log($(data).find('#favorites-in').html());
            //$('#favorites').html($(data).find('#favorites-in').html());
            //$('#favorites').html(data);
            //document.location.reload();
            //recalcBasketAjax({});
            $(".hrmb").html(data);
            var $v = $(".hrmb .basket_icon .hm-counter .hmc_val");
            var $m = $(".mob .header-menu .hm-counter .hmc_val");
            $m.text($v.text());
            if($v.text() !== "0"){
                var $a = $m.closest(".basket-empty");
                if($a.length > 0){
                    $a.removeClass("basket-empty");
                    $a.find("IMG").attr("SRC", $(".hrmb .basket_icon IMG").attr("SRC"));
                }
            }
            var newPrice = parseInt($v.text());
            if(isNaN(newPrice)) newPrice = 0;
            //console.log(newPrice, cPrice);
            if(newPrice > cPrice){
                try{
                    yaCounter38054255.reachGoal('item_add');
                }catch(e){
                    console.log(e.message, e);
                }
            }

            /*if ($(".what-the-size").hasClass("choice")){
                return $(".achtung22").fadeIn(),
                    !1,
                    setTimeout(function(){$('.achtung22').fadeOut()},5000);
            }*/

            //$(".achtung22").fadeIn();
            //setTimeout(function(){$('.achtung22').fadeOut()},5000);
            //if ($(".razmer44").hasClass("razmer33")){
            //}
            //var $mo = $this.closest('.modal-open');
            //if($mo.length > 0) $mo.find('.modal-close').click();
            
            //$(".achtung22").show();
            //$(".stock_list").html('<button title="Close (Esc)" type="button" class="mfp-close">×</button>'+data);
            $.magnificPopup.close();
            $.magnificPopup.open({
                items: {
                    src: $('.achtung22 .pu-content')
                },
                type:'inline',
                tClose: 'Закрыть (Esc)',
                midClick: true,
                showCloseBtn: true,
                closeBtnInside: true,
                closeOnContentClick: false, 
                closeOnBgClick: true/*,
                callbacks: {
                    beforeOpen: function() {
                        alert('test3');
                    },
                    close: function() {
                        alert('test');
                        $.magnificPopup.open({
                            items: {
                                src: $('.stock_list')
                            },
                            type:'inline',
                            tClose: 'Закрыть (Esc)',
                            midClick: true,
                            showCloseBtn: true,
                            closeBtnInside: true,
                            closeOnBgClick: false
                        });
                    },
                    afterClose: function(){
                        alert('test2');
                    }
                }*/
            });
            //setTimeout(function(){$('.mfp-container .mfp-content .pu-content').click()},3000);
            //setTimeout(function(){$.magnificPopup.close();},10000);
            setTimeout(function(){
                $('.mfp-bg,.mfp-wrap').animate({
                    opacity: '0'
                }, 5000, function(){
                    $(this).hide().css({opacity: '1'});
                    $.magnificPopup.close();
                    $(this).show();
                });
            },5000);
        }
    });
}

function sl_show_card(){
    var $this = $(this);
    var $thisTR = $this.closest('TR');
    var $tbl = $thisTR.closest('TABLE');
    
    //var key = $thisTR.find('.sl-buy A').data('key');
    //var shop = $thisTR.find('.sl-buy A').data('shop');
    var key = $thisTR.data('key');
    var shop = $thisTR.data('shop');
    
    var $card = $tbl.find('.item-line-card[data-key="'+key+'"][data-shop="'+shop+'"]');
    
    var $wrp = $card.find('.wrap');
    var $wbody = $wrp.find('.body');
    
    var h = $thisTR.height();
    //console.log(h);
    //console.log(bh);
    
    $wrp.css({height:'0px'}).animate({
        height: h+'px'
    }, 200, function(){
        $card.css({'position':'static'});
        $card.find('>TD').css({
            'display':'table-cell',
            'width':'auto'
        });
        var bh = $wbody.height();
        $(this).animate({
            height: bh+'px'
        }, 500, function(){
            console.log('done');
            $(this).css({
                overflow: 'auto',
                height: 'auto'
            });
        });
    });
    $card.find('>TD').css({
        'display':'inline-block',
        'width':'100%'
    });
    $card.css({
        'position':'absolute',
        'width':'100%'
    }).show();
      
    $thisTR.hide(200);
}

function GetStockBySize(obj){
    console.log("GetStockBySize()", obj, $(obj));
    var $this = $(obj);
    var $p = $this.closest('.item-data');
    var $wts = $p.find(".what-the-size.ring-size");
    var $b = $p.find("#get_stock");
    var $list = $(".stock_list");
    console.log($p, $b);
    console.log('/ajax/stock.php?id='+$b.data("id")+'&size='+$wts.find(".numbr").text());
    if($wts.length > 0 && !$wts.hasClass("choice")){
        $(".achtung").fadeIn();
        return false;
    }
    //console.log('/ajax/stock.php?id='+$b.data("id")+'&size='+$wts.find(".numbr").text());
    //$(".stock_list").html('');
    /*$.ajax({
        type: "GET",
        url: '/ajax/stock.php?id='+$this.data("id")+'&size='+$wts.find(".numbr").text(),
        dataType: "html",
        success: function(data){
            $(".stock_list").html('<button title="Close (Esc)" type="button" class="mfp-close">×</button>'+data);
        }

    });*/
    return false;
}
